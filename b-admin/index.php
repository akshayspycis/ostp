<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Login</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link href="../style/plugins/iCheck/square/blue.css" rel="stylesheet" type="text/css" />
    <meta name="keywords" content= "Login online vegetable store" />   
    <meta name="keywords" content= "Signup online vegetable store" />   
    <?php include 'includes/links.php';?>
    <script src="../style/plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
  <?php              $timezone = new DateTimeZone("Asia/Kolkata" );
                     $date = new DateTime();
                     $date->setTimezone($timezone );
     ?>
 <?php
session_start();
if(isset($_SESSION["c_id"]) ){
        if($_SESSION['user_name']!='Admin'){
            ?><script>$(location).attr('href','shop.php');</script><?php 
    }else{
             ?><script>$(location).attr('href','../ii_admin/home.php');</script><?php 
    }
}
else if (isset($_COOKIE['username']) && isset($_COOKIE['password'])){
        $_SESSION['user_name'] = $_COOKIE['username'];
        $_SESSION['password'] = $_COOKIE['password'];
        $_SESSION['c_id'] = $_COOKIE['c_id'];
        $_SESSION['user_name'] = $_COOKIE['name'];
        
        
        if($_COOKIE['username']=='Admin'){ 
                ?><script>$(location).attr('href','../ii_admin/home.php');</script><?php 
        }else{ ?>
               <script>$(location).attr('href','shop.php');</script><?php 
        }
        
        
}?>
<style>
    .skin-green .wrapper{
        background: #f9f9f9;
    }
         .content-wrapper{
            background:#f8f8f8 url("../style/images/overlay.png");
         }
         #loginbody{
              background:rgba(0,0,0,0.6);
              border-radius:10px;
         }
         .login-box{
               position:relative;
               margin-bottom:-10px;
               
              
         }
         .login-logo{
             position:relative;
         
         }
         .login-box-body{
              background:none;
         }
         .login-box-msg{
             color:white;
         }
         #Login_submit:active{
             border-radius:10px;
             background:white;
             color:black;
         }
        
     </style>
     <script>
         var day="<?php echo $date->format('l');?>";
    localStorage.clear();
     $(document).ready(function(){
         $("#login34").fadeIn(1000);
     });
 
     </script>
  </head>
 <body class="skin-green layout-top-nav fixed">
    <div class="wrapper">
     <header class="main-header">
<!--        <nav class="navbar navbar-static-top" style="background:rgba(0,0,0,0.7)">
          <div class="container">
            <div class="navbar-header">
               
            </div>

          </div> /.container-fluid 
        </nav>-->
      </header>
      <!-- Full Width Column -->
      <div class="content-wrapper">
          <div class="container">
              <div class="row">
   <div class="login-box" id="login34" style="display:none;margin-bottom:40px;">
      <div class="login-logo">
          <img src="style/dist/img/jeeves-logo.png" height="150" width="280"/>
      </div><!-- /.login-logo -->
      <div class="login-box-body" id="loginbody">
        <p class="login-box-msg" style ="font-family:century gothic;">Registered Users Sign In</p>
        <form id="Login">
          <div class="form-group has-feedback">
              <input type="text" class="form-control" placeholder="Mobile Number" name="contact_no" maxlength="10" id="contact_no" style ="font-family:century gothic;"/>
            <span class="glyphicon glyphicon-phone form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
              <input type="password" class="form-control" placeholder="Password" name="password" id="password" maxlength="4" style ="font-family:century gothic;"/>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">
              <div class="col-sm-6 col-xs-6" style="margin-top: 5px;">
                 <a  id="addUserDetails" style="cursor: pointer;" >
                    <span class="glyphicon glyphicon-list-alt" style="color: white"></span>
                    <span style="color:white;font-family:century gothic;padding-left:9px;">Sign Up</span> 
                </a>
           </div><!-- /.col -->
            <div class="col-sm-4 pull-right" >
                <button type="submit" class="btn btn-success btn-block btn-flat" id="Login_submit" >SIGN IN</button>
            </div><!-- /.col -->
          </div>
            
        </form>
</div>
    </div>   
              </div>
              
              
          </div>   
      </div>

    </div>

    <?php include 'includes/jslinks.php';?>
      <div class="modal fade" id="loginalert" role="dialog">
        <div class="modal-dialog modal-sm">
                
        <!-- Modal content-->
        <div class="modal-content">
             
         <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="color:red;font-weight:bold">Warning</h4>
        </div>
          
        <div class="modal-body">
        <div class="row">
        <div class="col-md-12">
            <p class="msg"></p>
         </div>
        </div>
        <div class="modal-footer">
        <button type="button" id="myBtn" class="btn btn-default" data-dismiss="modal">Cancel</button>
        
        </div>
        </div>
    
        </div>
        </div>
       </div>
     <div class="modal fade" id="saturday" role="dialog">
        <div class="modal-dialog ">
        <!-- Modal content-->
        <div class="modal-content">
         <div class="modal-header bg-danger">
            <h4 class="modal-title">SORRY !</h4>
        </div>
        <div class="modal-body">
        <div class="row">
            <div class="col-sm-11 conmessage" style="font-family:century gothic;text-align:justify">
                Our delivery service is off on Sunday.<br> So, Please place your order after 1 pm on Sunday to get delivery on Monday.
            </div>
        </div>
        </div>
       
        </div>
        </div>
       </div>
  </body>
 <div class="modal fade" id="insModel" role="dialog">
                    <div class="modal-dialog"><!-- Modal content-->
                            <div class="modal-content">
                            <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Add User Details </h4>
                            </div>
                            <form id ="insUserDetails" enctype="multipart/form-data">    
                            <div class="modal-body">
                            <div class="row">
                            <div class="col-xs-12">
                            <div class="box">

                            <div class="box-body">
                            <div class="row">
                            <div class="col-md-6">
                            <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" name="user_name" id ="user_name" value="" placeholder="Enter UserDetails  Name">
                            </div>
                            <!-- /.form-group -->
                            <div class="form-group">
                            <label for="dob">Date of Birth</label>
                            <input type="date"  class="form-control"  name="dob" id ="dob" value="" placeholder="DD/MM/YYYY">
                            </div>
                            <div class="form-group">
                            <label for="email">Email</label>

                            <input type="text" class="form-control"  name="email" id ="email" value="" placeholder="UserDetails Email">
                            </div>
                            <div class="form-group ">
                            <label for="password">Address</label>
                            <textarea name ="c_address" id="c_address" class="form-control" rows="3" value="" placeholder="Enter Address" style="resize:none"></textarea>
                            </div>
                            </div><!-- /.col -->
                            <div class="col-md-6">
                            <div class="form-group">
                            <label>Gender</label>
                            <select class="form-control select2" id="gender" name="gender">
                            <option value="" selected>Select Gender</option>
                            <option value="Male">Male</option>
                            <option value="Female" >Female</option>
                            </select>
                            </div><!-- /.form-group -->
                            <div class="form-group">
                            <label for="Contact Number">Contact Number</label>
                            <input type="text" id="contact_no" name="contact_no" value="" class="form-control" placeholder="UserDetails Contact" maxlength="10">
                            </div>
                            <div class="form-group">
                                <label for="password">Password (4 digit)</label>
                                <input type="password" id="password" name="password" value="" class="form-control" placeholder="UserDetails Password" maxlength="4">
                            </div>
                            <div class="form-group col-md-6">
                              <label for="image">Profile Pic</label>
                              <input type="hidden" id="pic" name="pic" >
                              <input type="file" name="file[]" id="pica" name="pica" style="margin-top: 10px;" onchange="encodeImageFileAsURL(this)">
                            </div>

                            <!-- /.form-group -->
                            </div><!-- /.col -->
                            </div><!-- /.row -->
                            </div><!-- /.box-body -->


                            </div><!-- /.box -->
                            </div><!-- /.col -->


                            </div><!-- /.box -->
                            <div class="modal-footer">
                            <div class="validate pull-left" style="font-weight:bold;"></span></div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>

                            </div>
                            </div>
                            </form>
                            </div>
                            </div>
                  </div> 
  <script>
      var pic=null;
      
 function encodeImageFileAsURL(element) {
  var file = element.files[0];
  var reader = new FileReader();
  reader.onloadend = function() {
    pic=reader.result;
    $("#pic").val(pic);
  }
  reader.readAsDataURL(file);
}
      
      $('#Login').submit(function() {
             var userfilter = /^\d{10}$/;
             var passfilter = /^\d{4}$/;
                            if(this.contact_no.value == "" || !userfilter.test(this.contact_no.value)){
                                 $("#loginalert").modal('show');
                                 $("#loginalert").on('shown.bs.modal', function(){
                                       $(".msg").text("Please fill valid User Id.") 
                                        });
                                        $("#myBtn").click(function(){
                                        $("#loginalert").modal("hide");
                                        });
                                        $("#loginalert").on('hide.bs.modal', function () {
                                         $('#contact_no').focus();
                                        });
                              return false;
                               
                           }
                           else if(this.password.value == "" || !passfilter.test(this.password.value)){
                               $("#loginalert").modal('show');
                                 $("#loginalert").on('shown.bs.modal', function(){
                                       $(".msg").text("Please fill valid password.") 
                                        });
                                        $("#myBtn").click(function(){
                                        $("#loginalert").modal("hide");
                                        });
                                        $("#loginalert").on('hide.bs.modal', function () {
                                         $('#password').focus();
                                        });
                                    return false;
                           }else{
                            $.ajax({
                            type:"post",
                            url:"../server/controller/user_details/UserLogin.php",
                            data:$('#Login').serialize(),
                            success: function(data){
                            var result = $.trim(data);
                            if(result==="admin" || result==="emp"){
                                $(location).attr('href','home.php');
                            }else if(result==="otheruser"){
                                $(location).attr('href','shop.php');  
                            }else{
                              $('#Login').each(function(){
                                    this.reset();
                              });
                           $("#loginalert").modal('show');
                                 $("#loginalert").on('shown.bs.modal', function(){
                                       $(".msg").text("Invalid User Id & Password") 
                                        });
                                        $("#myBtn").click(function(){
                                        $("#loginalert").modal("hide");
                                        });
                                        $("#loginalert").on('hide.bs.modal', function () {
                                         $('#contact_no').focus();
                                        });
                        }
                         }
                            });
                           }
                    return false;
    });
  </script>  
  <script src="../style/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
    <script>
      $(function () {
         $("#addUserDetails").click(function(){
            $("#insModel").modal('show');
        });
        $("#insModel").on('shown.bs.modal', function(){
                            $('#insUserDetails').off();
                            $('#insUserDetails').submit(function() {           
                                     var emailfilter = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
                                     var genderfilter = /^[a-zA-Z]+$/;
                                     var contactfilter =  /^\d{10}$/;
                                     var passwordfilter = /^\d{4}$/;
                                     
                                     if(this.user_name.value == ""){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please fill valid name").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">");
                                          $('#insUserDetails').find('#user_name').focus();
                                        return false;
                                       }else if(this.dob.value == ""){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please fill date of Birth").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">");
                                           $('#insUserDetails').find('#dob').focus();
                                         return false;
                                         
                                     }
                                     else if(this.email.value == "" || !emailfilter.test(this.email.value)){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please fill valid email").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">");
                                          $('#insUserDetails').find('#email').focus(); 
                                         return false;
                                         
                                     }
                                     else if(this.c_address.value == ""){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please fill valid address").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">");
                                         $('#insUserDetails').find('#c_address').focus();
                                         return false;
                                         
                                     }
                                     else if(this.gender.value == "" || !genderfilter.test(this.gender.value)){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please fill valid gender").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">");
                                         $('#insUserDetails').find('#gender').focus();
                                         return false;
                                         
                                     }
                                     else if(this.contact_no.value == "" || !contactfilter.test(this.contact_no.value)){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please fill valid contact no.").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">");
                                         $('#insUserDetails').find('#contact_no').focus();
                                         return false;
                                         
                                     }
                                     else if(this.password.value == "" || !passwordfilter.test(this.password.value)){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please fill four digit password").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">");
                                         $('#insUserDetails').find('#password').focus();
                                         return false;
                                         
                                     }
                                     else if(pic== null){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please Upload Profile pic").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">");
                                         $('#insUserDetails').find('#pic').focus();
                                         return false;
                                     }
                                     else{
                                        $.ajax({
                                                type:"post",
                                                url:"../server/controller/user_details/InsUserDetails.php",
                                                data:$('#insUserDetails').serialize(),
                                                success: function(data){  
                                                $('#insUserDetails').each(function(){
                                                    $(".validate").addClass("text-success").fadeIn(100).text("Thanks for being our awesome Employee . Please Login.").prepend("<span class=\"glyphicon glyphicon-ok text-success\">");
                                                    setTimeout(function (){
                                                          location.reload(true);
                                                     },3000);
                                                    return false;
                                                });
                                            } 
                                        });
                   return false;   
                  }
                                     
                    });
                  var modal = this;
            var hash = modal.id;
            window.location.hash = hash;
            window.onhashchange = function() {
                    if (!location.hash){
                            $(modal).modal('hide');
                    }
            }
      });  
$('#insUserDetails').on('hide.bs.modal', function() {
    	location.reload(true);
        
});
      });
    </script>
</html>