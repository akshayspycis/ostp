<?php include './includes/check_session.php';?>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Product Rate</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <?php include 'includes/links.php';?>
   
        <script type="text/javascript">
         var type=0
         cate={}
         var product_details_id=0
         $(document).ready(function() {
             loadEmployeePrivileges(employees_id,callback);
             $("#product_details").find("select").change(function () {
                product_details_id=$(this).val();
                onlad(product_details_id);
             }); 
         });  
         function callback(){
             if(user_type=="admin" || privileges_details_system[13]["selectm"]=="true"){
                loadProduct();
             }else{
                 showAccessDeniedModel();
             }
         }
         
         function loadProduct(){
             $.ajax({
                    type:"post",
                    url:"../server/controller/product_details/SelProductDetails.php",
                    success: function(data) {
                        var duce = jQuery.parseJSON(data);
                        $("#product_details").find("select").empty();
                        $.each(duce, function (index, article) {
                           $("#product_details").find("select").append($('<option>').attr({'value':article.product_details_id}).append(article.product_name));
                        });
                        $("#product_details").find("select").trigger("change");
                    }
                });
         }
         
        </script> 
  </head>
  <!-- ADD THE CLASS sidedar-collapse TO HIDE THE SIDEBAR PRIOR TO LOADING THE SITE -->
 <body class="wysihtml5-supported skin-yellow sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">

   <?php include 'includes/header.php';?>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
   <?php include 'includes/sidepanel.php';?>

      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Product Rate
            <small>Admin panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i> Home</li>
            <li class="active">Product Rate</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
           
          <div class="row">
            <div class="col-xs-12">
                <div class="box">
                <div class="box-header">
                    <div class="col-md-3 pull-left">
                      <div class="form-group" id="product_details">
                            <label>Select Product</label>
                            <select class="form-control" id="product_details_id" name="product_details_id">
                            </select>
                      </div><!-- /.form-group -->    
                  </div>
                  <div class="pull-right">
                      <button type="button" class="btn btn-success" id="add">Add +</button>
                  </div>
                </div><!-- /.box-header -->
                
                <div class="box-body">
                  <div class="table-responsive"> 
                   <table id="data" class="table table-bordered table-hover ">
                    <thead>
                      <tr>
                        <th>S.N.</th>
                        <th>Service Charge</th>
                        <th>Membership Discount</th>
                        <th>Date</th>
                        <th colspan="2">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                   
                  </table>
                  </div>         
                
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section>
      </div><!-- /.content-wrapper -->

     <?php include 'includes/footer.php';?>

     
    </div><!-- ./wrapper -->
    <?php include 'includes/jslinks.php';?>
    <!--Insert Category Insert Modal Start-->
 <div class="modal fade" id="insModal" role="dialog">
        <div class="modal-dialog modal-sm">
        <!-- Modal content-->
        <div class="modal-content">
         <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Insert Product Rate </h4>
        </div>
        <form id ="insProductRate">    
        <div class="modal-body">
        <div class="row">
        
        <div class="col-md-12">
        <div class="form-group">
            <input type="hidden"  name="product_details_id" id="product_details_id" class="form-control"  placeholder="Enter Product">
            <input type="number"  name="service_charge" id="service_charge" class="form-control"  placeholder="Enter Service Charge">
        </div>
        <div class="form-group">
            <input type="number"  name="discount" id="discount" class="form-control"  placeholder="Enter Membership discount without %">
        </div>
        <!-- /.form-group -->
        </div><!-- /.col -->
        </div><!-- /.box -->
        <div class="modal-footer">
            <div class="validate pull-left" style="font-weight:bold;"></span></div>
            <button type="submit" class="btn btn-primary">Submit</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        </div>
        </div>
       </form>
        </div>
        </div>
       </div>
      <!--Insert Category Insert Modal End-->
       <!--Insert Category Edit Modal Start-->
<div class="modal fade" id="updModel" role="dialog">
<div class="modal-dialog modal-sm">

<!-- Modal content-->
<div class="modal-content">

<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Edit Product </h4>
</div>
<form id ="updProductRate">    
<div class="modal-body">
<div class="row">

<div class="col-md-12">
<div class="form-group">
    <input type="hidden"  name="product_rate_id" id="product_rate_id" value="" class="form-control"/>
    <input type="text"  name="service_charge" id="service_charge" value="" class="form-control"  placeholder="Enter Product Name">
</div>
<div class="form-group">
    <input type="text"  name="discount" id="discount" value="" class="form-control"  placeholder="Enter Product Cost">
</div>
<!-- /.form-group -->

</div><!-- /.col -->
</div><!-- /.box -->


<div class="modal-footer">
<div class="validate pull-left" style="font-weight:bold;"></span></div>
<button type="submit" id ="update" class="btn btn-primary">Update</button>
<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>

</div>
</div>
</form>
</div>
</div>
</div>
        <!--Insert Category Edit Modal End-->
         <!--Insert Category Delete Modal Start-->
 <div class="modal fade" id="delModel" role="dialog">
        <div class="modal-dialog modal-sm">
                
        <!-- Modal content-->
        <div class="modal-content">
             
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Delete Category </h4>
        </div>
            <form id ="delProductRate">    
            <div class="modal-body">
                <div class="row">
                <div class="col-md-12">
                <input type="hidden"  name="product_rate_id" id="product_rate_id" value="" class="form-control"/>
                <p>Sure to want to delete "<span style ="color:red" id ="catname"></span>" Amount?</p>
                 </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="delete2" class="btn btn-danger">Delete</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
           </form>
       </div>
       </div>
       </div>
          <!--Insert Category Delete Modal End--> 
  </body>
  <script type="text/javascript" language="javascript">
      $(document).ready(function(){
            $("#add").click(function(){
                 if(user_type=="admin" || privileges_details_system[13]["insertm"]=="true"){
                    $("#insModal").modal('show');
                 }else{
                     showAccessDeniedModel();
                 }
            }); 
            $("#insModal").on('shown.bs.modal', function(){
                            $('#insProductRate').find('#product_details_id').val(product_details_id);
                            $('#insProductRate').off("submit");
                            $('#insProductRate').submit(function() {
                            if(this.service_charge.value == ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please fill valid Product ").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                $('#insProductRate').find('#service_charge').focus();
                                
                                return false;  
                           }else if(this.discount.value == ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please fill valid Product Cost").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                $('#insProductRate').find('#discount').focus();
                                return false;  
                           }else {
                                $(".validate").removeClass("text-danger").addClass("text-success").fadeIn(100).text("Ok").prepend("<span class=\"glyphicon glyphicon-ok text-success\">").fadeOut(1000);
                                $.ajax({
                                    type:"post",
                                    url:"../server/controller/product_rate/InsProductRate.php",
                                    data:$('#insProductRate').serialize(),
                                    success: function(data){ 
                                        location.reload(true);
                                    } 
                                });
                                return false; 
                            }
                   return false;
                    });
            var modal = this;
            var hash = modal.id;
            window.location.hash = hash;
            window.onhashchange = function() {
                    if (!location.hash){
                            $(modal).modal('hide');
                    }
            }
      });  
    $('#myModal').on('hide.bs.modal', function() {
            location.reload(true);
    });
         }); 
    
         function onlad(){
                    cate={}
                    $.ajax({
                    type:"post",
                    url:"../server/controller/product_rate/SelProductRate.php",
                    data:{'product_details_id':product_details_id},
                    success: function(data) {
                        var duce = jQuery.parseJSON(data);
                        $("#data tr:has(td)").remove();
                        $.each(duce, function (index, article) {
                            cate[article.product_rate_id]={};
                            cate[article.product_rate_id]= article;
                           $("#data").append($('<tr/>')
                                .append($('<td/>').html((index+1)))
                                .append($('<td/>').html(article.service_charge))
                                .append($('<td/>').html(article.discount))
                                .append($('<td/>').html(article.date))
                                .append($('<td/>').html($("<button>").attr({'type':'button'}).addClass("btn btn-success btn-xs").append("&nbsp;&nbsp;Edit&nbsp;&nbsp;").click(function (){
                                    if(user_type=="admin" || privileges_details_system[13]["updatem"]=="true"){
                                        updProductRate(article.product_rate_id)
                                     }else{
                                         showAccessDeniedModel();
                                     }
                                    
                                })))
                                .append($('<td/>').html($("<button>").attr({'type':'button'}).addClass("btn btn-danger btn-xs").append("&nbsp;&nbsp;Delete&nbsp;&nbsp;").click(function (){
                                    if(user_type=="admin" || privileges_details_system[13]["deletem"]=="true"){
                                        delProductRate(article.product_rate_id)
                                     }else{
                                         showAccessDeniedModel();
                                     }
                                })))
                            );
                        });
                    }
                });
         }
                    function updProductRate(product_rate_id) {
                      $("#updModel").modal('show');
                      $("#updModel").on('shown.bs.modal', function(){
                            $("#updProductRate").find("#product_rate_id").val(product_rate_id);  
                            $("#updProductRate").find("#service_charge").val(cate[product_rate_id]["service_charge"]);          
                            $("#updProductRate").find("#discount").val(cate[product_rate_id]["discount"]);          
                            $('#updProductRate').off("submit");
                            $('#updProductRate').submit(function() {
                            if(this.service_charge.value == ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please fill valid Product ").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                $('#updProductRate').find('#service_charge').focus();
                                return false;  
                           }else if(this.discount.value == ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please fill valid Product Cost").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                $('#updProductRate').find('#discount').focus();
                                return false;  
                           }else {
                                $(".validate").removeClass("text-danger").addClass("text-success").fadeIn(100).text("Ok").prepend("<span class=\"glyphicon glyphicon-ok text-success\">").fadeOut(1000);
                                $.ajax({
                                    type:"post",
                                    url:"../server/controller/product_rate/UpdProductRate.php",
                                    data:$('#updProductRate').serialize(),
                                    success: function(data){ 
                                         alert(data)
                                         location.reload(true);
                                    } 
                                });
                            }
                        return false;
                    });
                     var modal = this;
                            var hash = modal.id;
                            window.location.hash = hash;
                            window.onhashchange = function() {
                                    if (!location.hash){
                                            $(modal).modal('hide');
                                    }
                            }
                      });  
                    $('#editProductRate').on('hide.bs.modal', function() {
                            location.reload(true);
                    });
                }

</script>

<script type="text/javascript" language="javascript">
        function delProductRate(product_rate_id) {
            
            $("#delModel").modal('show');
                 var service_charge = cate[product_rate_id]["service_charge"];;
                 $("#delModel").on('shown.bs.modal', function(){
                    $("#delProductRate").find("#product_rate_id").val(product_rate_id);        
                    $("#delProductRate").find("#catname").text(service_charge); 
                    $('#delProductRate').off("submit");
                    $('#delProductRate').submit(function() {
                         $.ajax({
                            type:"post",
                            url:"../server/controller/product_rate/delProductRate.php",
                            data:{'product_rate_id':product_rate_id},
                            success: function(data){ 
                                location.reload(true);
                            } 
                  });
                    return false;
    });
    
    });
   
}

</script>
</html>

