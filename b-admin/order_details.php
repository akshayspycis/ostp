
                      <?php include './includes/check_session.php';?>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Order Details</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <?php include 'includes/links.php';?>
   
        <script type="text/javascript">
            var orderid;
         var type=0
         cate={}
         $(document).ready(function() {
             loadEmployeePrivileges(employees_id,callback);
         });  
         function callback(){
             if(user_type=="admin" || privileges_details_system[14]["selectm"]=="true"){
                 onlad("");
             }else{
                 showAccessDeniedModel();
             }
         }
         
        </script> 
  </head>
  <!-- ADD THE CLASS sidedar-collapse TO HIDE THE SIDEBAR PRIOR TO LOADING THE SITE -->
 <body class="wysihtml5-supported skin-yellow sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">

   <?php include 'includes/header.php';?>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
   <?php include 'includes/sidepanel.php';?>

      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Order Details
            <small>Admin panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i> Home</li>
            <li class="active">Order Details</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
           
          <div class="row">
            <div class="col-xs-12">
                <div class="box">
                
                <div class="box-body">
                  <div class="table-responsive"> 
                   <table id="data" class="table table-bordered table-hover ">
                    <thead>
                       <tr>
                        <th>S.N.</th>
                        <th>Order No.</th>
                        <th>Order Date</th>
                        <th>User pic</th>
                        <th>User Name</th>
                        <th>Contact No.</th>
                        <th>Alt Contact No</th>
                        <th>Address</th>
                        <th>No of Product</th>
                        <th>Status</th>
                        <th colspan="2">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                   
                  </table>
                  </div>         
                
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section>
      </div><!-- /.content-wrapper -->

     <?php include 'includes/footer.php';?>

     
    </div><!-- ./wrapper -->
    <?php include 'includes/jslinks.php';?>
    <!--Insert Category Insert Modal Start-->
 <div class="modal fade" id="showModal" role="dialog">
     <div class="modal-dialog modal-lg" style="max-width: 1600px ;">
        <!-- Modal content-->
        <div class="modal-content">
         <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Repaired Item</h4>
        </div>
        <div class="box">
                
                <div class="box-body">
                  <div class="table-responsive"> 
                   <table id="order_product" class="table table-bordered table-hover ">
                    <thead>
                       <tr>
                        <th>S.N.</th>
                        <th>Product Pic</th>
                        <th>Product Name</th>
                        <th>Brand Name</th>
                        <th>Model </th>
                        <th>Problem</th>
                        <th>Description</th>
                        <th>User Problem Description</th>
                        <th>Engineer Status</th>
                        <th colspan="2">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                   
                  </table>
                  </div>         
                
                </div><!-- /.box-body -->
              </div><!-- /.box -->
        </div>
        </div>
       </div>
 <div class="modal fade" id="showOrderAllotDetailsModal" role="dialog">
     <div class="modal-dialog modal-lg" style="max-width: 1600px ;">
        <!-- Modal content-->
        <div class="modal-content">
         <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Assigned Employee</h4>
        </div>
        <div class="box">
                
                <div class="box-body">
                  <div class="table-responsive"> 
                   <table id="order_allot_details" class="table table-bordered table-hover ">
                    <thead>
                       <tr>
                        <th>S.N.</th>
                        <th>Employee Pic</th>
                        <th>Employee Name</th>
                        <th>Contact No</th>
                        <th>Email</th>
                        <th>Employee Time </th>
                        <th>Employee Date</th>
                        <th>Customer Time</th>
                        <th>Customer Date</th>
                        <th colspan="2">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                   
                  </table>
                  </div>         
                
                </div><!-- /.box-body -->
              </div><!-- /.box -->
        </div>
        </div>
       </div>
      <!--Insert Category Insert Modal End-->
       <!--Insert Category Edit Modal Start-->
<div class="modal fade" id="updModel" role="dialog">
<div class="modal-dialog modal-sm">

<!-- Modal content-->
<div class="modal-content">

<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Edit User Problem Description</h4>
</div>
<form id ="updOrderDetails">    
<div class="modal-body">
<div class="row">
<div class="col-md-12">
<div class="form-group">
    <input type="hidden"  name="order_product_id" id="order_product_id" value="" class="form-control"/>
    <textarea name="editamt" id="editamt" value="" class="form-control"></textarea>
    
</div>
<!-- /.form-group -->

</div><!-- /.col -->

</div><!-- /.box -->


<div class="modal-footer">
<div class="validate pull-left" style="font-weight:bold;"></span></div>
<button type="submit" id ="update" class="btn btn-primary">Update</button>
<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>

</div>
</div>
</form>
</div>
</div>
</div>
<div class="modal fade" id="insModel" role="dialog">
<div class="modal-dialog modal-sm">

<!-- Modal content-->
<div class="modal-content">

<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Allot Problem to Employee</h4>
</div>
<form id ="insOrderAllotDetails">    
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <input type="hidden"  name="order_id" id="order_id" value="" class="form-control"/>
                    <input type="hidden"  name="employee_id" id="employee_id" value="" class="form-control"/>
                    <input type="hidden"  name="order_product_id" id="order_product_id" value="" class="form-control"/>
                    <div class="form-group" id="category">
                            <label>Select Roll</label>
                            <select class="form-control" id="brand_details_id" name="brand_details_id">
                            </select>
                    </div><!-- /.form-group -->    
                </div>
            </div><!-- /.col -->
            <div class="col-md-12">
        <div class="form-group">
            <label>Employee Name</label>
            <div class="">
                        <div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="user_details_first">
      
    </button>
                            <style>
                                .dropdown-toggle, .dropdown-menu {
    width: 271px;
}
                            </style>
                            <ul class="dropdown-menu" id="user_details" ></ul>
  </div>
                  </div>
            <input type="hidden"  name="roll_type_id" id="roll_type_id" class="form-control"  placeholder="Enter Roll">
        </div>
        <!-- /.form-group -->
        </div><!-- /.col -->
        <div class="col-md-12">
<div class="form-group ">
    <button type="button" id ="call_eng" class="btn btn-default col-md-12" disabled="true">Call to Employee (<b id="emp_contact_no"></b>)</button>
</div>
<!-- /.form-group -->

</div><!-- /.col -->
<div class="col-md-12" id="emp_time_details" style="display: none;">
            <div class="form-group ">
                <label>Employee Time</label>
                <input type="time" name="engg_time" id="engg_time" value="" class="form-control"/>
            </div>
            <div class="form-group ">
                <label>Employee Date</label>
                <input type="date" name="engg_date" id="engg_date" value="" class="form-control"/>
            </div>
        </div><!-- /.col -->
        
        <div class="col-md-12" style="margin-top: 10px;">
<div class="form-group ">
    <button type="button" id ="call_cus" class="btn btn-default col-md-12">Call to User (<b id="cus_contact_no"></b>)</button>
</div>
<div class="form-group " style="margin-top: 43px;">
    <button type="button" id ="call_cus_alt" class="btn btn-default col-md-12">Call to User (<b id="cus_alt_contact_no"></b>)</button>
</div>
<!-- /.form-group -->

</div><!-- /.col -->
        <div class="col-md-12" id="cus_time_details" style="display: none;">
            <div class="form-group ">
                <label>Customer Time</label>
                <input type="time" name="cus_time" id="cus_time" value="" class="form-control"/>
            </div>
            <div class="form-group ">
                <label>Customer Date</label>
                <input type="date" name="cus_date" id="cus_date" value="" class="form-control"/>
            </div>
        </div><!-- /.col -->
        </div><!-- /.box -->
        <div class="modal-footer">
        <div class="validate pull-left" style="font-weight:bold;"></span></div>
        <button type="submit" id ="update" class="btn btn-primary">Update</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>

        </div>
    </div>
</form>
</div>
</div>
</div>
        <!--Insert Category Edit Modal End-->
         <!--Insert Category Delete Modal Start-->
 <div class="modal fade" id="delModel" role="dialog">
        <div class="modal-dialog modal-sm">
                
        <!-- Modal content-->
        <div class="modal-content">
             
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Delete Category </h4>
        </div>
            <form id ="delOrderDetails">    
            <div class="modal-body">
                <div class="row">
                <div class="col-md-12">
                <input type="hidden"  name="order_id" id="order_id" value="" class="form-control"/>
                <p>Sure to want to delete "<span style ="color:red" id ="catname"></span>" ?</p>
                 </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="delete2" class="btn btn-danger">Delete</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
           </form>
       </div>
       </div>
       </div>
          <!--Insert Category Delete Modal End--> 
  </body>
  <script type="text/javascript" language="javascript">
      $(document).ready(function(){
            $("#call_eng").click(function(){
                 if($(this).text()=="Employee Call Confirmed"){
                     $("#emp_time_details").hide();
                     $(this).html("Call to Employee (<b id='emp_contact_no'></b>)");
                     setEmpContact($(this).find("#emp_contact_no"));
                 }else{
                     $("#emp_time_details").show();
                     $(this).html("Employee Call Confirmed");
                 }
            }); 
            $("#call_cus").click(function(){
                 showCus($(this),"cus_contact_no");
            }); 
            $("#call_cus_alt").click(function(){
                 showCus($(this),"cus_alt_contact_no");
            }); 
            function showCus(obj,id){
                if(obj.text()=="Customer Call Confirmed"){
                     $("#cus_time_details").hide();
                     obj.html("Call to Cutomer (<b id='"+id+"'></b>)");
                     setCusContact();
                 }else{
                     $("#cus_time_details").show();
                     obj.html("Customer Call Confirmed");
                 }
            }
            function setEmpContact(obj){
                    $("#insModel").find("#insOrderAllotDetails").find("#call_eng").find("#emp_contact_no").html(employee_details[$("#insModel").find("#insOrderAllotDetails").find("#employee_id").val()].contact_no);
            }
            function setCusContact(){
                $("#insModel").find("#insOrderAllotDetails").find("#call_cus").find("#cus_contact_no").html(cate[orderid].contact_no);
                $("#insModel").find("#insOrderAllotDetails").find("#call_cus_alt").find("#cus_alt_contact_no").html(cate[orderid].alt_contact_no);
            }
         }); 
    
         function onlad(order_id){
             orderid='';
                    cate={}
                    $.ajax({
                    type:"post",
                    url:"../server/controller/order_details/SelOrderDetails.php",
                    data:{'order_id':order_id},
                    success: function(data) {
                        var duce = jQuery.parseJSON(data);
                        $("#data tr:has(td)").remove();
                        $.each(duce, function (index, article) {
                            cate[article.order_id]={};
                            cate[article.order_id]= article;
                           var btn_class="btn-danger";
                           var status="Pending";
                           if(article.status=="true"){
                               btn_class="btn-success";
                               status="Approved";
                           }
                           var img= article.pic;
                           $("#data").append($('<tr/>')
                                .append($('<td/>').html((index+1)))
                                .append($('<td/>').html(article.order_no))
                                .append($('<td/>').html(article.order_date))
                                .append($('<td/>').html("<img src="+img+" height=\"50\" width=\"50\"/>"))
                                .append($('<td/>').html(article.user_name))
                                .append($('<td/>').html(article.contact_no))
                                .append($('<td/>').html(article.alt_contact_no))
                                .append($('<td/>').html(article.address_details))
                                .append($('<td/>').html($("<div>").append(article.count).append($("<a>").addClass("btn btn-social-icon pull-right").css({'margin-top': '-7px'}).append($("<i>").addClass("fa fa-list")).click(function(){
                                    if(user_type=="admin" || privileges_details_system[14]["selectm"]=="true"){
                                         showProduct(article.order_id)
                                     }else{
                                         showAccessDeniedModel();
                                     }  
                                }))))
                                .append($('<td/>').append($("<button></button>").addClass("btn "+btn_class+" btn-xs").attr({"id":"status"}).append(status).click(function(){
                                    if(user_type=="admin" || privileges_details_system[14]["updatem"]=="true"){
                                         updatestatus($(this),article.order_id);
                                     }else{
                                         showAccessDeniedModel();
                                     }  
                                })))
//                                .append($('<td/>').html($("<button>").attr({'type':'button'}).addClass("btn btn-success btn-xs").append("&nbsp;&nbsp;Edit&nbsp;&nbsp;").click(function (){
//                                    if(user_type=="admin" || privileges_details_system[14]["updatem"]=="true"){
//                                        updOrderDetails(article.order_id)
//                                     }else{
//                                         showAccessDeniedModel();
//                                     }
//                                    
//                                })))
                                .append($('<td/>').html($("<button>").attr({'type':'button'}).addClass("btn btn-danger btn-xs").append("&nbsp;&nbsp;Delete&nbsp;&nbsp;").click(function (){
                                    if(user_type=="admin" || privileges_details_system[14]["deletem"]=="true"){
                                        delOrderDetails(article.order_id)
                                     }else{
                                         showAccessDeniedModel();
                                     }
                                })))
                                        
                            );
                        });
                    }
                });
         }
         order_product={}
         function showProduct(order_id){
           orderid=order_id;
           $("#showModal").modal('show');
           $("#showModal").find(".modal-dialog").width($(window).width()-20);
           $("#showModal").on('shown.bs.modal', function(){
           order_product={}
                    $.ajax({
                    type:"post",
                    url:"../server/controller/order_product/SelOrderProduct.php",
                    data:{'order_id':order_id},
                    success: function(data) {
                        var duce = jQuery.parseJSON(data);
                        $("#order_product tr:has(td)").remove();
                        $.each(duce, function (index, article) {
                           order_product[article.order_product_id]={};
                           order_product[article.order_product_id]= article;
                           var img= "../server/"+article.icon.replace("../../", "");;
                           var btn_class="btn-danger";
                           var status="Pending";
                           var assign="Assigned";
                           if(article.status=="true"){
                               btn_class="btn-success";
                               status="Approved";
                           }else if(article.status==null){
                               status="No Assigned";
                               assign="Assign";
                           }
                           $("#order_product").append($('<tr/>')
                                .append($('<td/>').html((index+1)))
                                .append($('<td/>').html("<img src="+img+" height=\"50\" width=\"50\"/>"))
                                .append($('<td/>').html(article.product_name))
                                .append($('<td/>').html(article.brand_name))
                                .append($('<td/>').html(article.model_name))
                                .append($('<td/>').html(article.problem))
                                .append($('<td/>').html(article.discription))
                                .append($('<td/>').html(article.problem_discription))
//                                .append($('<td/>').html($("<div>").append(article.count).append($("<a>").addClass("btn btn-social-icon pull-right").css({'margin-top': '-7px'}).append($("<i>").addClass("fa fa-list")).click(function(){
//                                    if(user_type=="admin" || privileges_details_system[14]["selectm"]=="true"){
//                                         showProduct(article.order_id)
//                                     }else{
//                                         showAccessDeniedModel();
//                                     }  
//                                }))))
                                .append($('<td/>').append($("<button></button>").addClass("btn "+btn_class+" btn-xs").attr({"id":"status"}).append(status).click(function(){
//                                     if(user_type=="admin" || privileges_details_system[14]["updatem"]=="true"){
//                                         updatestatus($(this),article.order_product_id);
//                                     }else{
//                                         showAccessDeniedModel();
//                                     }  
                                })))
                                .append($('<td/>').html($("<button>").attr({'type':'button'}).addClass("btn btn-primary btn-xs").append("&nbsp;&nbsp;"+assign+"&nbsp;&nbsp;").click(function (){
                                    
                                    if(user_type=="admin" || privileges_details_system[14]["updatem"]=="true"){
                                        if(assign=="Assigned")
                                        selOrderAllotDetails(article.order_allot_detailsId);
                                        else
                                        insOrderAllotDetails(article.order_product_id)
                                     }else{
                                         showAccessDeniedModel();
                                     }
                                })))
                                .append($('<td/>').html($("<button>").attr({'type':'button'}).addClass("btn btn-success btn-xs").append("&nbsp;&nbsp;Edit&nbsp;&nbsp;").click(function (){
                                    if(user_type=="admin" || privileges_details_system[14]["updatem"]=="true"){
                                        updOrderProduct(article.order_product_id)
                                     }else{
                                         showAccessDeniedModel();
                                     }
                                })))
                                .append($('<td/>').html($("<button>").attr({'type':'button'}).addClass("btn btn-danger btn-xs").append("&nbsp;&nbsp;Delete&nbsp;&nbsp;").click(function (){
                                    if(user_type=="admin" || privileges_details_system[14]["deletem"]=="true"){
                                        delOrderProduct(article.order_product_id)
                                     }else{
                                         showAccessDeniedModel();
                                     }
                                })))
                                        
                            );
                        });
                    }
                });
            var modal = this;
            var hash = modal.id;
            window.location.hash = hash;
            window.onhashchange = function() {
                    if (!location.hash){
                            $(modal).modal('hide');
                    }
            }
      });  
    $('#myModal').on('hide.bs.modal', function() {
            location.reload(true);
    });
        
        }
        order_allot_details={}
                  function selOrderAllotDetails(order_allot_detailsId){
                  order_allot_details={}
           $("#showOrderAllotDetailsModal").modal('show');
           $("#showOrderAllotDetailsModal").on('shown.bs.modal', function(){
                    $.ajax({
                    type:"post",
                    url:"../server/controller/order_allot_details/SelOrderAllotDetails.php",
                    data:{'order_allot_detailsId':order_allot_detailsId},
                    success: function(data) {
                        var duce = jQuery.parseJSON(data);
                        $("#order_allot_details tr:has(td)").remove();
                        $.each(duce, function (index, article) {
                           order_allot_details[order_allot_detailsId]={};
                           order_allot_details[order_allot_detailsId]= article;
                           var img= "../server/"+article.pic.replace("../../", "");;
                           var btn_class="btn-danger";
                           var status="Pending";
                           var assign="Assigned";
                           if(article.status=="true"){
                               btn_class="btn-success";
                               status="Approved";
                           }else if(article.status==null){
                               status="No Assigned";
                               assign="Assign";
                           }
                           $("#order_allot_details").append($('<tr/>')
                                .append($('<td/>').html((index+1)))
                                .append($('<td/>').html("<img src="+img+" height=\"50\" width=\"50\"/>"))
                                .append($('<td/>').html(article.user_name))
                                .append($('<td/>').html(article.contact_no))
                                .append($('<td/>').html(article.email))
                                .append($('<td/>').html(article.engg_time))
                                .append($('<td/>').html(article.engg_date))
                                .append($('<td/>').html(article.cus_time))
                                .append($('<td/>').html(article.cus_date))
                                .append($('<td/>').html($("<button>").attr({'type':'button'}).addClass("btn btn-danger btn-xs").append("&nbsp;&nbsp;Delete&nbsp;&nbsp;").click(function (){
                                    if(user_type=="admin" || privileges_details_system[14]["deletem"]=="true"){
                                        delOrderAllotDetails(order_allot_detailsId)
                                     }else{
                                         showAccessDeniedModel();
                                     }
                                })))
                                        
                            );
                        });
                    }
                });
            var modal = this;
            var hash = modal.id;
            window.location.hash = hash;
            window.onhashchange = function() {
                    if (!location.hash){
                            $(modal).modal('hide');
                    }
            }
      });  
    $('#myModal').on('hide.bs.modal', function() {
            location.reload(true);
    });
        
                  }
         
            function updatestatus(obj,id){
                      var value;
                      if(obj.text().trim()=="Approved"){
                                value="false";
                                obj.empty();
                                obj.append("Pending");
                                obj.removeClass("btn-success");
                                obj.addClass("btn-danger");
                      }else{
                                value="true";
                                obj.empty();
                                obj.append("Approved");
                                obj.removeClass("btn-danger");
                                obj.addClass("btn-success");
                      }           
                      
                  $.ajax({
                    type:"post",
                    url:"../server/controller/order_details/UpdOrderDetailsStatus.php",
                    data:{'order_id':id,'status':value},
                    success: function(data) {
                    }
                  });
              }
              
                    function updOrderProduct(order_product_id) {
                      $("#updModel").modal('show');
                      $("#updModel").on('shown.bs.modal', function(){
                            $("#updOrderDetails").find("#order_product_id").val(order_product_id);  
                            $("#updOrderDetails").find("#editamt").val(order_product[order_product_id]["problem_discription"]);          
                            $('#updOrderDetails').off("submit");
                            $('#updOrderDetails').submit(function() {
                                $(".validate").removeClass("text-danger").addClass("text-success").fadeIn(100).text("Ok").prepend("<span class=\"glyphicon glyphicon-ok text-success\">").fadeOut(1000);
                                $.ajax({
                                    type:"post",
                                    url:"../server/controller/order_product/UpdOrderProduct.php",
                                    data:$('#updOrderDetails').serialize(),
                                    success: function(data){ 
                                         location.reload(true);
                                    } 
                                });
                            
                        return false;
                    });
                     var modal = this;
                            var hash = modal.id;
                            window.location.hash = hash;
                            window.onhashchange = function() {
                                    if (!location.hash){
                                            $(modal).modal('hide');
                                    }
                            }
                      });  
                    $('#editOrderDetails').on('hide.bs.modal', function() {
                            location.reload(true);
                    });
                }

</script>

<script type="text/javascript" language="javascript">
        function delOrderDetails(order_id) {
            
            $("#delModel").modal('show');
                 var order_name = cate[order_id]["order_no"];;
                 $("#delModel").on('shown.bs.modal', function(){
                    $("#delOrderDetails").find("#order_id").val(order_id);        
                    $("#delOrderDetails").find("#catname").text(order_name); 
                    $('#delOrderDetails').off("submit");
                    $('#delOrderDetails').submit(function() {
                         $.ajax({
                            type:"post",
                            url:"../server/controller/order_details/delOrderDetails.php",
                            data:{'order_id':order_id},
                            success: function(data){ 
                                location.reload(true);
                            } 
                  });
                    return false;
    });
    
    });
   
}

        function delOrderProduct(order_product_id) {
            
            $("#delModel").modal('show');
                 var order_name = order_product[order_product_id]["product_name"]+" "+order_product[order_product_id]["brand_name"]+" "+order_product[order_product_id]["model_name"]+" "+order_product[order_product_id]["problem"]+" "+order_product[order_product_id]["discription"]+" "+order_product[order_product_id]["problem_discription"];
                 $("#delModel").on('shown.bs.modal', function(){
                    $("#delOrderDetails").find("#catname").text(order_name); 
                    $('#delOrderDetails').off("submit");
                    $('#delOrderDetails').submit(function() {
                         $.ajax({
                            type:"post",
                            url:"../server/controller/order_product/DelOrderProduct.php",
                            data:{'order_product_id':order_product_id},
                            success: function(data){ 
                                alert(data)
                                location.reload(true);
                            } 
                  });
                    return false;
    });
    
    });
   
}

        function delOrderAllotDetails(order_allot_detailsId) {
            
            $("#delModel").modal('show');
                 var order_name = order_allot_details[order_allot_detailsId]["user_name"]+" from assigned order ";
                 $("#delModel").on('shown.bs.modal', function(){
                    $("#delOrderDetails").find("#catname").text(order_name); 
                    $('#delOrderDetails').off("submit");
                    $('#delOrderDetails').submit(function() {
                         $.ajax({
                            type:"post",
                            url:"../server/controller/order_allot_details/DelOrderAllotDetails.php",
                            data:{'order_allot_detailsId':order_allot_detailsId},
                            success: function(data){ 
                                location.reload(true);
                            } 
                  });
                    return false;
    });
    
    });
   
}

function insOrderAllotDetails(order_product_id){
           $("#insModel").modal('show');
           $("#insModel").on('shown.bs.modal', function(){
           $("#insModel").find("#order_product_id").val(order_product_id); 
           $("#insModel").find("#order_id").val(orderid); 
           $("#insModel").find("#insOrderAllotDetails").find("#call_cus").find("#cus_contact_no").html(cate[orderid].contact_no);
           $("#insModel").find("#insOrderAllotDetails").find("#call_cus_alt").find("#cus_alt_contact_no").html(cate[orderid].alt_contact_no);
           loadRolls();
           var modal = this;
           var hash = modal.id;
           window.location.hash = hash;
           window.onhashchange = function() {
                if (!location.hash){
                        $(modal).modal('hide');
                }
           }
      });  
     $('#insOrderAllotDetails').off("submit");
                            $('#insOrderAllotDetails').submit(function() {
                            if(this.employee_id.value == ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please Select valid Employee ").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                return false;  
                           }else if(this.engg_time.value == ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please Select valid Employee time").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                return false;  
                           }else if(this.engg_date.value == ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please Select valid Employee Date").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                return false;  
                           }else if(this.cus_time.value == ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please Select valid Customer time").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                return false;  
                           }else if(this.cus_date.value == ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please Select valid Customer Date").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                return false;  
                           }else {
                                $(".validate").removeClass("text-danger").addClass("text-success").fadeIn(100).text("Ok").prepend("<span class=\"glyphicon glyphicon-ok text-success\">").fadeOut(1000);
                                $.ajax({
                                    type:"post",
                                    url:"../server/controller/order_allot_details/InsOrderAllotDetails.php",
                                    data:$('#insOrderAllotDetails').serialize(),
                                    success: function(data){ 
                                        alert(data)
                                        location.reload(true);
                                    } 
                                });
                                return false; 
                            }
                   return false;
                    });  
    $('#myModal').on('hide.bs.modal', function() {
            location.reload(true);
    });
}

function loadRolls(){
             $.ajax({
                    type:"post",
                    url:"../server/controller/roll_type/SelRollType.php",
                    success: function(data) {
                        var duce = jQuery.parseJSON(data);
                        $("#insModel").find("#insOrderAllotDetails").find("#category").find("select").empty();
                        $.each(duce, function (index, article) {
                           $("#insModel").find("#insOrderAllotDetails").find("#category").find("select").append($('<option>').attr({'value':article.roll_type_id}).append(article.roll_type));
                        });
                        $("#insModel").find("#insOrderAllotDetails").find("#category").find("select").trigger("change");
                    }
                });
             $("#category").find("select").change(function () {
                roll_type_id=$(this).val();
                onloadUser(roll_type_id);
             }); 
         }
</script>
<script>
    employee_details={};
    function onloadUser(roll_type_id){
             employee_details={};
             $.ajax({
                type:"post",
                url:"../server/controller/rolls_details/SelRollsDetails.php",
                data:{'roll_type_id':roll_type_id},
                success: function(data) { 
                var duce = jQuery.parseJSON(data); //here data is passed to js object
                employee_details={}
                var inde=0;
                $("#user_details_first").empty();
                $("#user_details").empty();
                $.each(duce, function (index, article) {
                        employee_details[article.employee_id]=article;    
                        if(inde==0){
                            $("#user_details_first").append('<img src="../server/controller/user_details/'+article.pic+'" width="50" height="50" >'+article.user_name+'<span class="glyphicon glyphicon-chevron-down"></span>');
                            employee_id=article.employee_id;
                            inde++;
                        }
                        $("#user_details").append($('<li><a href="#"><img src="../server/controller/user_details/'+article.pic+'" width="50" height="50">'+article.user_name+'</a></li>').click(function (event){
                            employee_id=article.employee_id;
                            $("#insModel").find("#insOrderAllotDetails").find("#employee_id").val(employee_id);
                            $("#insModel").find("#insOrderAllotDetails").find("#call_eng").find("#emp_contact_no").html(article.contact_no);
                            $("#insModel").find("#insOrderAllotDetails").find("#call_eng").attr('disabled',false);
                            $("#user_details_first").html($(this).find("a").html()).append('<span class="glyphicon glyphicon-chevron-down"></span>');
                             event.preventDefault(); // ca
                        }));
                });
                }
            });
         }
</script>
</html>

