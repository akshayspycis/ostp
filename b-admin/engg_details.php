
                      <?php include './includes/check_session.php';?>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Engineer Details</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <?php include 'includes/links.php';?>
   
        <script type="text/javascript">
            var orderid;
         var type=0
         cate={}
         $(document).ready(function() {
             loadEmployeePrivileges(employees_id,callback);
         });  
         function callback(){
             if(user_type=="admin" || privileges_details_system[15]["selectm"]=="true"){
                 if(user_type=="admin"){
                     showProduct('');
                 }else{
                     showProduct(employees_id);
                 }
                 
             }else{
                 showAccessDeniedModel();
             }
         }
         
        </script> 
  </head>
  <!-- ADD THE CLASS sidedar-collapse TO HIDE THE SIDEBAR PRIOR TO LOADING THE SITE -->
 <body class="wysihtml5-supported skin-yellow sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">

   <?php include 'includes/header.php';?>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
   <?php include 'includes/sidepanel.php';?>

      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Engg Details
            <small>Admin panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i> Home</li>
            <li class="active">Engg Details</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
           
          <div class="row">
            <div class="col-xs-12">
                <div class="box">
                
                <div class="box-body">
                  <div class="table-responsive"> 
                   <table id="data" class="table table-bordered table-hover ">
                    <thead>
                       <tr>
                        <th>S.N.</th>
                        <th>Order No</th>
                        <th>Product Pic</th>
                        <th>Product Name</th>
                        <th>Brand Name</th>
                        <th>Model </th>
                        <th>Problem</th>
                        <th>Description</th>
                        <th>User Problem Description</th>
                        <th>Address</th>
                         <th>Your Time</th>
                        <th>Your Date</th>
                        <th>Customer Time</th>
                        <th>Customer Date</th>
                        <th>Engineer Status</th>
                        <th colspan="2">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                   
                  </table>
                  </div>         
                
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section>
      </div><!-- /.content-wrapper -->

     <?php include 'includes/footer.php';?>

     
    </div><!-- ./wrapper -->
    <?php include 'includes/jslinks.php';?>
    <!--Insert Category Insert Modal Start-->
 <div class="modal fade" id="showModal" role="dialog">
     <div class="modal-dialog modal-lg" style="max-width: 1600px ;">
        <!-- Modal content-->
        <div class="modal-content">
         <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Check Product Condition</h4>
        </div>
        <div class="box">
                <div class="box-body">
                      <div class="row">
                      <div class="col-md-3 col-xs-3" >
                        <div class="form-group " >
                            <a id="tab_1"  class="btn btn-default col-md-12">Pending</a>
                        </div>
                      </div>
                      <div class="col-md-3 col-xs-3" >
                        <div class="form-group " >
                            <a id="tab_2" class="btn btn-default col-md-12">Under Lab</a>
                        </div>
                      </div>
                      <div class="col-md-3 col-xs-3" >
                        <div class="form-group " >
                            <a id="tab_3" class="btn btn-default col-md-12">Resolve</a>
                        </div>
                      </div>
                      <div class="col-md-3 col-xs-3" >
                        <div class="form-group " >
                            <a id="tab_4" class="btn btn-default col-md-12">No Action</a>
                        </div>
                      </div>
                  </div>
                    <div class="box" id="tab_1_box" style="margin: 10px;display: none;" >
                      <div class="box-header">
                          <center><h5 class="box-title">Check Product Condition:<b>Pending</b></h5></center>
                      </div>
                      <form id ="insPending">    
                          <div class="box-body">
                                    <div class="row">
                                    <div class="col-md-12" >
                                    <div class="form-group ">
                                    <label>Reason Message</label>
                                    <textarea name="product_reason" id="product_reason" value=""  class="form-control"></textarea>
                                    </div>
                                    </div><!-- /.col -->
                                    <div class="col-md-12" >
                                    <div class="form-group ">
                                    <label>Repairing Duration in working hrs</label>
                                    <input type="number" name="product_repair_hrs" id="product_repair_hrs" value=""  class="form-control"/>
                                    <input type="hidden" name="product_status" id="product_status" value=""  class="form-control"/>
                                    <input type="hidden" name="order_product_id" id="order_product_id" value=""  class="form-control"/>
                                    <input type="hidden" name="engg_details_id" id="engg_details_id" value=""  class="form-control"/>
                                    </div>
                                    </div><!-- /.col -->
                                    </div><!-- /.box -->

                          </div>
                           <div class="box-footer">
                                    <div class="validate pull-left" style="font-weight:bold;"></span></div>
                                    <button type="submit" id ="update" class="btn btn-primary">Update</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                           </div>
                        </form>
                    </div>
                    <div class="box" id="tab_2_box" style="margin: 10px;display: none;" >
                      <div class="box-header">
                          <center><h5 class="box-title">Check Product Condition : <b>Under Lab</b></h5></center>
                      </div>
                      <form id ="insUnder">    
                          <div class="box-body">
                                    <div class="row">
                                    <div class="col-md-12" >
                                    <div class="form-group ">
                                    <label>Product Name / Brand Name / Model No /Reason Message </label>
                                    <textarea name="product_reason" id="product_reason" value=""  class="form-control"></textarea>
                                    </div>
                                    </div><!-- /.col -->
                                    <div class="col-md-12" >
                                    <div class="form-group ">
                                    <!--<label>Repairing Duration in working hrs</label>-->
                                    <input type="hidden" name="product_repair_hrs" id="product_repair_hrs" value=""  class="form-control"/>
                                    <input type="hidden" name="product_status" id="product_status" value=""  class="form-control"/>
                                    <input type="hidden" name="order_id" id="order_id" value=""  class="form-control"/>
                                    <input type="hidden" name="order_product_id" id="order_product_id" value=""  class="form-control"/>
                                    <input type="hidden" name="engg_details_id" id="engg_details_id" value=""  class="form-control"/>
                                    </div>
                                    </div><!-- /.col -->
                                       <div class="col-md-12" style="margin-top: 10px;">
<div class="form-group ">
    <button type="button" id ="call_cus" class="btn btn-default col-md-12">Get Authorised OTP from User (<b id="cus_contact_no"></b>)</button>
</div>
<div class="form-group " style="margin-top: 43px;">
    <button type="button" id ="call_cus_alt" class="btn btn-default col-md-12">Get Authorised OTP from User (<b id="cus_alt_contact_no"></b>)</button>
</div>
<!-- /.form-group -->

</div>
                                    <div class="col-md-12" id="cus_time_details" style="display: none;">
            <div class="form-group ">
                <label>Get Customer OTP on this No. <b id="cus_contact_no_display"></b></label>
                <input type="hidden" name="select_contact_no" id="select_contact_no" value="" class="form-control"/>
                <input type="number" name="otp" id="otp" value="" class="form-control"/>
            </div>
                                        <button type="button" id ="check_otp_under" class="btn btn-primary">Check OTP</button>
        </div>

                                    </div><!-- /.box -->
                                    

                          </div>
                          <div class="box-footer" style="display: none;">
                                    <div class="validate pull-left" style="font-weight:bold;"></span></div>
                                    <button type="submit" id ="update" class="btn btn-primary">Update</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                           </div>
                        </form>
                    </div>
                    <div class="box" id="tab_3_box" style="margin: 10px;display: none;" >
                      <div class="box-header">
                          <center><h5 class="box-title">Check Product Condition:<b>Resolve</b></h5></center>
                      </div>
                      <form id ="insResolve">    
                          <div class="box-body">
                                    <div class="row">
                                    <div class="col-md-12" >
                                    <div class="form-group ">
                                    <label>Product Name / Brand Name / Model No /Solution Message </label>
                                        <textarea name="product_reason" id="product_reason" value=""  class="form-control"></textarea>
                                    </div>
                                    </div><!-- /.col -->
                                    <div class="col-md-12" >
                                    <div class="form-group ">
                                        <input type="hidden" name="product_repair_hrs" id="product_repair_hrs" value=""  class="form-control"/>
                                        <input type="hidden" name="product_status" id="product_status" value=""  class="form-control"/>
                                        <input type="hidden" name="order_id" id="order_id" value=""  class="form-control"/>
                                        <input type="hidden" name="order_product_id" id="order_product_id" value=""  class="form-control"/>
                                        <input type="hidden" name="engg_details_id" id="engg_details_id" value=""  class="form-control"/>
                                    </div>
                                    </div><!-- /.col -->
                                       <div class="col-md-12" style="margin-top: 10px;">
                                            <div class="form-group ">
                                                <button type="button" id ="call_cus" class="btn btn-default col-md-12">Get Authorised OTP from User (<b id="cus_contact_no"></b>)</button>
                                            </div>
                                            <div class="form-group " style="margin-top: 43px;">
                                                <button type="button" id ="call_cus_alt" class="btn btn-default col-md-12">Get Authorised OTP from User (<b id="cus_alt_contact_no"></b>)</button>
                                            </div>
                                       </div>
                                    <div class="col-md-12" id="cus_time_details" style="display: none;">
            <div class="form-group ">
                <label>Get Customer OTP on this No. <b id="cus_contact_no_display"></b></label>
                <input type="hidden" name="select_contact_no" id="select_contact_no" value="" class="form-control"/>
                <input type="number" name="otp" id="otp" value="" class="form-control"/>
            </div>
                                        <button type="button" id ="check_otp_under" class="btn btn-primary">Check OTP</button>
        </div>

                                    </div><!-- /.box -->
                                    

                          </div>
                          <div class="box-footer" style="display: none;">
                                    <div class="validate pull-left" style="font-weight:bold;"></span></div>
                                    <button type="submit" id ="update" class="btn btn-primary">Update</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                           </div>
                        </form>
                    </div>
                    <div class="box" id="tab_4_box" style="margin: 10px;display: none;" >
                      <div class="box-header">
                          <center><h5 class="box-title">Check Product Condition:<b>No Action</b></h5></center>
                      </div>
                      <div class="box-body">

                      </div>
                    </div>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
        </div>
        </div>
       </div>
 <div class="modal fade" id="showEnggAllotDetailsModal" role="dialog">
     <div class="modal-dialog modal-lg" style="max-width: 1600px ;">
        <!-- Modal content-->
        <div class="modal-content">
         <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Assigned Employee</h4>
        </div>
        <div class="box">
                
                <div class="box-body">
                  <div class="table-responsive"> 
                   <table id="order_allot_details" class="table table-bordered table-hover ">
                    <thead>
                       <tr>
                        <th>S.N.</th>
                        <th>User Pic</th>
                        <th>User Name</th>
                        <th>Contact No</th>
                        <th>Alt Contact No</th>
                        <th colspan="2">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                   
                  </table>
                  </div>         
                
                </div><!-- /.box-body -->
              </div><!-- /.box -->
        </div>
        </div>
       </div>
 <div class="modal fade" id="show_payment_summary_modal" role="dialog">
     <div class="modal-dialog modal-lg" style="max-width: 1600px ;">
        <!-- Modal content-->
        <div class="modal-content">
         <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Payment Summary</h4>
        </div>
        <div class="box">
                
                <div class="box-body">
                  <div class="table-responsive"> 
                   <table id="show_payment_summary" class="table table-bordered table-hover ">
                    <thead>
                       <tr>
                        <th>S.N.</th>
                        <th>Description</th>
                        <th>Amount</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                   
                  </table>
                  </div>         
                
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <div class="form-group pull-left">
                        <label>Paid Amount</label>
                        <input type="number" name="paid_amt" id="paid_amt" value="" class="form-control"/>
                    </div>
                    <button type="button" class="btn btn-primary col-md-3 pull-right" id="payment_user" data-dismiss="modal">Pay</button>
                </div>
              </div><!-- /.box -->
        </div>
        </div>
       </div>
 <div class="modal fade" id="showSummary" role="dialog">
     <div class="modal-dialog modal-lg" style="max-width: 1600px ;">
        <!-- Modal content-->
        <div class="modal-content">
         <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Show Summary</h4>
        </div>
        <div class="box">
                <div class="box-body">
                  <div class="table-responsive"> 
                   <table id="order_product_status" class="table table-bordered table-hover ">
                    <thead>
                       <tr>
                        <th>S.N.</th>
                        <th>Status Date</th>
                        <th>Product Status</th>
                        <th>Product Reason</th>
                        <th>Repairing Hrs</th>
                        <th colspan="2">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                   
                  </table>
                  </div>         
                
                </div><!-- /.box-body -->
              </div><!-- /.box -->
        </div>
        </div>
       </div>
      <!--Insert Category Insert Modal End-->
       <!--Insert Category Edit Modal Start-->
<div class="modal fade" id="updModel" role="dialog">
<div class="modal-dialog modal-sm">

<!-- Modal content-->
<div class="modal-content">

<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Edit User Problem Description</h4>
</div>
<form id ="updEnggDetails">    
<div class="modal-body">
<div class="row">
<div class="col-md-12">
<div class="form-group">
    <input type="hidden"  name="order_product_id" id="order_product_id" value="" class="form-control"/>
    <textarea name="editamt" id="editamt" value="" class="form-control"></textarea>
    
</div>
<!-- /.form-group -->

</div><!-- /.col -->

</div><!-- /.box -->


<div class="modal-footer">
<div class="validate pull-left" style="font-weight:bold;"></span></div>
<button type="submit" id ="update" class="btn btn-primary">Update</button>
<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>

</div>
</div>
</form>
</div>
</div>
</div>
<div class="modal fade" id="insModel" role="dialog">
<div class="modal-dialog modal-sm">

<!-- Modal content-->
<div class="modal-content">

<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Customer Meeting Time</h4>
</div>
<form id ="insEnggAllotDetails">    
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <input type="hidden"  name="order_id" id="order_id" value="" class="form-control"/>
                    <input type="hidden"  name="order_allot_detailsId" id="order_allot_detailsId" value="" class="form-control"/>
                    <input type="hidden"  name="employee_id" id="employee_id" value="" class="form-control"/>
                    <input type="hidden"  name="order_product_id" id="order_product_id" value="" class="form-control"/>
                </div>
            </div><!-- /.col -->
            <div class="col-md-12" id="cus_time_details" >
                <div class="form-group ">
                    <label>Meeting Time</label>
                    <input type="hidden" name="meeting_time" id="meeting_time" value="" />
                    <input type="text" name="meeting_time_dis" id="meeting_time_dis" value="" disabled class="form-control"/>
                </div>
                <div class="form-group ">
                    <label>Meeting Date</label>
                    <input type="hidden" name="meeting_date" id="meeting_date" value="" />
                    <input type="text" name="meeting_date_dis" id="meeting_date_dis" value="" disabled class="form-control"/>
                </div>
                <div class="form-group ">
                    <label>Meeting Person</label>
                    <input type="text" name="meeting_person" id="meeting_person" value="" class="form-control"/>
                </div>
            </div><!-- /.col -->
        </div><!-- /.box -->
        <div class="modal-footer">
        <div class="validate pull-left" style="font-weight:bold;"></span></div>
        <button type="submit" id ="update" class="btn btn-primary">Update</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>

        </div>
    </div>
</form>
</div>
</div>
</div>
        <!--Insert Category Edit Modal End-->
         <!--Insert Category Delete Modal Start-->
 <div class="modal fade" id="delModel" role="dialog">
        <div class="modal-dialog modal-sm">
                
        <!-- Modal content-->
        <div class="modal-content">
             
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Delete Category </h4>
        </div>
            <form id ="delEnggDetails">    
            <div class="modal-body">
                <div class="row">
                <div class="col-md-12">
                <input type="hidden"  name="order_id" id="order_id" value="" class="form-control"/>
                <p>Sure to want to delete "<span style ="color:red" id ="catname"></span>" ?</p>
                 </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="delete2" class="btn btn-danger">Delete</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
           </form>
       </div>
       </div>
       </div>
          <!--Insert Category Delete Modal End--> 
  </body>
  <script type="text/javascript" language="javascript">
      $(document).ready(function(){
            $("#tab_1").click(function(){
                $("#tab_1_box").show();
                $("#tab_2_box").hide();
                $("#tab_3_box").hide();
                $("#tab_4_box").hide();
                $("#insPending").find("#product_status").val('1');
            }); 
            $("#tab_2").click(function(){
                $("#tab_2_box").show();
                $("#tab_1_box").hide();
                $("#tab_3_box").hide();
                $("#tab_4_box").hide();
                $("#insUnder").find("#product_status").val('2');
                setCustomerContact($("#insUnder").find("#order_id").val(),$("#insUnder"));
            }); 
            $("#tab_3").click(function(){
                $("#tab_3_box").show();
                $("#tab_2_box").hide();
                $("#tab_1_box").hide();
                $("#tab_4_box").hide();
                $("#insResolve").find("#product_status").val('3');
                setCustomerContact($("#insResolve").find("#order_id").val(),$("#insResolve"));
            }); 
            $("#tab_4").click(function(){
                $("#tab_4_box").show();
                $("#tab_2_box").hide();
                $("#tab_3_box").hide();
                $("#tab_1_box").hide();
                $("#insNoAction").find("#product_status").val('4');
            }); 
           $("#call_cus").click(function(){
                 showCus($(this),"cus_contact_no");
            }); 
            $("#call_cus_alt").click(function(){
                 showCus($(this),"cus_alt_contact_no");
            }); 
           $("#insResolve").find("#call_cus").click(function(){
                 showCusR($(this),"cus_contact_no");
            }); 
            $("#insResolve").find("#call_cus_alt").click(function(){
                 showCusR($(this),"cus_alt_contact_no");
            }); 
            $("#insUnder").find("#check_otp_under").click(function(){
                 checkOTP_for_UnderLab($("#insUnder").find("#select_contact_no").val(),$("#insUnder").find("#otp").val(),$("#showModal").find("#insUnder"));
            }); 
            $("#insResolve").find("#check_otp_under").click(function(){
                 checkOTP_for_UnderLab($("#insResolve").find("#select_contact_no").val(),$("#insResolve").find("#otp").val(),$("#showModal").find("#insResolve"));
            }); 
            function showCus(obj,id){
                if(obj.text()=="Customer Call Confirmed"){
                     $("#cus_time_details").hide();
                 }else{
                     $("#cus_time_details").show();
                 }
                 setCusContact(obj.find("#"+id));
            }
            function setCusContact(obj){
                $("#showModal").find("#insUnder").find("#cus_contact_no_display").html(obj.text());
                $("#showModal").find("#insUnder").find("#select_contact_no").val(obj.text());
                getOTP_for_UnderLab(obj.text());
            }
            function setCusContactR(obj){
                $("#showModal").find("#insResolve").find("#cus_contact_no_display").html(obj.text());
                $("#showModal").find("#insResolve").find("#select_contact_no").val(obj.text());
                getOTP_for_UnderLab(obj.text());
            }
            function showCusR(obj,id){
                if(obj.text()=="Customer Call Confirmed"){
                     $("#showModal").find("#insResolve").find("#cus_time_details").hide();
                 }else{
                     $("#showModal").find("#insResolve").find("#cus_time_details").show();
                 }
                 setCusContactR(obj.find("#"+id));
            }
            function setCusContactR(obj){
                $("#showModal").find("#insUnder").find("#cus_contact_no_display").html(obj.text());
                $("#showModal").find("#insUnder").find("#select_contact_no").val(obj.text());
                getOTP_for_UnderLab(obj.text());
            }
         }); 
         var check_otp_for_under_lab=false;
         function checkOTP_for_UnderLab(contact_no,otp ,obj){
             $.ajax({
                    type:"post",
                    url:"../server/controller/order_product_status/CheckOtpForUnder.php",
                    data:{'contact_no':contact_no,'otp_no':otp},
                    success: function(data) {
                        if(data.trim()=="false"){
                            alert("Internal Server Error.")
                        }else{
                            check_otp_for_under_lab=true;
                            obj.find(".box-footer").show();
                        }
                    }
                });
         }
         function getOTP_for_UnderLab(contact_no){
             $.ajax({
                    type:"post",
                    url:"../server/controller/order_product_status/GenrateOtpForUnder.php",
                    data:{'contact_no':contact_no},
                    success: function(data) {
                        if(data.trim()=="false"){
                            alert("Internal Server Error.")
                        }
                    }
                });
         }
         function setCustomerContact(order_id,obj){
             $.ajax({
                    type:"post",
                    url:"../server/controller/order_details/SelOrderDetails.php",
                    data:{'order_id':order_id},
                    success: function(data) {
                        var duce = jQuery.parseJSON(data);
                        $.each(duce, function (index, article) {
                            obj.find("#cus_contact_no").html(article.contact_no);
                            obj.find("#cus_alt_contact_no").html(article.alt_contact_no);
                        });
                    }
                });
         }
         function onlad(){
             orderid='';
                    cate={}
                    $.ajax({
                    type:"post",
                    url:"../server/controller/order_details/SelEnggDetails.php",
                    success: function(data) {
                        var duce = jQuery.parseJSON(data);
                        $("#data tr:has(td)").remove();
                        $.each(duce, function (index, article) {
                            cate[article.order_id]={};
                            cate[article.order_id]= article;
                           var btn_class="btn-danger";
                           var status="Pending";
                           if(article.status=="true"){
                               btn_class="btn-success";
                               status="Approved";
                           }
                           $("#data").append($('<tr/>')
                                .append($('<td/>').html((index+1)))
                                .append($('<td/>').html(article.order_no))
                                .append($('<td/>').html(article.order_date))
                                .append($('<td/>').html(article.user_name))
                                .append($('<td/>').html(article.contact_no))
                                .append($('<td/>').html(article.alt_contact_no))
                                .append($('<td/>').html(article.address_details))
                                .append($('<td/>').html($("<div>").append(article.count).append($("<a>").addClass("btn btn-social-icon pull-right").css({'margin-top': '-7px'}).append($("<i>").addClass("fa fa-list")).click(function(){
                                    if(user_type=="admin" || privileges_details_system[15]["selectm"]=="true"){
                                         showProduct(article.order_id)
                                     }else{
                                         showAccessDeniedModel();
                                     }  
                                }))))
                                .append($('<td/>').append($("<button></button>").addClass("btn "+btn_class+" btn-xs").attr({"id":"status"}).append(status).click(function(){
                                    if(user_type=="admin" || privileges_details_system[15]["updatem"]=="true"){
                                         updatestatus($(this),article.order_id);
                                     }else{
                                         showAccessDeniedModel();
                                     }  
                                })))
//                                .append($('<td/>').html($("<button>").attr({'type':'button'}).addClass("btn btn-success btn-xs").append("&nbsp;&nbsp;Edit&nbsp;&nbsp;").click(function (){
//                                    if(user_type=="admin" || privileges_details_system[15]["updatem"]=="true"){
//                                        updEnggDetails(article.order_id)
//                                     }else{
//                                         showAccessDeniedModel();
//                                     }
//                                    
//                                })))
                                .append($('<td/>').html($("<button>").attr({'type':'button'}).addClass("btn btn-danger btn-xs").append("&nbsp;&nbsp;Delete&nbsp;&nbsp;").click(function (){
                                    if(user_type=="admin" || privileges_details_system[15]["deletem"]=="true"){
                                        delEnggDetails(article.order_id)
                                     }else{
                                         showAccessDeniedModel();
                                     }
                                })))
                                        
                            );
                        });
                    }
                });
         }
         engg_details={}
         function showProduct(employees_id){
           engg_details={}
                    $.ajax({
                    type:"post",
                    url:"../server/controller/engg_details/SelEnggDetails.php",
                    data:{'employee_id':employees_id},
                    success: function(data) {
                        var duce = jQuery.parseJSON(data);
                        $("#data tr:has(td)").remove();
                        $.each(duce, function (index, article) {
                           engg_details[article.order_product_id]={};
                           engg_details[article.order_product_id]= article;
                           var img= "../server/"+article.icon.replace("../../", "");;
                           var btn_class="btn-danger";
                          var status="Accept";
                           if(article.status=="true"){
                               btn_class="btn-success";
                               status="Cancel";
                           }
                           var btn_meet;
                           if(article.order_product_status_id==null || article.order_product_status_id=="null"){
                               btn_meet=$("<button>").attr({'type':'button'}).addClass("btn btn-default btn-xs").append("&nbsp;&nbsp;Meetup").click(function (){
                                    if(user_type=="admin" || privileges_details_system[15]["selectm"]=="true"){
                                         checkProduct(article.order_id,article.order_product_id,article.order_allot_detailsId,$(this))
                                     }else{
                                         showAccessDeniedModel();
                                     }  
                                });
                           }else{
                               btn_meet=$("<button>").attr({'type':'button'}).addClass("btn btn-default btn-xs").append("&nbsp;&nbsp;Calculate Payment").click(function (){
                                    if(user_type=="admin" || privileges_details_system[15]["selectm"]=="true"){
                                         if(article.payment_details_id==null|| article.payment_details_id=='null')
                                         showPaymentSummary(article.order_product_id,article.user_id)
                                     }else{
                                         showAccessDeniedModel();
                                     }
                                });
                               if(!(article.payment_details_id==null|| article.payment_details_id=='null'))
                               btn_meet=$("<button>").attr({'type':'button'}).addClass("btn btn-success btn-xs").append("&nbsp;&nbsp;Paid By User").click(function (){
                                   
                               });
                           }
                           
                           $("#data").append($('<tr/>')
                                .append($('<td/>').html((index+1)))
                                .append($('<td/>').html(article.order_no))
                                .append($('<td/>').html("<img src="+img+" height=\"50\" width=\"50\"/>"))
                                .append($('<td/>').html(article.product_name))
                                .append($('<td/>').html(article.brand_name))
                                .append($('<td/>').html(article.model_name))
                                .append($('<td/>').html(article.problem))
                                .append($('<td/>').html(article.discription))
                                .append($('<td/>').html(article.problem_discription))
                                .append($('<td/>').html(article.address_details))
                                .append($('<td/>').html(article.engg_time))
                                .append($('<td/>').html(article.engg_date))
                                .append($('<td/>').html(article.cus_time))
                                .append($('<td/>').html(article.cus_date))
                                .append($('<td/>').append($("<button></button>").addClass("btn "+btn_class+" btn-xs").attr({"id":"status"}).append(status).click(function(){
                                     if(user_type=="admin" || privileges_details_system[15]["updatem"]=="true"){
                                         if(article.payment_details_id==null|| article.payment_details_id=='null')
                                         updatestatus($(this),article.order_allot_detailsId);
                                     }else{
                                         showAccessDeniedModel();
                                     }  
                                })))
//                                .append($('<td/>').html($("<button>").attr({'type':'button'}).addClass("btn btn-primary btn-xs").append("&nbsp;&nbsp;"+assign+"&nbsp;&nbsp;").click(function (){
//                                    
//                                    if(user_type=="admin" || privileges_details_system[15]["updatem"]=="true"){
//                                        if(assign=="Assigned")
//                                        selEnggAllotDetails(article.order_allot_detailsId);
//                                        else
//                                        insEnggAllotDetails(article.engg_details_id)
//                                     }else{
//                                         showAccessDeniedModel();
//                                     }
//                                })))
                                .append($('<td/>').html($("<button>").attr({'type':'button'}).addClass("btn btn-success btn-xs").append("&nbsp;&nbsp;Edit&nbsp;&nbsp;").click(function (){
                                    if(user_type=="admin" || privileges_details_system[15]["updatem"]=="true"){
                                        if(article.payment_details_id==null|| article.payment_details_id=='null')
                                        updOrderProduct(article.order_product_id)
                                     }else{
                                         showAccessDeniedModel();
                                     }
                                })))
                                .append($('<td/>').html($("<button>").attr({'type':'button'}).addClass("btn btn-primary btn-xs").append("&nbsp;&nbsp;Show Client Details&nbsp;&nbsp;").click(function (){
                                    if(user_type=="admin" || privileges_details_system[15]["selectm"]=="true"){
                                        selOrderDetails(article.order_id)
                                     }else{
                                         showAccessDeniedModel();
                                     }
                                })))
                                .append($('<td/>').html(btn_meet))
                                .append($('<td/>').html($("<button>").attr({'type':'button'}).addClass("btn btn-default btn-xs").append("Show Summary").click(function (){
                                    if(user_type=="admin" || privileges_details_system[15]["selectm"]=="true"){
                                         showSummary(article.order_id,article.order_product_id,article.order_allot_detailsId,$(this),article.payment_details_id);
                                     }else{
                                         showAccessDeniedModel();
                                     }
                                })))
                                        
                            );
                        });
                    }
                });
        }
        var check_engg_details=false;
        var check_engg_details_obj={};
        function checkEnggDetails(order_allot_detailsId){
                $.ajax({
                    type:"post",
                    url:"../server/controller/engg_details/CheckEnggDetails.php",
                    data:{'order_allot_detailsId':order_allot_detailsId},
                    success: function(data) {
                        check_engg_details_obj=jQuery.parseJSON(data);;
                        check_engg_details=true;
                    }
                });
        }
        function enggSubmit_InLab_OrderProduct(order_product_id,engg_details_id,order_id){
                $.ajax({
                    type:"post",
                    url:"../server/controller/engg_submit_in_lab_order_product/InsEnggSubmitInLabOrderProduct.php",
                    data:{'order_product_id':order_product_id,'engg_details_id':engg_details_id,'employee_id':employees_id,'order_id':order_id},
                    success: function(data) {
                        if(data.trim()=='Error'){
                            alert("Internal Server Error.");
                        }else if(data.trim()=='false'){
                            alert("You already Submit product in Lab.");
                        }else if(data.trim()=='true'){
                            alert("Product submit successfully in Lab.");
                        }else {
                            alert(data)
                        }
                        location.reload(true);
                    }
                });
        }
        function checkProduct(order_id,order_product_id,order_allot_detailsId,obj){
                    checkEnggDetails(order_allot_detailsId);
                    setTimeout(function (){
                        if(checkEnggDetails){
                            var aaa=null;
                            var engg_details_id=null;
                              $.each(check_engg_details_obj, function (index, article) {
                                  aaa=article.check;
                                  engg_details_id=article.engg_details_id;
                              });
                            if(aaa!=null && aaa=='true')
                                showCheckCondtion(order_id,order_product_id,order_allot_detailsId,engg_details_id,obj);
                            else
                                showClientMeet(order_id,order_product_id,order_allot_detailsId,engg_details_id,obj);
                            return;
                        }
                    },500);
        }
        function showClientMeet(order_id,order_product_id,order_allot_detailsId,engg_details_id,obj){
                    $("#insModel").modal('show');
                    $("#insModel").on('shown.bs.modal', function(){
                    $("#insModel").find("#order_id").val(order_id);  
                    $("#insModel").find("#order_allot_detailsId").val(order_allot_detailsId);  
                    $("#insModel").find("#order_product_id").val(order_product_id);  
                    $("#insModel").find("#order_product_id").val(order_product_id);  
                    $("#insModel").find("#employee_id").val(employees_id);  
                        $.each(check_engg_details_obj, function (index, article) {
                            $("#insModel").find("#meeting_time_dis").val(article.meeting_time);  
                            $("#insModel").find("#meeting_time").val(article.meeting_time);  
                            $("#insModel").find("#meeting_date").val(article.meeting_date);  
                            $("#insModel").find("#meeting_date_dis").val(article.meeting_date);  
                        });
                    $('#insEnggAllotDetails').off("submit");
                            $('#insEnggAllotDetails').submit(function() {
                            if(this.meeting_person.value == ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please Select valid Meeting Person").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                return false;  
                            }else {
                                $(".validate").removeClass("text-danger").addClass("text-success").fadeIn(100).text("Ok").prepend("<span class=\"glyphicon glyphicon-ok text-success\">").fadeOut(1000);
                                $.ajax({
                                    type:"post",
                                    url:"../server/controller/engg_details/InsEnggDetails.php",
                                    data:$('#insEnggAllotDetails').serialize(),
                                    success: function(data){ 
                                        if(data.trim()!='Error'){
                                            location.reload(true);
                                        }
                                    } 
                                });
                                return false; 
                            }
                   return false;
                    });  
    var modal = this;
            var hash = modal.id;
            window.location.hash = hash;
            window.onhashchange = function() {
                    if (!location.hash){
                            $(modal).modal('hide');
                    }
            }
      });  
    $('#myModal').on('hide.bs.modal', function() {
            location.reload(true);
    });
        
                  }

        function showCheckCondtion(order_id,order_product_id,order_allot_detailsId,engg_details_id,obj){
                    $("#showModal").modal('show');
                    
                    $("#showModal").on('shown.bs.modal', function(){
                          $('#insPending').off("submit");
                            $('#insPending').find("#order_product_id").val(order_product_id);
                            $('#insPending').find("#engg_details_id").val(engg_details_id);
                            $('#insUnder').find("#order_id").val(order_id);
                            $('#insUnder').find("#order_product_id").val(order_product_id);
                            $('#insUnder').find("#engg_details_id").val(engg_details_id);
                            $('#insResolve').find("#order_id").val(order_id);
                            $('#insResolve').find("#order_product_id").val(order_product_id);
                            $('#insResolve').find("#engg_details_id").val(engg_details_id);
                            $('#insNoAction').find("#order_product_id").val(order_product_id);
                            $('#insNoAction').find("#engg_details_id").val(engg_details_id);
                         $('#insPending').submit(function() {
                            if(this.product_reason.value == ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please fill valid Product Reason").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                return false;  
                           }else if(this.product_repair_hrs.value == ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please fill valid Repaired time in hrs").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                return false;  
                           }else {
                                $(".validate").removeClass("text-danger").addClass("text-success").fadeIn(100).text("Ok").prepend("<span class=\"glyphicon glyphicon-ok text-success\">").fadeOut(1000);
                                $.ajax({
                                    type:"post",
                                    url:"../server/controller/order_product_status/InsOrderProductStatus.php",
                                    data:$('#insPending').serialize(),
                                    success: function(data){ 
                                        alert(data)
                                        location.reload(true);
                                    } 
                                });
                                return false; 
                            }
                            return false;
                        });  
                         $('#insUnder').submit(function() {
                         if(check_otp_for_under_lab){
                               if(this.product_reason.value == ""){
                                    $(".validate").addClass("text-danger").fadeIn(100).text(" Please fill valid Product Reason").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                    return false;  
                               }else {
                                    $(".validate").removeClass("text-danger").addClass("text-success").fadeIn(100).text("Ok").prepend("<span class=\"glyphicon glyphicon-ok text-success\">").fadeOut(1000);
                                    $.ajax({
                                        type:"post",
                                        url:"../server/controller/order_product_status/InsOrderProductStatus.php",
                                        data:$('#insUnder').serialize(),
                                        success: function(data){ 
                                            alert(data)
                                            location.reload(true);
                                        } 
                                    });
                                    return false; 
                                }  
                         }else{
                             alert("Please Provide valid Otp.")
                         }
                           
                            return false;
                        });  
                         $('#insResolve').submit(function() {
                         if(check_otp_for_under_lab){
                               if(this.product_reason.value == ""){
                                    $(".validate").addClass("text-danger").fadeIn(100).text(" Please fill valid Product Reason").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                    return false;  
                               }else {
                                    $(".validate").removeClass("text-danger").addClass("text-success").fadeIn(100).text("Ok").prepend("<span class=\"glyphicon glyphicon-ok text-success\">").fadeOut(1000);
                                    $.ajax({
                                        type:"post",
                                        url:"../server/controller/order_product_status/InsOrderProductStatus.php",
                                        data:$('#insResolve').serialize(),
                                        success: function(data){ 
                                            alert(data)
                                            location.reload(true);
                                        } 
                                    });
                                    return false; 
                                }  
                         }else{
                             alert("Please Provide valid Otp.")
                         }
                           
                            return false;
                        });  
                       
    $('#myModal').on('hide.bs.modal', function() {
            location.reload(true);
    });
                   
            var modal = this;
            var hash = modal.id;
            window.location.hash = hash;
            window.onhashchange = function() {
                    if (!location.hash){
                            $(modal).modal('hide');
                    }
            }
      });  
    $('#myModal').on('hide.bs.modal', function() {
            location.reload(true);
    });
        
                  }
        order_product_status={}
        function showSummary(order_id,order_product_id,order_allot_detailsId,obj,payment_details_id){
           order_product_status={}
           $("#showSummary").modal('show');
           $("#showSummary").on('shown.bs.modal', function(){
                    $.ajax({
                    type:"post",
                    url:"../server/controller/order_product_status/SelOrderProductStatus.php",
                    data:{'order_product_id':order_product_id},
                    success: function(data) {
                        var duce = jQuery.parseJSON(data);
                        $("#order_product_status tr:has(td)").remove();
                        $.each(duce, function (index, article) {
                           order_product_status[article.order_product_status_id]={};
                           order_product_status[article.order_product_status_id]= article;
                           var btn_class;
                           var status="";
                           switch(article.product_status){
                               case '1':
                                   status="Pending";
                                   break;
                               case '2':    
                                   status="Under Lab";
                                   if((article.in_lab==null || article.in_lab=="null") && (article.return_item==null || article.return_item=="null")){
                                       btn_class=$("<button>").attr({'type':'button'}).addClass("btn btn-primary btn-xs").append("&nbsp;&nbsp;Submit In Lab&nbsp;&nbsp;").click(function (){
                                         enggSubmit_InLab_OrderProduct(order_product_id,article.engg_details_id,order_id);
                                       });
                                   }else if(article.return_item==null || article.return_item=="null"){
                                       btn_class=$("<button>").attr({'type':'button'}).addClass("btn btn-primary btn-xs").append("&nbsp;&nbsp;Submited&nbsp;&nbsp;").click(function (){
//                                         enggSubmit_InLab_OrderProduct(order_product_id,article.engg_details_id,order_id);
                                       });
                                   }else{
                                       btn_class=$("<button>").attr({'type':'button'}).addClass("btn btn-primary btn-xs").append("&nbsp;&nbsp;Returned&nbsp;&nbsp;").click(function (){
//                                         enggSubmit_InLab_OrderProduct(order_product_id,article.engg_details_id,order_id);
                                       });
                                   }
                                   
                                   
                                   
                                   
                                   break;
                               case '3' :   
                                   status="Resolve";
                                   break;
                               case '4':  
                                   status="No Action";
                                   break;
                               case '5':    
                                   status="Submit In Lab";
                                   break;
                               case '6' :   
                                   status="Return from Lab";
                                   break;
                               case '7' :   
                                   status="Return to Customer";
                                   break;
                           }
                           if(article.status=="true"){
                               btn_class="btn-success";
                               status="Cancel";
                           }
                           $("#order_product_status").append($('<tr/>')
                                .append($('<td/>').html((index+1)))
                                .append($('<td/>').html(article.status_date))
                                .append($('<td/>').html(status))
                                .append($('<td/>').html(article.product_reason))
                                .append($('<td/>').html(article.product_repair_hrs))
                                .append($('<td/>').html(btn_class))
                                .append($('<td/>').html($("<button>").attr({'type':'button'}).addClass("btn btn-danger btn-xs").append("&nbsp;&nbsp;Delete&nbsp;&nbsp;").click(function (){
                                    if(user_type=="admin" || privileges_details_system[15]["deletem"]=="true"){
                                        if(payment_details_id==null|| payment_details_id=='null'){
                                            if(article.product_status=='3'&& user_type=="admin")
                                                delOrderProductStatus(article.order_product_status_id)
                                            if(article.product_status=='1')
                                                delOrderProductStatus(article.order_product_status_id)
                                            if(article.product_status=='2'&& (article.return_item==null || article.return_item=="null"))
                                                delOrderProductStatus(article.order_product_status_id)
                                        }
                                     }else{
                                         showAccessDeniedModel();
                                     }
                                })))
                                        
                            );
                        });
                    }
                });
            var modal = this;
            var hash = modal.id;
            window.location.hash = hash;
            window.onhashchange = function() {
                    if (!location.hash){
                            $(modal).modal('hide');
                    }
            }
      });  
    $('#showSummary').on('hide.bs.modal', function() {
            location.reload(true);
    });
        
                  }
                  order_details={}
        function selOrderDetails(order_id){
                  order_details={}
           $("#showEnggAllotDetailsModal").modal('show');
           $("#showEnggAllotDetailsModal").on('shown.bs.modal', function(){
                    $.ajax({
                    type:"post",
                    url:"../server/controller/order_details/SelOrderDetails.php",
                    data:{'order_id':order_id},
                    success: function(data) {
                        var duce = jQuery.parseJSON(data);
                        $("#order_allot_details tr:has(td)").remove();
                        $.each(duce, function (index, article) {
                           order_allot_details[order_id]={};
                           order_allot_details[order_id]= article;
                           var img= "../server/"+article.pic.replace("../../", "");;
                           var btn_class="btn-danger";
                           var status="Accept";
                           if(article.status=="true"){
                               btn_class="btn-success";
                               status="Cancel";
                           }
                           $("#order_allot_details").append($('<tr/>')
                                .append($('<td/>').html((index+1)))
                                .append($('<td/>').html("<img src="+img+" height=\"50\" width=\"50\"/>"))
                                .append($('<td/>').html(article.user_name))
                                .append($('<td/>').html(article.contact_no))
                                .append($('<td/>').html(article.alt_contact_no))
                                .append($('<td/>').html($("<button>").attr({'type':'button'}).addClass("btn btn-danger btn-xs").append("&nbsp;&nbsp;Delete&nbsp;&nbsp;").click(function (){
//                                    if(user_type=="admin" || privileges_details_system[15]["deletem"]=="true"){
//                                        delEnggAllotDetails()
//                                     }else{
//                                         showAccessDeniedModel();
//                                     }
                                })))
                                        
                            );
                        });
                    }
                });
            var modal = this;
            var hash = modal.id;
            window.location.hash = hash;
            window.onhashchange = function() {
                    if (!location.hash){
                            $(modal).modal('hide');
                    }
            }
      });  
    $('#myModal').on('hide.bs.modal', function() {
            location.reload(true);
    });
        
                  }
                  
        function showPaymentSummary(order_product_id,user_id){
           $("#show_payment_summary_modal").modal('show');
           $("#show_payment_summary_modal").on('shown.bs.modal', function(){
           var payable_amt=0;
               
                    $.ajax({
                    type:"post",
                    url:"../server/controller/payment_details/SelPaymentDetails.php",
                    data:{'order_product_id':order_product_id},
                    success: function(data) {
                        var duce = jQuery.parseJSON(data);
                        $("#show_payment_summary tr:has(td)").remove();
                        var calculation=0;
                        $.each(duce, function (index, article) {
                           if(article.type=="M"){
                               calculation=calculation-parseFloat(article.amount);
                           }else{
                               calculation=calculation+parseFloat(article.amount);
                           } 
                           
                           $("#show_payment_summary").append($('<tr/>').css({'background':'#eee'})
                                .append($('<td/>').html((index+1)))
                                .append($('<td/>').html(article.discription))
                                .append($('<td/>').html(article.amount))
                            );
                        });
                        $("#show_payment_summary").append($('<tr/>').css({'background':'#ffeddf'})
                            .append($('<td/>').html(''))
                            .append($('<td/>').html('Sub Total '))
                            .append($('<td/>').html(calculation))
                        );
                        $("#show_payment_summary").append($('<tr/>').css({'background':'#fdfae7'})
                            .append($('<td/>').html(''))
                            .append($('<td/>').html('Gst '))
                            .append($('<td/>').html('18 %'))
                        );
                        payable_amt=calculation+calculation*18/100;
                        $("#show_payment_summary").append($('<tr/>').css({'background':'#d9ecfa'})
                            .append($('<td/>').html(''))
                            .append($('<td/>').html('Total '))
                            .append($('<td/>').html(payable_amt))
                        );
                
                    }
                });
            var modal = this;
            var hash = modal.id;
            window.location.hash = hash;
            window.onhashchange = function() {
                    if (!location.hash){
                            $(modal).modal('hide');
                    }
            }
            
            $("#show_payment_summary_modal").find('#payment_user').off();
            $("#show_payment_summary_modal").find('#payment_user').click(function (){
               var paid_amt=$("#show_payment_summary_modal").find("#paid_amt").val();
                 $.ajax({
                    type:"post",
                    url:"../server/controller/payment_details/InsPaymentDetails.php",
                    data:{'order_product_id':order_product_id,'user_id':user_id,'payable_amt':payable_amt,'paid_amt':paid_amt},
                    success: function(data) {
                        alert(data)
                        location.reload(true);
                    }
                  });
                  return false;
            });
      });  
    $('#show_payment_summary_modal').on('hide.bs.modal', function() {
            location.reload(true);
    });
        
                  }
         
            function updatestatus(obj,id){
                      var value;
                      if(obj.text().trim()=="Cancel"){
                                value="false";
                                obj.empty();
                                obj.append("Accepted");
                                obj.removeClass("btn-success");
                                obj.addClass("btn-danger");
                      }else{
                                value="true";
                                obj.empty();
                                obj.append("Accept");
                                obj.removeClass("btn-danger");
                                obj.addClass("btn-success");
                      }           
                  $.ajax({
                    type:"post",
                    url:"../server/controller/order_allot_details/UpdOrderAllotDetails.php",
                    data:{'order_allot_detailsId':id,'status':value},
                    success: function(data) {
                    }
                  });
              }
              
                    function updOrderProduct(order_product_id) {
                      $("#updModel").modal('show');
                      $("#updModel").on('shown.bs.modal', function(){
                            $("#updEnggDetails").find("#order_product_id").val(order_product_id);  
                            $("#updEnggDetails").find("#editamt").val(engg_details[order_product_id]["problem_discription"]);          
                            $('#updEnggDetails').off("submit");
                            $('#updEnggDetails').submit(function() {
                                $(".validate").removeClass("text-danger").addClass("text-success").fadeIn(100).text("Ok").prepend("<span class=\"glyphicon glyphicon-ok text-success\">").fadeOut(1000);
                                $.ajax({
                                    type:"post",
                                    url:"../server/controller/order_product/UpdOrderProduct.php",
                                    data:$('#updEnggDetails').serialize(),
                                    success: function(data){ 
                                         location.reload(true);
                                    } 
                                });
                            
                        return false;
                    });
                     var modal = this;
                            var hash = modal.id;
                            window.location.hash = hash;
                            window.onhashchange = function() {
                                    if (!location.hash){
                                            $(modal).modal('hide');
                                    }
                            }
                      });  
                    $('#editEnggDetails').on('hide.bs.modal', function() {
                            location.reload(true);
                    });
                }

</script>

<script type="text/javascript" language="javascript">
        function delEnggDetails(order_id) {
            
            $("#delModel").modal('show');
                 var order_name = cate[order_id]["order_no"];;
                 $("#delModel").on('shown.bs.modal', function(){
                    $("#delEnggDetails").find("#order_id").val(order_id);        
                    $("#delEnggDetails").find("#catname").text(order_name); 
                    $('#delEnggDetails').off("submit");
                    $('#delEnggDetails').submit(function() {
                         $.ajax({
                            type:"post",
                            url:"../server/controller/order_details/delEnggDetails.php",
                            data:{'order_id':order_id},
                            success: function(data){ 
                                location.reload(true);
                            } 
                  });
                    return false;
    });
    
    });
   
}

        function delEnggProduct(engg_details_id) {
            
            $("#delModel").modal('show');
                 var order_name = engg_details[engg_details_id]["product_name"]+" "+engg_details[engg_details_id]["brand_name"]+" "+engg_details[engg_details_id]["model_name"]+" "+engg_details[engg_details_id]["problem"]+" "+engg_details[engg_details_id]["discription"]+" "+engg_details[engg_details_id]["problem_discription"];
                 $("#delModel").on('shown.bs.modal', function(){
                    $("#delEnggDetails").find("#catname").text(order_name); 
                    $('#delEnggDetails').off("submit");
                    $('#delEnggDetails').submit(function() {
                         $.ajax({
                            type:"post",
                            url:"../server/controller/engg_details/DelEnggProduct.php",
                            data:{'engg_details_id':engg_details_id},
                            success: function(data){ 
                                alert(data)
                                location.reload(true);
                            } 
                  });
                    return false;
    });
    
    });
   
}

        function delOrderProductStatus(order_product_status_id) {
            
            $("#delModel").modal('show');
                 var order_name = order_product_status[order_product_status_id]["product_reason"]+" ";
                 $("#delModel").on('shown.bs.modal', function(){
                    $("#delEnggDetails").find("#catname").text(order_name); 
                    $('#delEnggDetails').off("submit");
                    $('#delEnggDetails').submit(function() {
                         $.ajax({
                            type:"post",
                            url:"../server/controller/order_product_status/DelOrderProductStatus.php",
                            data:{'order_product_status_id':order_product_status_id},
                            success: function(data){ 
                                location.reload(true);
                            } 
                  });
                    return false;
    });
    
    });
   
} 
        function delEnggAllotDetails(order_allot_detailsId) {
            
            $("#delModel").modal('show');
                 var order_name = order_allot_details[order_allot_detailsId]["user_name"]+" from assigned order ";
                 $("#delModel").on('shown.bs.modal', function(){
                    $("#delEnggDetails").find("#catname").text(order_name); 
                    $('#delEnggDetails').off("submit");
                    $('#delEnggDetails').submit(function() {
                         $.ajax({
                            type:"post",
                            url:"../server/controller/order_allot_details/DelEnggAllotDetails.php",
                            data:{'order_allot_detailsId':order_allot_detailsId},
                            success: function(data){ 
                                location.reload(true);
                            } 
                  });
                    return false;
    });
    
    });
   
}

function insEnggAllotDetails(engg_details_id){
           $("#insModel").modal('show');
           $("#insModel").on('shown.bs.modal', function(){
           $("#insModel").find("#engg_details_id").val(engg_details_id); 
           $("#insModel").find("#order_id").val(orderid); 
           $("#insModel").find("#insEnggAllotDetails").find("#call_cus").find("#cus_contact_no").html(cate[orderid].contact_no);
           $("#insModel").find("#insEnggAllotDetails").find("#call_cus_alt").find("#cus_alt_contact_no").html(cate[orderid].alt_contact_no);
           loadRolls();
           var modal = this;
           var hash = modal.id;
           window.location.hash = hash;
           window.onhashchange = function() {
                if (!location.hash){
                        $(modal).modal('hide');
                }
           }
      });  
     $('#insEnggAllotDetails').off("submit");
                            $('#insEnggAllotDetails').submit(function() {
                            if(this.employee_id.value == ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please Select valid Employee ").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                return false;  
                           }else if(this.engg_time.value == ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please Select valid Employee time").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                return false;  
                           }else if(this.engg_date.value == ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please Select valid Employee Date").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                return false;  
                           }else if(this.cus_time.value == ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please Select valid Customer time").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                return false;  
                           }else if(this.cus_date.value == ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please Select valid Customer Date").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                return false;  
                           }else {
                                $(".validate").removeClass("text-danger").addClass("text-success").fadeIn(100).text("Ok").prepend("<span class=\"glyphicon glyphicon-ok text-success\">").fadeOut(1000);
                                $.ajax({
                                    type:"post",
                                    url:"../server/controller/order_allot_details/InsEnggAllotDetails.php",
                                    data:$('#insEnggAllotDetails').serialize(),
                                    success: function(data){ 
                                        alert(data)
                                        location.reload(true);
                                    } 
                                });
                                return false; 
                            }
                   return false;
                    });  
    $('#myModal').on('hide.bs.modal', function() {
            location.reload(true);
    });
}

function loadRolls(){
             $.ajax({
                    type:"post",
                    url:"../server/controller/roll_type/SelRollType.php",
                    success: function(data) {
                        var duce = jQuery.parseJSON(data);
                        $("#insModel").find("#insEnggAllotDetails").find("#category").find("select").empty();
                        $.each(duce, function (index, article) {
                           $("#insModel").find("#insEnggAllotDetails").find("#category").find("select").append($('<option>').attr({'value':article.roll_type_id}).append(article.roll_type));
                        });
                        $("#insModel").find("#insEnggAllotDetails").find("#category").find("select").trigger("change");
                    }
                });
             $("#category").find("select").change(function () {
                roll_type_id=$(this).val();
                onloadUser(roll_type_id);
             }); 
         }
</script>
<script>
    employee_details={};
    function onloadUser(roll_type_id){
             employee_details={};
             $.ajax({
                type:"post",
                url:"../server/controller/rolls_details/SelRollsDetails.php",
                data:{'roll_type_id':roll_type_id},
                success: function(data) { 
                var duce = jQuery.parseJSON(data); //here data is passed to js object
                employee_details={}
                var inde=0;
                $("#user_details_first").empty();
                $("#user_details").empty();
                $.each(duce, function (index, article) {
                        employee_details[article.employee_id]=article;    
                        if(inde==0){
                            $("#user_details_first").append('<img src="../server/controller/user_details/'+article.pic+'" width="50" height="50" >'+article.user_name+'<span class="glyphicon glyphicon-chevron-down"></span>');
                            employee_id=article.employee_id;
                            inde++;
                        }
                        $("#user_details").append($('<li><a href="#"><img src="../server/controller/user_details/'+article.pic+'" width="50" height="50">'+article.user_name+'</a></li>').click(function (event){
                            employee_id=article.employee_id;
                            $("#insModel").find("#insEnggAllotDetails").find("#employee_id").val(employee_id);
                            $("#insModel").find("#insEnggAllotDetails").find("#call_eng").find("#emp_contact_no").html(article.contact_no);
                            $("#insModel").find("#insEnggAllotDetails").find("#call_eng").attr('disabled',false);
                            $("#user_details_first").html($(this).find("a").html()).append('<span class="glyphicon glyphicon-chevron-down"></span>');
                             event.preventDefault(); // ca
                        }));
                });
                }
            });
         }
</script>
</html>

