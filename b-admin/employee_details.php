<?php include './includes/check_session.php';?>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Employee Details</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <?php include 'includes/links.php';?>
   
        <script type="text/javascript">
         var type=0
         cate={}
         user_details={}
         employee_details={}
         var user_id=0;
         $(document).ready(function() {
             loadEmployeePrivileges(employees_id,callback);
         });  
         function callback(){
             if(user_type=="admin" || privileges_details_system[1]["selectm"]=="true"){
                     onlad();
             }else{
                 showAccessDeniedModel();
             }
         }
         
         function onloadUser(){
             $.ajax({
                type:"post",
                url:"../server/controller/employee_details/SelUserForEmployee.php",
                success: function(data) { 
                var duce = jQuery.parseJSON(data); //here data is passed to js object
                user_details={}
                var inde=0;
                $.each(duce, function (index, article) {
                    if(Object.keys(employee_details).length==0 || checkEmp(article.user_id)){
                        user_details[article.user_id]=article;    
                        if(inde==0){
                            $("#user_details_first").append('<img src="../server/controller/user_details/'+article.pic+'" width="50" height="50" >'+article.user_name+'<span class="glyphicon glyphicon-chevron-down"></span>');
                            user_id=article.user_id;
                            inde++;
                        }
                        $("#user_details").append($('<li><a href="#"><img src="../server/controller/user_details/'+article.pic+'" width="50" height="50">'+article.user_name+'</a></li>').click(function (){
                            user_id=article.user_id;
                            $("#user_details_first").html($(this).find("a").html()).append('<span class="glyphicon glyphicon-chevron-down"></span>');
                        }));
                    }
                });
                }
            });
         }
         
         function checkEmp(user_id){
            var b=true;
            $.each(employee_details, function (key, value) {
                if(value.user_id==user_id){
                    b= false;
                    return ;
                }
            });
            return b;
         }
         
        </script> 
  </head>
  <!-- ADD THE CLASS sidedar-collapse TO HIDE THE SIDEBAR PRIOR TO LOADING THE SITE -->
 <body class="wysihtml5-supported skin-yellow sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">

   <?php include 'includes/header.php';?>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
   <?php include 'includes/sidepanel.php';?>

      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Employee Details
            <small>Admin panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i> Home</li>
            <li class="active">Employee Details</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
           
          <div class="row">
            <div class="col-xs-12">
                <div class="box">
                <div class="box-header">
                  <div class="pull-left col-md-6">
                        <div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="user_details_first">
      
    </button>
        <ul class="dropdown-menu" id="user_details"></ul>
  </div>
                  </div>
                  <div class="pull-right">
                      <br>
                      <button type="button" class="btn btn-success" id="add">Add +</button>
                  </div>
                </div><!-- /.box-header -->
                
                <div class="box-body">
                  <div class="table-responsive"> 
                   <table id="data" class="table table-bordered table-hover ">
                    <thead>
                      <tr>
                        <th>S.N.</th>
                        <th>User Pic</th>
                        <th>User Name</th>
                        <th>Email</th>
                        <th>Contact No</th>
                        <th>Registration Date</th>
                        <th colspan="2">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                   
                  </table>
                  </div>         
                
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section>
      </div><!-- /.content-wrapper -->

     <?php include 'includes/footer.php';?>

     
    </div><!-- ./wrapper -->
    <?php include 'includes/jslinks.php';?>
 <div class="modal fade" id="delModel" role="dialog">
        <div class="modal-dialog modal-sm">
                
        <!-- Modal content-->
        <div class="modal-content">
             
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Delete Category </h4>
        </div>
        <form id ="delEmployeeDetails">    
        <div class="modal-body">
            <div class="row">
            <div class="col-md-12">
            <input type="hidden"  name="employee_id" id="employee_id" value="" class="form-control"/>
            <p>Sure to want to delete "<span style ="color:red" id ="catname"></span>" Employee?</p>
             </div>
            </div>
            <div class="modal-footer">
            <button type="submit" id="delete2" class="btn btn-danger">Delete</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>

            </div>
        </div>
       </form>
       </div>
       </div>
       </div>
          <!--Insert Category Delete Modal End--> 
  </body>
  <script type="text/javascript" language="javascript">
      $(document).ready(function(){
            $("#add").click(function(){
                if(user_type=="admin" || privileges_details_system[1]["selectm"]=="true"){
                     $.ajax({
                    type:"post",
                    url:"../server/controller/employee_details/InsEmployeeDetails.php",
                    data:{'user_id':user_id},   
                    success: function(data){ 
                        location.reload(true);
                    } 
               });
             }else{
                 showAccessDeniedModel();
             }
            }); 
        });
         function onlad(){
                    employee_details={}
                    $.ajax({
                    type:"post",
                    url:"../server/controller/employee_details/SelEmployeeDetails.php",
                    success: function(data) {
                        var duce = jQuery.parseJSON(data);
                        $("#data tr:has(td)").remove();
                        $.each(duce, function (index, article) {
                            employee_details[article.employee_id]={};
                            employee_details[article.employee_id]= article;
                            var img=$("<img>").attr({'src':'../server/controller/user_details/'+article.pic,'width':'50','height':'50'});
                           $("#data").append($('<tr/>')
                                .append($('<td/>').html((index+1)))
                                .append($('<td/>').html(img))
                                .append($('<td/>').html(article.user_name))
                                .append($('<td/>').html(article.email))
                                .append($('<td/>').html(article.contact_no))
                                .append($('<td/>').html(article.edate))
                                .append($('<td/>').html($("<button>").attr({'type':'button'}).addClass("btn btn-danger btn-xs").append("&nbsp;&nbsp;Delete&nbsp;&nbsp;").click(function (){
                                    delEmployeeDetails(article.employee_id)
                                })))
                            );
                        });
                        onloadUser();
                    }
                });
         }
</script>

<script type="text/javascript" language="javascript">
        function delEmployeeDetails(employee_id) {
            if(user_type=="admin" || privileges_details_system[1]["deletem"]=="true"){
                     $("#delModel").modal('show');
                 var amount = employee_details[employee_id]["user_name"];;
                 $("#delModel").on('shown.bs.modal', function(){
                    $("#delEmployeeDetails").find("#employee_id").val(employee_id);        
                    $("#delEmployeeDetails").find("#catname").text(amount); 
                    $('#delEmployeeDetails').off("submit");
                    $('#delEmployeeDetails').submit(function() {
                         $.ajax({
                            type:"post",
                            url:"../server/controller/employee_details/delEmployeeDetails.php",
                            data:{'employee_id':employee_id},
                            success: function(data){ 
                                location.reload(true);
                            } 
                  });
                    return false;
                 });
    
                });
   
             }else{
                 showAccessDeniedModel();
             }
            
}

</script>
</html>

