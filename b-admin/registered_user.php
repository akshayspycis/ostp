<?php include './includes/check_session.php';?>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Registered User</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <?php include 'includes/links.php';?>
   
        <script type="text/javascript">
         var type=0
         cate={}
         var employee_id=0;
         var employee_details={};
         $(document).ready(function() {
             loadEmployeePrivileges(employees_id,callback);
         });  
         function callback(){
             if(user_type=="admin" || privileges_details_system[17]["selectm"]=="true"){
                     onlad();
             }else{
                 showAccessDeniedModel();
             }
         }
         
         function onloadUser(){
             $.ajax({
                type:"post",
                url:"../server/controller/employee_details/SelEmployeeDetails.php",
                success: function(data) { 
                var duce = jQuery.parseJSON(data); //here data is passed to js object
                user_details={}
                    $.each(duce, function (index, article) {
                        employee_details[article.employee_id]=article;    
                        if(article.employee_id==2){
                            $("#user_details_first").append('<img src="../server/controller/user_details/'+article.pic+'" width="50" height="50" >'+article.user_name+'<span class="glyphicon glyphicon-chevron-down"></span>');
                            employee_id=article.employee_id;
                            onlad((article.employee_id));
                        }
                        $("#user_details").append($('<li><a href="#"><img src="../server/controller/user_details/'+article.pic+'" width="50" height="50">'+article.user_name+'</a></li>').click(function (){
                            $("#user_details_first").html($(this).find("a").html()).append('<span class="glyphicon glyphicon-chevron-down"></span>');
                            employee_id=article.employee_id;
                            onlad((article.employee_id));
                        }));
                    });
                }
            });
         }
        </script> 
  </head>
  <!-- ADD THE CLASS sidedar-collapse TO HIDE THE SIDEBAR PRIOR TO LOADING THE SITE -->
 <body class="wysihtml5-supported skin-yellow sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">

   <?php include 'includes/header.php';?>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
   <?php include 'includes/sidepanel.php';?>

      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Registered User
            <small>Admin panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i> Home</li>
            <li class="active">Registered User</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
           
          <div class="row">
            <div class="col-xs-12">
                <div class="box">
                
                
                <div class="box-body">
                  <div class="table-responsive"> 
                   <table id="data" class="table table-bordered table-hover ">
                    <thead>
                      <tr>
                        <th>S.N.</th>
                        <th>User Pic</th>
                        <th>User Name</th>
                        <th>Email</th>
                        <th>Contact No</th>
                        <th>Registration Date</th>
                        <th colspan="3">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                   
                  </table>
                  </div>         
                
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section>
      </div><!-- /.content-wrapper -->

     <?php include 'includes/footer.php';?>

     
    </div><!-- ./wrapper -->
    <?php include 'includes/jslinks.php';?>
    <!--Insert Category Insert Modal Start-->
 <div class="modal fade" id="insModal" role="dialog">
        <div class="modal-dialog modal-sm">
        <!-- Modal content-->
        <div class="modal-content">
         <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Insert User Data </h4>
        </div>
        <form id ="insUserData">    
                            <div class="modal-body">
                            <div class="row">
                            <div class="col-xs-12">
                            <div class="box">

                            <div class="box-body">
                            <div class="row">
                            <div class="col-md-12">
                            <div class="form-group">
                            <label for="name">Name</label>
                            <input type="hidden" class="form-control" name="employee_id" id ="employee_id" value="">
                            <input type="text" class="form-control" name="user_name" id ="user_name" value="" placeholder="Enter Name">
                            </div>
                            <!-- /.form-group -->
                            <div class="form-group">
                            <label for="email">Email</label>

                            <input type="text" class="form-control"  name="email" id ="email" value="" placeholder="Email">
                            </div>
                            <div class="form-group">
                            <label for="Contact Number">Contact Number</label>
                            <input type="text" id="contact_no" name="contact_no" value="" class="form-control" placeholder="Contact" maxlength="10">
                            </div>
                            </div><!-- /.col -->
                            </div><!-- /.row -->
                            </div><!-- /.box-body -->


                            </div><!-- /.box -->
                            </div><!-- /.col -->


                            </div><!-- /.box -->
                            <div class="modal-footer">
                            <div class="validate pull-left" style="font-weight:bold;"></span></div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>

                            </div>
                            </div>
       </form>
        </div>
        </div>
       </div>
    
 <div class="modal fade" id="insModalOffer" role="dialog">
        <div class="modal-dialog modal-sm">
        <!-- Modal content-->
        <div class="modal-content">
         <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Create <b id="user_offer_title"></b> for <b id="user_name_offer"></b></h4>
        </div>
        <form id ="insOffer">    
                            <div class="modal-body">
                            <div class="row">
                            <div class="col-xs-12">
                            <div class="box">

                            <div class="box-body">
                            <div class="row">
                            <div class="col-md-12">
                            <div class="form-group">
                            <label for="name">Amount</label>
                            <input type="hidden" class="form-control" name="user_id" id ="user_id" value="">
                            <input type="hidden" class="form-control" name="from_date" id ="from_date" value="">
                            <input type="hidden" class="form-control" name="to_date" id ="to_date" value="">
                            <input type="hidden" class="form-control" name="transation_type" id ="transation_type" value="">
                            <input type="number" class="form-control" name="amount" id ="amount" value="" placeholder="Enter Amount">
                            </div>
                            <!-- /.form-group -->
                            <div class="form-group">
                                <label for="email">From Date</label>
                                <input type="date" class="form-control"  name="from_date_temp" id ="from_date_temp" value="" placeholder="From Date">
                            </div>
                            <div class="form-group">
                                <label for="Contact Number">To Date</label>
                                <input type="date" id="to_date_temp" name="to_date_temp" value="" class="form-control" placeholder="To Date" >
                            </div>
                            <script type="text/javascript">
  $(document).ready(function(){
      
  var d = new Date();      
        
   function twoDigitDate(d){
      return ((d.getDate()).toString().length == 1) ? "0"+(d.getDate()).toString() : (d.getDate()).toString();
    };
        
    function twoDigitMonth(d){
     	return ((d.getMonth()+1).toString().length == 1) ? "0"+(d.getMonth()+1).toString() : (d.getMonth()+1).toString();
    };    
      
    var today_ISO_date = d.getFullYear()+"-"+twoDigitMonth(d)+"-"+twoDigitDate(d); // in yyyy-mm-dd format
    var today_ISO_date_dd = twoDigitDate(d)+"-"+twoDigitMonth(d)+"-"+d.getFullYear(); // in yyyy-mm-dd format
    document.getElementById('to_date_temp').setAttribute("value", today_ISO_date);
    document.getElementById('to_date').setAttribute("value", today_ISO_date_dd);
    document.getElementById('from_date_temp').setAttribute("value", today_ISO_date);
    document.getElementById('from_date').setAttribute("value", today_ISO_date_dd);
       
     var dd_mm_yyyy;
     $("#to_date_temp").change( function(){
       	changedDate = $(this).val(); //in yyyy-mm-dd format obtained from datepicker
        var date = new Date(changedDate);
        dd_mm_yyyy = twoDigitDate(date)+"-"+twoDigitMonth(date)+"-"+date.getFullYear(); // in dd-mm-yyyy format
        $("#to_date").val(dd_mm_yyyy);
        document.getElementById('to_date').setAttribute("value", dd_mm_yyyy);
        //console.log($(this).val());
        //console.log("Date picker clicked");
        });
     $("#from_date_temp").change( function(){
       	changedDate = $(this).val(); //in yyyy-mm-dd format obtained from datepicker
        var date = new Date(changedDate);
        dd_mm_yyyy = twoDigitDate(date)+"-"+twoDigitMonth(date)+"-"+date.getFullYear(); // in dd-mm-yyyy format
        $("#from_date").val(dd_mm_yyyy);
        document.getElementById('to_date').setAttribute("value", dd_mm_yyyy);
        //console.log($(this).val());
        //console.log("Date picker clicked");
        });
        
    });

</script>
                            </div><!-- /.col -->
                            </div><!-- /.row -->
                            </div><!-- /.box-body -->


                            </div><!-- /.box -->
                            </div><!-- /.col -->


                            </div><!-- /.box -->
                            <div class="modal-footer">
                            <div class="validate pull-left" style="font-weight:bold;"></span></div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>

                            </div>
                            </div>
       </form>
        </div>
        </div>
       </div>
      <!--Insert Category Insert Modal End-->
       <!--Insert Category Edit Modal Start-->
<div class="modal fade" id="updModel" role="dialog">
<div class="modal-dialog modal-sm">

<!-- Modal content-->
<div class="modal-content">

<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Edit User </h4>
</div>
<form id ="updUserData">    
<div class="modal-body">
                            <div class="row">
                            <div class="col-xs-12">
                            <div class="box">

                            <div class="box-body">
                            <div class="row">
                            <div class="col-md-12">
                            <div class="form-group">
                            <label for="name">Name</label>
                            <input type="hidden" class="form-control" name="user_id" id ="user_id" value="">
                            <input type="text" class="form-control" name="user_name" id ="user_name" value="" placeholder="Enter Name">
                            </div>
                            <!-- /.form-group -->
                            <div class="form-group">
                            <label for="email">Email</label>

                            <input type="text" class="form-control"  name="email" id ="email" value="" placeholder="Email">
                            </div>
                            <div class="form-group">
                            <label for="Contact Number">Contact Number</label>
                            <input type="text" id="contact_no" name="contact_no" value="" class="form-control" placeholder="Contact" maxlength="10">
                            </div>
                            </div><!-- /.col -->
                            </div><!-- /.row -->
                            </div><!-- /.box-body -->


                            </div><!-- /.box -->
                            </div><!-- /.col -->


                            </div><!-- /.box -->
                            <div class="modal-footer">
                            <div class="validate pull-left" style="font-weight:bold;"></span></div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>

                            </div>
                            </div>
</form>
</div>
</div>
</div>
        <!--Insert Category Edit Modal End-->
         <!--Insert Category Delete Modal Start-->
 <div class="modal fade" id="delModel" role="dialog">
        <div class="modal-dialog modal-sm">
                
        <!-- Modal content-->
        <div class="modal-content">
             
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Delete Category </h4>
        </div>
            <form id ="delUserData">    
            <div class="modal-body">
                <div class="row">
                <div class="col-md-12">
                <input type="hidden"  name="user_id" id="user_id" value="" class="form-control"/>
                <p>Sure to want to delete "<span style ="color:red" id ="catname"></span>" Amount?</p>
                 </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="delete2" class="btn btn-danger">Delete</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
           </form>
       </div>
       </div>
       </div>
          <!--Insert Category Delete Modal End--> 
  </body>
  <script type="text/javascript" language="javascript">
      $(document).ready(function(){
            $("#add").click(function(){
                 if(user_type=="admin" || privileges_details_system[17]["insertm"]=="true"){
                    $("#insModal").modal('show');
                 }else{
                     showAccessDeniedModel();
                 }
            }); 
            $("#insModal").on('shown.bs.modal', function(){
                            $('#insUserData').find("#employee_id").val(employees_id);
                            $('#insUserData').off("submit");
                            $('#insUserData').submit(function() {
                            var emailfilter = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
                                     var genderfilter = /^[a-zA-Z]+$/;
                                     var contactfilter =  /^\d{10}$/;
                                     var passwordfilter = /^\d{4}$/;
                                     
                                     if(this.user_name.value == ""){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please fill valid name").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">");
                                          $('#insUserData').find('#user_name').focus();
                                        return false;
                                    }else if(this.email.value == "" || !emailfilter.test(this.email.value)){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please fill valid email").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">");
                                          $('#insUserData').find('#email').focus(); 
                                         return false;
                                    }else if(this.contact_no.value == "" || !contactfilter.test(this.contact_no.value)){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please fill valid contact no.").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">");
                                         $('#insUserData').find('#contact_no').focus();
                                         return false;
                                         
                               }else {
                                $(".validate").removeClass("text-danger").addClass("text-success").fadeIn(100).text("Ok").prepend("<span class=\"glyphicon glyphicon-ok text-success\">").fadeOut(1000);
                                $.ajax({
                                    type:"post",
                                    url:"../server/controller/user_data/InsUserData.php",
                                    data:$('#insUserData').serialize(),
                                    success: function(data){ 
                                        location.reload(true);
                                    } 
                                });
                                return false; 
                            }
                   return false;
                    });
            var modal = this;
            var hash = modal.id;
            window.location.hash = hash;
            window.onhashchange = function() {
                    if (!location.hash){
                            $(modal).modal('hide');
                    }
            }
      });  
    $('#myModal').on('hide.bs.modal', function() {
            location.reload(true);
    });
         }); 
    
         function onlad(){
                    cate={}
                    $.ajax({
                    type:"post",
                    url:"../server/controller/user_details/SelUserDetails.php",
                    success: function(data) {
                        var duce = jQuery.parseJSON(data);
                        $("#data tr:has(td)").remove();
                        $.each(duce, function (index, article) {
                            cate[article.user_id]={};
                            cate[article.user_id]= article;
                           $("#data").append($('<tr/>')
                                .append($('<td/>').html((index+1)))
                                .append($('<td/>').html("<img src="+article.pic+" height=\"50\" width=\"50\"/>"))
                                .append($('<td/>').html(article.user_name))
                                .append($('<td/>').html(article.contact_no))
                                .append($('<td/>').html(article.email))
                                .append($('<td/>').html(article.date))
                                .append($('<td/>').html($("<button>").attr({'type':'button'}).addClass("btn btn-danger btn-xs").append("&nbsp;&nbsp;Delete&nbsp;&nbsp;").click(function (){
                                    if(user_type=="admin" || privileges_details_system[17]["deletem"]=="true"){
                                        delUserData(article.user_id)
                                     }else{
                                         showAccessDeniedModel();
                                     }
                                })))
                                .append($('<td/>').html($("<button>").attr({'type':'button','disabled':(article.amount!=null&&article.amount>=article.charge)?true:false}).addClass("btn btn-default btn-xs").append("&nbsp;&nbsp;Offer&nbsp;&nbsp;").click(function (){
                                    if(user_type=="admin" || privileges_details_system[17]["updatem"]=="true"){
                                        offerUserData(article.user_id,"offer")
                                     }else{
                                         showAccessDeniedModel();
                                     }
                                })))
                                .append($('<td/>').html($("<button>").attr({'type':'button','disabled':(article.amount!=null&&article.amount>=article.charge)?true:false}).addClass("btn btn-default btn-xs").append("&nbsp;&nbsp;Paid&nbsp;&nbsp;").click(function (){
                                    if(user_type=="admin" || privileges_details_system[17]["updatem"]=="true"){
                                        offerUserData(article.user_id,"paid")
                                     }else{
                                         showAccessDeniedModel();
                                     }
                                })))
                            );
                        });
                    }
                });
         }
                    function updUserData(user_id,transation_type) {
                      $("#updModel").modal('show');
                      $("#updModel").on('shown.bs.modal', function(){
                            $("#updUserData").find("#user_id").val(user_id);  
                            $("#updUserData").find("#user_name").val(cate[user_id]["user_name"]);          
                            $("#updUserData").find("#contact_no").val(cate[user_id]["contact_no"]);          
                            $("#updUserData").find("#email").val(cate[user_id]["email"]);          
                            $('#updUserData').off("submit");
                            $('#updUserData').submit(function() {
                            var genderfilter = /^[a-zA-Z]+$/;
                                     var contactfilter =  /^\d{10}$/;
                                     var passwordfilter = /^\d{4}$/;
                                     var emailfilter = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
                                     if(this.user_name.value == ""){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please fill valid name").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">");
                                          $('#updUserData').find('#user_name').focus();
                                        return false;
                                    }else if(this.email.value == "" || !emailfilter.test(this.email.value)){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please fill valid email").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">");
                                          $('#updUserData').find('#email').focus(); 
                                         return false;
                                   }else if(this.contact_no.value == "" || !contactfilter.test(this.contact_no.value)){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please fill valid contact no.").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">");
                                         $('#updUserData').find('#contact_no').focus();
                                         return false;
                                   }else {
                                       
                                        $(".validate").removeClass("text-danger").addClass("text-success").fadeIn(100).text("Ok").prepend("<span class=\"glyphicon glyphicon-ok text-success\">").fadeOut(1000);
                                        
                                        $.ajax({
                                            type:"post",
                                            url:"../server/controller/user_data/UpdUserData.php",
                                            data:$('#updUserData').serialize(),
                                            success: function(data){ 
                                                 alert(data)
                                                 location.reload(true);
                                            } 
                                        });
                                 }
                        return false;
                    });
                     var modal = this;
                            var hash = modal.id;
                            window.location.hash = hash;
                            window.onhashchange = function() {
                                    if (!location.hash){
                                            $(modal).modal('hide');
                                    }
                            }
                      });  
                    $('#editUserData').on('hide.bs.modal', function() {
                            location.reload(true);
                    });
                }
                    function 
                    offerUserData(user_id,transation_type) {
                      $("#insModalOffer").modal('show');
                      $("#insModalOffer").on('shown.bs.modal', function(){
                          if(transation_type=='offer'){
                            $("#insModalOffer").find("#user_offer_title").html("Offer");  
                          }else{
                              $("#insModalOffer").find("#user_offer_title").html("Paid");  
                          }
                            $("#insModalOffer").find("#user_id").val(user_id);  
                            $("#insModalOffer").find("#transation_type").val(transation_type);  
                            $("#insModalOffer").find(".modal-title").find("#user_name_offer").html(cate[user_id]["user_name"]);          
                            $('#insOffer').off("submit");
                            $('#insOffer').submit(function() {
                            var genderfilter = /^[a-zA-Z]+$/;
                                    if(this.amount.value == ""){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please fill valid amount").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">");
                                          $('#insOffer').find('#amount').focus();
                                        return false;
                                    }else if(this.from_date.value == "" ){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please fill valid Date").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">");
                                          $('#insOffer').find('#from_date_temp').focus(); 
                                         return false;
                                   }else if(this.to_date.value == "" ){
                                         $(".validate").addClass("text-danger").fadeIn(100).text("Please fill valid Date.").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">");
                                         $('#insOffer').find('#to_date_temp').focus();
                                         return false;
                                   }else {
                                        $(".validate").removeClass("text-danger").addClass("text-success").fadeIn(100).text("Ok").prepend("<span class=\"glyphicon glyphicon-ok text-success\">").fadeOut(1000);
                                        $.ajax({
                                            type:"post",
                                            url:"../server/controller/temp_wallet/InsTempWallet.php",
                                            data:$('#insOffer').serialize(),
                                            success: function(data){ 
                                                 alert(data)
                                                 location.reload(true);
                                            } 
                                        });
                                 }
                        return false;
                    });
                     var modal = this;
                            var hash = modal.id;
                            window.location.hash = hash;
                            window.onhashchange = function() {
                                    if (!location.hash){
                                            $(modal).modal('hide');
                                    }
                            }
                      });  
                    $('#editUserData').on('hide.bs.modal', function() {
                            location.reload(true);
                    });
                }

</script>

<script type="text/javascript" language="javascript">
        function delUserData(user_id) {
            
            $("#delModel").modal('show');
                 var user_name = cate[user_id]["user_name"];;
                 $("#delModel").on('shown.bs.modal', function(){
                    $("#delUserData").find("#user_id").val(user_id);        
                    $("#delUserData").find("#catname").text(user_name); 
                    $('#delUserData').off("submit");
                    $('#delUserData').submit(function() {
                         $.ajax({
                            type:"post",
                            url:"../server/controller/user_details/DelUserDetails.php",
                            data:{'user_id':user_id},
                            success: function(data){ 
                                alert(data)
                                location.reload(true);
                            } 
                  });
                    return false;
    });
    
    });
   
}

</script>
</html>

