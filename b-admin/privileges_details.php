<?php include './includes/check_session.php';?>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Privileges Details</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <?php include 'includes/links.php';?>
   
        <script type="text/javascript">
         var type=0
         cate={}
         user_details={}
         privileges_details={}
         module_type={}
         var user_id=0;
         var employee_id=0;
         $(document).ready(function() {
             loadEmployeePrivileges(employees_id,callback);
         });  
         function callback(){
             if(user_type=="admin" || privileges_details_system[2]["selectm"]=="true"){
                 onloadUser();
             }else{
                 showAccessDeniedModel();
             }
         }
         function onloadUser(){
             $.ajax({
                type:"post",
                url:"../server/controller/employee_details/SelEmployeeDetails.php",
                success: function(data) { 
                var duce = jQuery.parseJSON(data); //here data is passed to js object
                user_details={}
                var inde=0;
                    $.each(duce, function (index, article) {
                        user_details[article.user_id]=article;    
                        if(inde==0){
                            $("#user_details_first").append('<img src="../server/controller/user_details/'+article.pic+'" width="50" height="50" >'+article.user_name+'<span class="glyphicon glyphicon-chevron-down"></span>');
                            user_id=article.user_id;
                            employee_id=article.employee_id;
                            onloadPrivileges(article.employee_id);
                            inde++;
                        }
                        $("#user_details").append($('<li><a href="#"><img src="../server/controller/user_details/'+article.pic+'" width="50" height="50">'+article.user_name+'</a></li>').click(function (){
                            user_id=article.user_id;
                            $("#user_details_first").html($(this).find("a").html()).append('<span class="glyphicon glyphicon-chevron-down"></span>');
                            employee_id=article.employee_id;
                            onloadPrivileges(article.employee_id);
                        }));
                    });
                }
            });
         }
         
         function onloadPrivileges(employee_id){
             privileges_details={}
             $.ajax({
                type:"post",
                url:"../server/controller/privileges_details/SelPrivilegesDetails.php",
                data:{'employee_id':employee_id},
                success: function(data) { 
//                    alert(data)
                    $.each(jQuery.parseJSON(data), function (index, article) {
                        privileges_details[article.module_id]=article;    
                    });
                    onlad();
                }
            });
         }
         
        </script> 
  </head>
  <!-- ADD THE CLASS sidedar-collapse TO HIDE THE SIDEBAR PRIOR TO LOADING THE SITE -->
 <body class="wysihtml5-supported skin-yellow sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">

   <?php include 'includes/header.php';?>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
   <?php include 'includes/sidepanel.php';?>

      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Privileges Details
            <small>Admin panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i> Home</li>
            <li class="active">Privileges Details</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
           
          <div class="row">
            <div class="col-xs-12">
                <div class="box">
                <div class="box-header">
                  <div class="pull-left col-md-6">
                        <div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="user_details_first">
      
    </button>
        <ul class="dropdown-menu" id="user_details"></ul>
  </div>
                  </div>
                </div><!-- /.box-header -->
                
                <div class="box-body">
                  <div class="table-responsive"> 
                   <table id="data" class="table table-bordered table-hover ">
                    <thead>
                      <tr>
                        <th>S.N.</th>
                        <th>Module</th>
                        <th>Insert</th>
                        <th>Update</th>
                        <th>Delete</th>
                        <th>Browse</th>
                        <th colspan="2">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                   
                  </table>
                  </div>         
                
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section>
      </div><!-- /.content-wrapper -->

     <?php include 'includes/footer.php';?>

     
    </div><!-- ./wrapper -->
    <?php include 'includes/jslinks.php';?>
 <div class="modal fade" id="delModel" role="dialog">
        <div class="modal-dialog modal-sm">
                
        <!-- Modal content-->
        <div class="modal-content">
             
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Delete Category </h4>
        </div>
        <form id ="delPrivilegesDetails">    
        <div class="modal-body">
            <div class="row">
            <div class="col-md-12">
            <input type="hidden"  name="module_id" id="module_id" value="" class="form-control"/>
            <p>Sure to want to deletem "<span style ="color:red" id ="catname"></span>" Privileges?</p>
             </div>
            </div>
            <div class="modal-footer">
            <button type="submit" id="deletem2" class="btn btn-danger">Delete</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>

            </div>
        </div>
       </form>
       </div>
       </div>
       </div>
          <!--Insert Category Delete Modal End--> 
  </body>
  <script type="text/javascript" language="javascript">
      $(document).ready(function(){
            $("#add").click(function(){
                $.each(module_type, function (index, article) {    
                    updPrivilegesDetails(article.module_id)
                }); 
            }); 
        });
         function onlad(){
            module_type={}
            if(Object.keys(module_type).length==0){
                $.ajax({
                    type:"post",
                    url:"../server/controller/privileges_details/SelModuleType.php",
                    success: function(data) {
                        var duce = jQuery.parseJSON(data);
                        module_type= duce;
                        setRow(duce);
                    }
                });
            }else{
                setRow(module_type);
            }
         }
         function setRow(duce){
            $("#data tr:has(td)").remove();
            
            $.each(duce, function (index, article) {
               $("#data").append($('<tr/>')
                    .append($('<td/>').html((index+1)))
                    .append($('<td/>').html(article.module_name))
                    .append($('<td/>').html($("<button>").addClass("btn btn-default").append($("<i>").addClass("fa").addClass(checkPrivileges(article.module_id,"insertm")? "fa-close":"fa-check")).click(function (){
                        upd(article.module_id,"insertm",$(this).find("i"))
                     })))
                    .append($('<td/>').html($("<button>").addClass("btn btn-default").append($("<i>").addClass("fa").addClass(checkPrivileges(article.module_id,"updatem")? "fa-close":"fa-check")).click(function (){
                        upd(article.module_id,"updatem",$(this).find("i"))
                     })))
                    .append($('<td/>').html($("<button>").addClass("btn btn-default").append($("<i>").addClass("fa").addClass(checkPrivileges(article.module_id,"deletem")? "fa-close":"fa-check")).click(function (){
                        upd(article.module_id,"deletem",$(this).find("i"))
                     })))
                    .append($('<td/>').html($("<button>").addClass("btn btn-default").append($("<i>").addClass("fa").addClass(checkPrivileges(article.module_id,"selectm")? "fa-close":"fa-check")).click(function (){
                        upd(article.module_id,"selectm",$(this).find("i"))
                     })))
                    .append($('<td/>').html($("<button>").attr({'type':'button'}).addClass("btn btn-default ").append("&nbsp;&nbsp;Update&nbsp;&nbsp;").click(function (){
                        if(user_type=="admin" || privileges_details_system[2]["updatem"]=="true"){
                            updPrivilegesDetails(article.module_id)
                        }else{
                            showAccessDeniedModel();
                        }
                    })))
                );
            });
         }
         function checkPrivileges(module_id,type){
            var b=true;
            if(Object.keys(privileges_details).length!=0 && privileges_details[module_id]!=null && privileges_details[module_id][type]=="true"){
                b=false;
            }
            return b;
         }
         function upd(module_id,type,obj){
            if(obj.hasClass("fa-close")){
                obj.removeClass("fa-close");
                obj.addClass("fa-check");
            }else{
                obj.removeClass("fa-check");
                obj.addClass("fa-close");
            }
             if(Object.keys(privileges_details).length==0){
                 privileges_details={}
                 privileges_details[module_id]={};
                 privileges_details[module_id][type]=true;
             }else{
                 if(privileges_details[module_id]==undefined){
                     privileges_details[module_id]={};
                     privileges_details[module_id][type]=true;
                 }else{
                     if(privileges_details[module_id][type]=="true"){
                         privileges_details[module_id][type]=false;
                     }else{
                             privileges_details[module_id][type]=true;
                     }
                 }
             }
         }
</script>

<script type="text/javascript" language="javascript">
        function updPrivilegesDetails(module_id) {
            privileges_details[module_id]["module_id"]=module_id;
            privileges_details[module_id]["employee_id"]=employee_id;
            privileges_details[module_id]["insertm"]=privileges_details[module_id]["insertm"]==null?false:privileges_details[module_id]["insertm"];
            privileges_details[module_id]["updatem"]=privileges_details[module_id]["updatem"]==null?false:privileges_details[module_id]["updatem"];
            privileges_details[module_id]["deletem"]=privileges_details[module_id]["deletem"]==null?false:privileges_details[module_id]["deletem"];
            privileges_details[module_id]["selectm"]=privileges_details[module_id]["selectm"]==null?false:privileges_details[module_id]["selectm"];
             $.ajax({
                type:"post",
                url:"../server/controller/privileges_details/InsPrivilegesDetails.php",
                data:privileges_details[module_id],
                success: function(data){ 
                    alert(data)
//                    $("body").html(data)
//                    location.reload(true);
                } 
            });
            return false;
}

</script>
</html>

