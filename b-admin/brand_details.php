<?php include './includes/check_session.php';?>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Brand Details</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <?php include 'includes/links.php';?>
   
        <script type="text/javascript">
         var type=0
         cate={}
         $(document).ready(function() {
             loadEmployeePrivileges(employees_id,callback);
         });  
         function callback(){
             if(user_type=="admin" || privileges_details_system[7]["selectm"]=="true"){
                onlad();
             }else{
                 showAccessDeniedModel();
             }
         }
         
        </script> 
  </head>
  <!-- ADD THE CLASS sidedar-collapse TO HIDE THE SIDEBAR PRIOR TO LOADING THE SITE -->
 <body class="wysihtml5-supported skin-yellow sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">

   <?php include 'includes/header.php';?>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
   <?php include 'includes/sidepanel.php';?>

      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Brand Details
            <small>Admin panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i> Home</li>
            <li class="active">Brand Details</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
           
          <div class="row">
            <div class="col-xs-12">
                <div class="box">
                <div class="box-header">
                  <div class="pull-right">
                      <button type="button" class="btn btn-success" id="add">Add +</button>
                  </div>
                </div><!-- /.box-header -->
                
                <div class="box-body">
                  <div class="table-responsive"> 
                   <table id="data" class="table table-bordered table-hover ">
                    <thead>
                      <tr>
                        <th>S.N.</th>
                        <th>Brand Name</th>
                        <th colspan="2">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                   
                  </table>
                  </div>         
                
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section>
      </div><!-- /.content-wrapper -->

     <?php include 'includes/footer.php';?>

     
    </div><!-- ./wrapper -->
    <?php include 'includes/jslinks.php';?>
    <!--Insert Category Insert Modal Start-->
 <div class="modal fade" id="insModal" role="dialog">
        <div class="modal-dialog modal-sm">
        <!-- Modal content-->
        <div class="modal-content">
         <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Insert Brand Details </h4>
        </div>
        <form id ="insBrandDetails">    
        <div class="modal-body">
        <div class="row">
        
        <div class="col-md-12">
        <div class="form-group">
            <input type="text"  name="brand_name" id="brand_name" class="form-control"  placeholder="Enter Brand">
        </div>
        <!-- /.form-group -->
        </div><!-- /.col -->
        </div><!-- /.box -->
        <div class="modal-footer">
            <div class="validate pull-left" style="font-weight:bold;"></span></div>
            <button type="submit" class="btn btn-primary">Submit</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        </div>
        </div>
       </form>
        </div>
        </div>
       </div>
      <!--Insert Category Insert Modal End-->
       <!--Insert Category Edit Modal Start-->
<div class="modal fade" id="updModel" role="dialog">
<div class="modal-dialog modal-sm">

<!-- Modal content-->
<div class="modal-content">

<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Edit Brand </h4>
</div>
<form id ="updBrandDetails">    
<div class="modal-body">
<div class="row">

<div class="col-md-12">
<div class="form-group">
    <input type="hidden"  name="brand_details_id" id="brand_details_id" value="" class="form-control"/>
    <input type="text"  name="editamt" id="editamt" value="" class="form-control"  placeholder="Enter Brand Name">
</div>
<!-- /.form-group -->

</div><!-- /.col -->
</div><!-- /.box -->


<div class="modal-footer">
<div class="validate pull-left" style="font-weight:bold;"></span></div>
<button type="submit" id ="update" class="btn btn-primary">Update</button>
<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>

</div>
</div>
</form>
</div>
</div>
</div>
        <!--Insert Category Edit Modal End-->
         <!--Insert Category Delete Modal Start-->
 <div class="modal fade" id="delModel" role="dialog">
        <div class="modal-dialog modal-sm">
                
        <!-- Modal content-->
        <div class="modal-content">
             
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Delete Category </h4>
        </div>
            <form id ="delBrandDetails">    
            <div class="modal-body">
                <div class="row">
                <div class="col-md-12">
                <input type="hidden"  name="brand_details_id" id="brand_details_id" value="" class="form-control"/>
                <p>Sure to want to delete "<span style ="color:red" id ="catname"></span>" Amount?</p>
                 </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="delete2" class="btn btn-danger">Delete</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
           </form>
       </div>
       </div>
       </div>
          <!--Insert Category Delete Modal End--> 
  </body>
  <script type="text/javascript" language="javascript">
      $(document).ready(function(){
            $("#add").click(function(){
                 if(user_type=="admin" || privileges_details_system[7]["insertm"]=="true"){
                    $("#insModal").modal('show');
                 }else{
                     showAccessDeniedModel();
                 }
            }); 
            $("#insModal").on('shown.bs.modal', function(){
                            $('#insBrandDetails').off("submit");
                            $('#insBrandDetails').submit(function() {
                            if(this.brand_name.value == ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please fill valid Brand ").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                $('#brand_name').focus();
                                $('#insBrandDetails').each(function(){
                                    this.reset();
                                    return false;
                                });
                                return false;  
                           }else {
                                $(".validate").removeClass("text-danger").addClass("text-success").fadeIn(100).text("Ok").prepend("<span class=\"glyphicon glyphicon-ok text-success\">").fadeOut(1000);
                                $.ajax({
                                    type:"post",
                                    url:"../server/controller/brand_details/InsBrandDetails.php",
                                    data:$('#insBrandDetails').serialize(),
                                    success: function(data){ 
                                        location.reload(true);
                                    } 
                                });
                                return false; 
                            }
                   return false;
                    });
            var modal = this;
            var hash = modal.id;
            window.location.hash = hash;
            window.onhashchange = function() {
                    if (!location.hash){
                            $(modal).modal('hide');
                    }
            }
      });  
    $('#myModal').on('hide.bs.modal', function() {
            location.reload(true);
    });
         }); 
    
         function onlad(){
                    cate={}
                    $.ajax({
                    type:"post",
                    url:"../server/controller/brand_details/SelBrandDetails.php",
                    success: function(data) {
                        var duce = jQuery.parseJSON(data);
                        $("#data tr:has(td)").remove();
                        $.each(duce, function (index, article) {
                            cate[article.brand_details_id]={};
                            cate[article.brand_details_id]= article;
                           $("#data").append($('<tr/>')
                                .append($('<td/>').html((index+1)))
                                .append($('<td/>').html(article.brand_name))
                                .append($('<td/>').html($("<button>").attr({'type':'button'}).addClass("btn btn-success btn-xs").append("&nbsp;&nbsp;Edit&nbsp;&nbsp;").click(function (){
                                    if(user_type=="admin" || privileges_details_system[7]["updatem"]=="true"){
                                        updBrandDetails(article.brand_details_id)
                                     }else{
                                         showAccessDeniedModel();
                                     }
                                    
                                })))
                                .append($('<td/>').html($("<button>").attr({'type':'button'}).addClass("btn btn-danger btn-xs").append("&nbsp;&nbsp;Delete&nbsp;&nbsp;").click(function (){
                                    if(user_type=="admin" || privileges_details_system[7]["deletem"]=="true"){
                                        delBrandDetails(article.brand_details_id)
                                     }else{
                                         showAccessDeniedModel();
                                     }
                                })))
                            );
                        });
                    }
                });
         }
                    function updBrandDetails(brand_details_id) {
                      $("#updModel").modal('show');
                      $("#updModel").on('shown.bs.modal', function(){
                            $("#updBrandDetails").find("#brand_details_id").val(brand_details_id);  
                            $("#updBrandDetails").find("#editamt").val(cate[brand_details_id]["brand_name"]);          
                            $('#updBrandDetails').off("submit");
                            $('#updBrandDetails').submit(function() {
                            if(this.editamt.value == ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please fill valid Brand").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                $('#editamt').focus();
                           }else {
                                $(".validate").removeClass("text-danger").addClass("text-success").fadeIn(100).text("Ok").prepend("<span class=\"glyphicon glyphicon-ok text-success\">").fadeOut(1000);
                                $.ajax({
                                    type:"post",
                                    url:"../server/controller/brand_details/UpdBrandDetails.php",
                                    data:$('#updBrandDetails').serialize(),
                                    success: function(data){ 
                                         alert(data)
                                         location.reload(true);
                                    } 
                                });
                            }
                        return false;
                    });
                     var modal = this;
                            var hash = modal.id;
                            window.location.hash = hash;
                            window.onhashchange = function() {
                                    if (!location.hash){
                                            $(modal).modal('hide');
                                    }
                            }
                      });  
                    $('#editBrandDetails').on('hide.bs.modal', function() {
                            location.reload(true);
                    });
                }

</script>

<script type="text/javascript" language="javascript">
        function delBrandDetails(brand_details_id) {
            
            $("#delModel").modal('show');
                 var brand_name = cate[brand_details_id]["brand_name"];;
                 $("#delModel").on('shown.bs.modal', function(){
                    $("#delBrandDetails").find("#brand_details_id").val(brand_details_id);        
                    $("#delBrandDetails").find("#catname").text(brand_name); 
                    $('#delBrandDetails').off("submit");
                    $('#delBrandDetails').submit(function() {
                         $.ajax({
                            type:"post",
                            url:"../server/controller/brand_details/delBrandDetails.php",
                            data:{'brand_details_id':brand_details_id},
                            success: function(data){ 
                                location.reload(true);
                            } 
                  });
                    return false;
    });
    
    });
   
}

</script>
</html>

