<?php include './includes/check_session.php';?>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Spare Details</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <?php include 'includes/links.php';?>
   
        <script type="text/javascript">
         var type=0
         cate={}
         var product_details_id=0
         $(document).ready(function() {
             loadEmployeePrivileges(employees_id,callback);
             $("#product_details").find("select").change(function () {
                product_details_id=$(this).val();
                onlad(product_details_id);
             }); 
         });  
         function callback(){
             if(user_type=="admin" || privileges_details_system[12]["selectm"]=="true"){
                loadProduct();
             }else{
                 showAccessDeniedModel();
             }
         }
         
         function loadProduct(){
             $.ajax({
                    type:"post",
                    url:"../server/controller/product_details/SelProductDetails.php",
                    success: function(data) {
                        var duce = jQuery.parseJSON(data);
                        $("#product_details").find("select").empty();
                        $.each(duce, function (index, article) {
                           $("#product_details").find("select").append($('<option>').attr({'value':article.product_details_id}).append(article.product_name));
                        });
                        $("#product_details").find("select").trigger("change");
                    }
                });
         }
         
        </script> 
  </head>
  <!-- ADD THE CLASS sidedar-collapse TO HIDE THE SIDEBAR PRIOR TO LOADING THE SITE -->
 <body class="wysihtml5-supported skin-yellow sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">

   <?php include 'includes/header.php';?>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
   <?php include 'includes/sidepanel.php';?>

      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Spare Details
            <small>Admin panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i> Home</li>
            <li class="active">Spare Details</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
           
          <div class="row">
            <div class="col-xs-12">
                <div class="box">
                <div class="box-header">
                    <div class="col-md-3 pull-left">
                      <div class="form-group" id="product_details">
                            <label>Select Product</label>
                            <select class="form-control" id="product_details_id" name="product_details_id">
                            </select>
                      </div><!-- /.form-group -->    
                  </div>
                  <div class="pull-right">
                      <button type="button" class="btn btn-success" id="add">Add +</button>
                  </div>
                </div><!-- /.box-header -->
                
                <div class="box-body">
                  <div class="table-responsive"> 
                   <table id="data" class="table table-bordered table-hover ">
                    <thead>
                      <tr>
                        <th>S.N.</th>
                        <th>Spare Name</th>
                        <th>Spare Cost</th>
                        <th>Lebure Cost</th>
                        <th colspan="2">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                   
                  </table>
                  </div>         
                
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section>
      </div><!-- /.content-wrapper -->

     <?php include 'includes/footer.php';?>

     
    </div><!-- ./wrapper -->
    <?php include 'includes/jslinks.php';?>
    <!--Insert Category Insert Modal Start-->
 <div class="modal fade" id="insModal" role="dialog">
        <div class="modal-dialog modal-sm">
        <!-- Modal content-->
        <div class="modal-content">
         <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Insert Spare Details </h4>
        </div>
        <form id ="insSpareDetails">    
        <div class="modal-body">
        <div class="row">
        
        <div class="col-md-12">
        <div class="form-group">
            <input type="hidden"  name="product_details_id" id="product_details_id" class="form-control"  placeholder="Enter Spare">
            <input type="text"  name="spare_name" id="spare_name" class="form-control"  placeholder="Enter Spare">
        </div>
        <div class="form-group">
            <input type="number"  name="spare_cost" id="spare_cost" class="form-control"  placeholder="Spare Cost In Rupees">
        </div>
        <div class="form-group">
            <input type="number"  name="lebure_cost" id="lebure_cost" class="form-control"  placeholder="Lebure Cost In Rupees">
        </div>
        <!-- /.form-group -->
        </div><!-- /.col -->
        </div><!-- /.box -->
        <div class="modal-footer">
            <div class="validate pull-left" style="font-weight:bold;"></span></div>
            <button type="submit" class="btn btn-primary">Submit</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        </div>
        </div>
       </form>
        </div>
        </div>
       </div>
      <!--Insert Category Insert Modal End-->
       <!--Insert Category Edit Modal Start-->
<div class="modal fade" id="updModel" role="dialog">
<div class="modal-dialog modal-sm">

<!-- Modal content-->
<div class="modal-content">

<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Edit Spare </h4>
</div>
<form id ="updSpareDetails">    
<div class="modal-body">
<div class="row">

<div class="col-md-12">
<div class="form-group">
    <input type="hidden"  name="spare_details_id" id="spare_details_id" value="" class="form-control"/>
    <input type="text"  name="spare_name" id="spare_name" value="" class="form-control"  placeholder="Enter Spare Name">
</div>
<div class="form-group">
    <input type="text"  name="spare_cost" id="spare_cost" value="" class="form-control"  placeholder="Enter Spare Cost">
</div>
<div class="form-group">
    <input type="text"  name="lebure_cost" id="lebure_cost" value="" class="form-control"  placeholder="Enter Lebure Cost">
</div>
<!-- /.form-group -->

</div><!-- /.col -->
</div><!-- /.box -->


<div class="modal-footer">
<div class="validate pull-left" style="font-weight:bold;"></span></div>
<button type="submit" id ="update" class="btn btn-primary">Update</button>
<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>

</div>
</div>
</form>
</div>
</div>
</div>
        <!--Insert Category Edit Modal End-->
         <!--Insert Category Delete Modal Start-->
 <div class="modal fade" id="delModel" role="dialog">
        <div class="modal-dialog modal-sm">
                
        <!-- Modal content-->
        <div class="modal-content">
             
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Delete Category </h4>
        </div>
            <form id ="delSpareDetails">    
            <div class="modal-body">
                <div class="row">
                <div class="col-md-12">
                <input type="hidden"  name="spare_details_id" id="spare_details_id" value="" class="form-control"/>
                <p>Sure to want to delete "<span style ="color:red" id ="catname"></span>" Amount?</p>
                 </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="delete2" class="btn btn-danger">Delete</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
           </form>
       </div>
       </div>
       </div>
          <!--Insert Category Delete Modal End--> 
  </body>
  <script type="text/javascript" language="javascript">
      $(document).ready(function(){
            $("#add").click(function(){
                 if(user_type=="admin" || privileges_details_system[12]["insertm"]=="true"){
                    $("#insModal").modal('show');
                 }else{
                     showAccessDeniedModel();
                 }
            }); 
            $("#insModal").on('shown.bs.modal', function(){
                            $('#insSpareDetails').find('#product_details_id').val(product_details_id);
                            $('#insSpareDetails').off("submit");
                            $('#insSpareDetails').submit(function() {
                            if(this.spare_name.value == ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please fill valid Spare ").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                $('#insSpareDetails').find('#spare_name').focus();
                                
                                return false;  
                           }else if(this.spare_cost.value == ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please fill valid Spare Cost").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                $('#insSpareDetails').find('#spare_cost').focus();
                                return false;  
                           }else if(this.lebure_cost.value == ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please fill valid Lebure Cost").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                $('#insSpareDetails').find('#lebure_cost').focus();
                                return false;  
                           }else {
                                $(".validate").removeClass("text-danger").addClass("text-success").fadeIn(100).text("Ok").prepend("<span class=\"glyphicon glyphicon-ok text-success\">").fadeOut(1000);
                                $.ajax({
                                    type:"post",
                                    url:"../server/controller/spare_details/InsSpareDetails.php",
                                    data:$('#insSpareDetails').serialize(),
                                    success: function(data){ 
                                        location.reload(true);
                                    } 
                                });
                                return false; 
                            }
                   return false;
                    });
            var modal = this;
            var hash = modal.id;
            window.location.hash = hash;
            window.onhashchange = function() {
                    if (!location.hash){
                            $(modal).modal('hide');
                    }
            }
      });  
    $('#myModal').on('hide.bs.modal', function() {
            location.reload(true);
    });
         }); 
    
         function onlad(){
                    cate={}
                    $.ajax({
                    type:"post",
                    url:"../server/controller/spare_details/SelSpareDetails.php",
                    data:{'product_details_id':product_details_id},
                    success: function(data) {
                        var duce = jQuery.parseJSON(data);
                        $("#data tr:has(td)").remove();
                        $.each(duce, function (index, article) {
                            cate[article.spare_details_id]={};
                            cate[article.spare_details_id]= article;
                           $("#data").append($('<tr/>')
                                .append($('<td/>').html((index+1)))
                                .append($('<td/>').html(article.spare_name))
                                .append($('<td/>').html(article.spare_cost))
                                .append($('<td/>').html(article.lebure_cost))
                                .append($('<td/>').html($("<button>").attr({'type':'button'}).addClass("btn btn-success btn-xs").append("&nbsp;&nbsp;Edit&nbsp;&nbsp;").click(function (){
                                    if(user_type=="admin" || privileges_details_system[12]["updatem"]=="true"){
                                        updSpareDetails(article.spare_details_id)
                                     }else{
                                         showAccessDeniedModel();
                                     }
                                    
                                })))
                                .append($('<td/>').html($("<button>").attr({'type':'button'}).addClass("btn btn-danger btn-xs").append("&nbsp;&nbsp;Delete&nbsp;&nbsp;").click(function (){
                                    if(user_type=="admin" || privileges_details_system[12]["deletem"]=="true"){
                                        delSpareDetails(article.spare_details_id)
                                     }else{
                                         showAccessDeniedModel();
                                     }
                                })))
                            );
                        });
                    }
                });
         }
                    function updSpareDetails(spare_details_id) {
                      $("#updModel").modal('show');
                      $("#updModel").on('shown.bs.modal', function(){
                            $("#updSpareDetails").find("#spare_details_id").val(spare_details_id);  
                            $("#updSpareDetails").find("#spare_name").val(cate[spare_details_id]["spare_name"]);          
                            $("#updSpareDetails").find("#spare_cost").val(cate[spare_details_id]["spare_cost"]);          
                            $("#updSpareDetails").find("#lebure_cost").val(cate[spare_details_id]["lebure_cost"]);          
                            $('#updSpareDetails').off("submit");
                            $('#updSpareDetails').submit(function() {
                            if(this.spare_name.value == ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please fill valid Spare ").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                $('#updSpareDetails').find('#spare_name').focus();
                                return false;  
                           }else if(this.spare_cost.value == ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please fill valid Spare Cost").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                $('#updSpareDetails').find('#spare_cost').focus();
                                return false;  
                           }else if(this.lebure_cost.value == ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please fill valid Lebure Cost").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                $('#updSpareDetails').find('#lebure_cost').focus();
                                return false;  
                           }else {
                                $(".validate").removeClass("text-danger").addClass("text-success").fadeIn(100).text("Ok").prepend("<span class=\"glyphicon glyphicon-ok text-success\">").fadeOut(1000);
                                $.ajax({
                                    type:"post",
                                    url:"../server/controller/spare_details/UpdSpareDetails.php",
                                    data:$('#updSpareDetails').serialize(),
                                    success: function(data){ 
                                         alert(data)
                                         location.reload(true);
                                    } 
                                });
                            }
                        return false;
                    });
                     var modal = this;
                            var hash = modal.id;
                            window.location.hash = hash;
                            window.onhashchange = function() {
                                    if (!location.hash){
                                            $(modal).modal('hide');
                                    }
                            }
                      });  
                    $('#editSpareDetails').on('hide.bs.modal', function() {
                            location.reload(true);
                    });
                }

</script>

<script type="text/javascript" language="javascript">
        function delSpareDetails(spare_details_id) {
            
            $("#delModel").modal('show');
                 var spare_name = cate[spare_details_id]["spare_name"];;
                 $("#delModel").on('shown.bs.modal', function(){
                    $("#delSpareDetails").find("#spare_details_id").val(spare_details_id);        
                    $("#delSpareDetails").find("#catname").text(spare_name); 
                    $('#delSpareDetails').off("submit");
                    $('#delSpareDetails').submit(function() {
                         $.ajax({
                            type:"post",
                            url:"../server/controller/spare_details/delSpareDetails.php",
                            data:{'spare_details_id':spare_details_id},
                            success: function(data){ 
                                location.reload(true);
                            } 
                  });
                    return false;
    });
    
    });
   
}

</script>
</html>

