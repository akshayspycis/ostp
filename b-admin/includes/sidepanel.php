  <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="style/images/user.png" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p><?php echo $_SESSION['user_name']; ?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>

          <ul class="sidebar-menu">
            <li class="header">Main  Navigation</li>
            <li>
                <a href="home.php">
                <i class="fa fa-angle-double-right"></i>
                <span>Dashboard</span>
              </a>
            </li>
<!--            <li>
                <a href="quote-request.php">
                <i class="fa fa-angle-double-right"></i>
                <span>Quote Request</span>
              </a>
            </li>
            <li class="treeview">
                    <a href="banners.php">
                <i class="fa fa-angle-double-right"></i>
                <span>Banners</span>
              </a>
            </li>
            <li>
                <a href="feedback.php">
                <i class="fa fa-angle-double-right"></i>
                <span>Feedback </span>
              </a>
            </li> 
            <li>
                <a href="services.php">
                <i class="fa fa-angle-double-right"></i>
                <span>Services</span>
              </a>
            </li> 
            <li>
                <a href="blog_category.php">
                <i class="fa fa-angle-double-right"></i>
                <span>Blog Category</span>
              </a>
            </li> -->
<!--            <li>
                <a href="blog_details.php">
                <i class="fa fa-angle-double-right"></i>
                <span>Blog</span>
              </a>
            </li> -->
            <li>
                <a href="product_details.php">
                <i class="fa fa-angle-double-right"></i>
                <span>Product Details</span>
              </a>
            </li> 
            <li>
                <a href="product_rate.php">
                <i class="fa fa-angle-double-right"></i>
                <span>Product Rate</span>
              </a>
            </li> 
            <li>
                <a href="brand_details.php">
                <i class="fa fa-angle-double-right"></i>
                <span>Brand Details</span>
              </a>
            </li> 
            <li>
                <a href="model_details.php">
                <i class="fa fa-angle-double-right"></i>
                <span>Model Details</span>
              </a>
            </li> 
            <li>
                <a href="problem_details.php">
                <i class="fa fa-angle-double-right"></i>
                <span>Problem Details</span>
              </a>
            </li> 
            <li>
                <a href="employee_details.php">
                <i class="fa fa-angle-double-right"></i>
                <span>Employee Details</span>
              </a>
            </li> 
            <li>
                <a href="privileges_details.php">
                <i class="fa fa-angle-double-right"></i>
                <span>Privileges Details</span>
              </a>
            </li> 
            <li>
                <a href="roll_type.php">
                <i class="fa fa-angle-double-right"></i>
                <span>Roll Type</span>
              </a>
            </li> 
            <li>
                <a href="rolls_details.php">
                <i class="fa fa-angle-double-right"></i>
                <span>Rolls Details</span>
              </a>
            </li> 
            <li>
                <a href="charge_details.php">
                <i class="fa fa-angle-double-right"></i>
                <span>Charge Details</span>
              </a>
            </li> 
            <li>
                <a href="spare_details.php">
                <i class="fa fa-angle-double-right"></i>
                <span>Spare Details</span>
              </a>
            </li> 
            <li>
              <a href="user_data.php">
                <i class="fa fa-angle-double-right"></i>
                <span>User Details</span>
              </a>
            </li> 
            <li>
                <a href="registered_user.php">
                <i class="fa fa-angle-double-right"></i>
                <span>Registered User</span>
              </a>
            </li> 
            
            <li>
                <a href="order_details.php">
                <i class="fa fa-angle-double-right"></i>
                <span>Order Details</span>
              </a>
            </li> 
            <li>
                <a href="engg_details.php">
                <i class="fa fa-angle-double-right"></i>
                <span>Engineer Details</span>
              </a>
            </li> 
            <li>
                <a href="lab_management.php">
                <i class="fa fa-angle-double-right"></i>
                <span>Lab Management</span>
              </a>
            </li> 
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
         </ul>
        </section>
        <!-- /.sidebar -->
      </aside>