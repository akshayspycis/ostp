<script>var user_type;var employees_id;
    privileges_details_system={}
    function loadEmployeePrivileges(employee_id,callback){
        if(employee_id==undefined){
            window.location='logoutadmin.php'
        }
             if (typeof(Storage) !== "undefined") {
                if(localStorage.getItem("privileges_details_system")==null){
                    $.ajax({
                        type:"post",
                        url:"../server/controller/privileges_details/SelPrivilegesDetails.php",
                        data:{'employee_id':employee_id},
                        success: function(data) { 
                            $.each(jQuery.parseJSON(data), function (index, article) {
                                privileges_details_system[article.module_id]=article;    
                            });
                            localStorage.setItem("privileges_details_system", JSON.stringify(privileges_details_system));
                            callback();
                        }
                     });
                }else{
                    privileges_details_system=jQuery.parseJSON(localStorage.getItem("privileges_details_system"));
                    callback();
                }
            }
    }
    function showAccessDeniedModel(){
        $("#privileges_details_model").modal('hide');
        $("#privileges_details_model").modal('show');
    }
</script>
<?php 
 session_start();
    if(!isset($_SESSION['user_id'])){
             header("Location:../index.php");  
    }else{
        switch ($_SESSION['user_type']){
            case 'normal':
            { header("Location:../index.php");
                break;
            }
          }
          ?><script> 
              user_type="<?php echo $_SESSION['user_type'] ?>";
              employees_id=<?php echo $_SESSION['employee_id'] ?>;
//              onloadPrivileges(employees_id);
          </script>
<?php } ?>
          <div class="modal fade" id="privileges_details_model" role="dialog" style="display: none;">
        <div class="modal-dialog modal-sm">
                
        <!-- Modal content-->
        <div class="modal-content">
             
         <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="color:red;font-weight:bold">Error message </h4>
        </div>
          
        <div class="modal-body">
        <div class="row">
        <div class="col-md-12">
            <p class="msg">Access Denied You don't have permission to access this.Please contact with system administrator.</p>
         </div>
        </div>
        <div class="modal-footer">
        <button type="button" id="myBtn" class="btn btn-default" data-dismiss="modal">Cancel</button>
        
        </div>
        </div>
    
        </div>
        </div>
       </div>