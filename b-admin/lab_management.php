
                      <?php include './includes/check_session.php';?>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Lab Management</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <?php include 'includes/links.php';?>
   
        <script type="text/javascript">
            var orderid;
         var type=0
         cate={}
         $(document).ready(function() {
             loadEmployeePrivileges(employees_id,callback);
         });  
         function callback(){
             if(user_type=="admin" || privileges_details_system[16]["selectm"]=="true"){
                 if(user_type=="admin"){
                     showProduct('');
                 }else{
                     showProduct(employees_id);
                 }
                 
             }else{
                 showAccessDeniedModel();
             }
         }
         
        </script> 
  </head>
  <!-- ADD THE CLASS sidedar-collapse TO HIDE THE SIDEBAR PRIOR TO LOADING THE SITE -->
 <body class="wysihtml5-supported skin-yellow sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">

   <?php include 'includes/header.php';?>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
   <?php include 'includes/sidepanel.php';?>

      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Lab Management
            <small>Admin panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i> Home</li>
            <li class="active">Lab Details</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
           
          <div class="row">
            <div class="col-xs-12">
                <div class="box">
                
                <div class="box-body">
                  <div class="table-responsive"> 
                   <table id="data" class="table table-bordered table-hover ">
                    <thead>
                       <tr>
                        <th>S.N.</th>
                        <th>Order No</th>
                        <th>Product Pic</th>
                        <th>Product Name</th>
                        <th>Brand Name</th>
                        <th>Model </th>
                        <th>Problem</th>
                        <th>Description</th>
                        <th>User Problem Description</th>
                        <th>Reason for Under Lab</th>
                        <th>Receive Date</th>
                        <th>Return Date</th>
<!--                        <th>Address</th>
                         <th>Your Time</th>
                        <th>Your Date</th>
                        <th>Customer Time</th>
                        <th>Customer Date</th>
                        <th>Engineer Status</th>-->
                        <th colspan="2">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                   
                  </table>
                  </div>         
                
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section>
      </div><!-- /.content-wrapper -->

     <?php include 'includes/footer.php';?>

     
    </div><!-- ./wrapper -->
    <?php include 'includes/jslinks.php';?>
    <!--Insert Category Insert Modal Start-->
 <div class="modal fade" id="showModal" role="dialog">
     <div class="modal-dialog modal-sm" style="max-width: 1600px ;">
        <!-- Modal content-->
        <div class="modal-content">
         <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Check Product Condition</h4>
        </div>
        <div class="box">
                <div class="box-body">
                      <div class="row">
                      <div class="col-md-6 col-xs-6" >
                        <div class="form-group">
                            <a id="tab_1"  class="btn btn-default col-md-12">Add Extra Charge</a>
                        </div>
                      </div>
                      <div class="col-md-6 col-xs-6" >
                        <div class="form-group " >
                            <a id="tab_2" class="btn btn-default col-md-12">Add Spare</a>
                        </div>
                      </div>
                  </div>
                    <div class="box" id="tab_1_box" style="margin-top:10px; display: none;" >
                      <div class="box-header">
                          <center><h5 class="box-title"><b>Add Extra Charge</b></h5></center>
                      </div>
                      <form id ="insExtraCharge">    
                          <div class="box-body">
                                    <div class="row">
                                    <div class="col-md-12" >
                                    <div class="form-group ">
                                    <label>Reason Message</label>
                                    <textarea name="charge_reason" id="charge_reason" value=""  class="form-control"></textarea>
                                    </div>
                                    </div><!-- /.col -->
                                    <div class="col-md-12" >
                                    <div class="form-group ">
                                    <label>Extra Charge</label>
                                    <input type="number" name="amount" id="amount" value=""  class="form-control"/>
                                    <input type="hidden" name="order_product_id" id="order_product_id" value=""  class="form-control"/>
                                    <input type="hidden" name="lab_management_id" id="lab_management_id" value=""  class="form-control"/>
                                    </div>
                                    </div><!-- /.col -->
                                    </div><!-- /.box -->

                          </div>
                           <div class="box-footer">
                                    <div class="validate pull-left" style="font-weight:bold;"></span></div>
                                    <button type="submit"  class="btn btn-primary">Update</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                           </div>
                        </form>
                    </div>
                    <div class="box" id="tab_2_box" style="margin-top: 10px;display: none;" >
                      <div class="box-header">
                          <center><h5 class="box-title">Check Product Condition : <b>Under Lab</b></h5></center>
                      </div>
                      <form id ="insSpare">    
                          <div class="box-body">
                                    <div class="row">
                                    <div class="col-md-12" >
                                    <div class="form-group ">
                                     <label>Select Spare</label>
                                        <select class="form-control" id="spare_details_id" name="spare_details_id"></select>
                                    </div>
                                    </div><!-- /.col -->
                                    <div class="col-md-12" >
                                    <div class="form-group ">
                                    <label>Reason Message</label>
                                    <textarea name="spare_reason" id="spare_reason" value=""  class="form-control"></textarea>
                                    </div>
                                    </div><!-- /.col -->
                                    <div class="col-md-12" >
                                    <div class="form-group ">
                                    <input type="hidden" name="order_product_id" id="order_product_id" value=""  class="form-control"/>
                                    <input type="hidden" name="lab_management_id" id="lab_management_id" value=""  class="form-control"/>
                                    </div>
                                    </div><!-- /.col -->
                                    </div><!-- /.box -->

                          </div>
                           <div class="box-footer">
                                    <div class="validate pull-left" style="font-weight:bold;"></span></div>
                                    <button type="submit"  class="btn btn-primary">Update</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                           </div>
                        </form>
                    </div>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
        </div>
        </div>
       </div>
 <div class="modal fade" id="showEnggAllotDetailsModal" role="dialog">
     <div class="modal-dialog modal-lg" style="max-width: 1600px ;">
        <!-- Modal content-->
        <div class="modal-content">
         <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Assigned Employee</h4>
        </div>
        <div class="box">
                
                <div class="box-body">
                  <div class="table-responsive"> 
                   <table id="order_allot_details" class="table table-bordered table-hover ">
                    <thead>
                       <tr>
                        <th>S.N.</th>
                        <th>User Pic</th>
                        <th>User Name</th>
                        <th>Contact No</th>
                        <th>Alt Contact No</th>
                        <th colspan="2">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                   
                  </table>
                  </div>         
                
                </div><!-- /.box-body -->
              </div><!-- /.box -->
        </div>
        </div>
       </div>
 <div class="modal fade" id="showSummary" role="dialog">
     <div class="modal-dialog modal-lg" style="max-width: 1600px ;">
        <!-- Modal content-->
        <div class="modal-content">
         <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Show Summary</h4>
        </div>
            <div class="modal-body">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_3" data-toggle="tab">Extra Charge in Product</a></li>
              <li><a href="#tab_4" data-toggle="tab">Spare Details in Product</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_3">
                  <div class="box">
                <div class="box-body">
                  <div class="table-responsive"> 
                   <table id="order_lab_extra_charge" class="table table-bordered table-hover ">
                    <thead>
                       <tr>
                        <th>S.N.</th>
                        <th>Date</th>
                        <th>Charge Reason</th>
                        <th>Amount</th>
                        <th>User Status</th>
                        <th colspan="2">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                   
                  </table>
                  </div>         
                
                </div><!-- /.box-body -->
              </div><!-- /.box -->
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_4">
                  <div class="box">
                <div class="box-body">
                  <div class="table-responsive"> 
                   <table id="order_lab_spare" class="table table-bordered table-hover ">
                    <thead>
                       <tr>
                        <th>S.N.</th>
                        <th>Date</th>
                        <th>Spare Name</th>
                        <th>User Call</th>
                        <th>User Sms</th>
                        <th>Spare Reason</th>
                        <th>User Status</th>
                        <th colspan="2">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                   
                  </table>
                  </div>         
                
                </div><!-- /.box-body -->
              </div><!-- /.box -->
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
            </div>
        </div>
        </div>
       </div>
      <!--Insert Category Insert Modal End-->
       <!--Insert Category Edit Modal Start-->
<div class="modal fade" id="updModel" role="dialog">
<div class="modal-dialog modal-sm">

<!-- Modal content-->
<div class="modal-content">

<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Edit User Problem Description</h4>
</div>
<form id ="updEnggDetails">    
<div class="modal-body">
<div class="row">
<div class="col-md-12">
<div class="form-group">
    <input type="hidden"  name="order_product_id" id="order_product_id" value="" class="form-control"/>
    <textarea name="editamt" id="editamt" value="" class="form-control"></textarea>
    
</div>
<!-- /.form-group -->

</div><!-- /.col -->

</div><!-- /.box -->


<div class="modal-footer">
<div class="validate pull-left" style="font-weight:bold;"></span></div>
<button type="submit"  class="btn btn-primary">Update</button>
<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>

</div>
</div>
</form>
</div>
</div>
</div>
<div class="modal fade" id="insModel" role="dialog">
<div class="modal-dialog modal-sm">

<!-- Modal content-->
<div class="modal-content">

<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Customer Meeting Time</h4>
</div>
<form id ="insEnggAllotDetails">    
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <input type="hidden"  name="order_id" id="order_id" value="" class="form-control"/>
                    <input type="hidden"  name="order_allot_detailsId" id="order_allot_detailsId" value="" class="form-control"/>
                    <input type="hidden"  name="employee_id" id="employee_id" value="" class="form-control"/>
                    <input type="hidden"  name="order_product_id" id="order_product_id" value="" class="form-control"/>
                </div>
            </div><!-- /.col -->
            <div class="col-md-12" id="cus_time_details" >
                <div class="form-group ">
                    <label>Meeting Time</label>
                    <input type="hidden" name="meeting_time" id="meeting_time" value="" />
                    <input type="text" name="meeting_time_dis" id="meeting_time_dis" value="" disabled class="form-control"/>
                </div>
                <div class="form-group ">
                    <label>Meeting Date</label>
                    <input type="hidden" name="meeting_date" id="meeting_date" value="" />
                    <input type="text" name="meeting_date_dis" id="meeting_date_dis" value="" disabled class="form-control"/>
                </div>
                <div class="form-group ">
                    <label>Meeting Person</label>
                    <input type="text" name="meeting_person" id="meeting_person" value="" class="form-control"/>
                </div>
            </div><!-- /.col -->
        </div><!-- /.box -->
        <div class="modal-footer">
        <div class="validate pull-left" style="font-weight:bold;"></span></div>
        <button type="submit"  class="btn btn-primary">Update</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>

        </div>
    </div>
</form>
</div>
</div>
</div>
        <!--Insert Category Edit Modal End-->
         <!--Insert Category Delete Modal Start-->
 <div class="modal fade" id="delModel" role="dialog">
        <div class="modal-dialog modal-sm">
                
        <!-- Modal content-->
        <div class="modal-content">
             
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Delete Category </h4>
        </div>
            <form id ="delEnggDetails">    
            <div class="modal-body">
                <div class="row">
                <div class="col-md-12">
                <input type="hidden"  name="order_id" id="order_id" value="" class="form-control"/>
                <p>Sure to want to delete "<span style ="color:red" id ="catname"></span>" ?</p>
                 </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="delete2" class="btn btn-danger">Delete</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
           </form>
       </div>
       </div>
       </div>
          <!--Insert Category Delete Modal End--> 
  </body>
  <script type="text/javascript" language="javascript">
      $(document).ready(function(){
            $("#tab_1").click(function(){
                $("#tab_1_box").show();
                $("#tab_2_box").hide();
            }); 
            $("#tab_2").click(function(){
                $("#tab_2_box").show();
                $("#tab_1_box").hide();
                selSpareDetails($("#insSpare").find("#order_product_id").val());
            }); 
            
           
         }); 
         function selSpareDetails(order_product_id){
             $.ajax({
                    type:"post",
                    url:"../server/controller/spare_details/SelSpareDetailsLab.php",
                    data:{'order_product_id':order_product_id},
                    success: function(data) {
                        var duce = jQuery.parseJSON(data);
                        $("#insSpare").find("#spare_details_id").empty();
                        $.each(duce, function (index, article) {
//                            $("#product_details").find("select").append($('<option>').attr({'value':article.product_details_id}).append(article.product_name));
                            $("#insSpare").find("select").append($("<option>").attr({'value':article.spare_details_id}).append(article.spare_name+" ( "+article.spare_cost +" /-) labour cost ( "+article.lebure_cost+" /- )"));
                        });
                    }
                });
         }
         engg_details={}
         function showProduct(employees_id){
           engg_details={}
                    $.ajax({
                    type:"post",
                    url:"../server/controller/lab_management/SelLabManagement.php",
                    data:{'lab_management_id':''},
                    success: function(data) {
                        var duce = jQuery.parseJSON(data);
                        $("#data tr:has(td)").remove();
                        $.each(duce, function (index, article) {
                           engg_details[article.order_product_id]={};
                           engg_details[article.order_product_id]= article;
                           var img= "../server/"+article.icon.replace("../../", "");;
                           var btn_class="btn-primary";
                           var status="Return";
                           if(article.return_date_time!=''){
                               var btn_class="btn-danger";
                               var status="Returned";
                           }
                           $("#data").append($('<tr/>')
                                .append($('<td/>').html((index+1)))
                                .append($('<td/>').html(article.order_no))
                                .append($('<td/>').html("<img src="+img+" height=\"50\" width=\"50\"/>"))
                                .append($('<td/>').html(article.product_name))
                                .append($('<td/>').html(article.brand_name))
                                .append($('<td/>').html(article.model_name))
                                .append($('<td/>').html(article.problem))
                                .append($('<td/>').html(article.discription))
                                .append($('<td/>').html(article.problem_discription))
                                .append($('<td/>').html(article.product_reason))
                                .append($('<td/>').html(article.receive_date_time))
                                .append($('<td/>').html(article.return_date_time))
                                .append($('<td/>').html($("<button>").attr({'type':'button'}).addClass("btn "+btn_class+" btn-xs").append("&nbsp;&nbsp;"+status+"&nbsp;&nbsp;").click(function (){
                                    if(user_type=="admin" || privileges_details_system[16]["updatem"]=="true"){
                                        if(status=="Return")
                                           updLabManagementStatus(article.lab_management_id,article.order_product_id)
                                     }else{
                                         showAccessDeniedModel();
                                     }
                                })))
                                .append($('<td/>').html($("<button>").attr({'type':'button'}).addClass("btn btn-primary btn-xs").append("&nbsp;&nbsp;Show Client Details&nbsp;&nbsp;").click(function (){
                                    if(user_type=="admin" || privileges_details_system[16]["deletem"]=="true"){
                                        selOrderDetails(article.order_id)
                                     }else{
                                         showAccessDeniedModel();
                                     }
                                })))
                                .append($('<td/>').html($("<button>").attr({'type':'button'}).addClass("btn btn-default btn-xs").append("&nbsp;&nbsp;Work").click(function (){
                                    if(user_type=="admin" || privileges_details_system[16]["updatem"]=="true"){
                                        if(status!="Returned")
                                         checkProduct(article.order_product_id,article.lab_management_id,$(this))
                                     }else{
                                         showAccessDeniedModel();
                                     }
                                })))
                                .append($('<td/>').html($("<button>").attr({'type':'button'}).addClass("btn btn-default btn-xs").append("Show Summary").click(function (){
                                    if(user_type=="admin" || privileges_details_system[16]["selectm"]=="true"){
                                         showSummary(article.order_product_id,status);
                                     }else{
                                         showAccessDeniedModel();
                                     }
                                })))
                                        
                            );
                        });
                    }
                });
        }
        var check_engg_details=false;
        var check_engg_details_obj={};
        function checkEnggDetails(order_allot_detailsId){
                $.ajax({
                    type:"post",
                    url:"../server/controller/engg_details/CheckEnggDetails.php",
                    data:{'order_allot_detailsId':order_allot_detailsId},
                    success: function(data) {
                        check_engg_details_obj=jQuery.parseJSON(data);;
                        check_engg_details=true;
                    }
                });
        }
        function enggSubmit_InLab_OrderProduct(order_product_id,engg_details_id,order_id){
                $.ajax({
                    type:"post",
                    url:"../server/controller/engg_submit_in_lab_order_product/InsEnggSubmitInLabOrderProduct.php",
                    data:{'order_product_id':order_product_id,'engg_details_id':engg_details_id,'employee_id':employees_id,'order_id':order_id},
                    success: function(data) {
                        if(data.trim()=='Error'){
                            alert("Internal Server Error.");
                        }else if(data.trim()=='false'){
                            alert("You already Submit product in Lab.");
                        }else if(data.trim()=='true'){
                            alert("Product submit successfully in Lab.");
                        }else {
                            alert(data)
                        }
                    }
                });
        }
        function checkProduct(order_product_id,lab_management_id,obj){
             showCheckCondtion(order_product_id,lab_management_id,obj);
        }

        function showCheckCondtion(order_product_id,lab_management_id,obj){
                    $("#showModal").modal('show');
                    $("#showModal").on('shown.bs.modal', function(){
                          $('#insPending').off("submit");
                            $('#insExtraCharge').find("#order_product_id").val(order_product_id);
                            $('#insSpare').find("#order_product_id").val(order_product_id);
                            $('#insExtraCharge').find("#lab_management_id").val(lab_management_id);
                            $('#insSpare').find("#lab_management_id").val(lab_management_id);
                         $('#insExtraCharge').submit(function() {
                            if(this.charge_reason.value == ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please fill valid Charge Reason").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                return false;  
                           }else if(this.amount.value == ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please fill valid Amount").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                return false;  
                           }else {
                                $(".validate").removeClass("text-danger").addClass("text-success").fadeIn(100).text("Ok").prepend("<span class=\"glyphicon glyphicon-ok text-success\">").fadeOut(1000);
                                $.ajax({
                                    type:"post",
                                    url:"../server/controller/order_lab_extra_charge/InsOrderLabExtraCharge.php",
                                    data:$('#insExtraCharge').serialize(),
                                    success: function(data){ 
                                        alert(data)
                                        location.reload(true);
                                    } 
                                });
                                return false; 
                            }
                            return false;
                        });  
                         $('#insSpare').submit(function() {
                               if(this.spare_reason.value == ""){
                                    $(".validate").addClass("text-danger").fadeIn(100).text(" Please fill valid Product Reason").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                    return false;  
                               }else {
                                    $(".validate").removeClass("text-danger").addClass("text-success").fadeIn(100).text("Ok").prepend("<span class=\"glyphicon glyphicon-ok text-success\">").fadeOut(1000);
                                    $.ajax({
                                        type:"post",
                                        url:"../server/controller/order_lab_spare/InsOrderLabSpare.php",
                                        data:$('#insSpare').serialize(),
                                        success: function(data){ 
                                            alert(data)
                                            location.reload(true);
                                        } 
                                    });
                                    return false; 
                                }  
                            return false;
                        });  
                         $('#insPending').submit(function() {
                            if(this.product_reason.value == ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please fill valid Product Reason").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                return false;  
                           }else if(this.product_repair_hrs.value == ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please fill valid Repaired time in hrs").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                return false;  
                           }else {
                                $(".validate").removeClass("text-danger").addClass("text-success").fadeIn(100).text("Ok").prepend("<span class=\"glyphicon glyphicon-ok text-success\">").fadeOut(1000);
                                $.ajax({
                                    type:"post",
                                    url:"../server/controller/order_product_status/InsOrderProductStatus.php",
                                    data:$('#insPending').serialize(),
                                    success: function(data){ 
                                        alert(data)
                                        location.reload(true);
                                    } 
                                });
                                return false; 
                            }
                            return false;
                        });  
    $('#myModal').on('hide.bs.modal', function() {
            location.reload(true);
    });
                   
            var modal = this;
            var hash = modal.id;
            window.location.hash = hash;
            window.onhashchange = function() {
                    if (!location.hash){
                            $(modal).modal('hide');
                    }
            }
      });  
    $('#myModal').on('hide.bs.modal', function() {
            location.reload(true);
    });
        
                  }
        order_lab_extra_charge={}
        order_lab_spare={}
        function showSummary(order_product_id,status_pre){
           order_lab_extra_charge={}
           $("#showSummary").modal('show');
           $("#showSummary").on('shown.bs.modal', function(){
                    $.ajax({
                    type:"post",
                    url:"../server/controller/order_lab_extra_charge/SelOrderLabExtraCharge.php",
                    data:{'order_product_id':order_product_id},
                    success: function(data) {
                        var duce = jQuery.parseJSON(data);
                        $("#order_lab_extra_charge tr:has(td)").remove();
                        $.each(duce, function (index, article) {
                           order_lab_extra_charge[article.order_lab_extra_charge_id]={};
                           order_lab_extra_charge[article.order_lab_extra_charge_id]= article;
                           var btn_class="btn-success";
                           var status="Apporved";
                           if(article.status=="false"){
                               btn_class="btn-danger";
                               status="Pending";
                           }
                           $("#order_lab_extra_charge").append($('<tr/>')
                                .append($('<td/>').html((index+1)))
                                .append($('<td/>').html(article.lab_date))
                                .append($('<td/>').html(article.charge_reason))
                                .append($('<td/>').html(article.amount))
                                .append($('<td/>').html($("<button>").attr({'type':'button'}).addClass("btn "+btn_class+" btn-xs").append("&nbsp;&nbsp;"+status+"&nbsp;&nbsp;").click(function (){
                                    if(user_type=="admin" || privileges_details_system[16]["updatem"]=="true"){
                                        if(status=="Pending"){
                                            updOrderLabExtraChargeStatus($(this),article.order_lab_extra_charge_id)
                                        }
                                     }else{
                                         showAccessDeniedModel();
                                     }
                                })))
                                .append($('<td/>').html($("<button>").attr({'type':'button'}).addClass("btn btn-danger btn-xs").append("&nbsp;&nbsp;Delete&nbsp;&nbsp;").click(function (){
                                    if(user_type=="admin" || privileges_details_system[16]["deletem"]=="true"){
                                        if(status_pre!="Returned")
                                        delOrderProductStatus(article.order_lab_extra_charge_id)
                                     }else{
                                         showAccessDeniedModel();
                                     }
                                })))
                                        
                            );
                        });
                    }
                });
                    $.ajax({
                    type:"post",
                    url:"../server/controller/order_lab_spare/SelOrderLabSpare.php",
                    data:{'order_product_id':order_product_id},
                    success: function(data) {
                        var duce = jQuery.parseJSON(data);
                        $("#order_lab_spare tr:has(td)").remove();
                        $.each(duce, function (index, article) {
                           order_lab_spare[article.order_lab_spare_id]={};
                           order_lab_spare[article.order_lab_spare_id]= article;
                           var btn_class="btn-success";
                           var status="Apporved";
                           if(article.user_status=="false"){
                               btn_class="btn-danger";
                               status="Pending";
                           }
                           $("#order_lab_spare").append($('<tr/>')
                                .append($('<td/>').html((index+1)))
                                .append($('<td/>').html(article.lab_spare_date))
                                .append($('<td/>').html(article.spare_name))
                                .append($('<td/>').html(article.call_user))
                                .append($('<td/>').html(article.sms))
                                .append($('<td/>').html(article.spare_reason))
                                .append($('<td/>').html($("<button>").attr({'type':'button'}).addClass("btn "+btn_class+" btn-xs").append("&nbsp;&nbsp;"+status+"&nbsp;&nbsp;").click(function (){
                                    if(user_type=="admin" || privileges_details_system[16]["updatem"]=="true"){
                                        if(status=="Pending"){
                                            updOrderLabSpareStatus($(this),article.order_lab_spare_id)
                                        }
                                     }else{
                                         showAccessDeniedModel();
                                     }
                                })))
                                .append($('<td/>').html($("<button>").attr({'type':'button'}).addClass("btn btn-danger btn-xs").append("&nbsp;&nbsp;Delete&nbsp;&nbsp;").click(function (){
                                    if(user_type=="admin" || privileges_details_system[16]["deletem"]=="true"){
                                        if(status_pre!="Returned")
                                            delOrderLabSpare(article.order_lab_spare_id)
                                     }else{
                                         showAccessDeniedModel();
                                     }
                                })))
                                        
                            );
                        });
                    }
                });
          
      });  
    $('#showSummary').on('hide.bs.modal', function() {
            location.reload(true);
    });
        
                  }
                  order_details={}
        function selOrderDetails(order_id){
                  order_details={}
           $("#showEnggAllotDetailsModal").modal('show');
           $("#showEnggAllotDetailsModal").on('shown.bs.modal', function(){
                    $.ajax({
                    type:"post",
                    url:"../server/controller/order_details/SelOrderDetails.php",
                    data:{'order_id':order_id},
                    success: function(data) {
                        var duce = jQuery.parseJSON(data);
                        $("#order_allot_details tr:has(td)").remove();
                        $.each(duce, function (index, article) {
                           order_allot_details[order_id]={};
                           order_allot_details[order_id]= article;
                           var img= "../server/"+article.pic.replace("../../", "");;
                           var btn_class="btn-danger";
                           var status="Accept";
                           if(article.status=="true"){
                               btn_class="btn-success";
                               status="Cancel";
                           }
                           $("#order_allot_details").append($('<tr/>')
                                .append($('<td/>').html((index+1)))
                                .append($('<td/>').html("<img src="+img+" height=\"50\" width=\"50\"/>"))
                                .append($('<td/>').html(article.user_name))
                                .append($('<td/>').html(article.contact_no))
                                .append($('<td/>').html(article.alt_contact_no))
                                .append($('<td/>').html($("<button>").attr({'type':'button'}).addClass("btn btn-danger btn-xs").append("&nbsp;&nbsp;Delete&nbsp;&nbsp;").click(function (){
//                                    if(user_type=="admin" || privileges_details_system[16]["deletem"]=="true"){
//                                        delEnggAllotDetails()
//                                     }else{
//                                         showAccessDeniedModel();
//                                     }
                                })))
                                        
                            );
                        });
                    }
                });
            var modal = this;
            var hash = modal.id;
            window.location.hash = hash;
            window.onhashchange = function() {
                    if (!location.hash){
                            $(modal).modal('hide');
                    }
            }
      });  
    $('#myModal').on('hide.bs.modal', function() {
            location.reload(true);
    });
        
                  }
         
            function updOrderLabSpareStatus(obj,id){
                      var value;
                      if(obj.text().trim()=="Cancel"){
                                value="false";
                                obj.empty();
                                obj.append("Pending");
                                obj.removeClass("btn-success");
                                obj.addClass("btn-danger");
                      }else{
                                value="true";
                                obj.empty();
                                obj.append("Apporved");
                                obj.removeClass("btn-danger");
                                obj.addClass("btn-success");
                      }           
                  $.ajax({
                    type:"post",
                    url:"../server/controller/order_lab_spare/UpdOrderLabSpareStatus.php",
                    data:{'order_lab_spare_id':id,'status':value},
                    success: function(data) {
                        alert(data)
                    }
                  });
              }
            function updOrderLabExtraChargeStatus(obj,id){
                      var value;
                      if(obj.text().trim()=="Cancel"){
                                value="false";
                                obj.empty();
                                obj.append("Pending");
                                obj.removeClass("btn-success");
                                obj.addClass("btn-danger");
                      }else{
                                value="true";
                                obj.empty();
                                obj.append("Apporved");
                                obj.removeClass("btn-danger");
                                obj.addClass("btn-success");
                      }           
                  $.ajax({
                    type:"post",
                    url:"../server/controller/order_lab_extra_charge/UpdOrderLabExtraChargeStatus.php",
                    data:{'order_lab_extra_charge_id':id,'status':value},
                    success: function(data) {
                    }
                  });
              }
            function updLabManagementStatus(lab_management_id,order_product_id){
                  $.ajax({
                    type:"post",
                    url:"../server/controller/lab_management/UpdLabManagement.php",
                    data:{'lab_management_id':lab_management_id,'order_product_id':order_product_id},
                    success: function(data) {
                        alert(data);
                        location.reload(true);
                    }
                  });
              }
            function updatestatus(obj,id){
                      var value;
                      if(obj.text().trim()=="Cancel"){
                                value="false";
                                obj.empty();
                                obj.append("Accepted");
                                obj.removeClass("btn-success");
                                obj.addClass("btn-danger");
                      }else{
                                value="true";
                                obj.empty();
                                obj.append("Accept");
                                obj.removeClass("btn-danger");
                                obj.addClass("btn-success");
                      }           
                  $.ajax({
                    type:"post",
                    url:"../server/controller/order_allot_details/UpdOrderAllotDetails.php",
                    data:{'order_allot_detailsId':id,'status':value},
                    success: function(data) {
                    }
                  });
              }
              
                    function updOrderProduct(order_product_id) {
                      $("#updModel").modal('show');
                      $("#updModel").on('shown.bs.modal', function(){
                            $("#updEnggDetails").find("#order_product_id").val(order_product_id);  
                            $("#updEnggDetails").find("#editamt").val(engg_details[order_product_id]["problem_discription"]);          
                            $('#updEnggDetails').off("submit");
                            $('#updEnggDetails').submit(function() {
                                $(".validate").removeClass("text-danger").addClass("text-success").fadeIn(100).text("Ok").prepend("<span class=\"glyphicon glyphicon-ok text-success\">").fadeOut(1000);
                                $.ajax({
                                    type:"post",
                                    url:"../server/controller/order_product/UpdOrderProduct.php",
                                    data:$('#updEnggDetails').serialize(),
                                    success: function(data){ 
                                         location.reload(true);
                                    } 
                                });
                            
                        return false;
                    });
                     var modal = this;
                            var hash = modal.id;
                            window.location.hash = hash;
                            window.onhashchange = function() {
                                    if (!location.hash){
                                            $(modal).modal('hide');
                                    }
                            }
                      });  
                    $('#editEnggDetails').on('hide.bs.modal', function() {
                            location.reload(true);
                    });
                }

</script>

<script type="text/javascript" language="javascript">
        function delEnggDetails(order_id) {
            
            $("#delModel").modal('show');
                 var order_name = cate[order_id]["order_no"];;
                 $("#delModel").on('shown.bs.modal', function(){
                    $("#delEnggDetails").find("#order_id").val(order_id);        
                    $("#delEnggDetails").find("#catname").text(order_name); 
                    $('#delEnggDetails').off("submit");
                    $('#delEnggDetails').submit(function() {
                         $.ajax({
                            type:"post",
                            url:"../server/controller/order_details/delEnggDetails.php",
                            data:{'order_id':order_id},
                            success: function(data){ 
                                location.reload(true);
                            } 
                  });
                    return false;
    });
    
    });
   
}

        function delEnggProduct(engg_details_id) {
            
            $("#delModel").modal('show');
                 var order_name = engg_details[engg_details_id]["product_name"]+" "+engg_details[engg_details_id]["brand_name"]+" "+engg_details[engg_details_id]["model_name"]+" "+engg_details[engg_details_id]["problem"]+" "+engg_details[engg_details_id]["discription"]+" "+engg_details[engg_details_id]["problem_discription"];
                 $("#delModel").on('shown.bs.modal', function(){
                    $("#delEnggDetails").find("#catname").text(order_name); 
                    $('#delEnggDetails').off("submit");
                    $('#delEnggDetails').submit(function() {
                         $.ajax({
                            type:"post",
                            url:"../server/controller/engg_details/DelEnggProduct.php",
                            data:{'engg_details_id':engg_details_id},
                            success: function(data){ 
                                alert(data)
                                location.reload(true);
                            } 
                  });
                    return false;
    });
    
    });
   
}

        function delOrderLabSpare(order_lab_spare_id) {
            $("#delModel").modal('show');
                 var order_name = order_lab_spare[order_lab_spare_id]["spare_reason"]+" ";
                 $("#delModel").on('shown.bs.modal', function(){
                    $("#delEnggDetails").find("#catname").text(order_name); 
                    $('#delEnggDetails').off("submit");
                    $('#delEnggDetails').submit(function() {
                         $.ajax({
                            type:"post",
                            url:"../server/controller/order_lab_spare/DelOrderLabSpare.php",
                            data:{'order_lab_spare_id':order_lab_spare_id},
                            success: function(data){ 
                                location.reload(true);
                            } 
                  });
                    return false;
    });
    
    });
   
} 

        function delOrderProductStatus(order_lab_extra_charge_id) {
            $("#delModel").modal('show');
                 var order_name = order_lab_extra_charge[order_lab_extra_charge_id]["charge_reason"]+" ";
                 $("#delModel").on('shown.bs.modal', function(){
                    $("#delEnggDetails").find("#catname").text(order_name); 
                    $('#delEnggDetails').off("submit");
                    $('#delEnggDetails').submit(function() {
                         $.ajax({
                            type:"post",
                            url:"../server/controller/order_lab_extra_charge/DelOrderLabExtraCharge.php",
                            data:{'order_lab_extra_charge_id':order_lab_extra_charge_id},
                            success: function(data){
                                location.reload(true);
                            } 
                        });
                        return false;
                    });
                });
        } 
        function delEnggAllotDetails(order_allot_detailsId) {
            
            $("#delModel").modal('show');
                 var order_name = order_allot_details[order_allot_detailsId]["user_name"]+" from assigned order ";
                 $("#delModel").on('shown.bs.modal', function(){
                    $("#delEnggDetails").find("#catname").text(order_name); 
                    $('#delEnggDetails').off("submit");
                    $('#delEnggDetails').submit(function() {
                         $.ajax({
                            type:"post",
                            url:"../server/controller/order_allot_details/DelEnggAllotDetails.php",
                            data:{'order_allot_detailsId':order_allot_detailsId},
                            success: function(data){ 
                                location.reload(true);
                            } 
                  });
                    return false;
    });
    
    });
   
}

function insEnggAllotDetails(engg_details_id){
           $("#insModel").modal('show');
           $("#insModel").on('shown.bs.modal', function(){
           $("#insModel").find("#engg_details_id").val(engg_details_id); 
           $("#insModel").find("#order_id").val(orderid); 
           $("#insModel").find("#insEnggAllotDetails").find("#call_cus").find("#cus_contact_no").html(cate[orderid].contact_no);
           $("#insModel").find("#insEnggAllotDetails").find("#call_cus_alt").find("#cus_alt_contact_no").html(cate[orderid].alt_contact_no);
           loadRolls();
           var modal = this;
           var hash = modal.id;
           window.location.hash = hash;
           window.onhashchange = function() {
                if (!location.hash){
                        $(modal).modal('hide');
                }
           }
      });  
     $('#insEnggAllotDetails').off("submit");
                            $('#insEnggAllotDetails').submit(function() {
                            if(this.employee_id.value == ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please Select valid Employee ").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                return false;  
                           }else if(this.engg_time.value == ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please Select valid Employee time").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                return false;  
                           }else if(this.engg_date.value == ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please Select valid Employee Date").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                return false;  
                           }else if(this.cus_time.value == ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please Select valid Customer time").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                return false;  
                           }else if(this.cus_date.value == ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please Select valid Customer Date").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                return false;  
                           }else {
                                $(".validate").removeClass("text-danger").addClass("text-success").fadeIn(100).text("Ok").prepend("<span class=\"glyphicon glyphicon-ok text-success\">").fadeOut(1000);
                                $.ajax({
                                    type:"post",
                                    url:"../server/controller/order_allot_details/InsEnggAllotDetails.php",
                                    data:$('#insEnggAllotDetails').serialize(),
                                    success: function(data){ 
                                        alert(data)
                                        location.reload(true);
                                    } 
                                });
                                return false; 
                            }
                   return false;
                    });  
    $('#myModal').on('hide.bs.modal', function() {
            location.reload(true);
    });
}

function loadRolls(){
             $.ajax({
                    type:"post",
                    url:"../server/controller/roll_type/SelRollType.php",
                    success: function(data) {
                        var duce = jQuery.parseJSON(data);
                        $("#insModel").find("#insEnggAllotDetails").find("#category").find("select").empty();
                        $.each(duce, function (index, article) {
                           $("#insModel").find("#insEnggAllotDetails").find("#category").find("select").append($('<option>').attr({'value':article.roll_type_id}).append(article.roll_type));
                        });
                        $("#insModel").find("#insEnggAllotDetails").find("#category").find("select").trigger("change");
                    }
                });
             $("#category").find("select").change(function () {
                roll_type_id=$(this).val();
                onloadUser(roll_type_id);
             }); 
         }
</script>
<script>
    employee_details={};
    function onloadUser(roll_type_id){
             employee_details={};
             $.ajax({
                type:"post",
                url:"../server/controller/rolls_details/SelRollsDetails.php",
                data:{'roll_type_id':roll_type_id},
                success: function(data) { 
                var duce = jQuery.parseJSON(data); //here data is passed to js object
                employee_details={}
                var inde=0;
                $("#user_details_first").empty();
                $("#user_details").empty();
                $.each(duce, function (index, article) {
                        employee_details[article.employee_id]=article;    
                        if(inde==0){
                            $("#user_details_first").append('<img src="../server/controller/user_details/'+article.pic+'" width="50" height="50" >'+article.user_name+'<span class="glyphicon glyphicon-chevron-down"></span>');
                            employee_id=article.employee_id;
                            inde++;
                        }
                        $("#user_details").append($('<li><a href="#"><img src="../server/controller/user_details/'+article.pic+'" width="50" height="50">'+article.user_name+'</a></li>').click(function (event){
                            employee_id=article.employee_id;
                            $("#insModel").find("#insEnggAllotDetails").find("#employee_id").val(employee_id);
                            $("#insModel").find("#insEnggAllotDetails").find("#call_eng").find("#emp_contact_no").html(article.contact_no);
                            $("#insModel").find("#insEnggAllotDetails").find("#call_eng").attr('disabled',false);
                            $("#user_details_first").html($(this).find("a").html()).append('<span class="glyphicon glyphicon-chevron-down"></span>');
                             event.preventDefault(); // ca
                        }));
                });
                }
            });
         }
</script>
</html>

