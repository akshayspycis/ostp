<?php include './includes/check_session.php';?>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Model Details</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <?php include 'includes/links.php';?>
   
        <script type="text/javascript">
         var brand_details_id=0
         var product_details_id=0
         cate={}
         $(document).ready(function() {
             $("#category").find("select").change(function () {
                brand_details_id=$(this).val();
                product_details_id=$("#product_details").find("select").val();
                onlad(brand_details_id,product_details_id);
             }); 
             $("#product_details").find("select").change(function () {
                 $("#category").find("select").trigger("change");
             }); 
             loadEmployeePrivileges(employees_id,callback);
         });  
         function callback(){
             if(user_type=="admin" || privileges_details_system[8]["selectm"]=="true"){
                 loadProduct();
             }else{
                 showAccessDeniedModel();
             }
         }
         function loadBrand(){
             $.ajax({
                    type:"post",
                    url:"../server/controller/brand_details/SelBrandDetails.php",
                    success: function(data) {
                        var duce = jQuery.parseJSON(data);
                        $("#category").find("select").empty();
                        $.each(duce, function (index, article) {
                           $("#category").find("select").append($('<option>').attr({'value':article.brand_details_id}).append(article.brand_name));
                        });
                        $("#category").find("select").trigger("change");
                    }
                });
         }
         
         function loadProduct(){
             $.ajax({
                    type:"post",
                    url:"../server/controller/product_details/SelProductDetails.php",
                    success: function(data) {
                        var duce = jQuery.parseJSON(data);
                        $("#product_details").find("select").empty();
                        $.each(duce, function (index, article) {
                           $("#product_details").find("select").append($('<option>').attr({'value':article.product_details_id}).append(article.product_name));
                        });
                        loadBrand();
                    }
                });
         }
         
        </script> 
  </head>
  <!-- ADD THE CLASS sidedar-collapse TO HIDE THE SIDEBAR PRIOR TO LOADING THE SITE -->
 <body class="wysihtml5-supported skin-yellow sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">

   <?php include 'includes/header.php';?>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
   <?php include 'includes/sidepanel.php';?>

      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Model Details
            <small>Admin panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i> Home</li>
            <li class="active">Model Details</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
           
          <div class="row">
            <div class="col-xs-12">
                <div class="box">
                <div class="box-header">
                  <div class="pull-left col-md-12">
                  <div class="col-md-3">
                      <div class="form-group" id="product_details">
                            <label>Select Product</label>
                            <select class="form-control" id="product_details_id" name="product_details_id">
                            </select>
                      </div><!-- /.form-group -->    
                  </div>
                  <div class="col-md-3">
                      <div class="form-group" id="category">
                            <label>Select Brand</label>
                            <select class="form-control" id="brand_details_id" name="brand_details_id">
                            </select>
                        </div><!-- /.form-group -->    
                  </div>
                      <div class="pull-right">
                      <br>
                      <button type="button" class="btn btn-success" id="add">Add +</button>
                  </div>
                  </div>
                  
                </div><!-- /.box-header -->
                
                <div class="box-body">
                  <div class="table-responsive"> 
                   <table id="data" class="table table-bordered table-hover ">
                    <thead>
                      <tr>
                        <th>S.N.</th>
                        <th>Model</th>
                        <th colspan="2">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                   
                  </table>
                  </div>         
                
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section>
      </div><!-- /.content-wrapper -->

     <?php include 'includes/footer.php';?>

     
    </div><!-- ./wrapper -->
    <?php include 'includes/jslinks.php';?>
    <!--Insert Category Insert Modal Start-->
 <div class="modal fade" id="insModal" role="dialog">
        <div class="modal-dialog modal-sm">
        <!-- Modal content-->
        <div class="modal-content">
         <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Insert Model Details </h4>
        </div>
        <form id ="insModelDetails">    
        <div class="modal-body">
        <div class="row">
        
        <div class="col-md-12">
        <div class="form-group">
            <input type="hidden"  name="brand_details_id" id="brand_details_id" >
            <input type="hidden"  name="product_details_id" id="product_details_id" >
            <input type="text"  name="model_name" id="model_name" class="form-control"  placeholder="Enter Model">
        </div>
        <!-- /.form-group -->
        </div><!-- /.col -->
        </div><!-- /.box -->
        <div class="modal-footer">
            <div class="validate pull-left" style="font-weight:bold;"></span></div>
            <button type="submit" class="btn btn-primary">Submit</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        </div>
        </div>
       </form>
        </div>
        </div>
       </div>
      <!--Insert Category Insert Modal End-->
       <!--Insert Category Edit Modal Start-->
<div class="modal fade" id="updModel" role="dialog">
<div class="modal-dialog modal-sm">

<!-- Modal content-->
<div class="modal-content">

<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Edit Model </h4>
</div>
<form id ="updModelDetails">    
<div class="modal-body">
<div class="row">

<div class="col-md-12">
<div class="form-group">
    <input type="hidden"  name="model_details_id" id="model_details_id" value="" class="form-control"/>
    <input type="text"  name="editamt" id="editamt" value="" class="form-control"  placeholder="Enter Category Name">
</div>
<!-- /.form-group -->

</div><!-- /.col -->
</div><!-- /.box -->


<div class="modal-footer">
<div class="validate pull-left" style="font-weight:bold;"></span></div>
<button type="submit" id ="update" class="btn btn-primary">Update</button>
<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>

</div>
</div>
</form>
</div>
</div>
</div>
        <!--Insert Category Edit Modal End-->
         <!--Insert Category Delete Modal Start-->
 <div class="modal fade" id="delModel" role="dialog">
        <div class="modal-dialog modal-sm">
                
        <!-- Modal content-->
        <div class="modal-content">
             
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Delete Category </h4>
        </div>
        <form id ="delModelDetails">    
        <div class="modal-body">
            <div class="row">
            <div class="col-md-12">
            <input type="hidden"  name="model_details_id" id="model_details_id" value="" class="form-control"/>
            <p>Sure to want to delete "<span style ="color:red" id ="catname"></span>" Amount?</p>
             </div>
            </div>
            <div class="modal-footer">
            <button type="submit" id="delete2" class="btn btn-danger">Delete</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>

            </div>
        </div>
       </form>
       </div>
       </div>
       </div>
          <!--Insert Category Delete Modal End--> 
  </body>
  <script type="text/javascript" language="javascript">
      $(document).ready(function(){
            $("#add").click(function(){
                 if(user_type=="admin" || privileges_details_system[8]["insertm"]=="true"){
                    $("#insModal").modal('show');
                 }else{
                     showAccessDeniedModel();
                 }
            }); 
            $("#insModal").on('shown.bs.modal', function(){
                            $("#insModal").find("#brand_details_id").val(brand_details_id);
                            $("#insModal").find("#product_details_id").val(product_details_id);
                            $('#insModelDetails').off("submit");
                            $('#insModelDetails').submit(function() {
                            if(this.model_name.value == ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please fill valid Model ").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                $('#model_name').focus();
                                $('#insModelDetails').each(function(){
                                    this.reset();
                                    return false;
                                });
                                return false;  
                           }else {
                                $(".validate").removeClass("text-danger").addClass("text-success").fadeIn(100).text("Ok").prepend("<span class=\"glyphicon glyphicon-ok text-success\">").fadeOut(1000);
                                $.ajax({
                                    type:"post",
                                    url:"../server/controller/model_details/InsModelDetails.php",
                                    data:$('#insModelDetails').serialize(),
                                    success: function(data){ 
                                        location.reload(true);
                                    } 
                                });
                                return false; 
                            }
                   return false;
                    });
            var modal = this;
            var hash = modal.id;
            window.location.hash = hash;
            window.onhashchange = function() {
                    if (!location.hash){
                            $(modal).modal('hide');
                    }
            }
      });  
    $('#myModal').on('hide.bs.modal', function() {
            location.reload(true);
    });
         }); 
    
         function onlad(brand_details_id,product_details_id){
                    cate={}
                    $.ajax({
                    type:"post",
                    url:"../server/controller/model_details/SelModelDetails.php",
                    data:{'brand_details_id':brand_details_id,'product_details_id':product_details_id},
                    success: function(data) {
                        var duce = jQuery.parseJSON(data);
                        $("#data tr:has(td)").remove();
                        $.each(duce, function (index, article) {
                            cate[article.model_details_id]= article;
                           $("#data").append($('<tr/>')
                                .append($('<td/>').html((index+1)))
                                .append($('<td/>').html(article.model_name))
                                .append($('<td/>').html($("<button>").attr({'type':'button'}).addClass("btn btn-success btn-xs").append("&nbsp;&nbsp;Edit&nbsp;&nbsp;").click(function (){
                                    if(user_type=="admin" || privileges_details_system[8]["updatem"]=="true"){
                                        updModelDetails(article.model_details_id)
                                     }else{
                                         showAccessDeniedModel();
                                     }
                                    
                                })))
                                .append($('<td/>').html($("<button>").attr({'type':'button'}).addClass("btn btn-danger btn-xs").append("&nbsp;&nbsp;Delete&nbsp;&nbsp;").click(function (){
                                    if(user_type=="admin" || privileges_details_system[8]["deletem"]=="true"){
                                        delModelDetails(article.model_details_id)
                                     }else{
                                         showAccessDeniedModel();
                                     }
                                })))
                            );
                        });
                    }
                });
         }
                    function updModelDetails(model_details_id) {
                      $("#updModel").modal('show');
                      $("#updModel").on('shown.bs.modal', function(){
                            $("#updModelDetails").find("#model_details_id").val(model_details_id);  
                            $("#updModelDetails").find("#editamt").val(cate[model_details_id]["model_name"]);          
                            $('#updModelDetails').off("submit");
                            $('#updModelDetails').submit(function() {
                            if(this.editamt.value == ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please fill valid Amount ").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                $('#editamt').focus();
                           }else {
                                $(".validate").removeClass("text-danger").addClass("text-success").fadeIn(100).text("Ok").prepend("<span class=\"glyphicon glyphicon-ok text-success\">").fadeOut(1000);
                                $.ajax({
                                    type:"post",
                                    url:"../server/controller/model_details/UpdModelDetails.php",
                                    data:$('#updModelDetails').serialize(),
                                    success: function(data){ 
                                         alert(data)
                                         location.reload(true);
                                    } 
                                });
                            }
                        return false;
                    });
                     var modal = this;
                            var hash = modal.id;
                            window.location.hash = hash;
                            window.onhashchange = function() {
                                    if (!location.hash){
                                            $(modal).modal('hide');
                                    }
                            }
                      });  
                    $('#editModelDetails').on('hide.bs.modal', function() {
                            location.reload(true);
                    });
                }

</script>

<script type="text/javascript" language="javascript">
        function delModelDetails(model_details_id) {
            
            $("#delModel").modal('show');
                 var model_name = cate[model_details_id]["model_name"];;
                 $("#delModel").on('shown.bs.modal', function(){
                    $("#delModelDetails").find("#model_details_id").val(model_details_id);        
                    $("#delModelDetails").find("#catname").text(model_name); 
                    $('#delModelDetails').off("submit");
                    $('#delModelDetails').submit(function() {
                         $.ajax({
                            type:"post",
                            url:"../server/controller/model_details/delModelDetails.php",
                            data:{'model_details_id':model_details_id},
                            success: function(data){ 
                                location.reload(true);
                            } 
                  });
                    return false;
    });
    
    });
   
}

</script>
</html>

