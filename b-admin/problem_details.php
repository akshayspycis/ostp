<?php include './includes/check_session.php';?>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Problem Details</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <?php include 'includes/links.php';?>
   
        <script type="text/javascript">
         var brand_details_id=0
         var product_details_id=0
         var model_details_id=0
         cate={}
         $(document).ready(function() {
             $("#category").find("select").change(function () {
                model_details_id=$(this).val();
                brand_details_id=$("#brand_details").find("select").val();
                product_details_id=$("#product_details").find("select").val();
                onlad(brand_details_id,product_details_id,model_details_id);
             }); 
             $("#brand_details").find("select").change(function () {
                 loadModel();
             }); 
             $("#product_details").find("select").change(function () {
                 loadBrand();
             }); 
             loadEmployeePrivileges(employees_id,callback);
         });  
         function callback(){
             if(user_type=="admin" || privileges_details_system[9]["selectm"]=="true"){
                 loadProduct();
             }else{
                 showAccessDeniedProblem();
             }
         }
         function loadModel(){
             brand_details_id=$("#brand_details").find("select").val();
             product_details_id=$("#product_details").find("select").val();
             $.ajax({
                    type:"post",
                    url:"../server/controller/model_details/SelModelDetails.php",
                    data:{'product_details_id':product_details_id,'brand_details_id':brand_details_id},
                    success: function(data) {
                        var duce = jQuery.parseJSON(data);
                        $("#category").find("select").empty();
                        $("#category").find("select").append($('<option>').attr({'value':""}).append("Select"));
                        $.each(duce, function (index, article) {
                           $("#category").find("select").append($('<option>').attr({'value':article.model_details_id}).append(article.model_name));
                        });
                        $("#category").find("select").trigger("change");
                        
                    }
                });
         }
         function loadBrand(){
             $.ajax({
                    type:"post",
                    url:"../server/controller/brand_details/SelBrandDetails.php",
                    success: function(data) {
                        var duce = jQuery.parseJSON(data);
                        $("#brand_details").find("select").empty();
                        $("#brand_details").find("select").append($('<option>').attr({'value':""}).append("Select"));
                        $.each(duce, function (index, article) {
                           $("#brand_details").find("select").append($('<option>').attr({'value':article.brand_details_id}).append(article.brand_name));
                        });
                        $("#brand_details").find("select").trigger("change");
                    }
                });
         }
         
         
         
         function loadProduct(){
             $.ajax({
                    type:"post",
                    url:"../server/controller/product_details/SelProductDetails.php",
                    success: function(data) {
                        var duce = jQuery.parseJSON(data);
                        $("#product_details").find("select").empty();
                        $.each(duce, function (index, article) {
                           $("#product_details").find("select").append($('<option>').attr({'value':article.product_details_id}).append(article.product_name));
                        });
                        $("#product_details").find("select").trigger("change");
                    }
                });
         }
         
        </script> 
  </head>
  <!-- ADD THE CLASS sidedar-collapse TO HIDE THE SIDEBAR PRIOR TO LOADING THE SITE -->
 <body class="wysihtml5-supported skin-yellow sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">

   <?php include 'includes/header.php';?>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
   <?php include 'includes/sidepanel.php';?>

      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Problem Details
            <small>Admin panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i> Home</li>
            <li class="active">Problem Details</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
           
          <div class="row">
            <div class="col-xs-12">
                <div class="box">
                <div class="box-header">
                  <div class="pull-left col-md-12">
                  <div class="col-md-3">
                      <div class="form-group" id="product_details">
                            <label>Select Product</label>
                            <select class="form-control" >
                            </select>
                      </div><!-- /.form-group -->    
                  </div>
                  <div class="col-md-3">
                      <div class="form-group" id="brand_details">
                            <label>Select Brand</label>
                            <select class="form-control" >
                            </select>
                      </div><!-- /.form-group -->    
                  </div>
                  <div class="col-md-3">
                      <div class="form-group" id="category">
                            <label>Select Model</label>
                            <select class="form-control" >
                            </select>
                        </div><!-- /.form-group -->    
                  </div>
                      <div class="pull-right">
                      <br>
                      <button type="button" class="btn btn-success" id="add">Add +</button>
                  </div>
                  </div>
                  
                </div><!-- /.box-header -->
                
                <div class="box-body">
                  <div class="table-responsive"> 
                   <table id="data" class="table table-bordered table-hover ">
                    <thead>
                      <tr>
                        <th>S.N.</th>
                        <th>Problem</th>
                        <th>Description</th>
                        <th colspan="2">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                   
                  </table>
                  </div>         
                
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section>
      </div><!-- /.content-wrapper -->

     <?php include 'includes/footer.php';?>

     
    </div><!-- ./wrapper -->
    <?php include 'includes/jslinks.php';?>
    <!--Insert Category Insert Modal Start-->
 <div class="modal fade" id="insModal" role="dialog">
        <div class="modal-dialog modal-sm">
        <!-- Modal content-->
        <div class="modal-content">
         <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Insert Problem Details </h4>
        </div>
        <form id ="insProblemDetails">    
        <div class="modal-body">
        <div class="row">
        
        <div class="col-md-12">
        <div class="form-group">
            <input type="hidden"  name="brand_details_id" id="brand_details_id" >
            <input type="hidden"  name="model_details_id" id="model_details_id" >
            <input type="hidden"  name="product_details_id" id="product_details_id" >
            <input type="text"  name="problem" id="problem" class="form-control"  placeholder="Enter Problem">
        </div>
        <!-- /.form-group -->
        </div><!-- /.col -->
        <div class="col-md-12">
        <div class="form-group">
            <textarea name="discription" id="discription" class="form-control"  placeholder="Enter Discription"></textarea>
        </div>
        <!-- /.form-group -->
        </div><!-- /.col -->
        </div><!-- /.box -->
        <div class="modal-footer">
            <div class="validate pull-left" style="font-weight:bold;"></span></div>
            <button type="submit" class="btn btn-primary">Submit</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        </div>
        </div>
       </form>
        </div>
        </div>
       </div>
      <!--Insert Category Insert Modal End-->
       <!--Insert Category Edit Modal Start-->
<div class="modal fade" id="updProblem" role="dialog">
<div class="modal-dialog modal-sm">

<!-- Modal content-->
<div class="modal-content">

<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Edit Problem </h4>
</div>
<form id ="updProblemDetails">    
<div class="modal-body">
<div class="row">

<div class="col-md-12">
<div class="form-group">
    <input type="hidden"  name="problem_details_id" id="problem_details_id" value="" class="form-control"/>
    <input type="text"  name="problem" id="problem" value="" class="form-control"  placeholder="Enter Category Name">
</div>
<!-- /.form-group -->

</div><!-- /.col -->
<div class="col-md-12">
<div class="form-group">
    <textarea  name="discription" id="discription" value="" class="form-control"  placeholder="Enter Discription"></textarea>
</div>
<!-- /.form-group -->

</div><!-- /.col -->
</div><!-- /.box -->


<div class="modal-footer">
<div class="validate pull-left" style="font-weight:bold;"></span></div>
<button type="submit" id ="update" class="btn btn-primary">Update</button>
<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>

</div>
</div>
</form>
</div>
</div>
</div>
        <!--Insert Category Edit Modal End-->
         <!--Insert Category Delete Modal Start-->
 <div class="modal fade" id="delProblem" role="dialog">
        <div class="modal-dialog modal-sm">
                
        <!-- Modal content-->
        <div class="modal-content">
             
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Delete Problem</h4>
        </div>
        <form id ="delProblemDetails">    
        <div class="modal-body">
            <div class="row">
            <div class="col-md-12">
                <input type="hidden"  name="problem_details_id" id="problem_details_id" value="" class="form-control"/>
                <p>Sure to want to delete "<span style ="color:red" id ="catname"></span>" Amount?</p>
             </div>
            </div>
            <div class="modal-footer">
            <button type="submit" id="delete2" class="btn btn-danger">Delete</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>

            </div>
        </div>
       </form>
       </div>
       </div>
       </div>
          <!--Insert Category Delete Modal End--> 
  </body>
  <script type="text/javascript" language="javascript">
      $(document).ready(function(){
            $("#add").click(function(){
                 if(user_type=="admin" || privileges_details_system[9]["insertm"]=="true"){
                    $("#insModal").modal('show');
                 }else{
                     showAccessDeniedProblem();
                 }
            }); 
            $("#insModal").on('shown.bs.modal', function(){
                            $("#insModal").find("#brand_details_id").val(brand_details_id);
                            $("#insModal").find("#product_details_id").val(product_details_id);
                            $("#insModal").find("#model_details_id").val(model_details_id);
                            $('#insProblemDetails').off("submit");
                            $('#insProblemDetails').submit(function() {
                            if(this.problem.value == ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please fill valid Problem ").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                $('#problem').focus();
                                $('#insProblemDetails').each(function(){
                                    this.reset();
                                    return false;
                                });
                                return false;  
                           } else if(this.discription.value == ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please fill valid Discription").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                $('#discription').focus();
                                $('#insProblemDetails').each(function(){
                                    this.reset();
                                    return false;
                                });
                                return false;  
                           }else{
                                $(".validate").removeClass("text-danger").addClass("text-success").fadeIn(100).text("Ok").prepend("<span class=\"glyphicon glyphicon-ok text-success\">").fadeOut(1000);
                                $.ajax({
                                    type:"post",
                                    url:"../server/controller/problem_details/InsProblemDetails.php",
                                    data:$('#insProblemDetails').serialize(),
                                    success: function(data){ 
                                        location.reload(true);
                                    } 
                                });
                                return false; 
                            }
                   return false;
                    });
            var modal = this;
            var hash = modal.id;
            window.location.hash = hash;
            window.onhashchange = function() {
                    if (!location.hash){
                            $(modal).modal('hide');
                    }
            }
      });  
    $('#myModal').on('hide.bs.modal', function() {
            location.reload(true);
    });
         }); 
    
         function onlad(brand_details_id,product_details_id,model_details_id){
                    cate={}
                    $.ajax({
                    type:"post",
                    url:"../server/controller/problem_details/SelProblemDetails.php",
                    data:{'brand_details_id':brand_details_id,'product_details_id':product_details_id,'model_details_id':model_details_id},
                    success: function(data) {
                        var duce = jQuery.parseJSON(data);
                        $("#data tr:has(td)").remove();
                        $.each(duce, function (index, article) {
                            cate[article.problem_details_id]= article;
                           $("#data").append($('<tr/>')
                                .append($('<td/>').html((index+1)))
                                .append($('<td/>').html(article.problem))
                                .append($('<td/>').html(article.discription))
                                .append($('<td/>').html($("<button>").attr({'type':'button'}).addClass("btn btn-success btn-xs").append("&nbsp;&nbsp;Edit&nbsp;&nbsp;").click(function (){
                                    if(user_type=="admin" || privileges_details_system[9]["updatem"]=="true"){
                                        updProblemDetails(article.problem_details_id)
                                     }else{
                                         showAccessDeniedProblem();
                                     }
                                    
                                })))
                                .append($('<td/>').html($("<button>").attr({'type':'button'}).addClass("btn btn-danger btn-xs").append("&nbsp;&nbsp;Delete&nbsp;&nbsp;").click(function (){
                                    if(user_type=="admin" || privileges_details_system[9]["deletem"]=="true"){
                                        delProblemDetails(article.problem_details_id)
                                     }else{
                                         showAccessDeniedProblem();
                                     }
                                })))
                            );
                        });
                    }
                });
         }
                    function updProblemDetails(problem_details_id) {
                      $("#updProblem").modal('show');
                      $("#updProblem").on('shown.bs.modal', function(){
                            $("#updProblemDetails").find("#problem_details_id").val(problem_details_id);  
                            $("#updProblemDetails").find("#problem").val(cate[problem_details_id]["problem"]);          
                            $("#updProblemDetails").find("#discription").val(cate[problem_details_id]["discription"]);          
                            $('#updProblemDetails').off("submit");
                            $('#updProblemDetails').submit(function() {
                            if(this.problem.value == ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please fill valid Problem ").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                $('#updProblemDetails').find('#problem').focus();
                           }else if(this.discription.value == ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please fill valid Discription").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                $('#updProblemDetails').find('#discription').focus();
                           }else {
                                $(".validate").removeClass("text-danger").addClass("text-success").fadeIn(100).text("Ok").prepend("<span class=\"glyphicon glyphicon-ok text-success\">").fadeOut(1000);
                                $.ajax({
                                    type:"post",
                                    url:"../server/controller/problem_details/UpdProblemDetails.php",
                                    data:$('#updProblemDetails').serialize(),
                                    success: function(data){ 
                                         alert(data)
                                         location.reload(true);
                                    } 
                                });
                            }
                        return false;
                    });
                     var modal = this;
                            var hash = modal.id;
                            window.location.hash = hash;
                            window.onhashchange = function() {
                                    if (!location.hash){
                                            $(modal).modal('hide');
                                    }
                            }
                      });  
                    $('#editProblemDetails').on('hide.bs.modal', function() {
                            location.reload(true);
                    });
                }

</script>

<script type="text/javascript" language="javascript">
        function delProblemDetails(problem_details_id) {
            
            $("#delProblem").modal('show');
                 var problem = cate[problem_details_id]["problem"];;
                 $("#delProblem").on('shown.bs.modal', function(){
                    $("#delProblemDetails").find("#model_details_id").val(problem_details_id);        
                    $("#delProblemDetails").find("#catname").text(problem); 
                    $('#delProblemDetails').off("submit");
                    $('#delProblemDetails').submit(function() {
                         $.ajax({
                            type:"post",
                            url:"../server/controller/problem_details/delProblemDetails.php",
                            data:{'problem_details_id':problem_details_id},
                            success: function(data){ 
                                location.reload(true);
                            } 
                  });
                    return false;
    });
    
    });
   
}

</script>
</html>

