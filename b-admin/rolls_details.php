<?php include './includes/check_session.php';?>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Rolls Details</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <?php include 'includes/links.php';?>
   
        <script type="text/javascript">
         var type=0
         cate={}
         user_details={}
         employee_details={}
         var employee_id=0;
         var roll_type_id=0;
         $(document).ready(function() {
             loadEmployeePrivileges(employees_id,callback);
             $("#category").find("select").change(function () {
                roll_type_id=$(this).val();
                onlad(roll_type_id);
             }); 
         });  
         function callback(){
             if(user_type=="admin" || privileges_details_system[11]["selectm"]=="true"){
                     loadRolls();
             }else{
                 showAccessDeniedModel();
             }
         }
         
         function loadRolls(){
             $.ajax({
                    type:"post",
                    url:"../server/controller/roll_type/SelRollType.php",
                    success: function(data) {
                        var duce = jQuery.parseJSON(data);
                        $("#category").find("select").empty();
                        $.each(duce, function (index, article) {
                           $("#category").find("select").append($('<option>').attr({'value':article.roll_type_id}).append(article.roll_type));
                        });
                        $("#category").find("select").trigger("change");
                    }
                });
         }
         function onloadUser(){
             $.ajax({
                type:"post",
                url:"../server/controller/employee_details/SelEmployeeDetails.php",
                success: function(data) { 
                var duce = jQuery.parseJSON(data); //here data is passed to js object
                employee_details={}
                var inde=0;
                $("#user_details_first").empty();
                $("#user_details").empty();
                $.each(duce, function (index, article) {
                    if(Object.keys(rolls_details).length==0 || checkEmp(article.employee_id)){
                        employee_details[article.employee_details_id]=article;    
                        if(inde==0){
                            $("#user_details_first").append('<img src="../server/controller/user_details/'+article.pic+'" width="50" height="50" >'+article.user_name+'<span class="glyphicon glyphicon-chevron-down"></span>');
                            employee_id=article.employee_id;
                            inde++;
                        }
                        
                        $("#user_details").append($('<li><a href="#"><img src="../server/controller/user_details/'+article.pic+'" width="50" height="50">'+article.user_name+'</a></li>').click(function (event){
                            employee_id=article.employee_id;
                            $("#insModal").find("#employee_id").val(employee_id);
                            $("#user_details_first").html($(this).find("a").html()).append('<span class="glyphicon glyphicon-chevron-down"></span>');
                             event.preventDefault(); // ca
                        }));
                    }
                });
                $("#insModal").modal('show');
                }
            });
         }
         function checkEmp(employee_id){
            var b=true;
            $.each(rolls_details, function (key, value) {
                if(value.employee_id==employee_id){
                    b= false;
                    return ;
                }
            });
            return b;
         }
         
        </script> 
  </head>
  <!-- ADD THE CLASS sidedar-collapse TO HIDE THE SIDEBAR PRIOR TO LOADING THE SITE -->
 <body class="wysihtml5-supported skin-yellow sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">

   <?php include 'includes/header.php';?>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
   <?php include 'includes/sidepanel.php';?>

      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Rolls Details
            <small>Admin panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i> Home</li>
            <li class="active">Rolls Details</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
           
          <div class="row">
            <div class="col-xs-12">
                <div class="box">
                <div class="box-header">
                    <div class="col-md-3 pull-left">
                      <div class="form-group" id="category">
                            <label>Select Roll</label>
                            <select class="form-control" id="brand_details_id" name="brand_details_id">
                            </select>
                        </div><!-- /.form-group -->    
                  </div>
                  
                  <div class="pull-right">
                      <br>
                      <button type="button" class="btn btn-success" id="add">Add +</button>
                  </div>
                </div><!-- /.box-header -->
                
                <div class="box-body">
                  <div class="table-responsive"> 
                   <table id="data" class="table table-bordered table-hover ">
                    <thead>
                      <tr>
                        <th>S.N.</th>
                        <th>User Pic</th>
                        <th>User Name</th>
                        <th>Email</th>
                        <th>Contact No</th>
                        <th>Registration Date</th>
                        <th colspan="2">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                   
                  </table>
                  </div>         
                
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section>
      </div><!-- /.content-wrapper -->

     <?php include 'includes/footer.php';?>

     
    </div><!-- ./wrapper -->
    <?php include 'includes/jslinks.php';?>
 <div class="modal fade" id="delModel" role="dialog">
        <div class="modal-dialog modal-sm">
                
        <!-- Modal content-->
        <div class="modal-content">
             
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Delete Category </h4>
        </div>
        <form id ="delRollsDetails">    
        <div class="modal-body">
            <div class="row">
            <div class="col-md-12">
            <input type="hidden"  name="employee_id" id="employee_id" value="" class="form-control"/>
            <p>Sure to want to delete "<span style ="color:red" id ="catname"></span>" Rolls?</p>
             </div>
            </div>
            <div class="modal-footer">
            <button type="submit" id="delete2" class="btn btn-danger">Delete</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>

            </div>
        </div>
       </form>
       </div>
       </div>
       </div>
    <div class="modal fade" id="insModal" role="dialog">
        <div class="modal-dialog modal-sm">
        <!-- Modal content-->
        <div class="modal-content">
         <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Insert Model Details </h4>
        </div>
        <form id ="insRollsDetails">    
        <div class="modal-body">
        <div class="row">
        
        <div class="col-md-12">
        <div class="form-group">
            <div class="">
                        <div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="user_details_first">
      
    </button>
                            <style>
                                .dropdown-toggle, .dropdown-menu {
    width: 271px;
}
                            </style>
                            <ul class="dropdown-menu" id="user_details" ></ul>
  </div>
                  </div>
            <input type="hidden"  name="employee_id" id="employee_id" class="form-control"  placeholder="Enter Roll">
            <input type="hidden"  name="roll_type_id" id="roll_type_id" class="form-control"  placeholder="Enter Roll">
        </div>
        <!-- /.form-group -->
        </div><!-- /.col -->
        </div><!-- /.box -->
        <div class="modal-footer">
            <div class="validate pull-left" style="font-weight:bold;"></span></div>
            <button type="submit" class="btn btn-primary">Submit</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        </div>
        </div>
       </form>
        </div>
        </div>
       </div>
          <!--Insert Category Delete Modal End--> 
  </body>
  <script type="text/javascript" language="javascript">
      $(document).ready(function(){
            $("#add").click(function(){
                 if(user_type=="admin" || privileges_details_system[11]["insertm"]=="true"){
                    onloadUser();
                 }else{
                     showAccessDeniedModel();
                 }
            }); 
            $("#insModal").on('shown.bs.modal', function(){
                
                            $("#insModal").find("#roll_type_id").val(roll_type_id);
                            $("#insModal").find("#employee_id").val(employee_id);
                            $('#insRollsDetails').off("submit");
                            $('#insRollsDetails').submit(function() {
                                $(".validate").removeClass("text-danger").addClass("text-success").fadeIn(100).text("Ok").prepend("<span class=\"glyphicon glyphicon-ok text-success\">").fadeOut(1000);
                                $.ajax({
                                    type:"post",
                                    url:"../server/controller/rolls_details/InsRollsDetails.php",
                                    data:$('#insRollsDetails').serialize(),
                                    success: function(data){ 
                                        alert(data)
                                        location.reload(true);
                                    } 
                                });
                   return false;
                    });
            var modal = this;
            var hash = modal.id;
            window.location.hash = hash;
            window.onhashchange = function() {
                    if (!location.hash){
                            $(modal).modal('hide');
                    }
            }
      });  
    $('#myModal').on('hide.bs.modal', function() {
            location.reload(true);
    });
         }); 
         function onlad(){
                    rolls_details={}
                    $.ajax({
                    type:"post",
                    url:"../server/controller/rolls_details/SelRollsDetails.php",
                    data:{'roll_type_id':roll_type_id},
                    success: function(data) {
                        var duce = jQuery.parseJSON(data);
                        $("#data tr:has(td)").remove();
                        $.each(duce, function (index, article) {
                            var text="Create Head";
                            var cls="btn-default";
                            if(article.head=="true"){
                                text="Remove Head";
                                cls="btn-success";
                            }
                            rolls_details[article.rolls_details_id]= article;
                            var img=$("<img>").attr({'src':'../server/controller/user_details/'+article.pic,'width':'50','height':'50'});
                           $("#data").append($('<tr/>')
                                .append($('<td/>').html((index+1)))
                                .append($('<td/>').html(img))
                                .append($('<td/>').html(article.user_name))
                                .append($('<td/>').html(article.email))
                                .append($('<td/>').html(article.contact_no))
                                .append($('<td/>').html(article.date))
                                .append($('<td/>').html($("<button>").attr({'type':'button'}).addClass("btn btn-danger btn-xs").append("&nbsp;&nbsp;Delete&nbsp;&nbsp;").click(function (){
                                    if(user_type=="admin" || privileges_details_system[10]["deletem"]=="true"){
                                        delRollsDetails(article.rolls_details_id)
                                     }else{
                                         showAccessDeniedModel();
                                     }
                                })))
                                .append($('<td/>').html($("<button>").attr({'type':'button'}).addClass("btn "+cls+" btn-xs").append("&nbsp;&nbsp;"+text+"&nbsp;&nbsp;").click(function (){
                                     if(user_type=="admin" || privileges_details_system[10]["updatem"]=="true"){
                                        updRollsDetails($(this),article.rolls_details_id,cls);
                                     }else{
                                         showAccessDeniedModel();
                                     }
                                })))
                            );
                        });
                    }
                });
         }
         
</script>

<script type="text/javascript" language="javascript">
    function updRollsDetails(obj,id,cls){
                      var value;
                      if(cls=="btn-success"){
                                value="false";
                                obj.empty();
                                obj.append("Create Head");
                                obj.removeClass("btn-success");
                                obj.addClass("btn-default");
                      }else{
                                value="true";
                                obj.empty();
                                obj.append("Remove head");
                                obj.removeClass("btn-default");
                                obj.addClass("btn-success");
                      }           
                      
                  $.ajax({
                    type:"post",
                    url:"../server/controller/rolls_details/UpdRollsDetails.php",
                    data:{'rolls_details_id':id,'head':value},
                    success: function(data) {
                    }
                  });
              }
              
        function delRollsDetails(rolls_details_id) {
            if(user_type=="admin" || privileges_details_system[1]["deletem"]=="true"){
                     $("#delModel").modal('show');
                 var amount = rolls_details[rolls_details_id]["user_name"];;
                 $("#delModel").on('shown.bs.modal', function(){
                    $("#delRollsDetails").find("#catname").text(amount); 
                    $('#delRollsDetails').off("submit");
                    $('#delRollsDetails').submit(function() {
                         $.ajax({
                            type:"post",
                            url:"../server/controller/rolls_details/delRollsDetails.php",
                            data:{'rolls_details_id':rolls_details_id},
                            success: function(data){ 
                                location.reload(true);
                            } 
                  });
                    return false;
                 });
    
                });
   
             }else{
                 showAccessDeniedModel();
             }
            
}

</script>
</html>

