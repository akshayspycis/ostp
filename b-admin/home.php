<?php include './includes/check_session.php';?>
<!DOCTYPE html>
<html>
  <head>
      
    <meta charset="UTF-8">
    <title>Home</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <?php include 'includes/links.php';?>
   
        <style>
            #data tr th{
                background:green;
                color:white; 
            }   
        .table-hover tbody tr:hover td, .table-hover tbody tr:hover th {
        background:yellowgreen;
        color:black;
        font-weight:bold;
        }
    </style>
      <script type="text/javascript">
          queries={}
          $(document).ready(function(){
              loadEmployeePrivileges(employees_id);
              onlad();
            });  
      </script>     
  </head>
  <!-- ADD THE CLASS sidedar-collapse TO HIDE THE SIDEBAR PRIOR TO LOADING THE SITE -->
  <body class="wysihtml5-supported skin-yellow sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
<?php
date_default_timezone_set("Asia/Calcutta");
?>
   <?php include 'includes/header.php';?>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
        <?php include 'includes/sidepanel.php';?>

      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Admin panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i> Home</li>
            <li class="active">Dashboard</li>
          </ol>
        </section>
         
        <!-- Main content -->
        <section class="content">
                <div class="row">
                <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green">
                <div class="inner">
                <h3><?php echo date("h:i"); ?></h3>
                <p><?php echo date("d-m-Y"); ?></p>
                </div>
                <div class="icon">
                <i class="glyphicon glyphicon-time"></i>
                </div>
                    <a href="home.php" class="small-box-footer"><i class="fa fa-arrow-circle-right"></i></a>
                </div>
                </div><!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                <div class="inner">
                <h3 id ="tcustomer">2</h3>
                <p>News</p>
                </div>
                <div class="icon">
                <i class="ion ion-stats-bars"></i>
                </div>
                    <a href="customer.php" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
                </div><!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-yellow">
                <div class="inner">
                <h3 id ="tproduct">2</h3>
                <p>Queries</p>
                </div>
                <div class="icon">
                <i class="ion ion-person-add"></i>
                </div>
                    <a href="products.php" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
                </div><!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-light-blue">
                <div class="inner">
                <h3 id ="torder">2</h3>
                <p>Gallery</p>
                </div>
                <div class="icon">
                <i class="ion ion-pie-graph"></i>
                </div>
                    <a href="orders.php" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
                </div><!-- ./col -->
                </div><!-- /.row -->
                <div class="row">
            <div class="col-sm-12">
                    <section class="content">
           
          <div class="row">
            <div class="col-xs-12">
           

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Queries</h3>
                  <div class="pull-right">
                      <!--<button type="button" class="btn btn-success" id="addFeedback">Add Feedback</button>-->
                  </div>
                </div><!-- /.box-header -->
                
                <div class="box-body">
                  <div class="table-responsive"> 
                   <table id="data" class="table table-bordered table-hover ">
                    <thead>
                       <tr>
                        <th width="5%">S.N.</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Contact</th>
                        <th>Location</th>
                        <th>Preferred time</th>
                        <th>Date</th>
                        <th colspan="2" style="text-align:center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                    <tfoot>
                      
                    </tfoot>
                  </table>
                  </div>         
                
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section>
      </div><!-- /.content-wrapper -->

     <?php // include 'includes/footer.php';?>
    <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->
    <?php include 'includes/jslinks.php';?>
     <div class="modal fade" id="delete1" role="dialog">
        <div class="modal-dialog">
                
        <!-- Modal content-->
        <div class="modal-content">
             
         <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete Feedbacks </h4>
        </div>
        <form id ="deleteFeedback">    
        <div class="modal-body">
        <div class="row">
        <div class="col-md-6">
        <input type="hidden"  name="sid" id="sid" value="" class="form-control"/>
        <p id ="msg">Sure to want to delete ?</p>
         </div>
        </div>
        <div class="modal-footer">
        <button type="submit" id="delete2" class="btn btn-danger">Ok</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        
        </div>
        </div>
       </form>
        </div>
        </div>
       </div> 
    <script type="text/javascript" language="javascript">
      /* Code for Product Delete start */  
        $(document).on("click", "#delete", function () {
                 var quick_query_id = $(this).data('quick_query_id');
               
                    $("#delete1").on('shown.bs.modal', function(){
                    $("#delete1").find("#quick_query_id").val(quick_query_id);
                       $('#deleteFeedback').off("submit");
                        $('#deleteFeedback').submit(function() {
                         $.ajax({
                            type:"post",
                            url:"../server/controller/DellQuickQuery.php",
                            data:"id="+quick_query_id,
                            success: function(data){ 
                                onlad();
                        $('#deleteFeedback').each(function(){
                                this.reset();
                                onlad();
               $('#delete1').modal('hide');
                        return false;
                     });
             } 
                  });
                    return false;
    });
    
    });
   
});
  /* Code for Product Delete end */  
     
//...........................................................................................................

function onlad(){ 
//              alert("success");
                /* Ajax call for Product Display*/
                    $.ajax({
                    type:"post",
                    url:"../server/controller/SelQuickQuery.php",
                    success: function(data) {
//                        alert(data.trim());
                      var duce = jQuery.parseJSON(data);
                        $("#data tr:has(td)").remove();
                        $.each(duce, function (index, article) {
                            queries[article.id]=article;
                          $("#data").append($('<tr/>')
                                .append($('<td/>').html((index+1)))
                                .append($('<td/>').html(article.name))
                                .append($('<td/>').html(article.email))
                                .append($('<td/>').html(article.contact))
                                .append($('<td/>').html(article.location))
                                .append($('<td/>').html(article.preferred_time))
                                .append($('<td/>').html(article.date))
//                                .append($('<td/>').html("<button type=\"button\" class=\"btn btn-success btn-xs\" data-quick_query_id="+article.quick_query_id+" id=\"edit\" data-toggle=\"modal\" data-target=\"#editQueries\")>&nbsp;&nbsp;Edit&nbsp;&nbsp;</button>"))
                                .append($('<td/>').html("<button type=\"button\" class=\"btn btn-danger btn-xs\"  data-quick_query_id="+article.id+"   id =\"delete\" data-toggle=\"modal\" data-target=\"#delete1\"\")>Delete</button>"))
                             );
                        });
                    }
                });
                }
</script>
  </body>
</html>
