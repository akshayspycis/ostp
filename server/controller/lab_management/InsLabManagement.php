<?php
    include_once '../../models/LabManagement.php'; 
    include_once '../../managers/LabManagementMgr.php'; 
    $lab_management = new LabManagement();
    $lab_management->setOrder_product_id($_POST['order_product_id']);
    $lab_management->setOrder_id($_POST['order_id']);
    $lab_management->setReceive_date_time($_POST['receive_date_time']);
    $lab_management->setReturn_date_time($_POST['return_date_time']);
    $lab_management->setStatus($_POST['status']);
    $lab_managementMgr = new LabManagementMgr();
    if ($lab_managementMgr->insLabManagement($lab_management)) {
        echo 'Query inserted Successfully.';
    } else {
        echo 'Error';
    }
?>