<?php
    include_once '../../models/LabManagement.php'; 
    include_once '../../managers/LabManagementMgr.php'; 
        include_once '../../models/EnggReturnInLabOrderProduct.php'; 
    include_once '../../managers/EnggReturnInLabOrderProductMgr.php'; 
    $lab_management = new LabManagement();
    $date = new DateTime("now", new DateTimeZone("Asia/Kolkata"));
    $lab_management->setLab_management_id($_POST['lab_management_id']);
    $lab_management->setReturn_date_time($date->format('d-m-Y'));
    $lab_managementMgr = new LabManagementMgr();
    if ($lab_managementMgr->updateLabManagement($lab_management)) {
        $engg_return_in_lab_order_product = new EnggReturnInLabOrderProduct();
        $engg_return_in_lab_order_product->setData_of_return($date->format('d-m-Y'));
        $engg_return_in_lab_order_product->setOrder_product_id($_POST['order_product_id']);
        $engg_return_in_lab_order_product->setLab_management_id($_POST['lab_management_id']);
        $engg_return_in_lab_order_productMgr = new EnggReturnInLabOrderProductMgr();
        if ($engg_return_in_lab_order_productMgr->insEnggReturnInLabOrderProduct($engg_return_in_lab_order_product)) {
            echo 'Query inserted Successfully.';
        } else {
            echo 'Error';
        }
    } else {
        echo 'Error';
    }
?>