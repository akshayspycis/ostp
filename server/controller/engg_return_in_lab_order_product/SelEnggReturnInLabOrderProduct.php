<?php
    include_once '../../models/EnggReturnInLabOrderProduct.php';
    include_once '../../managers/EnggReturnInLabOrderProductMgr.php';
    $obj = new EnggReturnInLabOrderProductMgr();
    $engg_return_in_lab_order_product = $obj->selEnggReturnInLabOrderProduct($_POST['engg_details_id']);
    $str = array();    
    while($row = $engg_return_in_lab_order_product->fetch()){
        $arr = array(
            'engg_return_in_lab_order_product_id' => $row['engg_return_in_lab_order_product_id'], 
            'engg_details_id' => $row['engg_details_id'], 
            'employee_id' => $row['employee_id'], 
            'data_of_return' => $row['data_of_return'], 
            'order_product_id' => $row['order_product_id'], 
            'lab_management_id' => $row['lab_management_id'], 
        );
        array_push($str, $arr); 
    }
    echo json_encode($str);
?>