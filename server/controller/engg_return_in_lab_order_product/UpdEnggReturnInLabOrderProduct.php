<?php
    include_once '../../models/EnggReturnInLabOrderProduct.php'; 
    include_once '../../managers/EnggReturnInLabOrderProductMgr.php'; 
    $engg_return_in_lab_order_product = new EnggReturnInLabOrderProduct();
    $engg_return_in_lab_order_product->setOrder_id($_POST['engg_return_in_lab_order_product_id']);
    $engg_return_in_lab_order_product->setStatus($_POST['status']);
    $engg_return_in_lab_order_productMgr = new EnggReturnInLabOrderProductMgr();
    if ($engg_return_in_lab_order_productMgr->updateEnggReturnInLabOrderProduct($engg_return_in_lab_order_product)) {
        echo 'Query inserted Successfully.';
    } else {
        echo 'Error';
    }
?>