<?php
    include_once '../../models/EnggReturnInLabOrderProduct.php'; 
    include_once '../../managers/EnggReturnInLabOrderProductMgr.php'; 
    $engg_return_in_lab_order_product = new EnggReturnInLabOrderProduct();
    $engg_return_in_lab_order_product->setEngg_details_id($_POST['engg_details_id']);
    $engg_return_in_lab_order_product->setEmployee_id($_POST['employee_id']);
    $engg_return_in_lab_order_product->setData_of_return($_POST['data_of_return']);
    $engg_return_in_lab_order_product->setOrder_product_id($_POST['order_product_id']);
    $engg_return_in_lab_order_product->setLab_management_id($_POST['lab_management_id']);
    $engg_return_in_lab_order_productMgr = new EnggReturnInLabOrderProductMgr();
    if ($engg_return_in_lab_order_productMgr->insEnggReturnInLabOrderProduct($engg_return_in_lab_order_product)) {
        echo 'Query inserted Successfully.';
    } else {
        echo 'Error';
    }
?>