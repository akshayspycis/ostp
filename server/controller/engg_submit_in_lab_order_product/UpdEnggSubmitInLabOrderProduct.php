<?php
    include_once '../../models/EnggSubmitInLabOrderProduct.php'; 
    include_once '../../managers/EnggSubmitInLabOrderProductMgr.php'; 
    $engg_submit_in_lab_order_product = new EnggSubmitInLabOrderProduct();
    $engg_submit_in_lab_order_product->setOrder_id($_POST['engg_submit_in_lab_order_product_id']);
    $engg_submit_in_lab_order_product->setStatus($_POST['status']);
    $engg_submit_in_lab_order_productMgr = new EnggSubmitInLabOrderProductMgr();
    if ($engg_submit_in_lab_order_productMgr->updateEnggSubmitInLabOrderProduct($engg_submit_in_lab_order_product)) {
        echo 'Query inserted Successfully.';
    } else {
        echo 'Error';
    }
?>