<?php
    include_once '../../models/EnggSubmitInLabOrderProduct.php'; 
    include_once '../../managers/EnggSubmitInLabOrderProductMgr.php'; 
    include_once '../../models/LabManagement.php'; 
    include_once '../../managers/LabManagementMgr.php'; 
    $date = new DateTime("now", new DateTimeZone("Asia/Kolkata"));
    $engg_submit_in_lab_order_product = new EnggSubmitInLabOrderProduct();
    $engg_submit_in_lab_order_product->setEngg_details_id($_POST['engg_details_id']);
    $engg_submit_in_lab_order_product->setEmployee_id($_POST['employee_id']);
    $engg_submit_in_lab_order_product->setDate_of_submit($date->format('d-m-Y'));
    $engg_submit_in_lab_order_product->setOrder_product_id($_POST['order_product_id']);
    $engg_submit_in_lab_order_productMgr = new EnggSubmitInLabOrderProductMgr();
    $engg_submit_in_lab_order_product_sle=$engg_submit_in_lab_order_productMgr->checkEnggSubmitInLabOrderProduct($engg_submit_in_lab_order_product);
    if($row = $engg_submit_in_lab_order_product_sle->fetch()){
        echo 'false';
    } else {
        $lab_management = new LabManagement();
            $lab_management->setOrder_product_id($_POST['order_product_id']);
            $lab_management->setOrder_id($_POST['order_id']);
            $lab_management->setReceive_date_time($date->format('d-m-Y'));
//            $lab_management->setReturn_date_time($_POST['return_date_time']);
            $lab_management->setStatus('false');
            $lab_managementMgr = new LabManagementMgr();
        if ($lab_managementMgr->insLabManagement($lab_management) && $engg_submit_in_lab_order_productMgr->insEnggSubmitInLabOrderProduct($engg_submit_in_lab_order_product)) {
            echo 'true';
        } else {
            echo 'Error';
        }
    }
    
?>