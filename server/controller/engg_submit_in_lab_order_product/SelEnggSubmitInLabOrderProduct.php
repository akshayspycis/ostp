<?php
    include_once '../../models/EnggSubmitInLabOrderProduct.php';
    include_once '../../managers/EnggSubmitInLabOrderProductMgr.php';
    $obj = new EnggSubmitInLabOrderProductMgr();
    $engg_submit_in_lab_order_product = $obj->selEnggSubmitInLabOrderProduct($_POST['engg_details_id']);
    $str = array();    
    while($row = $engg_submit_in_lab_order_product->fetch()){
        $arr = array(
            'engg_submit_in_lab_order_product_id' => $row['engg_submit_in_lab_order_product_id'], 
            'engg_details_id' => $row['engg_details_id'], 
            'employee_id' => $row['employee_id'], 
            'date_of_submit' => $row['date_of_submit'], 
            'order_product_id' => $row['order_product_id'], 
            'lab_management_id' => $row['lab_management_id'], 
        );
        array_push($str, $arr); 
    }
    echo json_encode($str);
?>