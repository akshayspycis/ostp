<?php
    include_once '../../models/EnggDetails.php'; 
    include_once '../../managers/EnggDetailsMgr.php'; 
    $engg_details = new EnggDetails();
    $engg_details->setOrder_id($_POST['order_id']);
    $engg_details->setOrder_allot_details_id($_POST['order_allot_detailsId']);
    $engg_details->setMeeting_time($_POST['meeting_time']);
    $engg_details->setMeeting_date($_POST['meeting_date']);
    $engg_details->setMeeting_person($_POST['meeting_person']);
    $engg_details->setOrder_product_id($_POST['order_product_id']);
    $engg_details->setEmployee_id($_POST['employee_id']);
    $engg_detailsMgr = new EnggDetailsMgr();
    if ($engg_detailsMgr->insEnggDetails($engg_details)) {
        echo 'Query inserted Successfully.';
    } else {
        echo 'Error';
    }
?>