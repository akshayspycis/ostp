<?php
    include_once '../../models/EnggDetails.php';
    include_once '../../managers/EnggDetailsMgr.php';
    $obj = new EnggDetailsMgr();
    $date = new DateTime("now", new DateTimeZone("Asia/Kolkata"));
    $engg_details = $obj->checkEnggDetails($_POST['order_allot_detailsId'],$date->format('d-m-Y'));
    $str = array();    
    if($row = $engg_details->fetch()){
        $arr = array(
            'engg_details_id' => $row['engg_details_id'], 
//            'order_product_status_id' => $row['order_product_status_id'], 
            'check' => 'true', 
        );
        array_push($str, $arr); 
    }else{
        $arr = array(
            'meeting_time' => $date->format('H:i'), 
            'meeting_date' => $date->format('d-m-Y'), 
        );
        array_push($str, $arr); 
    }
    echo json_encode($str);
?>