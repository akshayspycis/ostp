<?php
    include_once '../../models/EnggDetails.php'; 
    include_once '../../managers/EnggDetailsMgr.php'; 
    $engg_details = new EnggDetails();
    $engg_details->setOrder_id($_POST['engg_details_id']);
    $engg_details->setStatus($_POST['status']);
    $engg_detailsMgr = new EnggDetailsMgr();
    if ($engg_detailsMgr->updateEnggDetails($engg_details)) {
        echo 'Query inserted Successfully.';
    } else {
        echo 'Error';
    }
?>