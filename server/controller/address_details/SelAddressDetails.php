<?php
    if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
        exit(0);
    }
    $str_json = file_get_contents('php://input'); //($_POST doesn't work here)
    $response = json_decode($str_json, true);
    include_once '../../models/AddressDetails.php';
    include_once '../../managers/AddressDetailsMgr.php';
    $obj = new AddressDetailsMgr();
    $address_details = $obj->selAddressDetails($response['user_id']);
    $str = array();    
    while($row = $address_details->fetch()){
            $arr = array(
            'address_details_id' => $row['address_details_id'], 
            'address_1' => $row['address_1'] ,
            'address_2' => $row['address_2'] ,
            'city' => $row['city'] ,
            'pincode' => $row['pincode'] ,
            'state' => $row['state'] ,
            'edit' => false ,
       );
        array_push($str, $arr); 
    }
    echo json_encode($str);
?>