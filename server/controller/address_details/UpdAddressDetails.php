<?php
    if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
        exit(0);
    }
    $str_json = file_get_contents('php://input'); //($response doesn't work here)
    $response = json_decode($str_json, true);
    
    include_once '../../models/AddressDetails.php'; 
    include_once '../../managers/AddressDetailsMgr.php'; 
    
    $address_details = new AddressDetails();
    $address_details->setAddress_1($response['address_1']);
    $address_details->setAddress_2($response['address_2']);
    $address_details->setCity($response['city']);
    $address_details->setPincode($response['pincode']);
    $address_details->setState($response['state']);
    $address_detailsMgr = new AddressDetailsMgr();
    if ($address_detailsMgr->updateAddressDetails($address_details)) {
        echo 'Query inserted Successfully.';
    } else {
        echo 'Error';
    }
?>