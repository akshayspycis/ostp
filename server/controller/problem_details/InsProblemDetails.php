<?php
    include_once '../../models/ProblemDetails.php'; 
    include_once '../../managers/ProblemDetailsMgr.php'; 
    $problem_details = new ProblemDetails();
    $problem_details->setBrand_details_id($_POST['brand_details_id']);
    $problem_details->setModel_details_id($_POST['model_details_id']);
    $problem_details->setProduct_details_id($_POST['product_details_id']);
    $problem_details->setProblem($_POST['problem']);
    $problem_details->setDiscription($_POST['discription']);
    $problem_detailsMgr = new ProblemDetailsMgr();
    if ($problem_detailsMgr->insProblemDetails($problem_details)) {
        echo 'Query inserted Successfully.';
    } else {
        echo 'Error';
    }
?>