<?php
    include_once '../../models/ProblemDetails.php'; 
    include_once '../../managers/ProblemDetailsMgr.php'; 
    $problem_details = new ProblemDetails();
    $problem_details->setProblem($_POST['problem']);
    $problem_details->setDiscription($_POST['discription']);
    $problem_details->setProblem_details_id($_POST['problem_details_id']);
    $problem_detailsMgr = new ProblemDetailsMgr();
    if ($problem_detailsMgr->updateProblemDetails($problem_details)) {
        echo 'Query inserted Successfully.';
    } else {
        echo 'Error';
    }
?>