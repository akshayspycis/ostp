<?php
    include_once '../../models/ProblemDetails.php';
    include_once '../../managers/ProblemDetailsMgr.php';
    $obj = new ProblemDetailsMgr();
    $problem_details = $obj->selProblemDetails($_POST['brand_details_id'],$_POST['product_details_id'],$_POST['model_details_id']);
    $str = array();    
    while($row = $problem_details->fetch()){
            $arr = array(
            'problem_details_id' => $row['problem_details_id'], 
            'brand_details_id' => $row['brand_details_id'] ,
            'product_details_id' => $row['product_details_id'] ,
            'model_details_id' => $row['model_details_id'] ,
            'problem' => $row['problem'] ,
            'discription' => $row['discription'] 
       );
        array_push($str, $arr); 
    }
    echo json_encode($str);
?>