<?php
    include_once '../../models/TempWallet.php'; 
    include_once '../../models/OffilineTransactionDetails.php'; 
    include_once '../../managers/TempWalletMgr.php'; 
    include_once '../../managers/OffilineTransactionDetailsMgr.php'; 
    $temp_wallet = new TempWallet();
    $temp_wallet->setUser_data_id($_POST['user_data_id']);
    $temp_wallet->setTransaction_code(time());
    $temp_wallet->setAmount($_POST['amount']);
    $temp_wallet->setFrom_date($_POST['from_date']);
    $temp_wallet->setTo_date($_POST['to_date']);
    $temp_walletMgr = new TempWalletMgr();
    if ($temp_walletMgr->insTempWallet($temp_wallet)) {
        $offline_transaction_details=new OffilineTransactionDetails();
        $offline_transaction_details->setTransation_type($_POST['transation_type']);
        $offline_transaction_details->setType("registration");
        $offline_transaction_details->setWallet_id("(select temp_wallet_id from temp_wallet order by temp_wallet_id desc limit 1)");
        $offline_transaction_detailsMgr = new OffilineTransactionDetailsMgr();
        if ($offline_transaction_detailsMgr->insOffilineTransactionDetails($offline_transaction_details)) {
            echo 'Query inserted Successfully.';
        }else{
            echo 'Error';
        }
    } else {
        echo 'Error';
    }
?>