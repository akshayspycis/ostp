<?php
    include_once '../../models/OrderLabExtraCharge.php'; 
    include_once '../../managers/OrderLabExtraChargeMgr.php'; 
    $order_lab_extra_charge = new OrderLabExtraCharge();
    $order_lab_extra_charge->setOrder_lab_extra_charge_id($_POST['order_lab_extra_charge_id']);
    $order_lab_extra_charge->setAmount($_POST['amount']);
    $order_lab_extra_chargeMgr = new OrderLabExtraChargeMgr();
    if ($order_lab_extra_chargeMgr->updateOrderLabExtraCharge($order_lab_extra_charge)) {
        echo 'Query inserted Successfully.';
    } else {
        echo 'Error';
    }
?>