<?php
    include_once '../../models/OrderLabExtraCharge.php'; 
    include_once '../../managers/OrderLabExtraChargeMgr.php'; 
        $date = new DateTime("now", new DateTimeZone("Asia/Kolkata"));
    $order_lab_extra_charge = new OrderLabExtraCharge();
    $order_lab_extra_charge->setCharge_reason($_POST['charge_reason']);
    $order_lab_extra_charge->setAmount($_POST['amount']);
    $order_lab_extra_charge->setLab_date($date->format('d-m-Y'));
    $order_lab_extra_charge->setOrder_product_id($_POST['order_product_id']);
    $order_lab_extra_charge->setLab_management_id($_POST['lab_management_id']);
    $order_lab_extra_charge->setStatus("false");
    $order_lab_extra_chargeMgr = new OrderLabExtraChargeMgr();
    if ($order_lab_extra_chargeMgr->insOrderLabExtraCharge($order_lab_extra_charge)) {
        echo 'Query inserted Successfully.';
    } else {
        echo 'Error';
    }
?>