<?php
    if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }
    // Access-Control headers are received during OPTIONS requests
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
        exit(0);
    }
    $str_json = file_get_contents('php://input'); //($response doesn't work here)
    $response = json_decode($str_json, true);
    include_once '../../models/OrderLabExtraCharge.php'; 
    include_once '../../managers/OrderLabExtraChargeMgr.php'; 
    $order_lab_extra_charge = new OrderLabExtraCharge();
    $order_lab_extra_charge->setOrder_lab_extra_charge_id($response['order_lab_extra_charge_id']);
    $order_lab_extra_charge->setStatus($response['status']);
    $order_lab_extra_chargeMgr = new OrderLabExtraChargeMgr();
    if ($order_lab_extra_chargeMgr->updateOrderLabExtraChargeStatus($order_lab_extra_charge)) {
        echo 1;
    } else {
        echo 0;
    }
?>