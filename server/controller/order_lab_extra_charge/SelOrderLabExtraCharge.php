<?php
    include_once '../../models/OrderLabExtraCharge.php';
    include_once '../../managers/OrderLabExtraChargeMgr.php';
    $obj = new OrderLabExtraChargeMgr();
    $order_lab_extra_charge = $obj->selOrderLabExtraCharge($_POST['order_product_id']);
    $str = array();    
    while($row = $order_lab_extra_charge->fetch()){
        $arr = array(
            'order_lab_extra_charge_id' => $row['order_lab_extra_charge_id'], 
            'charge_reason' => $row['charge_reason'], 
            'amount' => $row['amount'], 
            'lab_date' => $row['lab_date'], 
            'order_product_id' => $row['order_product_id'], 
            'lab_management_id' => $row['lab_management_id'], 
            'status' => $row['status'], 
        );
        array_push($str, $arr); 
    }
    echo json_encode($str);
?>