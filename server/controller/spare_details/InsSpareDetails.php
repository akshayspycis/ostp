<?php
    include_once '../../models/SpareDetails.php'; 
    include_once '../../managers/SpareDetailsMgr.php'; 
    $spare_details = new SpareDetails();
    $spare_details->setSpare_name($_POST['spare_name']);
    $spare_details->setSpare_cost($_POST['spare_cost']);
    $spare_details->setLebure_cost($_POST['lebure_cost']);
    $spare_details->setProduct_details_id($_POST['product_details_id']);
    $spare_detailsMgr = new SpareDetailsMgr();
    if ($spare_detailsMgr->insSpareDetails($spare_details)) {
        echo 'Query inserted Successfully.';
    } else {
        echo 'Error';
    }
?>