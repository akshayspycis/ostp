<?php
    include_once '../../models/SpareDetails.php';
    include_once '../../managers/SpareDetailsMgr.php';
    $obj = new SpareDetailsMgr();
    $spare_details = $obj->selSpareDetailsLab($_POST['order_product_id']);
    $str = array();    
    while($row = $spare_details->fetch()){
            $arr = array(
            'spare_details_id' => $row['spare_details_id'], 
            'spare_name' => $row['spare_name'] ,
            'spare_cost' => $row['spare_cost'] ,
            'lebure_cost' => $row['lebure_cost'] ,
            'product_details_id' => $row['product_details_id'] ,
       );
        array_push($str, $arr); 
    }
    echo json_encode($str);
?>