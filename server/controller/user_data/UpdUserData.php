<?php
    include_once '../../models/UserData.php'; 
    include_once '../../managers/UserDataMgr.php'; 
    $user_data = new UserData();
    $user_data->setUser_name($_POST['user_name']);
    $user_data->setContact_no($_POST['contact_no']);
    $user_data->setEmail($_POST['email']);
    $user_data->setUser_data_id($_POST['user_data_id']);
    $user_dataMgr = new UserDataMgr();
    if ($user_dataMgr->updateUserData($user_data)) {
        echo 'Query inserted Successfully.';
    } else {
        echo 'Error';
    }
?>