<?php
    include_once '../../models/UserData.php';
    include_once '../../managers/UserDataMgr.php';
    $obj = new UserDataMgr();
    $user_data = $obj->selUserData($_POST['employee_id']);
    $str = array();    
    while($row = $user_data->fetch()){
        $arr = array(
            'user_data_id' => $row['user_data_id'], 
            'user_name' => $row['user_name'] ,
            'contact_no' => $row['contact_no'] ,
            'email' => $row['email'] ,
            'visit' => $row['visit'] ,
            'employee_id' => $row['employee_id'] ,
            'amount' => $row['amount'] ,
            'charge' => $row['charge'] ,
       );
        array_push($str, $arr); 
    }
    echo json_encode($str);
?>