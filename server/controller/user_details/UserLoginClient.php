<?php
    if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }
    // Access-Control headers are received during OPTIONS requests
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
        exit(0);
    }
 session_start();                
    include_once '../../models/UserDetails.php'; 
    include_once '../../models/LoginDetails.php'; 
    include_once '../../managers/UserLoginMgr.php'; 
    $str_json = file_get_contents('php://input'); //($response doesn't work here)
    $response = json_decode($str_json, true);
    $date = new DateTime("now", new DateTimeZone("Asia/Kolkata"));
    $user_details = new UserDetails();
    $login_details = new LoginDetails();
    
    $user_details->setEmail($response["contact_no"]);  
    $login_details->setPassword(md5($response["password"]));  
    
    $userloginMgr = new UserLoginMgr();
    $user_email = $user_details->getEmail();
    $user_password= $login_details->getPassword();
    $userlogin = $userloginMgr->login($user_email,$user_password);
    if($row = $userlogin->fetch()){
           $user_details->setUser_id($row['user_id']);
           $user_details->setUser_name($row['user_name']);
           $user_details->setEmail($row['email']);
           $user_details->setContact_no($row['contact_no']);
           $login_details->setPassword($row['password']);
           $login_details->setUser_type($row['user_type']);
           $timezone = new DateTimeZone("Asia/Kolkata" );
           $date = new DateTime();
           $date->setTimezone($timezone );
           $arr = array(
                'user_id' => $user_details->getUser_id(), 
                'user_name' => $user_details->getUser_name() ,
                'email' => $user_details->getEmail() ,
                'contact_no' => $user_details->getContact_no() ,
                'pic' => $row['pic'] ,
                'user_type' => $login_details->getUser_type() ,
                'date' => $date->format( 'H:i:s A  /  D, M jS, Y' ) ,
           );
           echo json_encode($arr);
     }else{
         echo json_encode(array('status' =>'null'));
     }
?>