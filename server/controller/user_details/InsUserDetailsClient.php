<?php
    if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }
    // Access-Control headers are received during OPTIONS requests
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
        exit(0);
    }
    include_once '../../models/UserDetails.php'; 
    include_once '../../models/UserProfileDetails.php'; 
    include_once '../../models/LoginDetails.php'; 
    include_once '../../managers/UserDetailsMgr.php'; 
    include_once '../../managers/UserProfileDetailsMgr.php'; 
    include_once '../../managers/LoginDetailsMgr.php'; 
    $str_json = file_get_contents('php://input'); //($_POST doesn't work here)
    $response = json_decode($str_json, true);
    $date = new DateTime("now", new DateTimeZone("Asia/Kolkata"));
    $user_details = new UserDetails();
    $login_details = new LoginDetails();
    $user_details->setUser_name($response['user_name']);
//    $user_details->setGender($_POST["gender"]);  
//    $user_details->setDob($_POST["dob"]);  
    $user_details->setEmail($response['email']);  
    $user_details->setContact_no($response['contact_no']);   
    $login_details->setPassword(md5($response['password']));   
    $user_details->setDate($date->format('d-m-Y'));   
//    $upload_dir = "../../dist/pic/";
//    $img = $_POST["pic"];
//    $img = str_replace('data:image/png;base64,', '', $img);
//    $img = str_replace('data:image/jpeg;base64,', '', $img);
//    $img = str_replace(' ', '+', $img);
//    $data = base64_decode($img);
//    $file = $upload_dir.time().".png";
//    $success = file_put_contents($file, $data);
//    $user_profile_details=new UserProfileDetails();
//    $user_profile_details->setPic($file);
//    $user_profile_details->setDate($date->format('D, d M Y'));
    $user_detailsMgr = new UserDetailsMgr();
    if ($user_detailsMgr->insUserDetails($user_details)) {
        $login_detailsMgr = new LoginDetailsMgr();
        if($login_detailsMgr->insLoginDetails($login_details)){
//            $user_profile_details_mgr = new UserProfileDetailsMgr();
//            if($user_profile_details_mgr->insUserProfileDetails($user_profile_details)){
                echo $user_detailsMgr->selMaxUserId();
//            }else{
//                echo 'Error';
//            }
        }else{
            echo 'false';
        }
    } else {
        echo 'false';
    }
?>
