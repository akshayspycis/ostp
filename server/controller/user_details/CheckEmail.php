<?php
  if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }
    // Access-Control headers are received during OPTIONS requests
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
        exit(0);
    }
    include_once '../../managers/UserDetailsMgr.php';
    $str_json = file_get_contents('php://input'); //($_POST doesn't work here)
    $response = json_decode($str_json, true);
    $email=$response['email'];
    $user_detailsMgr=new UserDetailsMgr();
    $contact_check=$user_detailsMgr->checkEmail($email);
    if($row = $contact_check->fetch()){
        echo 'false';
    }else{
        echo 'true';
    }
    
    
    
