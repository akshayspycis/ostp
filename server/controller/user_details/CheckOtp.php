<?php
if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }
    // Access-Control headers are received during OPTIONS requests
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
        exit(0);
    }
include_once '../../models/Otp.php';
include_once '../../managers/OtpMgr.php';
$str_json = file_get_contents('php://input'); //($_POST doesn't work here)
$response = json_decode($str_json, true);
$contact_no=$response['contact_no'];
$otp_no=$response['otp_no'];
$opt = new Otp();    
    $opt->setContact_no($contact_no);
    $opt->setOtp_no($otp_no);
    $optMgr = new OtpMgr();    
    $as = $optMgr->checkOtp($opt);
    $otp_id="";
    if($row = $as->fetch()){
       $otp_id= $row['otp_id'];
    }else{
        $otp_id='0';
    }
    $str = array();    
    if ($otp_id>0 && $optMgr->delOtp($otp_id)){
        echo 'true';
    } else {
        echo 'false';
    }