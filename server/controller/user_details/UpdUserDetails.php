<?php
    if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }
    // Access-Control headers are received during OPTIONS requests
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
        exit(0);
    }
    include_once '../../models/UserDetails.php'; 
    include_once '../../managers/UserDetailsMgr.php'; 
    $str_json = file_get_contents('php://input'); //($_POST doesn't work here)
    $response = json_decode($str_json, true);
    $user_details = new UserDetails();
    $user_details->setUser_id($response['user_id']);
    $user_details->setUser_name($response['user_name']);
    $user_details->setEmail($response['email']);  
    $user_details->setContact_no($response['contact_no']);   
    $user_detailsMgr = new UserDetailsMgr();
    if ($user_detailsMgr->updateUserDetails($user_details)) {
        echo true;
    } else {
        echo false;
    }
?>
