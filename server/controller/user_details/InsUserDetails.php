
<?php
    include_once '../../models/UserDetails.php'; 
    include_once '../../models/UserProfileDetails.php'; 
    include_once '../../models/LoginDetails.php'; 
    include_once '../../managers/UserDetailsMgr.php'; 
    include_once '../../managers/UserProfileDetailsMgr.php'; 
    include_once '../../managers/LoginDetailsMgr.php'; 
    
    $date = new DateTime("now", new DateTimeZone("Asia/Kolkata"));
    $user_details = new UserDetails();
    $login_details = new LoginDetails();
    $user_details->setUser_name($_POST["user_name"]);
    $user_details->setGender($_POST["gender"]);  
    $user_details->setDob($_POST["dob"]);  
    $user_details->setEmail($_POST["email"]);  
    $user_details->setContact_no($_POST["contact_no"]);   
    $login_details->setPassword(md5($_POST["password"]));   
    $login_details->setUser_type("emp");   
    $user_details->setDate($date->format('D, d M Y'));   
    $upload_dir = "../../dist/upload/user_profile_details/";
    $img = $_POST["pic"];
    $img = str_replace('data:image/png;base64,', '', $img);
    $img = str_replace('data:image/jpeg;base64,', '', $img);
    $img = str_replace(' ', '+', $img);
    $data = base64_decode($img);
    $file = $upload_dir.time().".png";
    $success = file_put_contents($file, $data);
    $user_profile_details=new UserProfileDetails();
    $user_profile_details->setPic($file);
    $user_profile_details->setDate($date->format('D, d M Y'));
    $user_detailsMgr = new UserDetailsMgr();
    if ($user_detailsMgr->insUserDetails($user_details)) {
        $login_detailsMgr = new LoginDetailsMgr();
        if($login_detailsMgr->insLoginDetails($login_details)){
            $user_profile_details_mgr = new UserProfileDetailsMgr();
            if($user_profile_details_mgr->insUserProfileDetails($user_profile_details)){
                echo 'Query inserted Successfully.';
            }else{
                echo 'Error';
            }
        }else{
            echo 'Error';
        }
    } else {
        echo 'Error';
    }
?>
