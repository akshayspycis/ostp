<?php
    if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }
    // Access-Control headers are received during OPTIONS requests
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
        exit(0);
    }
    include_once '../../models/UserProfileDetails.php'; 
    include_once '../../managers/UserProfileDetailsMgr.php'; 
    $str_json = file_get_contents('php://input'); //($_POST doesn't work here)
    $response = json_decode($str_json, true);
    $date = new DateTime("now", new DateTimeZone("Asia/Kolkata"));
    $upload_dir = "../../dist/upload/user_profile_details/";
    $img = $response["pic"];
    $img = str_replace('data:image/png;base64,', '', $img);
    $img = str_replace('data:image/jpeg;base64,', '', $img);
    $img = str_replace(' ', '+', $img);
    $data = base64_decode($img);
    $file = $upload_dir.time().".png";
    $success = file_put_contents($file, $data);
    $user_profile_details=new UserProfileDetails();
    $user_profile_details->setUser_id($response["user_id"]);
    $user_profile_details->setPic($file);
    $user_profile_details->setDate($date->format('D, d M Y'));
    $user_profile_details_mgr = new UserProfileDetailsMgr();
    if($user_profile_details_mgr->insUserProfileDetails($user_profile_details)){
        $arr = array(
            'pic' => $file
             ); 
        echo json_encode($arr);
    }else{
        $arr = array(
            'pic' => 'null'
             ); 
        echo json_encode($arr);
    }
        
?>