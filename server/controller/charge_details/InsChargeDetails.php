<?php
    include_once '../../models/ChargeDetails.php'; 
    include_once '../../managers/ChargeDetailsMgr.php'; 
    $charge_details = new ChargeDetails();
    $charge_details->setAmount($_POST['amount']);
    $charge_details->setType($_POST['type']);
    $charge_detailsMgr = new ChargeDetailsMgr();
    if ($charge_detailsMgr->insChargeDetails($charge_details)) {
        echo 'Query inserted Successfully.';
    } else {
        echo 'Error';
    }
?>