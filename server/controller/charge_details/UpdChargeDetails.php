<?php
    include_once '../../models/ChargeDetails.php'; 
    include_once '../../managers/ChargeDetailsMgr.php'; 
    $charge_details = new ChargeDetails();
    $charge_details->setAmount($_POST['editamt']);
    $charge_details->setCharge_details_id($_POST['charge_details_id']);
    $charge_detailsMgr = new ChargeDetailsMgr();
    if ($charge_detailsMgr->updateChargeDetails($charge_details)) {
        echo 'Query inserted Successfully.';
    } else {
        echo 'Error';
    }
?>