<?php
    include_once '../../models/ChargeDetails.php';
    include_once '../../managers/ChargeDetailsMgr.php';
    $obj = new ChargeDetailsMgr();
    $charge_details = $obj->selChargeDetails($_POST['type']);
    $str = array();    
    while($row = $charge_details->fetch()){
            $arr = array(
            'charge_details_id' => $row['charge_details_id'], 
            'amount' => $row['amount'] ,
            'type' => $row['type'] ,
            'date' => $row['date'] ,
       );
        array_push($str, $arr); 
    }
    echo json_encode($str);
?>