<?php
    include_once '../../models/RollType.php';
    include_once '../../managers/RollTypeMgr.php';
    $obj = new RollTypeMgr();
    $roll_type = $obj->selRollType();
    $str = array();    
    while($row = $roll_type->fetch()){
            $arr = array(
            'roll_type_id' => $row['roll_type_id'], 
            'roll_type' => $row['roll_type'] 
       );
        array_push($str, $arr); 
    }
    echo json_encode($str);
?>