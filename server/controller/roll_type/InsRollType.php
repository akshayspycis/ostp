<?php
    include_once '../../models/RollType.php'; 
    include_once '../../managers/RollTypeMgr.php'; 
    $roll_type = new RollType();
    $roll_type->setRoll_type($_POST['roll_type']);
    $roll_typeMgr = new RollTypeMgr();
    if ($roll_typeMgr->insRollType($roll_type)) {
        echo 'Query inserted Successfully.';
    } else {
        echo 'Error';
    }
?>