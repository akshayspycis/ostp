<?php
    include_once '../../models/ProductDetails.php';
    include_once '../../managers/ProductDetailsMgr.php';
    $obj = new ProductDetailsMgr();
    $product_details = $obj->selProductDetails();
    $str = array();    
    while($row = $product_details->fetch()){
        $arr = array(
            'product_details_id' => $row['product_details_id'], 
            'product_name' => $row['product_name'] ,
            'icon' => $row['icon'] ,
            'status' => $row['status'] ,
       );
        array_push($str, $arr); 
    }
    echo json_encode($str);
?>