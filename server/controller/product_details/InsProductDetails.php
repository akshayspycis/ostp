<?php
    include_once '../../models/ProductDetails.php'; 
    include_once '../../managers/ProductDetailsMgr.php'; 
    $product_details = new ProductDetails();
    $product_details->setProduct_name($_POST["product_name"]);  
    $product_details->setStatus($_POST["status"]);   
    $upload_dir = "../../dist/upload/product_details/";
    $img = $_POST["nimage"];
    $ext=".png";
    if (stripos($img, 'png') !== false) {
        $ext=".png";
    }else if (stripos($a, 'jpeg') !== false) {
        $ext=".jpeg";
    }
    $img = str_replace('data:image/png;base64,', '', $img);
    $img = str_replace('data:image/jpeg;base64,', '', $img);
    $img = str_replace(' ', '+', $img);
    $data = base64_decode($img);
    $file = $upload_dir.time().$ext;
    $success = file_put_contents($file, $data);
    $product_details->setIcon($file);   
    $product_detailsMgr = new ProductDetailsMgr();
    if ($product_detailsMgr->insProductDetails($product_details)) {
        echo 'Query inserted Successfully.';
    } else {
        echo 'Error';
    }
?>