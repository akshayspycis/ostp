<?php
    include_once '../../models/OrderProductStatus.php'; 
    include_once '../../managers/OrderProductStatusMgr.php'; 
    $date = new DateTime("now", new DateTimeZone("Asia/Kolkata"));
    $engg_details = new OrderProductStatus();
    $engg_details->setOrder_product_id($_POST['order_product_id']);
    $engg_details->setProduct_status($_POST['product_status']);
    $engg_details->setProduct_reason($_POST['product_reason']);
    $engg_details->setProduct_repair_hrs($_POST['product_repair_hrs']);
    $engg_details->setEngg_details_id($_POST['engg_details_id']);
    $engg_details->setStatus_date($date->format('d-m-Y'));
    $engg_detailsMgr = new OrderProductStatusMgr();
    if ($engg_detailsMgr->insOrderProductStatus($engg_details)) {
        echo 'Query inserted Successfully.';
    } else {
        echo 'Error';
    }
?>