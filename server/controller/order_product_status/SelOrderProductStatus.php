<?php
    include_once '../../models/OrderProductStatus.php';
    include_once '../../managers/OrderProductStatusMgr.php';
    $obj = new OrderProductStatusMgr();
    $order_product_status = $obj->selOrderProductStatus($_POST['order_product_id']);
    $str = array();    
    while($row = $order_product_status->fetch()){
        $arr = array(
            'order_product_status_id' => $row['order_product_status_id'], 
            'order_product_id' => $row['order_product_id'], 
            'product_status' => $row['product_status'], 
            'product_reason' => $row['product_reason'], 
            'product_repair_hrs' => $row['product_repair_hrs'], 
            'engg_details_id' => $row['engg_details_id'], 
            'status_date' => $row['status_date'], 
            'in_lab' => $row['in_lab'], 
            'return_item' => $row['return_item'], 
       );
        array_push($str, $arr); 
    }
    echo json_encode($str);
?>