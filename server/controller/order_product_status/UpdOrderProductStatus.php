<?php
    include_once '../../models/OrderProductStatus.php'; 
    include_once '../../managers/OrderProductStatusMgr.php'; 
    $order_product_status = new OrderProductStatus();
    $order_product_status->setOrder_id($_POST['order_product_status_id']);
    $order_product_status->setStatus($_POST['status']);
    $order_product_statusMgr = new OrderProductStatusMgr();
    if ($order_product_statusMgr->updateOrderProductStatus($order_product_status)) {
        echo 'Query inserted Successfully.';
    } else {
        echo 'Error';
    }
?>