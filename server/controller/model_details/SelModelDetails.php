<?php
    include_once '../../models/ModelDetails.php';
    include_once '../../managers/ModelDetailsMgr.php';
    $obj = new ModelDetailsMgr();
    $model_details = $obj->selModelDetails($_POST['brand_details_id'],$_POST['product_details_id']);
    $str = array();    
    while($row = $model_details->fetch()){
            $arr = array(
            'model_details_id' => $row['model_details_id'], 
            'brand_details_id' => $row['brand_details_id'] ,
            'product_details_id' => $row['product_details_id'] ,
            'model_name' => $row['model_name'] 
       );
        array_push($str, $arr); 
    }
    echo json_encode($str);
?>