<?php
    include_once '../../models/ModelDetails.php'; 
    include_once '../../managers/ModelDetailsMgr.php'; 
    $model_details = new ModelDetails();
    $model_details->setModel_name($_POST['model_name']);
    $model_details->setBrand_details_id($_POST['brand_details_id']);
    $model_details->setProduct_details_id($_POST['product_details_id']);
    $model_detailsMgr = new ModelDetailsMgr();
    if ($model_detailsMgr->insModelDetails($model_details)) {
        echo 'Query inserted Successfully.';
    } else {
        echo 'Error';
    }
?>