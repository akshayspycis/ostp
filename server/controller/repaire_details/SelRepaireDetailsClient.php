<?php
    if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
        exit(0);
    }
    $str_json = file_get_contents('php://input'); //($_POST doesn't work here)
    $response = json_decode($str_json, true);
    include_once '../../managers/RepaireDetailsMgr.php';
    $obj = new RepaireDetailsMgr();
    $order = $obj->selRepaireDetails($response['order_product_id']);
    $str = array();    
    while($row = $order->fetch()){
            $arr = array(
            'dae' => $row['dae'], 
            'id' => $row['order_product_status_id'] ,
            'status' => $row['product_status'] ,
            'message' => $row['product_reason'] ,
            'amount' => $row['product_repair_hrs'] ,
            'type' => $row['OP'] ,
       );
        array_push($str, $arr); 
    }
    echo json_encode($str);
?>