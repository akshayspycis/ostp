<?php
    include_once '../../managers/PaymentDetailsMgr.php';
    $obj = new PaymentDetailsMgr();
    $engg_details = $obj->selFromProductRateServiceCharge($_POST['order_product_id']);
    $str = array();    
    while($row = $engg_details->fetch()){
        $arr = array(
            'discription' => 'Service Charge' ,
            'amount' => $row['service_charge'] ,
        );
        array_push($str, $arr); 
    }
    $engg_details = $obj->selFromProductRateMembershipDiscount($_POST['order_product_id']);
    while($row = $engg_details->fetch()){
        $arr = array(
            'type' => 'M' ,
            'discription' => 'Membership Discount ( '.$row['discount'].' )' ,
            'amount' => $row['total'] ,
        );
        array_push($str, $arr); 
    }
    $engg_details = $obj->selFromExtraCharge($_POST['order_product_id']);
    while($row = $engg_details->fetch()){
        $arr = array(
            'discription' => 'Extra Charge for '.$row['charge_reason'] ,
            'amount' => $row['amount'] ,
        );
        array_push($str, $arr); 
    }
    $engg_details = $obj->selFromSpareCharge($_POST['order_product_id']);
    while($row = $engg_details->fetch()){
        $arr = array(
            'discription' => 'Charge for '.$row['spare_reason'].' (Spare Name : '.$row['spare_name'].' ( '.$row['spare_cost'].' Rs. ) Labour Cost ( '.$row['lebure_cost'].' Rs. ) )' ,
            'amount' => $row['spare_cost']+$row['lebure_cost'] ,
        );
        array_push($str, $arr); 
    }
    echo json_encode($str);
?>