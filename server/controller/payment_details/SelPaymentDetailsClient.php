<?php
    if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
        exit(0);
    }
    $str_json = file_get_contents('php://input'); //($response doesn't work here)
    $response = json_decode($str_json, true);
    include_once '../../managers/PaymentDetailsMgr.php';
    $obj = new PaymentDetailsMgr();
    $engg_details = $obj->selFromProductRateServiceCharge($response['order_product_id']);
    $str = array();    
    while($row = $engg_details->fetch()){
        $arr = array(
            'discription' => 'Service Charge' ,
            'amount' => $row['service_charge'] ,
        );
        array_push($str, $arr); 
    }
    $engg_details = $obj->selFromProductRateMembershipDiscount($response['order_product_id']);
    while($row = $engg_details->fetch()){
        $arr = array(
            'type' => 'M' ,
            'discription' => 'Membership Discount ( '.$row['discount'].' %)' ,
            'amount' => $row['total'] ,
        );
        array_push($str, $arr); 
    }
    $engg_details = $obj->selFromExtraCharge($response['order_product_id']);
    while($row = $engg_details->fetch()){
        $arr = array(
            'discription' => 'Extra Charge for '.$row['charge_reason'] ,
            'amount' => $row['amount'] ,
        );
        array_push($str, $arr); 
    }
    $engg_details = $obj->selFromSpareCharge($response['order_product_id']);
    while($row = $engg_details->fetch()){
        $arr = array(
            'discription' => 'Charge for '.$row['spare_reason'].' (Spare Name : '.$row['spare_name'].' ( '.$row['spare_cost'].' Rs. ) Labour Cost ( '.$row['lebure_cost'].' Rs. ) )' ,
            'amount' => $row['spare_cost']+$row['lebure_cost'] ,
        );
        array_push($str, $arr); 
    }
    echo json_encode($str);
?>