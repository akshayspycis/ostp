<?php
    include_once '../../models/PaymentDetails.php'; 
    include_once '../../managers/PaymentDetailsMgr.php'; 
    $date = new DateTime("now", new DateTimeZone("Asia/Kolkata"));
    $payment_details = new PaymentDetails();
    $payment_details->setOrder_product_id($_POST['order_product_id']);
    $payment_details->setUser_id($_POST['user_id']);
    $payment_details->setPayable_amt($_POST['payable_amt']);
    $payment_details->setPaid_amt($_POST['paid_amt']);
    $payment_details->setPayment_date($date->format('d-m-Y'));
    $payment_detailsMgr = new PaymentDetailsMgr();
    if ($payment_detailsMgr->insPaymentDetails($payment_details)) {
        echo 'Query inserted Successfully.';
    } else {
        echo 'Error';
    }
?>