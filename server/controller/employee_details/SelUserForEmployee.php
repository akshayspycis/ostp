<?php
//SelUserForEmployee
    include_once '../../models/UserDetails.php';
    include_once '../../managers/UserDetailsMgr.php';
    $obj = new UserDetailsMgr();
    $user_details = $obj->selUserDetails("");
    $str = array();    
    while($row = $user_details->fetch()){
        if($row['user_type']=="emp"){
            $arr = array(
                'user_id' => $row['user_id'], 
                'user_name' => $row['user_name'] ,
                'dob' => $row['dob'] ,
                'email' => $row['email'] ,
                'contact_no' => $row['contact_no'] ,
                'date' => $row['date'] ,
                'pic' => $row['pic'] ,
            );
        }
        array_push($str, $arr); 
    }
    echo json_encode($str);
?>