<?php
    include_once '../../models/EmployeeDetails.php'; 
    include_once '../../managers/EmployeeDetailsMgr.php'; 
    $employee_details = new EmployeeDetails();
    $employee_details->setAmount($_POST['editamt']);
    $employee_details->setCharge_details_id($_POST['employee_details_id']);
    $employee_detailsMgr = new EmployeeDetailsMgr();
    if ($employee_detailsMgr->updateEmployeeDetails($employee_details)) {
        echo 'Query inserted Successfully.';
    } else {
        echo 'Error';
    }
?>