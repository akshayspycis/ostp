<?php
    include_once '../../models/EmployeeDetails.php'; 
    include_once '../../managers/EmployeeDetailsMgr.php'; 
    $date = new DateTime("now", new DateTimeZone("Asia/Kolkata"));
    $employee_details = new EmployeeDetails();
    $employee_details->setUser_id($_POST['user_id']);
    $employee_details->setDate($date->format('D, d M Y'));   
    $employee_detailsMgr = new EmployeeDetailsMgr();
    if ($employee_detailsMgr->insEmployeeDetails($employee_details)) {
        echo 'Query inserted Successfully.';
    } else {
        echo 'Error';
    }
?>