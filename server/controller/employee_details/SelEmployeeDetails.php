<?php
    include_once '../../models/EmployeeDetails.php';
    include_once '../../managers/EmployeeDetailsMgr.php';
    $obj = new EmployeeDetailsMgr();
    $employee_details = $obj->selEmployeeDetails("");
    $str = array();    
    while($row = $employee_details->fetch()){
            $arr = array(
            'employee_id' => $row['employee_id'], 
            'user_id' => $row['user_id'] ,
            'user_name' => $row['user_name'] ,
            'dob' => $row['dob'] ,
            'email' => $row['email'] ,
            'contact_no' => $row['contact_no'] ,
            'gender' => $row['gender'] ,
            'pic' => $row['pic'] ,
            'date' => $row['date'] ,
            'edate' => $row['edate'] ,
       );
        array_push($str, $arr); 
    }
    echo json_encode($str);
?>