<?php
    include_once '../../models/ProductRate.php';
    include_once '../../managers/ProductRateMgr.php';
    $obj = new ProductRateMgr();
    $product_rate = $obj->selProductRate($_POST['product_details_id']);
    $str = array();    
    while($row = $product_rate->fetch()){
            $arr = array(
            'product_rate_id' => $row['product_rate_id'], 
            'product_details_id' => $row['product_details_id'] ,
            'service_charge' => $row['service_charge'] ,
            'discount' => $row['discount'] ,
            'date' => $row['date'] ,
       );
        array_push($str, $arr); 
    }
    echo json_encode($str);
?>