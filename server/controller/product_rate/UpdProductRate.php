<?php
    include_once '../../models/ProductRate.php'; 
    include_once '../../managers/ProductRateMgr.php'; 
    $product_rate = new ProductRate();
    $product_rate->setService_charge($_POST['service_charge']);
    $product_rate->setDiscount($_POST['discount']);
    $product_rate->setProduct_rate_id($_POST['product_rate_id']);
    $product_rateMgr = new ProductRateMgr();
    if ($product_rateMgr->updateProductRate($product_rate)) {
        echo 'Query inserted Successfully.';
    } else {
        echo 'Error';
    }
?>