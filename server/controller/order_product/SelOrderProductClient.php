<?php
    if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
        exit(0);
    }
    $str_json = file_get_contents('php://input'); //($_POST doesn't work here)
    $response = json_decode($str_json, true);
    include_once '../../models/OrderProduct.php';
    include_once '../../managers/OrderProductMgr.php';
        include_once '../../mgr/Url.php';
    $obj = new OrderProductMgr();
    $order_product = $obj->selOrderProduct($response ['order_id']);
    include_once '../../managers/PaymentDetailsMgr.php';
    $obj1 = new PaymentDetailsMgr();
    $str = array();    
    while($row = $order_product->fetch()){
       $arr = array(
            'order_product_id' => $row['order_product_id'], 
            'product_name' => $row['product_name'], 
            'icon' =>$url.str_replace('../../', '',$row['icon']),
            'status' => $row['status'], 
            'order_allot_detailsId' => $row['order_allot_detailsId'], 
            'brand_name' => $row['brand_name'], 
            'model_name' => $row['model_name'], 
            'problem' => $row['problem'], 
            'discription' => $row['discription'], 
            'problem_discription' => $row['problem_discription'], 
            'engg_details_id' => $row['engg_details_id'], 
            'order_product_status_id' => $row['order_product_status_id'], 
            'repaire' => $row['repaire'], 
       );
        array_push($str, $arr); 
    }
    $str1 = array();    
    for ($index = 0; $index < count($str); $index++) {
        $str[$index]['amount']=$obj1->selAllAmount($str[$index]['order_product_id'],$obj1);
        array_push($str1, $str[$index]); 
    }
    echo json_encode($str1);
    
?>