<?php
    include_once '../../models/OrderProduct.php';
    include_once '../../managers/OrderProductMgr.php';
    $obj = new OrderProductMgr();
    $order_product = $obj->selOrderProduct($_POST['order_id']);
    $str = array();    
    while($row = $order_product->fetch()){
       $arr = array(
            'order_product_id' => $row['order_product_id'], 
            'product_name' => $row['product_name'], 
            'icon' => $row['icon'], 
            'status' => $row['status'], 
            'order_allot_detailsId' => $row['order_allot_detailsId'], 
            'brand_name' => $row['brand_name'], 
            'model_name' => $row['model_name'], 
            'problem' => $row['problem'], 
            'discription' => $row['discription'], 
            'problem_discription' => $row['problem_discription'], 
       );
        array_push($str, $arr); 
    }
    echo json_encode($str);
?>