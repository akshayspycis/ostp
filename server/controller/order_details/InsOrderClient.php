<?php
    if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
        exit(0);
    }
    $str_json = file_get_contents('php://input'); //($response doesn't work here)
//    echo $str_json;
    $response = json_decode($str_json, true);
    $date = new DateTime("now", new DateTimeZone("Asia/Kolkata"));
    include_once '../../models/OrderDetails.php'; 
    include_once '../../managers/OrderDetailsMgr.php'; 
    include_once '../../managers/OrderProductMgr.php'; 
    include_once '../../models/OrderProduct.php'; 
    $order = new OrderDetails();
    $order->setUser_id($response['user_id']);
    $order->setOrder_date($date->format('D, d M Y'));
    $order->setOrder_no(time());
    $order->setStatus('false');
    $order->setAlt_contact_no($response['alt_contact_no']);
    $order->setAddress_details_id($response['address_details_id']);
    $orderMgr = new OrderDetailsMgr();
    if ($orderMgr->insOrderDetails($order)) {
        $order_productMgr = new OrderProductMgr();
        $order_product_list=array();
    foreach ($response['order_products'] as $val) {
        $order_product=new OrderProduct();
            foreach ($val as $key => $value) {
                if($key=='product_details_id') $order_product->setProduct_details_id($value);
                if($key=='brand_details_id') $order_product->setBrand_details_id($value);
                if($key=='model_details_id') $order_product->setModel_details_id($value);
                if($key=='problem_details_id') $order_product->setProblem_details_id($value);
                if($key=='problem_discription') $order_product->setProblem_discription($value);
            }
            array_push($order_product_list, $order_product);
        }
        if($order_productMgr->insOrderProduct($order_product_list)){
            echo time();
        }else{
            echo 0;
        }
    } else {
        echo 0;
    }
?>