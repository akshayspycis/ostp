<?php
    if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
        exit(0);
    }
    $str_json = file_get_contents('php://input'); //($_POST doesn't work here)
    $response = json_decode($str_json, true);
    include_once '../../models/OrderDetails.php';
    include_once '../../managers/OrderDetailsMgr.php';
    $obj = new OrderDetailsMgr();
    $order = $obj->selOrderDetailsClient($response['user_id']);
    $str = array();    
    while($row = $order->fetch()){
            $arr = array(
            'order_id' => $row['order_id'], 
            'order_date' => $row['order_date'] ,
            'order_no' => $row['order_no'] ,
            'status' => $row['status'] ,
            'count' => $row['count'] ,
       );
        array_push($str, $arr); 
    }
    echo json_encode($str);
?>