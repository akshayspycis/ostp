<?php
    include_once '../../models/OrderDetails.php'; 
    include_once '../../managers/OrderDetailsMgr.php'; 
    $order = new OrderDetails();
    $order->setOrder_id($_POST['order_id']);
    $order->setStatus($_POST['status']);
    $orderMgr = new OrderDetailsMgr();
    if ($orderMgr->updateOrderDetails($order)) {
        echo 'Query inserted Successfully.';
    } else {
        echo 'Error';
    }
?>