<?php
    include_once '../../models/OrderLabSpare.php'; 
    include_once '../../managers/OrderLabSpareMgr.php'; 
            $date = new DateTime("now", new DateTimeZone("Asia/Kolkata"));
    $order_lab_spare = new OrderLabSpare();
    $order_lab_spare->setSpare_details_id($_POST['spare_details_id']);
    $order_lab_spare->setUser_status("false");
    $order_lab_spare->setCall_user("false");
    $order_lab_spare->setSms("false");
    $order_lab_spare->setLab_spare_date($date->format('d-m-Y'));
    $order_lab_spare->setLab_management_id($_POST['lab_management_id']);
    $order_lab_spare->setOrder_product_id($_POST['order_product_id']);
    $order_lab_spare->setSpare_reason($_POST['spare_reason']);
    $order_lab_spareMgr = new OrderLabSpareMgr();
    if ($order_lab_spareMgr->insOrderLabSpare($order_lab_spare)) {
        echo 'Query inserted Successfully.';
    } else {
        echo 'Error';
    }
?>