<?php
    include_once '../../models/OrderLabSpare.php'; 
    include_once '../../managers/OrderLabSpareMgr.php'; 
    $order_lab_spare = new OrderLabSpare();
    $order_lab_spare->setOrder_lab_spare_id($_POST['order_lab_spare_id']);
    $order_lab_spare->setUser_status($_POST['status']);
    $order_lab_spareMgr = new OrderLabSpareMgr();
    if ($order_lab_spareMgr->updateOrderLabSpareStatus($order_lab_spare)) {
        echo 'Query inserted Successfully.';
    } else {
        echo 'Error';
    }
?>