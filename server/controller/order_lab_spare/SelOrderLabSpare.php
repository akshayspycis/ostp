<?php
    include_once '../../models/OrderLabSpare.php';
    include_once '../../managers/OrderLabSpareMgr.php';
    $obj = new OrderLabSpareMgr();
    $order_lab_spare = $obj->selOrderLabSpare($_POST['order_product_id']);
    $str = array();    
    while($row = $order_lab_spare->fetch()){
        $arr = array(
            'order_lab_spare_id' => $row['order_lab_spare_id'], 
            'spare_name' => $row['spare_name'], 
            'user_status' => $row['user_status'], 
            'call_user' => $row['call_user'], 
            'sms' => $row['sms'], 
            'lab_spare_date' => $row['lab_spare_date'], 
            'lab_management_id' => $row['lab_management_id'], 
            'order_product_id' => $row['order_product_id'], 
            'spare_reason' => $row['spare_reason'], 
        );
        array_push($str, $arr); 
    }
    echo json_encode($str);
?>