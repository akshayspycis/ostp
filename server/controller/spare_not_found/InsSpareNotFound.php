<?php
    include_once '../../models/SpareNotFound.php'; 
    include_once '../../managers/SpareNotFoundMgr.php'; 
    $spare_not_found = new SpareNotFound();
    $spare_not_found->setOrder_product_id($_POST['order_product_id']);
    $spare_not_found->setMessage($_POST['message']);
    $spare_not_found->setSnf_date($_POST['snf_date']);
    $spare_not_found->setStatus($_POST['status']);
    $spare_not_found->setLab_management_id($_POST['lab_management_id']);
    $spare_not_foundMgr = new SpareNotFoundMgr();
    if ($spare_not_foundMgr->insSpareNotFound($spare_not_found)) {
        echo 'Query inserted Successfully.';
    } else {
        echo 'Error';
    }
?>