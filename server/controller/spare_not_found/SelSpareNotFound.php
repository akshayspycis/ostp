<?php
    include_once '../../models/SpareNotFound.php';
    include_once '../../managers/SpareNotFoundMgr.php';
    $obj = new SpareNotFoundMgr();
    $spare_not_found = $obj->selSpareNotFound($_POST['spare_not_found_id']);
    $str = array();    
    while($row = $spare_not_found->fetch()){
        $arr = array(
            'spare_not_found_id' => $row['spare_not_found_id'], 
            'order_product_id' => $row['order_product_id'], 
            'message' => $row['message'], 
            'snf_date' => $row['snf_date'], 
            'status' => $row['status'], 
            'lab_management_id' => $row['lab_management_id'], 
        );
        array_push($str, $arr); 
    }
    echo json_encode($str);
?>