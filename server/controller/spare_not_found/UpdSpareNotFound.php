<?php
    include_once '../../models/SpareNotFound.php'; 
    include_once '../../managers/SpareNotFoundMgr.php'; 
    $spare_not_found = new SpareNotFound();
    $spare_not_found->setSpare_not_found_id($_POST['spare_not_found_id']);
    $spare_not_found->setStatus($_POST['status']);
    $spare_not_foundMgr = new SpareNotFoundMgr();
    if ($spare_not_foundMgr->updateSpareNotFound($spare_not_found)) {
        echo 'Query inserted Successfully.';
    } else {
        echo 'Error';
    }
?>