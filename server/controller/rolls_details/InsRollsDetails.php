<?php
    include_once '../../models/RollsDetails.php'; 
    include_once '../../managers/RollsDetailsMgr.php'; 
    $rolls_details = new RollsDetails();
    $rolls_details->setEmployee_id($_POST['employee_id']);
    $rolls_details->setRoll_type_id($_POST['roll_type_id']);
    $rolls_detailsMgr = new RollsDetailsMgr();
    if ($rolls_detailsMgr->insRollsDetails($rolls_details)) {
        echo 'Query inserted Successfully.';
    } else {
        echo 'Error';
    }
?>