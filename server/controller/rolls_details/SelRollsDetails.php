<?php
    include_once '../../models/RollsDetails.php';
    include_once '../../managers/RollsDetailsMgr.php';
    $obj = new RollsDetailsMgr();
    $rolls_details = $obj->selRollsDetails($_POST['roll_type_id']);
    $str = array();    
    while($row = $rolls_details->fetch()){
            $arr = array(
            'rolls_details_id' => $row['rolls_details_id'], 
            'employee_id' => $row['employee_id'] ,
            'roll_type_id' => $row['roll_type_id'] ,
            'user_name' => $row['user_name'] ,
            'email' => $row['email'] ,
            'contact_no' => $row['contact_no'] ,
            'date' => $row['date'] ,
            'pic' => $row['pic'], 
            'head' => $row['head'] ,
       );
        array_push($str, $arr); 
    }
    echo json_encode($str);
?>