<?php
    include_once '../../models/RollsDetails.php'; 
    include_once '../../managers/RollsDetailsMgr.php'; 
    $rolls_details = new RollsDetails();
    $rolls_details->setHead($_POST['head']);
    $rolls_details->setRolls_details_id($_POST['rolls_details_id']);
    $rolls_detailsMgr = new RollsDetailsMgr();
    if ($rolls_detailsMgr->updateRollsDetails($rolls_details)) {
        echo 'Query inserted Successfully.';
    } else {
        echo 'Error';
    }
?>