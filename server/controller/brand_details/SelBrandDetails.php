<?php
    include_once '../../models/BrandDetails.php';
    include_once '../../managers/BrandDetailsMgr.php';
    $obj = new BrandDetailsMgr();
    $brand_details = $obj->selBrandDetails();
    $str = array();    
    while($row = $brand_details->fetch()){
            $arr = array(
            'brand_details_id' => $row['brand_details_id'], 
            'brand_name' => $row['brand_name'] 
       );
        array_push($str, $arr); 
    }
    echo json_encode($str);
?>