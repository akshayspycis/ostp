<?php
    include_once '../../models/PrivilegesDetails.php'; 
    include_once '../../managers/PrivilegesDetailsMgr.php'; 
    $date = new DateTime("now", new DateTimeZone("Asia/Kolkata"));
    $employee_details = new PrivilegesDetails();
    $employee_details->setEmployee_id($_POST['employee_id']);
    $employee_details->setModule_id($_POST['module_id']);
    $employee_details->setInsertm($_POST['insertm']);
    $employee_details->setUpdatem($_POST['updatem']);
    $employee_details->setDeletem($_POST['deletem']);
    $employee_details->setSelectm($_POST['selectm']);
    $employee_detailsMgr = new PrivilegesDetailsMgr();
    if ($employee_detailsMgr->insPrivilegesDetails($employee_details)) {
        echo 'Query inserted Successfully.';
    } else {
        echo 'Error';
    }
?>