<?php
    include_once '../../models/OrderAllotDetails.php'; 
    include_once '../../managers/OrderAllotDetailsMgr.php'; 
    $order_allot_details = new OrderAllotDetails();
    $order_allot_details->setOrder_id($_POST['order_id']);
    $order_allot_details->setOrder_product_id($_POST['order_product_id']);
    $order_allot_details->setEmployee_id($_POST['employee_id']);
    $order_allot_details->setEngg_time($_POST['engg_time']);
    $order_allot_details->setEngg_date($_POST['engg_date']);
    $order_allot_details->setCus_time($_POST['cus_time']);
    $order_allot_details->setCus_date($_POST['cus_date']);
    $order_allot_details->setStatus('false');
    $order_allot_detailsMgr = new OrderAllotDetailsMgr();
    if ($order_allot_detailsMgr->insOrderAllotDetails($order_allot_details)) {
        echo 'Query inserted Successfully.';
    } else {
        echo 'Error';
    }
?>