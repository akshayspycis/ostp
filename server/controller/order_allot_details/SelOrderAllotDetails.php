<?php
    include_once '../../models/OrderAllotDetails.php';
    include_once '../../managers/OrderAllotDetailsMgr.php';
    $obj = new OrderAllotDetailsMgr();
    $order_allot_details = $obj->selOrderAllotDetails($_POST['order_allot_detailsId']);
    $str = array();    
    while($row = $order_allot_details->fetch()){
       $arr = array(
            'order_id' => $row['order_id'] ,
            'order_product_id' => $row['order_product_id'] ,
            'employee_id' => $row['employee_id'] ,
            'pic' => $row['pic'] ,
            'user_name' => $row['user_name'] ,
            'email' => $row['email'] ,
            'contact_no' => $row['contact_no'] ,
            'engg_time' => $row['engg_time'] ,
            'engg_date' => $row['engg_date'] ,
            'cus_time' => $row['cus_time'] ,
            'cus_date' => $row['cus_date'] ,
            'status' => $row['status'] ,
       );
        array_push($str, $arr); 
    }
    echo json_encode($str);
?>