<?php
    class SpareNotFound{
        private $spare_not_found_id;
        private $order_product_id;
        private $message;
        private $snf_date;
        private $status;
        private $lab_management_id;
        
        function getSpare_not_found_id() {
            return $this->spare_not_found_id;
        }

        function getOrder_product_id() {
            return $this->order_product_id;
        }

        function getMessage() {
            return $this->message;
        }

        function getSnf_date() {
            return $this->snf_date;
        }

        function getStatus() {
            return $this->status;
        }

        function getLab_management_id() {
            return $this->lab_management_id;
        }

        function setSpare_not_found_id($spare_not_found_id) {
            $this->spare_not_found_id = $spare_not_found_id;
        }

        function setOrder_product_id($order_product_id) {
            $this->order_product_id = $order_product_id;
        }

        function setMessage($message) {
            $this->message = $message;
        }

        function setSnf_date($snf_date) {
            $this->snf_date = $snf_date;
        }

        function setStatus($status) {
            $this->status = $status;
        }

        function setLab_management_id($lab_management_id) {
            $this->lab_management_id = $lab_management_id;
        }


    }


