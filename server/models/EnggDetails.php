<?php
    class EnggDetails{
        private $engg_details_id;
        private $order_id;
        private $order_allot_details_id;
        private $meeting_time;
        private $meeting_date;
        private $meeting_person;
        private $order_product_id;
        private $employee_id;
        function getEngg_details_id() {
            return $this->engg_details_id;
        }

        function getOrder_id() {
            return $this->order_id;
        }

        function getOrder_allot_details_id() {
            return $this->order_allot_details_id;
        }

        function getMeeting_time() {
            return $this->meeting_time;
        }

        function getMeeting_date() {
            return $this->meeting_date;
        }

        function getMeeting_person() {
            return $this->meeting_person;
        }

        function getOrder_product_id() {
            return $this->order_product_id;
        }

        function getEmployee_id() {
            return $this->employee_id;
        }

        function setEngg_details_id($engg_details_id) {
            $this->engg_details_id = $engg_details_id;
        }

        function setOrder_id($order_id) {
            $this->order_id = $order_id;
        }

        function setOrder_allot_details_id($order_allot_details_id) {
            $this->order_allot_details_id = $order_allot_details_id;
        }

        function setMeeting_time($meeting_time) {
            $this->meeting_time = $meeting_time;
        }

        function setMeeting_date($meeting_date) {
            $this->meeting_date = $meeting_date;
        }

        function setMeeting_person($meeting_person) {
            $this->meeting_person = $meeting_person;
        }

        function setOrder_product_id($order_product_id) {
            $this->order_product_id = $order_product_id;
        }

        function setEmployee_id($employee_id) {
            $this->employee_id = $employee_id;
        }


        }
