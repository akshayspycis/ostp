<?php
    class ProductRate{
        private $product_rate_id;
        private $product_details_id;
        private $service_charge;
        private $discount;
        private $date;
        function getProduct_rate_id() {
            return $this->product_rate_id;
        }

        function getProduct_details_id() {
            return $this->product_details_id;
        }

        function getService_charge() {
            return $this->service_charge;
        }

        function getDiscount() {
            return $this->discount;
        }

        function getDate() {
            return $this->date;
        }

        function setProduct_rate_id($product_rate_id) {
            $this->product_rate_id = $product_rate_id;
        }

        function setProduct_details_id($product_details_id) {
            $this->product_details_id = $product_details_id;
        }

        function setService_charge($service_charge) {
            $this->service_charge = $service_charge;
        }

        function setDiscount($discount) {
            $this->discount = $discount;
        }

        function setDate($date) {
            $this->date = $date;
        }


    }
