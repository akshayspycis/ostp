<?php
    class ModuleType{
        private $module_id;
        private $module_name;
        function getModule_id() {
            return $this->module_id;
        }

        function getModule_name() {
            return $this->module_name;
        }

        function setModule_id($module_id) {
            $this->module_id = $module_id;
        }

        function setModule_name($module_name) {
            $this->module_name = $module_name;
        }


    }
