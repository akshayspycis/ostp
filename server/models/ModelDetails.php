<?php
    class ModelDetails{
        private $model_details_id;
        private $brand_details_id;
        private $product_details_id;
        private $model_name;
        function getModel_details_id() {
            return $this->model_details_id;
        }

        function getBrand_details_id() {
            return $this->brand_details_id;
        }

        function getProduct_details_id() {
            return $this->product_details_id;
        }

        function getModel_name() {
            return $this->model_name;
        }

        function setModel_details_id($model_details_id) {
            $this->model_details_id = $model_details_id;
        }

        function setBrand_details_id($brand_details_id) {
            $this->brand_details_id = $brand_details_id;
        }

        function setProduct_details_id($product_details_id) {
            $this->product_details_id = $product_details_id;
        }

        function setModel_name($model_name) {
            $this->model_name = $model_name;
        }


    }


