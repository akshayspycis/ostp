<?php
    class AddressDetails{
        private $address_details_id;
        private $user_id;
        private $address_1;
        private $address_2;
        private $city;
        private $pincode;
        private $state;
        private $user_data_id;
        function getAddress_details_id() {
            return $this->address_details_id;
        }

        function getUser_id() {
            return $this->user_id;
        }

        function getAddress_1() {
            return $this->address_1;
        }

        function getAddress_2() {
            return $this->address_2;
        }

        function getCity() {
            return $this->city;
        }

        function getPincode() {
            return $this->pincode;
        }

        function getState() {
            return $this->state;
        }

        function getUser_data_id() {
            return $this->user_data_id;
        }

        function setAddress_details_id($address_details_id) {
            $this->address_details_id = $address_details_id;
        }

        function setUser_id($user_id) {
            $this->user_id = $user_id;
        }

        function setAddress_1($address_1) {
            $this->address_1 = $address_1;
        }

        function setAddress_2($address_2) {
            $this->address_2 = $address_2;
        }

        function setCity($city) {
            $this->city = $city;
        }

        function setPincode($pincode) {
            $this->pincode = $pincode;
        }

        function setState($state) {
            $this->state = $state;
        }

        function setUser_data_id($user_data_id) {
            $this->user_data_id = $user_data_id;
        }


    }


