<?php
    class UserData{
        private $user_data_id;
        private $user_name;
        private $contact_no;
        private $email;
        private $visit;
        private $employee_id;
        function getUser_data_id() {
            return $this->user_data_id;
        }

        function getUser_name() {
            return $this->user_name;
        }

        function getContact_no() {
            return $this->contact_no;
        }

        function getEmail() {
            return $this->email;
        }

        function getVisit() {
            return $this->visit;
        }

        function getEmployee_id() {
            return $this->employee_id;
        }

        function setUser_data_id($user_data_id) {
            $this->user_data_id = $user_data_id;
        }

        function setUser_name($user_name) {
            $this->user_name = $user_name;
        }

        function setContact_no($contact_no) {
            $this->contact_no = $contact_no;
        }

        function setEmail($email) {
            $this->email = $email;
        }

        function setVisit($visit) {
            $this->visit = $visit;
        }

        function setEmployee_id($employee_id) {
            $this->employee_id = $employee_id;
        }


    }

