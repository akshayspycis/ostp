
<?php
    class OrderAllotDetails{
        private $order_allot_detailsId;
        private $order_id;
        private $order_product_id;
        private $employee_id;
        private $engg_time;
        private $engg_date;
        private $cus_time;
        private $cus_date;
        private $status;

        
        function getOrder_allot_detailsId() {
            return $this->order_allot_detailsId;
        }

        function getOrder_id() {
            return $this->order_id;
        }

        function getOrder_product_id() {
            return $this->order_product_id;
        }

        function getEmployee_id() {
            return $this->employee_id;
        }

        function getEngg_time() {
            return $this->engg_time;
        }

        function getEngg_date() {
            return $this->engg_date;
        }

        function getCus_time() {
            return $this->cus_time;
        }

        function getCus_date() {
            return $this->cus_date;
        }

        function getStatus() {
            return $this->status;
        }

        function getEng_call() {
            return $this->eng_call;
        }

        function setOrder_allot_detailsId($order_allot_detailsId) {
            $this->order_allot_detailsId = $order_allot_detailsId;
        }

        function setOrder_id($order_id) {
            $this->order_id = $order_id;
        }

        function setOrder_product_id($order_product_id) {
            $this->order_product_id = $order_product_id;
        }

        function setEmployee_id($employee_id) {
            $this->employee_id = $employee_id;
        }

        function setEngg_time($engg_time) {
            $this->engg_time = $engg_time;
        }

        function setEngg_date($engg_date) {
            $this->engg_date = $engg_date;
        }

        function setCus_time($cus_time) {
            $this->cus_time = $cus_time;
        }

        function setCus_date($cus_date) {
            $this->cus_date = $cus_date;
        }

        function setStatus($status) {
            $this->status = $status;
        }

        

    }


