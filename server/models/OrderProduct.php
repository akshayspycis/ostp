<?php
    class OrderProduct{
        private $order_product_id;
        private $user_id;
        private $product_details_id;
        private $problem_details_id;
        private $model_details_id;
        private $problem_discription;
        private $order_id;
        private $brand_details_id;
        function getOrder_product_id() {
            return $this->order_product_id;
        }

        function getUser_id() {
            return $this->user_id;
        }

        function getProduct_details_id() {
            return $this->product_details_id;
        }

        function getProblem_details_id() {
            return $this->problem_details_id;
        }

        function getModel_details_id() {
            return $this->model_details_id;
        }

        function getProblem_discription() {
            return $this->problem_discription;
        }

        function getOrder_id() {
            return $this->order_id;
        }

        function getBrand_details_id() {
            return $this->brand_details_id;
        }

        function setOrder_product_id($order_product_id) {
            $this->order_product_id = $order_product_id;
        }

        function setUser_id($user_id) {
            $this->user_id = $user_id;
        }

        function setProduct_details_id($product_details_id) {
            $this->product_details_id = $product_details_id;
        }

        function setProblem_details_id($problem_details_id) {
            $this->problem_details_id = $problem_details_id;
        }

        function setModel_details_id($model_details_id) {
            $this->model_details_id = $model_details_id;
        }

        function setProblem_discription($problem_discription) {
            $this->problem_discription = $problem_discription;
        }

        function setOrder_id($order_id) {
            $this->order_id = $order_id;
        }

        function setBrand_details_id($brand_details_id) {
            $this->brand_details_id = $brand_details_id;
        }


    }


