<?php
    class PrivilegesDetails{
        private $privileges_details_id;
        private $employee_id;
        private $module_id;
        private $insertm;
        private $updatem;
        private $deletem;
        private $selectm;
        
        function getPrivileges_details_id() {
            return $this->privileges_details_id;
        }

        function getEmployee_id() {
            return $this->employee_id;
        }

        function getModule_id() {
            return $this->module_id;
        }

        function getInsertm() {
            return $this->insertm;
        }

        function getUpdatem() {
            return $this->updatem;
        }

        function getDeletem() {
            return $this->deletem;
        }

        function getSelectm() {
            return $this->selectm;
        }

        function setPrivileges_details_id($privileges_details_id) {
            $this->privileges_details_id = $privileges_details_id;
        }

        function setEmployee_id($employee_id) {
            $this->employee_id = $employee_id;
        }

        function setModule_id($module_id) {
            $this->module_id = $module_id;
        }

        function setInsertm($insertm) {
            $this->insertm = $insertm;
        }

        function setUpdatem($updatem) {
            $this->updatem = $updatem;
        }

        function setDeletem($deletem) {
            $this->deletem = $deletem;
        }

        function setSelectm($selectm) {
            $this->selectm = $selectm;
        }


    }
