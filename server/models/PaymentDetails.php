<?php
    class PaymentDetails{
        private $payment_details_id;
        private $order_product_id;
        private $user_id;
        private $payable_amt;
        private $paid_amt;
        private $payment_date;
        function getPayment_details_id() {
            return $this->payment_details_id;
        }

        function getOrder_product_id() {
            return $this->order_product_id;
        }

        function getUser_id() {
            return $this->user_id;
        }

        function getPayable_amt() {
            return $this->payable_amt;
        }

        function getPaid_amt() {
            return $this->paid_amt;
        }

        function getPayment_date() {
            return $this->payment_date;
        }

        function setPayment_details_id($payment_details_id) {
            $this->payment_details_id = $payment_details_id;
        }

        function setOrder_product_id($order_product_id) {
            $this->order_product_id = $order_product_id;
        }

        function setUser_id($user_id) {
            $this->user_id = $user_id;
        }

        function setPayable_amt($payable_amt) {
            $this->payable_amt = $payable_amt;
        }

        function setPaid_amt($paid_amt) {
            $this->paid_amt = $paid_amt;
        }

        function setPayment_date($payment_date) {
            $this->payment_date = $payment_date;
        }


    }


