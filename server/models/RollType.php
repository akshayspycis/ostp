<?php
    class RollType{
        private $roll_type_id;
        private $roll_type;
        function getRoll_type_id() {
            return $this->roll_type_id;
        }

        function getRoll_type() {
            return $this->roll_type;
        }

        function setRoll_type_id($roll_type_id) {
            $this->roll_type_id = $roll_type_id;
        }

        function setRoll_type($roll_type) {
            $this->roll_type = $roll_type;
        }

    }


