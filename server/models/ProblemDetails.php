<?php
    class ProblemDetails{
        private $problem_details_id;
        private $brand_details_id;
        private $model_details_id;
        private $product_details_id;
        private $problem;
        private $discription;
        function getProblem_details_id() {
            return $this->problem_details_id;
        }

        function getBrand_details_id() {
            return $this->brand_details_id;
        }

        function getModel_details_id() {
            return $this->model_details_id;
        }

        function getProduct_details_id() {
            return $this->product_details_id;
        }

        function getProblem() {
            return $this->problem;
        }

        function getDiscription() {
            return $this->discription;
        }

        function setProblem_details_id($problem_details_id) {
            $this->problem_details_id = $problem_details_id;
        }

        function setBrand_details_id($brand_details_id) {
            $this->brand_details_id = $brand_details_id;
        }

        function setModel_details_id($model_details_id) {
            $this->model_details_id = $model_details_id;
        }

        function setProduct_details_id($product_details_id) {
            $this->product_details_id = $product_details_id;
        }

        function setProblem($problem) {
            $this->problem = $problem;
        }

        function setDiscription($discription) {
            $this->discription = $discription;
        }


    }


