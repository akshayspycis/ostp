<?php
    class RollsDetails{
        private $rolls_details_id;
        private $employee_id;
        private $roll_type_id;
        private $date;
        private $head;
        function getRolls_details_id() {
            return $this->rolls_details_id;
        }

        function getEmployee_id() {
            return $this->employee_id;
        }

        function getRoll_type_id() {
            return $this->roll_type_id;
        }

        function getDate() {
            return $this->date;
        }

        function getHead() {
            return $this->head;
        }

        function setRolls_details_id($rolls_details_id) {
            $this->rolls_details_id = $rolls_details_id;
        }

        function setEmployee_id($employee_id) {
            $this->employee_id = $employee_id;
        }

        function setRoll_type_id($roll_type_id) {
            $this->roll_type_id = $roll_type_id;
        }

        function setDate($date) {
            $this->date = $date;
        }

        function setHead($head) {
            $this->head = $head;
        }


    }


