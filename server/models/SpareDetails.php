<?php
    class SpareDetails{
        private $spare_details_id;
        private $spare_name;
        private $spare_cost;
        private $lebure_cost;
        private $product_details_id;
        function getSpare_details_id() {
            return $this->spare_details_id;
        }

        function getSpare_name() {
            return $this->spare_name;
        }

        function getSpare_cost() {
            return $this->spare_cost;
        }

        function getLebure_cost() {
            return $this->lebure_cost;
        }

        function getProduct_details_id() {
            return $this->product_details_id;
        }

        function setSpare_details_id($spare_details_id) {
            $this->spare_details_id = $spare_details_id;
        }

        function setSpare_name($spare_name) {
            $this->spare_name = $spare_name;
        }

        function setSpare_cost($spare_cost) {
            $this->spare_cost = $spare_cost;
        }

        function setLebure_cost($lebure_cost) {
            $this->lebure_cost = $lebure_cost;
        }

        function setProduct_details_id($product_details_id) {
            $this->product_details_id = $product_details_id;
        }


    }


