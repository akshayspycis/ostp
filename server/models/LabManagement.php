<?php
    class LabManagement{
        private $lab_management_id;
        private $order_product_id;
        private $order_id;
        private $receive_date_time;
        private $return_date_time;
        private $status;
        function getLab_management_id() {
            return $this->lab_management_id;
        }

        function getOrder_product_id() {
            return $this->order_product_id;
        }

        function getOrder_id() {
            return $this->order_id;
        }

        function getReceive_date_time() {
            return $this->receive_date_time;
        }

        function getReturn_date_time() {
            return $this->return_date_time;
        }

        function getStatus() {
            return $this->status;
        }

        function setLab_management_id($lab_management_id) {
            $this->lab_management_id = $lab_management_id;
        }

        function setOrder_product_id($order_product_id) {
            $this->order_product_id = $order_product_id;
        }

        function setOrder_id($order_id) {
            $this->order_id = $order_id;
        }

        function setReceive_date_time($receive_date_time) {
            $this->receive_date_time = $receive_date_time;
        }

        function setReturn_date_time($return_date_time) {
            $this->return_date_time = $return_date_time;
        }

        function setStatus($status) {
            $this->status = $status;
        }


        }
