<?php
    class TempWallet{
        private $temp_wallet_id;
        private $user_data_id;
        private $transaction_code;
        private $amount;
        private $from_date;
        private $to_date;
        
        function getTemp_wallet_id() {
            return $this->temp_wallet_id;
        }

        function getUser_data_id() {
            return $this->user_data_id;
        }

        function getTransaction_code() {
            return $this->transaction_code;
        }

        function getAmount() {
            return $this->amount;
        }

        function getFrom_date() {
            return $this->from_date;
        }

        function getTo_date() {
            return $this->to_date;
        }

        function setTemp_wallet_id($temp_wallet_id) {
            $this->temp_wallet_id = $temp_wallet_id;
        }

        function setUser_data_id($user_data_id) {
            $this->user_data_id = $user_data_id;
        }

        function setTransaction_code($transaction_code) {
            $this->transaction_code = $transaction_code;
        }

        function setAmount($amount) {
            $this->amount = $amount;
        }

        function setFrom_date($from_date) {
            $this->from_date = $from_date;
        }

        function setTo_date($to_date) {
            $this->to_date = $to_date;
        }


    }
