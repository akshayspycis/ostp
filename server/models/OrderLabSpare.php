<?php
    class OrderLabSpare{
        private $order_lab_spare_id;
        private $spare_details_id;
        private $user_status;
        private $call_user;
        private $sms;
        private $lab_spare_date;
        private $lab_management_id;
        private $order_product_id;
        private $spare_reason;
        
        function getOrder_lab_spare_id() {
            return $this->order_lab_spare_id;
        }

        function getSpare_details_id() {
            return $this->spare_details_id;
        }

        function getUser_status() {
            return $this->user_status;
        }

        function getCall_user() {
            return $this->call_user;
        }

        function getSms() {
            return $this->sms;
        }

        function getLab_spare_date() {
            return $this->lab_spare_date;
        }

        function getLab_management_id() {
            return $this->lab_management_id;
        }

        function getOrder_product_id() {
            return $this->order_product_id;
        }

        function getSpare_reason() {
            return $this->spare_reason;
        }

        function setOrder_lab_spare_id($order_lab_spare_id) {
            $this->order_lab_spare_id = $order_lab_spare_id;
        }

        function setSpare_details_id($spare_details_id) {
            $this->spare_details_id = $spare_details_id;
        }

        function setUser_status($user_status) {
            $this->user_status = $user_status;
        }

        function setCall_user($call_user) {
            $this->call_user = $call_user;
        }

        function setSms($sms) {
            $this->sms = $sms;
        }

        function setLab_spare_date($lab_spare_date) {
            $this->lab_spare_date = $lab_spare_date;
        }

        function setLab_management_id($lab_management_id) {
            $this->lab_management_id = $lab_management_id;
        }

        function setOrder_product_id($order_product_id) {
            $this->order_product_id = $order_product_id;
        }

        function setSpare_reason($spare_reason) {
            $this->spare_reason = $spare_reason;
        }


        }
