<?php
    class Otp{
        
        private $otp_id;
        private $contact_no;
        private $otp_no;
        public function getOtp_id() {
            return $this->otp_id;
        }

        public function getContact_no() {
            return $this->contact_no;
        }

        public function getOtp_no() {
            return $this->otp_no;
        }

        public function setOtp_id($otp_id) {
            $this->otp_id = $otp_id;
        }

        public function setContact_no($contact_no) {
            $this->contact_no = $contact_no;
        }

        public function setOtp_no($otp_no) {
            $this->otp_no = $otp_no;
        }


}



