<?php
    class OrderLabExtraCharge{
        private $order_lab_extra_charge_id;
        private $charge_reason;
        private $amount;
        private $lab_date;
        private $order_product_id;
        private $lab_management_id;
        private $status;
        
        function getOrder_lab_extra_charge_id() {
            return $this->order_lab_extra_charge_id;
        }

        function getCharge_reason() {
            return $this->charge_reason;
        }

        function getAmount() {
            return $this->amount;
        }

        function getLab_date() {
            return $this->lab_date;
        }

        function getOrder_product_id() {
            return $this->order_product_id;
        }

        function getLab_management_id() {
            return $this->lab_management_id;
        }

        function getStatus() {
            return $this->status;
        }

        function setOrder_lab_extra_charge_id($order_lab_extra_charge_id) {
            $this->order_lab_extra_charge_id = $order_lab_extra_charge_id;
        }

        function setCharge_reason($charge_reason) {
            $this->charge_reason = $charge_reason;
        }

        function setAmount($amount) {
            $this->amount = $amount;
        }

        function setLab_date($lab_date) {
            $this->lab_date = $lab_date;
        }

        function setOrder_product_id($order_product_id) {
            $this->order_product_id = $order_product_id;
        }

        function setLab_management_id($lab_management_id) {
            $this->lab_management_id = $lab_management_id;
        }

        function setStatus($status) {
            $this->status = $status;
        }


        }
