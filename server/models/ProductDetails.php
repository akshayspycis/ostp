<?php
    class ProductDetails{
        private $product_details_id;
        private $product_name;
        private $icon;
        private $status;
        function getProduct_details_id() {
            return $this->product_details_id;
        }

        function getProduct_name() {
            return $this->product_name;
        }

        function getIcon() {
            return $this->icon;
        }

        function getStatus() {
            return $this->status;
        }

        function setProduct_details_id($product_details_id) {
            $this->product_details_id = $product_details_id;
        }

        function setProduct_name($product_name) {
            $this->product_name = $product_name;
        }

        function setIcon($icon) {
            $this->icon = $icon;
        }

        function setStatus($status) {
            $this->status = $status;
        }


    }
