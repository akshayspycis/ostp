<?php
    class OffilineTransactionDetails{
        private $offline_transaction_details_id;
        private $date;
        private $wallet_id;
        private $type;
        private $transation_type;
        
        function getOffline_transaction_details_id() {
            return $this->offline_transaction_details_id;
        }

        function getDate() {
            return $this->date;
        }

        function getWallet_id() {
            return $this->wallet_id;
        }

        function getType() {
            return $this->type;
        }

        function getTransation_type() {
            return $this->transation_type;
        }

        function setOffline_transaction_details_id($offline_transaction_details_id) {
            $this->offline_transaction_details_id = $offline_transaction_details_id;
        }

        function setDate($date) {
            $this->date = $date;
        }

        function setWallet_id($wallet_id) {
            $this->wallet_id = $wallet_id;
        }

        function setType($type) {
            $this->type = $type;
        }

        function setTransation_type($transation_type) {
            $this->transation_type = $transation_type;
        }
    }
