<?php
    class OrderProductStatus{
        private $order_product_status_id;
        private $order_product_id;
        private $product_status;
        private $product_reason;
        private $product_repair_hrs;
        private $engg_details_id;
        private $status_date;
        function getOrder_product_status_id() {
            return $this->order_product_status_id;
        }

        function getOrder_product_id() {
            return $this->order_product_id;
        }

        function getProduct_status() {
            return $this->product_status;
        }

        function getProduct_reason() {
            return $this->product_reason;
        }

        function getProduct_repair_hrs() {
            return $this->product_repair_hrs;
        }

        function getEngg_details_id() {
            return $this->engg_details_id;
        }

        function getStatus_date() {
            return $this->status_date;
        }

        function setOrder_product_status_id($order_product_status_id) {
            $this->order_product_status_id = $order_product_status_id;
        }

        function setOrder_product_id($order_product_id) {
            $this->order_product_id = $order_product_id;
        }

        function setProduct_status($product_status) {
            $this->product_status = $product_status;
        }

        function setProduct_reason($product_reason) {
            $this->product_reason = $product_reason;
        }

        function setProduct_repair_hrs($product_repair_hrs) {
            $this->product_repair_hrs = $product_repair_hrs;
        }

        function setEngg_details_id($engg_details_id) {
            $this->engg_details_id = $engg_details_id;
        }

        function setStatus_date($status_date) {
            $this->status_date = $status_date;
        }


    }


