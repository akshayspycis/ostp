<?php
    class ChargeDetails{
        private $charge_details_id;
        private $amount;
        private $type;
        private $date;
        function getCharge_details_id() {
            return $this->charge_details_id;
        }

        function getAmount() {
            return $this->amount;
        }

        function getType() {
            return $this->type;
        }

        function getDate() {
            return $this->date;
        }

        function setCharge_details_id($charge_details_id) {
            $this->charge_details_id = $charge_details_id;
        }

        function setAmount($amount) {
            $this->amount = $amount;
        }

        function setType($type) {
            $this->type = $type;
        }

        function setDate($date) {
            $this->date = $date;
        }


    }


