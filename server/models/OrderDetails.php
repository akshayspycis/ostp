<?php
    class OrderDetails{
        private $order_id;
        private $user_id;
        private $order_date;
        private $order_no;
        private $status;
        private $alt_contact_no;
        private $address_details_id;
        function getOrder_id() {
            return $this->order_id;
        }

        function getUser_id() {
            return $this->user_id;
        }

        function getOrder_date() {
            return $this->order_date;
        }

        function getOrder_no() {
            return $this->order_no;
        }

        function getStatus() {
            return $this->status;
        }

        function getAlt_contact_no() {
            return $this->alt_contact_no;
        }

        function getAddress_details_id() {
            return $this->address_details_id;
        }

        function setOrder_id($order_id) {
            $this->order_id = $order_id;
        }

        function setUser_id($user_id) {
            $this->user_id = $user_id;
        }

        function setOrder_date($order_date) {
            $this->order_date = $order_date;
        }

        function setOrder_no($order_no) {
            $this->order_no = $order_no;
        }

        function setStatus($status) {
            $this->status = $status;
        }

        function setAlt_contact_no($alt_contact_no) {
            $this->alt_contact_no = $alt_contact_no;
        }

        function setAddress_details_id($address_details_id) {
            $this->address_details_id = $address_details_id;
        }


    }


