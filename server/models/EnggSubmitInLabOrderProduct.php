<?php
    class EnggSubmitInLabOrderProduct{
        private $engg_submit_in_lab_order_product_id;
        private $engg_details_id;
        private $employee_id;
        private $date_of_submit;
        private $order_product_id;
        private $lab_management_id;
        function getEngg_submit_in_lab_order_product_id() {
            return $this->engg_submit_in_lab_order_product_id;
        }

        function getEngg_details_id() {
            return $this->engg_details_id;
        }

        function getEmployee_id() {
            return $this->employee_id;
        }

        function getDate_of_submit() {
            return $this->date_of_submit;
        }

        function getOrder_product_id() {
            return $this->order_product_id;
        }

        function getLab_management_id() {
            return $this->lab_management_id;
        }

        function setEngg_submit_in_lab_order_product_id($engg_submit_in_lab_order_product_id) {
            $this->engg_submit_in_lab_order_product_id = $engg_submit_in_lab_order_product_id;
        }

        function setEngg_details_id($engg_details_id) {
            $this->engg_details_id = $engg_details_id;
        }

        function setEmployee_id($employee_id) {
            $this->employee_id = $employee_id;
        }

        function setDate_of_submit($date_of_submit) {
            $this->date_of_submit = $date_of_submit;
        }

        function setOrder_product_id($order_product_id) {
            $this->order_product_id = $order_product_id;
        }

        function setLab_management_id($lab_management_id) {
            $this->lab_management_id = $lab_management_id;
        }


        }
