<?php
    class EmployeeDetails{
        private $employee_id;
        private $user_id;
        private $date;
        function getEmployee_id() {
            return $this->employee_id;
        }

        function getUser_id() {
            return $this->user_id;
        }

        function getDate() {
            return $this->date;
        }

        function setEmployee_id($employee_id) {
            $this->employee_id = $employee_id;
        }

        function setUser_id($user_id) {
            $this->user_id = $user_id;
        }

        function setDate($date) {
            $this->date = $date;
        }


        }
