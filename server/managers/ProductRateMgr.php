<?php
    include_once '../../dbhelper/DatabaseHelper.php';
    class ProductRateMgr{    
        //method to insert product_rate in database
        public function insProductRate(ProductRate $product_rate) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO product_rate( "
                    . "product_details_id,"
                    . "service_charge,"
                    . "discount,"
                    . "date) "
                    . "VALUES ('".$product_rate->getProduct_details_id()."',"
                    . "'".$product_rate->getService_charge()."',"
                    . "'".$product_rate->getDiscount()."',"
                    . "CONVERT_TZ( NOW( ) ,  '+00:00',  '+05:29' ))";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }

        //method to delete news in database
        public function delProductRate($product_rate_id) {
            $dbh = new DatabaseHelper();
             $sql = "delete from product_rate where product_rate_id = '".$product_rate_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        
        //method to select ProductRate from database
        public function selProductRate($product_details_id) {
            $dbh = new DatabaseHelper();
            $sql= "select * from product_rate where product_details_id='".$product_details_id."' order by product_rate_id desc";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
        
        public function updateProductRate(ProductRate $product_rate) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE product_rate SET " 
                    ."service_charge='".$product_rate->getService_charge()."',"
                    ."discount='".$product_rate->getDiscount()."'"
                    ." WHERE product_rate_id=".$product_rate->getProduct_rate_id()."";
             $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            
            $dbh->closeConnection();
             
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        }
          
    }
?>
