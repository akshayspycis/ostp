<?php
    require_once '../../dbhelper/DatabaseHelper.php';
    class PrivilegesDetailsMgr{    
        //method to insert privileges_details in database
        public function insPrivilegesDetails(PrivilegesDetails $privileges_details) {
            $dbh = new DatabaseHelper();
            $this->delPrivilegesDetails($privileges_details->getEmployee_id(),$privileges_details->getModule_id());
            $sql = "INSERT INTO privileges_details( "
                    . "employee_id, "
                    . "module_id, "
                    . "insertm, "
                    . "updatem, "
                    . "deletem, "
                    . "selectm) "
                    . "VALUES ('".$privileges_details->getEmployee_id()."',"
                    . "'".$privileges_details->getModule_id()."',"
                    . "'".$privileges_details->getInsertm()."',"
                    . "'".$privileges_details->getUpdatem()."',"
                    . "'".$privileges_details->getDeletem()."',"
                    . "'".$privileges_details->getSelectm()."')";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }

        //method to delete news in database
        public function delPrivilegesDetails($employee_id,$module_id) {
            $dbh = new DatabaseHelper();
             $sql = "delete from privileges_details where employee_id = '".$employee_id."' and module_id='".$module_id."'";
             
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        //method to select PrivilegesDetails from database
            public function selPrivilegesDetails($employee_id) {
                $dbh = new DatabaseHelper();
                $sql="";
                if($employee_id!=""){
                    $sql = "select * from privileges_details where employee_id=".$employee_id;
                }
                $stmt = $dbh->createConnection()->prepare($sql);            
                $stmt->execute();

                $dbh->closeConnection();
                return $stmt;
            }
        }
?>
