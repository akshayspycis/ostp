<?php
    include_once '../../dbhelper/DatabaseHelper.php';
    class TempWalletMgr{    
        //method to insert temp_wallet in database
        public function insTempWallet(TempWallet $temp_wallet) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO temp_wallet( "
                    . "user_data_id,"
                    . "transaction_code,"
                    . "amount,"
                    . "from_date,"
                    . "to_date) "
                    . "VALUES ('".$temp_wallet->getUser_data_id()."',"
                    . "'".$temp_wallet->getTransaction_code()."',"
                    . "'".$temp_wallet->getAmount()."',"
                    . "'".$temp_wallet->getFrom_date()."',"
                    . "'".$temp_wallet->getTo_date()."')";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }

        //method to delete news in database
        public function delTempWallet($temp_wallet_id) {
            $dbh = new DatabaseHelper();
             $sql = "delete from temp_wallet where temp_wallet_id = '".$temp_wallet_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        
        //method to select TempWallet from database
        public function selTempWallet($brand_details_id,$product_details_id) {
            $dbh = new DatabaseHelper();
            $sql= "select * from temp_wallet order by temp_wallet_id desc";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
        
        public function updateTempWallet(TempWallet $temp_wallet) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE temp_wallet SET " 
                    ."amount='".$temp_wallet->getAmount()."',"
                    ."from_date='".$temp_wallet->getFrom_date()."',"
                    ."to_date='".$temp_wallet->getTo_date()."'"
                    ." WHERE temp_wallet_id=".$temp_wallet->getModel_details_id()."";
             $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            
            $dbh->closeConnection();
             
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        }
          
    }
?>
