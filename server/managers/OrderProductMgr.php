<?php
    include_once '../../dbhelper/DatabaseHelper.php';
    class OrderProductMgr{    
        //method to insert order_product in database
        public function insOrderProduct($order_product_list) {
            $dbh = new DatabaseHelper();
            $sql='';
            foreach ($order_product_list as $order_product) {
                $sql.= "INSERT INTO order_product( "
                    . "product_details_id,"
                    . "problem_details_id,"
                    . "model_details_id,"
                    . "problem_discription,"
                    . "order_id,"
                    . "brand_details_id) "
                    . " VALUES ('".$order_product->getProduct_details_id()."',"
                    . "'".$order_product->getProblem_details_id()."',"
                    . "'".$order_product->getModel_details_id()."',"
                    . "'".$order_product->getProblem_discription()."',"
                    . "(select order_id from order_details order by order_id desc limit 1),"
                    . "'".$order_product->getBrand_details_id()."');";
            }
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }

        //method to delete news in database
        public function delOrderProduct($order_product_id) {
            $dbh = new DatabaseHelper();
             $sql = "delete from order_product where order_product_id = '".$order_product_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        
        //method to select Order from database
        public function selOrderProduct($order_id) {
            $dbh = new DatabaseHelper();
            $sql= "select *,(select product_name from product_details where product_details_id=ord.product_details_id) as product_name,"
                    . "(select icon from product_details where product_details_id=ord.product_details_id) as icon,"
                    . "(SELECT order_allot_detailsId FROM order_allot_details where order_product_id=ord.order_product_id) as order_allot_detailsId,"
                    . "(SELECT status FROM order_allot_details where order_product_id=ord.order_product_id) as status,"
                    . "(SELECT engg_details_id FROM engg_details where order_product_id=ord.order_product_id order by engg_details_id desc limit 1) as engg_details_id,"
                    . "(SELECT order_product_status_id FROM order_product_status where order_product_id=ord.order_product_id order by order_product_status_id desc limit 1) as order_product_status_id,"
                    . "(SELECT order_product_status_id FROM order_product_status where order_product_id=ord.order_product_id and product_status='3' order by order_product_status_id desc limit 1) as repaire,"
                    . "(select brand_name from brand_details where brand_details_id=ord.brand_details_id) as brand_name,"
                    . "(select model_name from model_details where model_details_id=ord.model_details_id) as model_name,"
                    . "(select problem from problem_details where problem_details_id=ord.problem_details_id) as problem, "
                    . "(select discription from problem_details where problem_details_id=ord.problem_details_id) as discription "
                    . "from order_product ord where ord.order_id='".$order_id."' order by ord.order_id desc";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
        
        public function updateOrderProduct(OrderProduct $order_product) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE order_product SET " 
                    ."problem_discription='".$order_product->getProblem_discription()."'"
                    ." WHERE order_product_id=".$order_product->getOrder_product_id().""; 
             $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            
            $dbh->closeConnection();
             
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        }
          
    }
?>
