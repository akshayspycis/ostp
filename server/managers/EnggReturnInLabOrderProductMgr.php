<?php
    include_once '../../dbhelper/DatabaseHelper.php';
    class EnggReturnInLabOrderProductMgr{    
        //method to insert engg_return_in_lab_order_product in database
        public function insEnggReturnInLabOrderProduct(EnggReturnInLabOrderProduct $engg_return_in_lab_order_product) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO engg_return_in_lab_order_product( "
                    . "engg_details_id,"
                    . "employee_id,"
                    . "data_of_return,"
                    . "order_product_id,"
                    . "lab_management_id) "
                    . " VALUES ('".$engg_return_in_lab_order_product->getEngg_details_id()."',"
                    . "'".$engg_return_in_lab_order_product->getEmployee_id()."',"
                    . "'".$engg_return_in_lab_order_product->getData_of_return()."',"
                    . "'".$engg_return_in_lab_order_product->getOrder_product_id()."',"
                    . "'".$engg_return_in_lab_order_product->getLab_management_id()."')";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }
        //method to delete news in database
        public function delEnggReturnInLabOrderProduct($engg_return_in_lab_order_product_id) {
            $dbh = new DatabaseHelper();
             $sql = "delete from engg_return_in_lab_order_product where engg_return_in_lab_order_product_id = '".$engg_return_in_lab_order_product_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        
        //method to select Order from database
        public function selEnggReturnInLabOrderProduct($engg_return_in_lab_order_product_id) {
            $dbh = new DatabaseHelper();
            if($engg_return_in_lab_order_product_id!="")
                   $sql= "select *,(select product_name from product_details where product_details_id=ord.product_details_id) as product_name,"
                    . "(select order_no from order_details where order_id=ord.order_id) as order_no,"
                    . "(select icon from product_details where product_details_id=ord.product_details_id) as icon,"
                    . "(SELECT order_allot_detailsId FROM order_allot_details where order_product_id=ord.order_product_id) as order_allot_detailsId,"
                    . "(SELECT status FROM order_allot_details where order_product_id=ord.order_product_id) as status,"
                    . "(select brand_name from brand_details where brand_details_id=ord.brand_details_id) as brand_name,"
                    . "(select model_name from model_details where model_details_id=ord.model_details_id) as model_name,"
                    . "(select problem from problem_details where problem_details_id=ord.problem_details_id) as problem, "
                    . "(select CONCAT(address_1,' ',address_2,' ',city,'  ',state,'  ',pincode) from address_details where address_details_id=(select address_details_id from order_details where order_id=ord.order_id)) as address_details, "
                    . "(select discription from problem_details where problem_details_id=ord.problem_details_id) as discription "
                    . "from order_product ord inner join order_allot_details oad on oad.order_product_id=ord.order_product_id where oad.employee_id='".$employee_id."' order by ord.order_id desc";
            else
                  $sql= "select *,(select product_name from product_details where product_details_id=ord.product_details_id) as product_name,"
                    . "(select order_no from order_details where order_id=ord.order_id) as order_no,"
                    . "(select icon from product_details where product_details_id=ord.product_details_id) as icon,"
                    . "(SELECT order_allot_detailsId FROM order_allot_details where order_product_id=ord.order_product_id) as order_allot_detailsId,"
                    . "(SELECT status FROM order_allot_details where order_product_id=ord.order_product_id) as status,"
                    . "(select brand_name from brand_details where brand_details_id=ord.brand_details_id) as brand_name,"
                    . "(select model_name from model_details where model_details_id=ord.model_details_id) as model_name,"
                    . "(select problem from problem_details where problem_details_id=ord.problem_details_id) as problem, "
                    . "(select CONCAT(address_1,' ',address_2,' ',city,'  ',state,'  ',pincode) from address_details where address_details_id=(select address_details_id from order_details where order_id=ord.order_id)) as address_details, "
                    . "(select discription from problem_details where problem_details_id=ord.problem_details_id) as discription "
                    . "from order_product ord inner join order_allot_details oad on oad.order_product_id=ord.order_product_id order by ord.order_id desc";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
        
        public function checkEnggReturnInLabOrderProduct($order_allot_detailsId,$date) {
            $dbh = new DatabaseHelper();
               $sql= "select * from engg_return_in_lab_order_product ed where ed.order_allot_detailsId='".$order_allot_detailsId."' and STR_TO_DATE(meeting_date,'%d-%m-%Y')=STR_TO_DATE('"+$date+"','%d-%m-%Y') order by ord.order_allot_detailsId desc limit 1";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
        
        public function updateEnggReturnInLabOrderProduct(EnggReturnInLabOrderProduct $engg_return_in_lab_order_product) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE engg_return_in_lab_order_product SET " 
                    ."status='".$engg_return_in_lab_order_product->getStatus()."'"
                    ." WHERE order_product_id=".$engg_return_in_lab_order_product->getOrder_allot_detailsId().""; 
             $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        }
          
    }
?>
