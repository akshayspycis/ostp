<?php
    include_once '../../dbhelper/DatabaseHelper.php';
    class OrderProductStatusMgr{    
        //method to insert order_product_status in database
        public function insOrderProductStatus(OrderProductStatus $order_product_status) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO order_product_status( "
                    . "order_product_id,"
                    . "product_status,"
                    . "product_reason,"
                    . "product_repair_hrs,"
                    . "engg_details_id,"
                    . "status_date) "
                    . " VALUES ('".$order_product_status->getOrder_product_id()."',"
                    . "'".$order_product_status->getProduct_status()."',"
                    . "'".$order_product_status->getProduct_reason()."',"
                    . "'".$order_product_status->getProduct_repair_hrs()."',"
                    . "'".$order_product_status->getEngg_details_id()."',"
                    . "'".$order_product_status->getStatus_date()."')";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }

        //method to delete news in database
        public function delOrderProductStatus($order_product_status_id) {
            $dbh = new DatabaseHelper();
             $sql = "delete from order_product_status where order_product_status_id = '".$order_product_status_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        
        //method to select Order from database
        public function selOrderProductStatus($order_product_id) {
            $dbh = new DatabaseHelper();
            $sql='';
            if($order_product_id!="")
                   $sql= "select * ,(select lm.lab_management_id from lab_management lm Left join engg_return_in_lab_order_product erlop on erlop.order_product_id=lm.order_product_id where lm.order_product_id=ops.order_product_id "
                       . "and return_date_time='' or erlop.data_of_return='' limit 1) as in_lab,"
                    . "(select engg_return_in_lab_order_product_id from engg_return_in_lab_order_product erlop where erlop.order_product_id=ops.order_product_id "
                       . "and erlop.data_of_return!='' limit 1) as return_item "
                    . "from order_product_status ops  where ops.order_product_id='".$order_product_id."' order by ops.order_product_status_id desc";
            else
                  $sql= "select * ,(select lm.lab_management_id from lab_management lm Left join engg_return_in_lab_order_product erlop on erlop.order_product_id=lm.order_product_id where lm.order_product_id=ops.order_product_id "
                       . "and return_date_time='' or erlop.data_of_return='' limit 1) as in_lab,"
                    . "(select engg_return_in_lab_order_product_id from engg_return_in_lab_order_product erlop where erlop.order_product_id=ops.order_product_id "
                       . "and erlop.data_of_return!='' limit 1) as return_item "
                    . "from order_product_status ops order by ops.order_product_status_id desc";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
        
        public function checkOrderProductStatus($order_allot_detailsId,$date) {
            $dbh = new DatabaseHelper();
               $sql= "select * from order_product_status ed where ed.order_allot_detailsId='".$order_allot_detailsId."' and STR_TO_DATE(meeting_date,'%d-%m-%Y')=STR_TO_DATE('"+$date+"','%d-%m-%Y') order by ord.order_allot_detailsId desc limit 1";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
        
        public function updateOrderProductStatus(OrderProductStatus $order_product_status) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE order_product_status SET " 
                    ."status='".$order_product_status->getStatus()."'"
                    ." WHERE order_product_id=".$order_product_status->getOrder_allot_detailsId().""; 
            $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }
?>
