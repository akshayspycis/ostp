<?php
    include_once '../../dbhelper/DatabaseHelper.php';
    class SpareNotFoundMgr{    
        //method to insert spare_not_found in database
        public function insSpareNotFound(SpareNotFound $spare_not_found) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO spare_not_found( "
                    . "order_product_id,"
                    . "message,"
                    . "snf_date,"
                    . "status,"
                    . "lab_management_id) "
                    . " VALUES ('".$spare_not_found->getOrder_product_id()."',"
                    . "'".$spare_not_found->getMessage()."',"
                    . "'".$spare_not_found->getSnf_date()."',"
                    . "'".$spare_not_found->getStatus()."',"
                    . "'".$spare_not_found->getLab_management_id()."')";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }
        //method to delete news in database
        public function delSpareNotFound($spare_not_found_id) {
            $dbh = new DatabaseHelper();
             $sql = "delete from spare_not_found where spare_not_found_id = '".$spare_not_found_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        
        //method to select Order from database
        public function selSpareNotFound($spare_not_found_id) {
            $dbh = new DatabaseHelper();
            if($spare_not_found_id!="")
                   $sql= "select *,(select product_name from product_details where product_details_id=ord.product_details_id) as product_name,"
                    . "(select order_no from order_details where order_id=ord.order_id) as order_no,"
                    . "(select icon from product_details where product_details_id=ord.product_details_id) as icon,"
                    . "(SELECT order_allot_detailsId FROM order_allot_details where order_product_id=ord.order_product_id) as order_allot_detailsId,"
                    . "(SELECT status FROM order_allot_details where order_product_id=ord.order_product_id) as status,"
                    . "(select brand_name from brand_details where brand_details_id=ord.brand_details_id) as brand_name,"
                    . "(select model_name from model_details where model_details_id=ord.model_details_id) as model_name,"
                    . "(select problem from problem_details where problem_details_id=ord.problem_details_id) as problem, "
                    . "(select CONCAT(address_1,' ',address_2,' ',city,'  ',state,'  ',pincode) from address_details where address_details_id=(select address_details_id from order_details where order_id=ord.order_id)) as address_details, "
                    . "(select discription from problem_details where problem_details_id=ord.problem_details_id) as discription "
                    . "from order_product ord inner join order_allot_details oad on oad.order_product_id=ord.order_product_id where oad.employee_id='".$employee_id."' order by ord.order_id desc";
            else
                  $sql= "select *,(select product_name from product_details where product_details_id=ord.product_details_id) as product_name,"
                    . "(select order_no from order_details where order_id=ord.order_id) as order_no,"
                    . "(select icon from product_details where product_details_id=ord.product_details_id) as icon,"
                    . "(SELECT order_allot_detailsId FROM order_allot_details where order_product_id=ord.order_product_id) as order_allot_detailsId,"
                    . "(SELECT status FROM order_allot_details where order_product_id=ord.order_product_id) as status,"
                    . "(select brand_name from brand_details where brand_details_id=ord.brand_details_id) as brand_name,"
                    . "(select model_name from model_details where model_details_id=ord.model_details_id) as model_name,"
                    . "(select problem from problem_details where problem_details_id=ord.problem_details_id) as problem, "
                    . "(select CONCAT(address_1,' ',address_2,' ',city,'  ',state,'  ',pincode) from address_details where address_details_id=(select address_details_id from order_details where order_id=ord.order_id)) as address_details, "
                    . "(select discription from problem_details where problem_details_id=ord.problem_details_id) as discription "
                    . "from order_product ord inner join order_allot_details oad on oad.order_product_id=ord.order_product_id order by ord.order_id desc";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
        
        public function checkSpareNotFound($order_allot_detailsId,$date) {
            $dbh = new DatabaseHelper();
               $sql= "select * from spare_not_found ed where ed.order_allot_detailsId='".$order_allot_detailsId."' and STR_TO_DATE(meeting_date,'%d-%m-%Y')=STR_TO_DATE('"+$date+"','%d-%m-%Y') order by ord.order_allot_detailsId desc limit 1";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
        
        public function updateSpareNotFound(SpareNotFound $spare_not_found) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE spare_not_found SET " 
                    ."status='".$spare_not_found->getStatus()."'"
                    ." WHERE spare_not_found_id=".$spare_not_found->getSpare_not_found_id().""; 
             $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        }
          
    }
?>
