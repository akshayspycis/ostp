<?php
    include_once '../../dbhelper/DatabaseHelper.php';
    class EnggDetailsMgr{    
        //method to insert engg_details in database
        public function insEnggDetails(EnggDetails $engg_details) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO engg_details( "
                    . "order_id,"
                    . "order_allot_detailsId,"
                    . "meeting_time,"
                    . "meeting_date,"
                    . "meeting_person,"
                    . "order_product_id,"
                    . "employee_id) "
                    . " VALUES ('".$engg_details->getOrder_id()."',"
                    . "'".$engg_details->getOrder_allot_details_id()."',"
                    . "'".$engg_details->getMeeting_time()."',"
                    . "'".$engg_details->getMeeting_date()."',"
                    . "'".$engg_details->getMeeting_person()."',"
                    . "'".$engg_details->getOrder_product_id()."',"
                    . "'".$engg_details->getEmployee_id()."')";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }

        //method to delete news in database
        public function delEnggDetails($engg_details) {
            $dbh = new DatabaseHelper();
             $sql = "delete from engg_details where engg_details = '".$engg_details."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        
        //method to select Order from database
        public function selEnggDetails($employee_id) {
            $dbh = new DatabaseHelper();
            if($employee_id!="")
                   $sql= "select *,(select product_name from product_details where product_details_id=ord.product_details_id) as product_name,"
                    . "(select order_product_status_id from order_product_status where order_product_id=ord.order_product_id and product_status='3') as order_product_status_id,"
                    . "(select order_no from order_details where order_id=ord.order_id) as order_no,"
                    . "(select payment_details_id from payment_details where order_product_id=ord.order_product_id ) as payment_details_id,"
                    . "(select user_id from order_details where order_id=ord.order_id) as user_id,"
                    . "(select icon from product_details where product_details_id=ord.product_details_id) as icon,"
                    . "(SELECT order_allot_detailsId FROM order_allot_details where order_product_id=ord.order_product_id) as order_allot_detailsId,"
                    . "(SELECT status FROM order_allot_details where order_product_id=ord.order_product_id) as status,"
                    . "(select brand_name from brand_details where brand_details_id=ord.brand_details_id) as brand_name,"
                    . "(select model_name from model_details where model_details_id=ord.model_details_id) as model_name,"
                    . "(select problem from problem_details where problem_details_id=ord.problem_details_id) as problem, "
                    . "(select CONCAT(address_1,' ',address_2,' ',city,'  ',state,'  ',pincode) from address_details where address_details_id=(select address_details_id from order_details where order_id=ord.order_id)) as address_details, "
                    . "(select discription from problem_details where problem_details_id=ord.problem_details_id) as discription "
                    . "from order_product ord inner join order_allot_details oad on oad.order_product_id=ord.order_product_id where oad.employee_id='".$employee_id."' order by ord.order_id desc";
            else
                  $sql= "select *,(select product_name from product_details where product_details_id=ord.product_details_id) as product_name,"
                    . "(select order_product_status_id from order_product_status where order_product_id=ord.order_product_id and product_status='3') as order_product_status_id,"
                    . "(select order_no from order_details where order_id=ord.order_id) as order_no,"
                    . "(select payment_details_id from payment_details where order_product_id=ord.order_product_id ) as payment_details_id,"
                    . "(select user_id from order_details where order_id=ord.order_id) as user_id,"
                    . "(select icon from product_details where product_details_id=ord.product_details_id) as icon,"
                    . "(SELECT order_allot_detailsId FROM order_allot_details where order_product_id=ord.order_product_id) as order_allot_detailsId,"
                    . "(SELECT status FROM order_allot_details where order_product_id=ord.order_product_id) as status,"
                    . "(select brand_name from brand_details where brand_details_id=ord.brand_details_id) as brand_name,"
                    . "(select model_name from model_details where model_details_id=ord.model_details_id) as model_name,"
                    . "(select problem from problem_details where problem_details_id=ord.problem_details_id) as problem, "
                    . "(select CONCAT(address_1,' ',address_2,' ',city,'  ',state,'  ',pincode) from address_details where address_details_id=(select address_details_id from order_details where order_id=ord.order_id)) as address_details, "
                    . "(select discription from problem_details where problem_details_id=ord.problem_details_id) as discription "
                    . "from order_product ord inner join order_allot_details oad on oad.order_product_id=ord.order_product_id order by ord.order_id desc";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
        
        public function checkEnggDetails($order_allot_detailsId,$date) {
            $dbh = new DatabaseHelper();
            $sql= "select * from engg_details ed where order_allot_detailsId='".$order_allot_detailsId."' and STR_TO_DATE(meeting_date,'%d-%m-%Y')=STR_TO_DATE('".$date."','%d-%m-%Y') order by order_allot_detailsId desc limit 1";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
        
        public function updateEnggDetails(EnggDetails $engg_details) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE engg_details SET " 
                    ."status='".$engg_details->getStatus()."'"
                    ." WHERE order_product_id=".$engg_details->getOrder_allot_detailsId().""; 
             $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        }
          
    }
?>
