

<?php
    include_once '../../dbhelper/DatabaseHelper.php';
    
        class UserProfileDetailsMgr{    

        //method to insert user_profile_details in database
        public function insUserProfileDetails(UserProfileDetails $user_profile_details) {
            $dbh = new DatabaseHelper();
            $sql="";
            if($user_profile_details->getUser_id()!=NULL){
                $sql = "INSERT INTO user_profile_details( "
                    . "user_id, "
                    . "pic, "
                    . "date) "
                    . "VALUES ('".$user_profile_details->getUser_id()."',"
                    . "'".$user_profile_details->getPic()."',"
                    . "'".$user_profile_details->getDate()."')";
            }else{
                $sql = "INSERT INTO user_profile_details( "
                    . "user_id, "
                    . "pic, "
                    . "date) "
                    . "VALUES ((select user_id from user_details order by user_id desc limit 1),"
                    . "'".$user_profile_details->getPic()."',"
                    . "'".$user_profile_details->getDate()."')";
            }
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }

        //method to delete news in database
        public function delUserProfileDetails($user_profile_details_id) {
            $dbh = new DatabaseHelper();
             $sql = "delete from user_profile_details where user_profile_details_id = '".$user_profile_details_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        
        //method to select UserProfileDetails from database
        public function selUserProfileDetails($user_profile_details_id) {
            $dbh = new DatabaseHelper();
            $sql="";
            if($course_id!=""){
                $sql = "select * from user_profile_details where $user_profile_details_id=".$user_profile_details_id;
            }else{
                $sql = "select * from user_profile_details ";
            }
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            
            $dbh->closeConnection();
            return $stmt;
        }
        //        method to update enquiry in database
  public function updateUserProfileDetails(UserProfileDetails $user_profile_details) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE user_profile_details SET " 
                    ."user_profile_details_id='".$user_profile_details->getUser_profile_details_id()."',"
                    ."user_id='".$user_profile_details->getUser_id()."',"
                    ."pic='".$user_profile_details->getPic()."',"
                    ."date='".$user_profile_details->getDate()."'"
                    ."WHERE user_profile_details_id=".$user_profile_details->getCourse_batch_details_id()."";
             $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            
            $dbh->closeConnection();
             
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        }
          public function updUserProfileDetailstatus(UserProfileDetails $user_profile_details) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE user_profile_details SET " 
                    ."status='".$user_profile_details->getStatus()."'"
                   ."WHERE user_profile_details_id=".$user_profile_details->getCourse_batch_details_id()."";
                    
          
            
            $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            
            $dbh->closeConnection();
             
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        } 
          
    }
?>
