<?php
    require_once '../../dbhelper/DatabaseHelper.php';
    class ModuleTypeMgr{    
        public function selModuleType() {
            $dbh = new DatabaseHelper();
            $sql = "select * from module_type ";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
    }
?>
