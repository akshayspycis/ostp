<?php
    include_once '../../dbhelper/DatabaseHelper.php';
    class BrandDetailsMgr{    
        //method to insert brand_details in database
        public function insBrandDetails(BrandDetails $brand_details) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO brand_details( "
                    . "brand_name) "
                    . "VALUES ('".$brand_details->getBrand_name()."')";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }

        //method to delete news in database
        public function delBrandDetails($brand_details_id) {
            $dbh = new DatabaseHelper();
             $sql = "delete from brand_details where brand_details_id = '".$brand_details_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        
        //method to select BrandDetails from database
        public function selBrandDetails() {
            $dbh = new DatabaseHelper();
            $sql= "select * from brand_details";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
        
        public function updateBrandDetails(BrandDetails $brand_details) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE brand_details SET " 
                    ."brand_name='".$brand_details->getBrand_name()."'"
                    ." WHERE brand_details_id=".$brand_details->getBrand_details_id()."";
             $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            $dbh->closeConnection();
             
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        }
          
    }
?>
