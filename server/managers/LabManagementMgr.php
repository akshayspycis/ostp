<?php
    include_once '../../dbhelper/DatabaseHelper.php';
    class LabManagementMgr{    
        //method to insert lab_management in database
        public function insLabManagement(LabManagement $lab_management) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO lab_management( "
                    . "order_product_id,"
                    . "order_id,"
                    . "receive_date_time,"
                    . "return_date_time,"
                    . "status) "
                    . " VALUES ('".$lab_management->getOrder_product_id()."',"
                    . "'".$lab_management->getOrder_id()."',"
                    . "'".$lab_management->getReceive_date_time()."',"
                    . "'".$lab_management->getReturn_date_time()."',"
                    . "'".$lab_management->getStatus()."')";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }
        //method to delete news in database
        public function delLabManagement($lab_management_id) {
            $dbh = new DatabaseHelper();
             $sql = "delete from lab_management where lab_management_id = '".$lab_management_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        
        //method to select Order from database
        public function selLabManagement() {
            $dbh = new DatabaseHelper();
                   $sql= "select *,(select product_name from product_details where product_details_id=ord.product_details_id) as product_name,"
                    . "(select product_reason from order_product_status where order_product_id=ord.order_product_id and product_status='2' order by order_product_status_id desc limit 1) as product_reason,"
                    . "(select order_no from order_details where order_id=ord.order_id) as order_no,"
                    . "(select icon from product_details where product_details_id=ord.product_details_id) as icon,"
                    . "(SELECT order_allot_detailsId FROM order_allot_details where order_product_id=ord.order_product_id) as order_allot_detailsId,"
                    . "(SELECT status FROM order_allot_details where order_product_id=ord.order_product_id) as status,"
                    . "(select brand_name from brand_details where brand_details_id=ord.brand_details_id) as brand_name,"
                    . "(select model_name from model_details where model_details_id=ord.model_details_id) as model_name,"
                    . "(select problem from problem_details where problem_details_id=ord.problem_details_id) as problem, "
                    . "(select CONCAT(address_1,' ',address_2,' ',city,'  ',state,'  ',pincode) from address_details where address_details_id=(select address_details_id from order_details where order_id=ord.order_id)) as address_details, "
                    . "(select discription from problem_details where problem_details_id=ord.problem_details_id) as discription "
                    . "from lab_management lm inner join order_product ord on lm.order_product_id=ord.order_product_id inner join order_allot_details oad on oad.order_product_id=ord.order_product_id  order by ord.order_id desc";
            
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
        
        public function checkLabManagement($order_allot_detailsId,$date) {
            $dbh = new DatabaseHelper();
               $sql= "select * from lab_management ed where ed.order_allot_detailsId='".$order_allot_detailsId."' and STR_TO_DATE(meeting_date,'%d-%m-%Y')=STR_TO_DATE('"+$date+"','%d-%m-%Y') order by ord.order_allot_detailsId desc limit 1";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
        
        public function updateLabManagement(LabManagement $lab_management) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE lab_management SET " 
                    ."return_date_time='".$lab_management->getReturn_date_time()."'"
                    ." WHERE lab_management_id=".$lab_management->getLab_management_id().""; 
             $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        }
          
    }
?>
