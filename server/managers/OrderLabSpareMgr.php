<?php
    include_once '../../dbhelper/DatabaseHelper.php';
    class OrderLabSpareMgr{    
        //method to insert order_lab_spare in database
        public function insOrderLabSpare(OrderLabSpare $order_lab_spare) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO order_lab_spare( "
                    . "spare_details_id,"
                    . "user_status,"
                    . "call_user,"
                    . "sms,"
                    . "lab_spare_date,"
                    . "lab_management_id,"
                    . "order_product_id,"
                    . "spare_reason) "
                    . " VALUES ('".$order_lab_spare->getSpare_details_id()."',"
                    . "'".$order_lab_spare->getUser_status()."',"
                    . "'".$order_lab_spare->getCall_user()."',"
                    . "'".$order_lab_spare->getSms()."',"
                    . "'".$order_lab_spare->getLab_spare_date()."',"
                    . "'".$order_lab_spare->getLab_management_id()."',"
                    . "'".$order_lab_spare->getOrder_product_id()."',"
                    . "'".$order_lab_spare->getSpare_reason()."')";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }
        //method to delete news in database
        public function delOrderLabSpare($order_lab_spare_id) {
            $dbh = new DatabaseHelper();
             $sql = "delete from order_lab_spare where order_lab_spare_id = '".$order_lab_spare_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        
        //method to select Order from database
        public function selOrderLabSpare($order_product_id) {
            $dbh = new DatabaseHelper();
            if($order_product_id!="")
                   $sql= "select * ,(select CONCAT(spare_name,' ( ',spare_cost,' / - ) Lebure Cost ( ',lebure_cost,' /- )') from spare_details where spare_details_id=ols.spare_details_id) as spare_name "
                    . "from order_lab_spare ols where ols.order_product_id='".$order_product_id."' order by ols.order_lab_spare_id desc";
            else
                  $sql= "select * ,(select CONCAT(spare_name,' ( ',spare_cost,' / - ) Lebure Cost ( ',lebure_cost,' /- )') from spare_details where spare_details_id=ols.spare_details_id) as spare_name "
                    . "from order_lab_spare  order by order_lab_spare_id desc";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
        
        public function checkOrderLabSpare($order_allot_detailsId,$date) {
            $dbh = new DatabaseHelper();
               $sql= "select * from order_lab_spare ed where ed.order_allot_detailsId='".$order_allot_detailsId."' and STR_TO_DATE(meeting_date,'%d-%m-%Y')=STR_TO_DATE('"+$date+"','%d-%m-%Y') order by ord.order_allot_detailsId desc limit 1";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
        
        public function updateOrderLabSpare(OrderLabSpare $order_lab_spare) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE order_lab_spare SET " 
                    ."user_status='".$order_lab_spare->getUser_status()."'"
                    ." WHERE order_lab_spare_id=".$order_lab_spare->getOrder_lab_spare_id().""; 
             $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        }
        public function updateOrderLabSpareStatus(OrderLabSpare $order_lab_spare) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE order_lab_spare SET " 
                    ."user_status='".$order_lab_spare->getUser_status()."'"
                    ." WHERE order_lab_spare_id=".$order_lab_spare->getOrder_lab_spare_id().""; 
             $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        }
          
    }
?>
