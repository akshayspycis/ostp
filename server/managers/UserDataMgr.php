<?php
    include_once '../../dbhelper/DatabaseHelper.php';
    
        class UserDataMgr{    

        //method to insert user_data in database
        public function insUserData(UserData $user_data) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO user_data( "
                    . "user_name, "
                    . "contact_no, "
                    . "email, "
                    . "visit, "
                    . "employee_id) "
                    . "VALUES ('".$user_data->getUser_name()."',"
                    . "'".$user_data->getContact_no()."',"
                    . "'".$user_data->getEmail()."',"
                    . "'".$user_data->getVisit()."',"
                    . "'".$user_data->getEmployee_id()."')";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }

        //method to delete news in database
        public function delUserData($user_data_id) {
            $dbh = new DatabaseHelper();
             $sql = "delete from user_data where user_data_id = '".$user_data_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        
        //method to select UserData from database
        public function selUserData($employee_id) {
            $dbh = new DatabaseHelper();
            $sql="";
            if($employee_id=='2'){
                $sql = "select *,(select amount from charge_details where type='1' order by charge_details_id desc limit 1) as charge,(select sum(amount) from temp_wallet where user_data_id=ud.user_data_id) as amount from user_data ud where (select contact_no from user_details where contact_no!=ud.contact_no limit 1) order by user_data_id desc";
            }else{
                $sql = "select *,(select amount from charge_details where type='1' order by charge_details_id desc limit 1) as charge,(select sum(amount) from temp_wallet where user_data_id=ud.user_data_id) as amount  from user_data ud where employee_id='".$employee_id."' and (select contact_no from user_details where contact_no!=ud.contact_no limit 1) order by user_data_id desc";
            }
            
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
        
        public function checkUserData(UserData $user_data) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO user_data (user_name, contact_no)
                        SELECT * FROM (SELECT '".$user_data->getUser_name()."', '".$user_data->getContact_no()."') AS tmp
                        WHERE NOT EXISTS (
                            SELECT contact_no FROM user_data WHERE contact_no = '".$user_data->getContact_no()."'
                        ) LIMIT 1";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                $sql = "update user_data set visit=COALESCE(visit,0)+1 where contact_no='".$user_data->getContact_no()."';";
                $stmt = $dbh->createConnection()->prepare($sql); 
                $i = $stmt->execute();            
                $dbh->closeConnection();
                if ($i > 0) {                
                    $sql = ""
                    .""
                    . "select *"
                    . ",(select amount from charge_details where type='1' order by charge_details_id desc limit 1) as charge"
                    . ",COALESCE((select sum(amount) from temp_wallet where user_data_id=ud.user_data_id and STR_TO_DATE(from_date, '%d-%m-%Y')<=DATE_FORMAT(now(), '%Y-%m-%d') and STR_TO_DATE(to_date, '%d-%m-%Y')<=DATE_FORMAT(now(),'%Y-%m-%d') ), 0) as amount "
                    . "from user_data ud where contact_no='".$user_data->getContact_no()."'";
                    $stmt = $dbh->createConnection()->prepare($sql);            
                    $stmt->execute();
                } else {
                    return FALSE;
                }
            } else {
                return FALSE;
            }
            $dbh->closeConnection();
            return $stmt;
        }
        
        //        method to update enquiry in database
  public function updateUserData(UserData $user_data) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE user_data SET " 
                    ."user_name='".$user_data->getUser_name()."',"
                    ."email='".$user_data->getEmail()."',"
                    ."contact_no='".$user_data->getContact_no()."'"
                    ."WHERE user_data_id=".$user_data->getUser_data_id()."";
             $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        }
        
          public function updUserDataVisit(UserData $user_data) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE user_data SET " 
                    ."visit='".$user_data->getVisit()."'"
                   ."WHERE user_data_id=".$user_data->getUser_data_id()."";
            $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            
            $dbh->closeConnection();
             
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        } 
          
    }
?>
