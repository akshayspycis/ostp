<?php
    include_once '../../dbhelper/DatabaseHelper.php';
    class OrderDetailsMgr{    
        //method to insert order_details in database
        public function insOrderDetails(OrderDetails $order_details) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO order_details( "
                    . "user_id,"
                    . "order_date,"
                    . "order_no,"
                    . "status,"
                    . "alt_contact_no,"
                    . "address_details_id) "
                    . " VALUES ('".$order_details->getUser_id()."',"
                    . "'".$order_details->getOrder_date()."',"
                    . "'".$order_details->getOrder_no()."',"
                    . "'".$order_details->getStatus()."',"
                    . "'".$order_details->getAlt_contact_no()."',"
                    . "'".$order_details->getAddress_details_id()."')";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }

        //method to delete news in database
        public function delOrderDetails($order_details_id) {
            $dbh = new DatabaseHelper();
             $sql = "delete from order_details where order_id = '".$order_details_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        
        //method to select Order from database
        public function selOrderDetails($order_id) {
            $dbh = new DatabaseHelper();
            if($order_id!="")
            $sql= "select *,(select user_name from user_details where user_id=ord.user_id) as user_name,"
                    . "(select pic from user_profile_details where user_id=ord.user_id) as pic,"
                    . "(select contact_no from user_details where user_id=ord.user_id) as contact_no,"
                    . "(select CONCAT(address_1,' ',address_2,' ',city,'  ',state,'  ',pincode) from address_details where address_details_id=ord.address_details_id) as address_details, "
                    . "(select count(*) from order_product where order_id=ord.order_id) as count "
                    . "from order_details ord where order_id=".$order_id." order by order_id desc";
            else
            $sql= "select *,(select user_name from user_details where user_id=ord.user_id) as user_name,"
                    . "(select pic from user_profile_details where user_id=ord.user_id) as pic,"
                    . "(select contact_no from user_details where user_id=ord.user_id) as contact_no,"
                    . "(select CONCAT(address_1,' ',address_2,' ',city,'  ',state,'  ',pincode) from address_details where address_details_id=ord.address_details_id) as address_details, "
                    . "(select count(*) from order_product where order_id=ord.order_id) as count "
                    . "from order_details ord order by ord.order_id desc";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
        
        public function selOrderDetailsClient($user_id) {
            $dbh = new DatabaseHelper();
            $sql= "select *,"
                    . "(select count(*) from order_product where order_id=ord.order_id) as count "
                    . "from order_details ord where user_id=".$user_id." order by order_id desc";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
        
        public function updateOrderDetails(OrderDetails $order_details) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE order_details SET " 
                    ."status='".$order_details->getStatus()."'"
                    ." WHERE order_id=".$order_details->getOrder_id().""; 
             $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            
            $dbh->closeConnection();
             
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        }
          
    }
?>
