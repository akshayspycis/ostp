<?php
    include_once '../../dbhelper/DatabaseHelper.php';
    class RollsDetailsMgr{    
        //method to insert rolls_details in database
        public function insRollsDetails(RollsDetails $rolls_details) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO rolls_details( "
                    . "employee_id,"
                    . "roll_type_id,"
                    . "date,"
                    . "head) "
                    . "VALUES ('".$rolls_details->getEmployee_id()."',"
                    . "'".$rolls_details->getRoll_type_id()."',"
                    . "CONVERT_TZ(NOW(),'+00:00','+05:29'),"
                    . "'false')";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }

        //method to delete news in database
        public function delRollsDetails($rolls_details_id) {
            $dbh = new DatabaseHelper();
             $sql = "delete from rolls_details where rolls_details_id = '".$rolls_details_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        
        //method to select RollsDetails from database
        public function selRollsDetails($roll_type_id) {
            $dbh = new DatabaseHelper();
            $sql= "select *,rd.date,(select pic from user_profile_details where user_id=ud.user_id) as pic from rolls_details rd inner join user_details ud on ud.user_id=(select user_id from employee_details where employee_id=rd.employee_id) where roll_type_id=".$roll_type_id." order by rolls_details_id desc";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
        public function updateRollsDetails(RollsDetails $rolls_details) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE rolls_details SET " 
                    ."head='".$rolls_details->getHead()."'"
                   ." WHERE rolls_details_id=".$rolls_details->getRolls_details_id()."";
            $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            
            $dbh->closeConnection();
             
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        } 
          
    }
?>
