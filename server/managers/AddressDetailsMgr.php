<?php
    include_once '../../dbhelper/DatabaseHelper.php';
    class AddressDetailsMgr{    
        //method to insert address_details in database
        public function insAddressDetails(AddressDetails $address_details) {
            $dbh = new DatabaseHelper();
            $sql="";
            if($address_details->getAddress_details_id()==''){
                $sql = "INSERT INTO address_details( "
                    . "user_id,"
                    . "address_1,"
                    . "address_2,"
                    . "city,"
                    . "pincode,"
                    . "state,"
                    . "user_data_id) "
                    . "VALUES ('".$address_details->getUser_id()."',"
                    . "'".$address_details->getAddress_1()."',"
                    . "'".$address_details->getAddress_2()."',"
                    . "'".$address_details->getCity()."',"
                    . "'".$address_details->getPincode()."',"
                    . "'".$address_details->getState()."',"
                    . "'".$address_details->getUser_data_id()."')";
            }else{
                $sql ="UPDATE address_details SET " 
                    ."address_1='".$address_details->getAddress_1()."',"
                    ."address_2='".$address_details->getAddress_2()."',"
                    ."city='".$address_details->getCity()."',"
                    ."pincode='".$address_details->getPincode()."',"
                    ."state='".$address_details->getState()."'"
                    ." WHERE address_details_id=".$address_details->getAddress_details_id()."";
            }
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }

        //method to delete news in database
        public function delAddressDetails($address_details_id) {
            $dbh = new DatabaseHelper();
             $sql = "delete from address_details where address_details_id = '".$address_details_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        
        //method to select AddressDetails from database
        public function selAddressDetails($user_id) {
            $dbh = new DatabaseHelper();
            $sql= "select * from address_details where user_id=".$user_id." order by address_details_id desc";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
        
        public function updateAddressDetails(AddressDetails $address_details) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE address_details SET " 
                    ."address_1='".$address_details->getAddress_1()."',"
                    ."address_2='".$address_details->getAddress_2()."',"
                    ."city='".$address_details->getCity()."',"
                    ."pincode='".$address_details->getPincode()."',"
                    ."state='".$address_details->getState()."',"
                    ." WHERE address_details_id=".$address_details->getCharge_details_id()."";
            $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }
          
    }
?>
