<?php
    include_once '../../dbhelper/DatabaseHelper.php';
    class ChargeDetailsMgr{    
        //method to insert charge_details in database
        public function insChargeDetails(ChargeDetails $charge_details) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO charge_details( "
                    . "amount,"
                    . "type,"
                    . "date) "
                    . "VALUES ('".$charge_details->getAmount()."',"
                    . "'".$charge_details->getType()."',"
                    . "CONVERT_TZ( NOW( ) ,  '+00:00',  '+05:29' ))";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }

        //method to delete news in database
        public function delChargeDetails($charge_details_id) {
            $dbh = new DatabaseHelper();
             $sql = "delete from charge_details where charge_details_id = '".$charge_details_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        
        //method to select ChargeDetails from database
        public function selChargeDetails($type) {
            $dbh = new DatabaseHelper();
            $sql= "select * from charge_details where type=".$type." order by charge_details_id desc";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
        
        public function updateChargeDetails(ChargeDetails $charge_details) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE charge_details SET " 
                    ."amount='".$charge_details->getAmount()."'"
                    ." WHERE charge_details_id=".$charge_details->getCharge_details_id()."";
             $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            
            $dbh->closeConnection();
             
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        }
          
    }
?>
