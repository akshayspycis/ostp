<?php
    include_once '../../dbhelper/DatabaseHelper.php';
    class OrderAllotDetailsMgr{    
        //method to insert order_allot_details in database
        public function insOrderAllotDetails(OrderAllotDetails $order_allot_details) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO order_allot_details( "
                    . "order_id,"
                    . "order_product_id,"
                    . "employee_id,"
                    . "engg_time,"
                    . "engg_date,"
                    . "cus_time,"
                    . "cus_date,"
                    . "status) "
                    . " VALUES ('".$order_allot_details->getOrder_id()."',"
                    . "'".$order_allot_details->getOrder_product_id()."',"
                    . "'".$order_allot_details->getEmployee_id()."',"
                    . "'".$order_allot_details->getEngg_time()."',"
                    . "'".$order_allot_details->getEngg_date()."',"
                    . "'".$order_allot_details->getCus_time()."',"
                    . "'".$order_allot_details->getCus_date()."',"
                    . "'".$order_allot_details->getStatus()."')";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }

        //method to delete news in database
        public function delOrderAllotDetails($order_allot_detailsId) {
            $dbh = new DatabaseHelper();
             $sql = "delete from order_allot_details where order_allot_detailsId = '".$order_allot_detailsId."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        
        //method to select Order from database
        public function selOrderAllotDetails($order_allot_detailsId) {
            $dbh = new DatabaseHelper();
            if($order_allot_detailsId!="")
                $sql= "select *,(select pic from user_profile_details where user_id=ud.user_id) as pic from order_allot_details oad inner join user_details ud on ud.user_id=(select user_id from employee_details where employee_id=oad.employee_id) where oad.order_allot_detailsId=".$order_allot_detailsId."";
            else
                $sql= "select *,(select user_name from user_details where user_id=ord.user_id) as user_name,"
                        . "(select CONCAT(address_1,' ',address_2,' ',city,'  ',state,'  ',pincode) from address_details where address_details_id=ord.address_details_id) as address_details, "
                        . "(select count(*) from order_product where order_id=ord.order_id) as count "
                        . "from order_allot_details ord order by ord.order_id desc";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
        
        public function updateOrderAllotDetails(OrderAllotDetails $order_allot_details) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE order_allot_details SET " 
                    ."status='".$order_allot_details->getStatus()."'"
                    ." WHERE order_allot_detailsId=".$order_allot_details->getOrder_allot_detailsId().""; 
             $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        }
          
    }
?>
