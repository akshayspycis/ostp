<?php
    include_once '../../dbhelper/DatabaseHelper.php';
    class PaymentDetailsMgr{    
        public function insPaymentDetails(PaymentDetails $payment_details) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO payment_details( "
                    . "order_product_id, "
                    . "user_id, "
                    . "payable_amt, "
                    . "paid_amt, "
                    . "payment_date) "
                    . "VALUES ('".$payment_details->getOrder_product_id()."',"
                    . "'".$payment_details->getUser_id()."',"
                    . "'".$payment_details->getPayable_amt()."',"
                    . "'".$payment_details->getPaid_amt()."',"
                    . "'".$payment_details->getPayment_date()."')";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }
        public function selFromProductRateServiceCharge($order_product_id) {
            $dbh = new DatabaseHelper();
            $sql= "SELECT (SELECT service_charge FROM product_rate where product_details_id=op.product_details_id order by product_rate_id desc limit 1) as service_charge FROM  order_product op where op.order_product_id='".$order_product_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
        public function selFromProductRateMembershipDiscount($order_product_id) {
            $dbh = new DatabaseHelper();
            $sql= "SELECT (SELECT service_charge FROM product_rate where product_details_id=op.product_details_id order by product_rate_id desc limit 1)*(SELECT discount FROM product_rate where product_details_id=op.product_details_id order by product_rate_id desc limit 1)/100 as total,(SELECT discount FROM product_rate where product_details_id=op.product_details_id order by product_rate_id desc limit 1) as discount FROM  order_product op where op.order_product_id='".$order_product_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
        public function selFromExtraCharge($order_product_id) {
            $dbh = new DatabaseHelper();
            $sql= "SELECT * FROM  order_lab_extra_charge op where op.order_product_id='".$order_product_id."' and status='true'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
        public function selFromSpareCharge($order_product_id) {
            $dbh = new DatabaseHelper();
            $sql= "SELECT * FROM  order_lab_spare op inner join spare_details sd on sd.spare_details_id=op.spare_details_id where op.order_product_id='".$order_product_id."' and op.user_status='true'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
        public function selAllAmount($order_product_id,$that) {
       $count=0;     
       $engg_details = $that->selFromProductRateServiceCharge($order_product_id);
    $str = array();    
    while($row = $engg_details->fetch()){
       $count=$count+$row['service_charge'];
    }
    $engg_details = $that->selFromProductRateMembershipDiscount($order_product_id);
    while($row = $engg_details->fetch()){
           $count=$count-$row['total'];
    }
    $engg_details = $that->selFromExtraCharge($order_product_id);
    while($row = $engg_details->fetch()){
        $count=$count+$row['amount'];
    }
    $engg_details = $that->selFromSpareCharge($order_product_id);
    while($row = $engg_details->fetch()){
        $count=$count+$row['spare_cost']+$row['lebure_cost'] ;
    }
    return $count;
    }
        
    }
?>
