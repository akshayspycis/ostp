<?php
    include_once '../../dbhelper/DatabaseHelper.php';
    class ModelDetailsMgr{    
        //method to insert model_details in database
        public function insModelDetails(ModelDetails $model_details) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO model_details( "
                    . "brand_details_id,"
                    . "product_details_id,"
                    . "model_name) "
                    . "VALUES ('".$model_details->getBrand_details_id()."',"
                    . "'".$model_details->getProduct_details_id()."',"
                    . "'".$model_details->getModel_name()."')";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }

        //method to delete news in database
        public function delModelDetails($model_details_id) {
            $dbh = new DatabaseHelper();
             $sql = "delete from model_details where model_details_id = '".$model_details_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        
        //method to select ModelDetails from database
        public function selModelDetails($brand_details_id,$product_details_id) {
            $dbh = new DatabaseHelper();
            $sql= "select * from model_details where brand_details_id=".$brand_details_id." and product_details_id='".$product_details_id."' order by model_details_id desc";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
        
        public function updateModelDetails(ModelDetails $model_details) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE model_details SET " 
                    ."model_name='".$model_details->getModel_name()."'"
                    ." WHERE model_details_id=".$model_details->getModel_details_id()."";
             $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            
            $dbh->closeConnection();
             
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        }
          
    }
?>
