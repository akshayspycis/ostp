<?php
    include_once '../../dbhelper/DatabaseHelper.php';
    class EnggSubmitInLabOrderProductMgr{    
        //method to insert engg_submit_in_lab_order_product in database
        public function insEnggSubmitInLabOrderProduct(EnggSubmitInLabOrderProduct $engg_submit_in_lab_order_product) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO engg_submit_in_lab_order_product( "
                    . "engg_details_id,"
                    . "employee_id,"
                    . "date_of_submit,"
                    . "order_product_id,"
                    . "lab_management_id) "
                    . " VALUES ('".$engg_submit_in_lab_order_product->getEngg_details_id()."',"
                    . "'".$engg_submit_in_lab_order_product->getEmployee_id()."',"
                    . "'".$engg_submit_in_lab_order_product->getDate_of_submit()."',"
                    . "'".$engg_submit_in_lab_order_product->getOrder_product_id()."',"
                    . "(select lab_management_id from lab_management order by lab_management_id desc limit 1))";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }
        //method to delete news in database
        public function delEnggSubmitInLabOrderProduct($engg_submit_in_lab_order_product_id) {
            $dbh = new DatabaseHelper();
             $sql = "delete from engg_submit_in_lab_order_product where engg_submit_in_lab_order_product_id = '".$engg_submit_in_lab_order_product_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        
        //method to select Order from database
        public function selEnggSubmitInLabOrderProduct($engg_submit_in_lab_order_product_id) {
            $dbh = new DatabaseHelper();
            if($engg_submit_in_lab_order_product_id!="")
                   $sql= "select *,(select product_name from product_details where product_details_id=ord.product_details_id) as product_name,"
                    . "(select order_no from order_details where order_id=ord.order_id) as order_no,"
                    . "(select icon from product_details where product_details_id=ord.product_details_id) as icon,"
                    . "(SELECT order_allot_detailsId FROM order_allot_details where order_product_id=ord.order_product_id) as order_allot_detailsId,"
                    . "(SELECT status FROM order_allot_details where order_product_id=ord.order_product_id) as status,"
                    . "(select brand_name from brand_details where brand_details_id=ord.brand_details_id) as brand_name,"
                    . "(select model_name from model_details where model_details_id=ord.model_details_id) as model_name,"
                    . "(select problem from problem_details where problem_details_id=ord.problem_details_id) as problem, "
                    . "(select CONCAT(address_1,' ',address_2,' ',city,'  ',state,'  ',pincode) from address_details where address_details_id=(select address_details_id from order_details where order_id=ord.order_id)) as address_details, "
                    . "(select discription from problem_details where problem_details_id=ord.problem_details_id) as discription "
                    . "from order_product ord inner join order_allot_details oad on oad.order_product_id=ord.order_product_id where oad.employee_id='".$employee_id."' order by ord.order_id desc";
            else
                  $sql= "select *,(select product_name from product_details where product_details_id=ord.product_details_id) as product_name,"
                    . "(select order_no from order_details where order_id=ord.order_id) as order_no,"
                    . "(select icon from product_details where product_details_id=ord.product_details_id) as icon,"
                    . "(SELECT order_allot_detailsId FROM order_allot_details where order_product_id=ord.order_product_id) as order_allot_detailsId,"
                    . "(SELECT status FROM order_allot_details where order_product_id=ord.order_product_id) as status,"
                    . "(select brand_name from brand_details where brand_details_id=ord.brand_details_id) as brand_name,"
                    . "(select model_name from model_details where model_details_id=ord.model_details_id) as model_name,"
                    . "(select problem from problem_details where problem_details_id=ord.problem_details_id) as problem, "
                    . "(select CONCAT(address_1,' ',address_2,' ',city,'  ',state,'  ',pincode) from address_details where address_details_id=(select address_details_id from order_details where order_id=ord.order_id)) as address_details, "
                    . "(select discription from problem_details where problem_details_id=ord.problem_details_id) as discription "
                    . "from order_product ord inner join order_allot_details oad on oad.order_product_id=ord.order_product_id order by ord.order_id desc";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
        
        public function checkEnggSubmitInLabOrderProduct(EnggSubmitInLabOrderProduct $engg_submit_in_lab_order_product) {
            $dbh = new DatabaseHelper();
               $sql= "select * from lab_management lm Left join engg_return_in_lab_order_product erlop on erlop.order_product_id=lm.order_product_id where lm.order_product_id='".$engg_submit_in_lab_order_product->getOrder_product_id()."' "
                       . "and return_date_time='' or erlop.data_of_return='' limit 1";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
        
        public function updateEnggSubmitInLabOrderProduct(EnggSubmitInLabOrderProduct $engg_submit_in_lab_order_product) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE engg_submit_in_lab_order_product SET " 
                    ."status='".$engg_submit_in_lab_order_product->getStatus()."'"
                    ." WHERE order_product_id=".$engg_submit_in_lab_order_product->getOrder_allot_detailsId().""; 
             $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        }
          
    }
?>
