<?php
    include_once '../../dbhelper/DatabaseHelper.php';
    class ProductDetailsMgr{    
        //method to insert product_details in database
        public function insProductDetails(ProductDetails $product_details) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO product_details( "
                    . "product_name,"
                    . "icon,"
                    . "status) "
                    . "VALUES ('".$product_details->getProduct_name()."',"
                    . "'".$product_details->getIcon()."',"
                    . "'".$product_details->getStatus()."')";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }

        //method to delete news in database
        public function delProductDetails($product_details_id) {
            $dbh = new DatabaseHelper();
             $sql = "delete from product_details where product_details_id = '".$product_details_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        
        //method to select ProductDetails from database
        public function selProductDetails() {
            $dbh = new DatabaseHelper();
            $sql= "select * from product_details order by product_details_id desc";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
        public function selProductDetailsClient() {
            $dbh = new DatabaseHelper();
            $sql= "select * from product_details pd inner join product_rate pr on pr.product_details_id=pd.product_details_id  AND pr.product_rate_id = (
            SELECT MAX(m2.product_rate_id)
            FROM product_rate m2
            WHERE m2.product_details_id = pd.product_details_id
        ) order by pd.product_details_id desc";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
        
        public function updateProductDetails(ProductDetails $product_details) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE product_details SET " 
                    ."product_name='".$product_details->getProduct_name()."'"
                    ." WHERE product_details_id=".$product_details->getProduct_details_id()."";
             $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            
            $dbh->closeConnection();
             
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        }
        public function updProductDetailsStatus(ProductDetails $product_details) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE product_details SET " 
                    ."status='".$product_details->getStatus()."'"
                   ." WHERE product_details_id=".$product_details->getProduct_details_id()."";
            $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            
            $dbh->closeConnection();
             
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        } 
          
    }
?>
