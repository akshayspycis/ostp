<?php
    include_once '../../dbhelper/DatabaseHelper.php';
    class OrderLabExtraChargeMgr{    
        //method to insert order_lab_extra_charge in database
        public function insOrderLabExtraCharge(OrderLabExtraCharge $order_lab_extra_charge) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO order_lab_extra_charge( "
                    . "charge_reason,"
                    . "amount,"
                    . "lab_date,"
                    . "order_product_id,"
                    . "lab_management_id,"
                    . "status) "
                    . " VALUES ('".$order_lab_extra_charge->getCharge_reason()."',"
                    . "'".$order_lab_extra_charge->getAmount()."',"
                    . "'".$order_lab_extra_charge->getLab_date()."',"
                    . "'".$order_lab_extra_charge->getOrder_product_id()."',"
                    . "'".$order_lab_extra_charge->getLab_management_id()."',"
                    . "'".$order_lab_extra_charge->getStatus()."')";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }
        //method to delete news in database
        public function delOrderLabExtraCharge($order_lab_extra_charge_id) {
            $dbh = new DatabaseHelper();
            $sql = "delete from order_lab_extra_charge where order_lab_extra_charge_id = '".$order_lab_extra_charge_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        
        //method to select Order from database
        public function selOrderLabExtraCharge($order_product_id) {
            $dbh = new DatabaseHelper();
            if($order_product_id!="")
                   $sql= "select * "
                    . "from order_lab_extra_charge where order_product_id='".$order_product_id."' order by order_lab_extra_charge_id desc";
            else
                  $sql= "select * "
                    . "from order_lab_extra_charge order by order_lab_extra_charge_id desc";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
        
        public function checkOrderLabExtraCharge($order_allot_detailsId,$date) {
            $dbh = new DatabaseHelper();
               $sql= "select * from order_lab_extra_charge ed where ed.order_allot_detailsId='".$order_allot_detailsId."' and STR_TO_DATE(meeting_date,'%d-%m-%Y')=STR_TO_DATE('"+$date+"','%d-%m-%Y') order by ord.order_allot_detailsId desc limit 1";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
        
        public function updateOrderLabExtraCharge(OrderLabExtraCharge $order_lab_extra_charge) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE order_lab_extra_charge SET " 
                    ."amount='".$order_lab_extra_charge->getAmount()."'"
                    ." WHERE order_lab_extra_charge_id=".$order_lab_extra_charge->getOrder_lab_extra_charge_id().""; 
             $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        }
        public function updateOrderLabExtraChargeStatus(OrderLabExtraCharge $order_lab_extra_charge) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE order_lab_extra_charge SET " 
                    ."status='".$order_lab_extra_charge->getStatus()."'"
                    ." WHERE order_lab_extra_charge_id=".$order_lab_extra_charge->getOrder_lab_extra_charge_id().""; 
             $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        }
          
    }
?>
