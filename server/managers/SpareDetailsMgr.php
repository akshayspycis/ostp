<?php
    include_once '../../dbhelper/DatabaseHelper.php';
    class SpareDetailsMgr{    
        //method to insert spare_details in database
        public function insSpareDetails(SpareDetails $spare_details) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO spare_details( "
                    . "spare_name,"
                    . "spare_cost,"
                    . "lebure_cost,"
                    . "product_details_id) "
                    . "VALUES ('".$spare_details->getSpare_name()."',"
                    . "'".$spare_details->getSpare_cost()."',"
                    . "'".$spare_details->getLebure_cost()."',"
                    . "'".$spare_details->getProduct_details_id()."')";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }

        //method to delete news in database
        public function delSpareDetails($spare_details_id) {
            $dbh = new DatabaseHelper();
             $sql = "delete from spare_details where spare_details_id = '".$spare_details_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        
        //method to select SpareDetails from database
        public function selSpareDetails($product_details_id) {
            $dbh = new DatabaseHelper();
            $sql= "select * from spare_details where product_details_id='".$product_details_id."' order by spare_details_id desc";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
        public function selSpareDetailsLab($order_product_id) {
            $dbh = new DatabaseHelper();
            $sql= "select * from spare_details where product_details_id=(select product_details_id from order_product where order_product_id='".$order_product_id."') order by spare_details_id desc";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
        
        public function updateSpareDetails(SpareDetails $spare_details) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE spare_details SET " 
                    ."spare_name='".$spare_details->getSpare_name()."',"
                    ."spare_cost='".$spare_details->getSpare_cost()."',"
                    ."lebure_cost='".$spare_details->getLebure_cost()."'"
                    ." WHERE spare_details_id=".$spare_details->getSpare_details_id()."";
             $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            
            $dbh->closeConnection();
             
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        }
          
    }
?>
