<?php
    include_once '../../dbhelper/DatabaseHelper.php';
    class OffilineTransactionDetailsMgr{    
        //method to insert offline_transaction_details in database
        public function insOffilineTransactionDetails(OffilineTransactionDetails $offline_transaction_details) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO offline_transaction_details( "
                    . "date,"
                    . "wallet_id,"
                    . "type,"
                    . "transation_type) "
                    . "VALUES (CONVERT_TZ( NOW( ) ,  '+00:00',  '+05:29' ),"
                    . "".$offline_transaction_details->getWallet_id().","
                    . "'".$offline_transaction_details->getType()."',"
                    . "'".$offline_transaction_details->getTransation_type()."')";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }

        //method to delete news in database
        public function delOffilineTransactionDetails($offline_transaction_details_id) {
            $dbh = new DatabaseHelper();
             $sql = "delete from offline_transaction_details where offline_transaction_details_id = '".$offline_transaction_details_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        
        //method to select OffilineTransactionDetails from database
        public function selOffilineTransactionDetails($brand_details_id,$product_details_id) {
            $dbh = new DatabaseHelper();
            $sql= "select * from offline_transaction_details order by offline_transaction_details_id desc";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
        
        public function updateOffilineTransactionDetails(OffilineTransactionDetails $offline_transaction_details) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE offline_transaction_details SET " 
                    ."model_name='".$offline_transaction_details->getModel_name()."'"
                    ." WHERE offline_transaction_details_id=".$offline_transaction_details->getModel_details_id()."";
             $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            
            $dbh->closeConnection();
             
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        }
          
    }
?>
