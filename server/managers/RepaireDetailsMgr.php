<?php
    include_once '../../dbhelper/DatabaseHelper.php';
    class RepaireDetailsMgr{    
        public function selRepaireDetails($order_product_id) {
            $dbh = new DatabaseHelper();
            $sql= "select
ops.status_date as dae ,
ops.order_product_status_id,
ops.product_status,
ops.product_reason,
ops.product_repair_hrs,
'OP'
     from order_product_status ops where ops.order_product_id='".$order_product_id."'
     UNION select
olec.lab_date,
olec.order_lab_extra_charge_id,
olec.status,
olec.charge_reason,
olec.amount,
'OL'
      from order_lab_extra_charge olec where olec.order_product_id='".$order_product_id."'
      UNION select
ols.lab_spare_date,
ols.order_lab_spare_id,
ols.user_status,
CONCAT(ols.spare_reason,' ',sd.spare_name,' ( ',sd.spare_cost,' /-Rs.) and Lebure cost is (',sd.lebure_cost,'  Rs.)'),
sd.spare_cost+sd.lebure_cost,
'LS'
      from order_lab_spare ols inner join spare_details sd on sd.spare_details_id=ols.spare_details_id 
 where ols.order_product_id='".$order_product_id."'
 order by STR_TO_DATE(dae,'%d-%m-%Y')";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
    }
?>
