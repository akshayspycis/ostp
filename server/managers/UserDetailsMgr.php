

<?php
    include_once '../../dbhelper/DatabaseHelper.php';
    
        class UserDetailsMgr{    

        //method to insert user_details in database
        public function insUserDetails(UserDetails $user_details) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO user_details( "
                    . "user_name, "
                    . "dob, "
                    . "email, "
                    . "contact_no, "
                    . "date, "
                    . "gender) "
                    . "VALUES ('".$user_details->getUser_name()."',"
                    . "'".$user_details->getDob()."',"
                    . "'".$user_details->getEmail()."',"
                    . "'".$user_details->getContact_no()."',"
                    . "'".$user_details->getDate()."',"
                    . "'".$user_details->getGender()."')";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }

        //method to delete news in database
        public function delUserDetails($user_id) {
            $dbh = new DatabaseHelper();
             $sql = "delete from user_details where user_id = '".$user_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        
        //method to select UserDetails from database
        public function selUserDetails($user_id) {
            $dbh = new DatabaseHelper();
            $sql="";
            if($user_id!=""){
                $sql = "select *,(select user_type from login_details l where l.user_id=ud.user_id limit 1) as user_type,"
                        . "(select pic from user_profile_details up where up.user_id=ud.user_id) as pic,"
                        . "(select user_profile_details_id from user_profile_details up where up.user_id=ud.user_id) "
                        . "as user_profile_details_id from user_details ud  where ud.user_id=".$user_id." limit 1";
            }else{
                $sql = "select *,(select user_type from login_details l where l.user_id=ud.user_id limit 1) as user_type,"
                        . "(select pic from user_profile_details up where up.user_id=ud.user_id) as pic,"
                        . "(select user_profile_details_id from user_profile_details up where up.user_id=ud.user_id) "
                        . "as user_profile_details_id from user_details ud inner join login_details l on l.user_id=ud.user_id where l.user_type='emp'";
            }
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
        public function selRegisteredUser() {
            $dbh = new DatabaseHelper();
                $sql = "select *,(select user_type from login_details l where l.user_id=ud.user_id limit 1) as user_type,"
                        . "(select pic from user_profile_details up where up.user_id=ud.user_id) as pic,"
                        . "(select user_profile_details_id from user_profile_details up where up.user_id=ud.user_id) "
                        . "as user_profile_details_id from user_details ud inner join login_details l on l.user_id=ud.user_id where l.user_type!='emp' or l.user_type='admin'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
        
        public function selMaxUserId() {
           $dbh = new DatabaseHelper();
           $sql= "select user_id from user_details order by user_id desc limit 1";
           $stmt = $dbh->createConnection()->prepare($sql);            
           $stmt->execute();
           $dbh->closeConnection();
           if($row = $stmt->fetch()){
             return $row['user_id']; 
           }else{
               return null; 
           }
        }
        public function checkContact($contact_no) {
            $dbh = new DatabaseHelper();
            $sql = "select * from user_details where contact_no='".$contact_no."' limit 1";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
        public function checkEmail($email) {
            $dbh = new DatabaseHelper();
            $sql = "select * from user_details where email='".$email."' limit 1";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
        //        method to update enquiry in database
  public function updateUserDetails(UserDetails $user_details) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE user_details SET " 
                    ."user_name='".$user_details->getUser_name()."',"
                    ."email='".$user_details->getEmail()."',"
                    ."contact_no='".$user_details->getContact_no()."'"
                    ."WHERE user_id=".$user_details->getUser_id()."";
             $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        }
          public function updUserDetailstatus(UserDetails $user_details) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE user_details SET " 
                    ."status='".$user_details->getStatus()."'"
                   ."WHERE user_id=".$user_details->getCourse_batch_details_id()."";
                    
          
            
            $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            
            $dbh->closeConnection();
             
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        } 
          
    }
?>
