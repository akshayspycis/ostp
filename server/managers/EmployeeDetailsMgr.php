<?php
    require_once '../../dbhelper/DatabaseHelper.php';
    class EmployeeDetailsMgr{    
        //method to insert employee_details in database
        public function insEmployeeDetails(EmployeeDetails $employee_details) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO employee_details( "
                    . "user_id, "
                    . "date) "
                    . "VALUES ('".$employee_details->getUser_id()."',"
                    . "'".$employee_details->getDate()."')";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }

        //method to delete news in database
        public function delEmployeeDetails($employee_id) {
            $dbh = new DatabaseHelper();
             $sql = "delete from employee_details where employee_id = '".$employee_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        
        //method to select EmployeeDetails from database
        public function selEmployeeDetails($category_id) {
            $dbh = new DatabaseHelper();
            $sql="";
            if($category_id!=""){
                $sql = "select *,ed.date as edate,(select pic from user_profile_details up where up.user_id=ud.user_id) as pic
                        from employee_details ed inner join user_details ud on ud.user_id=ed.user_id where category_id=".$category_id." order by STR_TO_DATE(date, '%d-%m-%Y') desc";
            }else{
                $sql = "select *,ed.date as edate,(select pic from user_profile_details up where up.user_id=ud.user_id) as pic
                        from employee_details ed inner join user_details ud on ud.user_id=ed.user_id order by STR_TO_DATE(ed.date, '%d-%m-%Y') desc;";
            }
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            
            $dbh->closeConnection();
            return $stmt;
        }
    }
?>
