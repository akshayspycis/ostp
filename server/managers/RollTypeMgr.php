<?php
    include_once '../../dbhelper/DatabaseHelper.php';
    class RollTypeMgr{    
        //method to insert roll_type in database
        public function insRollType(RollType $roll_type) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO roll_type( "
                    . "roll_type) "
                    . "VALUES ('".$roll_type->getRoll_type()."')";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }

        //method to delete news in database
        public function delRollType($roll_type_id) {
            $dbh = new DatabaseHelper();
             $sql = "delete from roll_type where roll_type_id = '".$roll_type_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        
        //method to select RollType from database
        public function selRollType() {
            $dbh = new DatabaseHelper();
            $sql= "select * from roll_type";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
        
        public function updateRollType(RollType $roll_type) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE roll_type SET " 
                    ."roll_type='".$roll_type->getRoll_type()."'"
                    ." WHERE roll_type_id=".$roll_type->getRoll_type_id()."";
             $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            $dbh->closeConnection();
             
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        }
          
    }
?>
