<?php
    include_once '../../dbhelper/DatabaseHelper.php';
    class ProblemDetailsMgr{    
        //method to insert problem_details in database
        public function insProblemDetails(ProblemDetails $problem_details) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO problem_details( "
                    . "brand_details_id,"
                    . "product_details_id,"
                    . "model_details_id,"
                    . "problem,"
                    . "discription) "
                    . "VALUES ('".$problem_details->getBrand_details_id()."',"
                    . "'".$problem_details->getProduct_details_id()."',"
                    . "'".$problem_details->getModel_details_id()."',"
                    . "'".$problem_details->getProblem()."',"
                    . "'".$problem_details->getDiscription()."')";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }

        //method to delete news in database
        public function delProblemDetails($problem_details_id) {
            $dbh = new DatabaseHelper();
             $sql = "delete from problem_details where problem_details_id = '".$problem_details_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        
        //method to select ProblemDetails from database
        public function selProblemDetails($brand_details_id,$product_details_id,$model_details_id) {
            $dbh = new DatabaseHelper();
            $sql= "select * from problem_details where product_details_id='".$product_details_id."' and brand_details_id='".$brand_details_id."'   and model_details_id='".$model_details_id."' order by problem_details_id desc";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
        public function selProblemDetailsClient($product_details_id) {
            $dbh = new DatabaseHelper();
            $sql= "select prd.*,brand_name,model_name from problem_details prd left join brand_details bd on bd.brand_details_id=prd.brand_details_id left join model_details md on md.model_details_id=prd.model_details_id where  prd.product_details_id='".$product_details_id."'  order by prd.problem_details_id desc";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
        
        public function updateProblemDetails(ProblemDetails $problem_details) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE problem_details SET " 
                    ."problem='".$problem_details->getProblem()."',"
                    ."discription='".$problem_details->getDiscription()."'"
                    ." WHERE problem_details_id=".$problem_details->getProblem_details_id()."";
             $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            
            $dbh->closeConnection();
             
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        }
          
    }
?>
